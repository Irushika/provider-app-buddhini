import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { storage } from 'firebase-admin/lib/storage';

@Component({
  selector: 'app-new-appoinment',
  templateUrl: './new-appoinment.page.html',
  styleUrls: ['./new-appoinment.page.scss'],
})
export class NewAppoinmentPage implements OnInit {

  appointment: any;
  JID: any;
  mycategory: any;
  UID: any;
  user: any;
  category:any;

  constructor(private navCtrl: NavController,
    private route: Router,
    public db: AngularFireDatabase,
    public alertController: AlertController,
    private storage: Storage,) { }

  ngOnInit() {
    this.loaddetails();
  }

  loadingAppointment(){
   
    let userInfo = JSON.parse( localStorage.getItem('userInfo'));
   console.log(userInfo);
     this.category = userInfo.category;
     console.log("cat 2", this.category);
    this.storage.get('user').then(
      user => {
        console.log(user)
        if (user){ this.UID = user.UID;  }   
       }
    )

    // let mycategory=localStorage.getItem(this.mycategory);
    this.db
      .list("/jobs", (ref) => ref.orderByChild("category").equalTo(this.category))
      .valueChanges([],{idField: 'id'})
      .subscribe((appointment) => {
        console.log(appointment);
        this.appointment = appointment;
        window.localStorage.setItem("apointmentInfo", JSON.stringify(appointment[0]));

      });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: "Alert",
      message: "No appointment found.",
      buttons: ["OK"],
    });
    await alert.present();
  }

  new_appointment_info(JID) {
    console.log(JID);
    this.route.navigate(['./appoinment-submit', JID]);
  }

  loaddetails(){
    console.log("my-profile");
    let phone = window.localStorage.getItem("userPhone");
    if (phone) {
      console.log(phone);
      this.db
        .list("/users", (ref) => ref.orderByChild("phoneNumber").equalTo(phone))
        .valueChanges()
        .subscribe((user) => {
          if (user.length > 0) {
            this.user = user[0];
            this.category=this.user.category;
            console.log('cat 1' ,this.category);
            window.localStorage.setItem("userInfo", JSON.stringify(user[0]));
            window.localStorage.setItem("myCategory", this.category);
            // window.localStorage.setItem("UID", this.UID);
            //Now load Appointements 
            this.loadingAppointment(); 
            
          } else {
            this.presentAlert();
          }
        });
      }
    }

}




