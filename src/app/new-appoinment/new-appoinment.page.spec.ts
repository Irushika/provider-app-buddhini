import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewAppoinmentPage } from './new-appoinment.page';

describe('NewAppoinmentPage', () => {
  let component: NewAppoinmentPage;
  let fixture: ComponentFixture<NewAppoinmentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewAppoinmentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewAppoinmentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
