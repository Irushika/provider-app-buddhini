import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { NewAppoinmentPageRoutingModule } from './new-appoinment-routing.module';

import { NewAppoinmentPage } from './new-appoinment.page';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    NewAppoinmentPageRoutingModule,
    PipesModule
  ],
  declarations: [NewAppoinmentPage]
})
export class NewAppoinmentPageModule {}
