import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectDateTimePage } from './select-date-time.page';

describe('SelectDateTimePage', () => {
  let component: SelectDateTimePage;
  let fixture: ComponentFixture<SelectDateTimePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectDateTimePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectDateTimePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
