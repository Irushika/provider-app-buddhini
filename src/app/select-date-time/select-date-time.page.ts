import { Component, OnInit } from '@angular/core';
import { LocationPage } from '../location/location.page';  
import { ModalController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Time } from '@angular/common';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-select-date-time',
  templateUrl: './select-date-time.page.html',
  styleUrls: ['./select-date-time.page.scss'],
})
export class SelectDateTimePage implements OnInit {

  JID:any;
  job:any;
  public date: string;
  public time: string;
  public address:string;
  public budject:string;
  public information:string;
  public estimatetime:string;

  constructor(private modalController: ModalController, 
    private route: Router,
    private storage: Storage,
    public activatedRoute: ActivatedRoute,
    public db: AngularFireDatabase,) { }

  ngOnInit() {
    this.JID = this.activatedRoute.snapshot.paramMap.get('JID');
     this.loadJobDetails();
  }

  location(){
    this.modalController.create({component:LocationPage}).then((modalElement)=>
    {
      modalElement.present();
    }
    )
  }
	
  loadJobDetails(){
    
    if (this.JID) {
      console.log(this.JID);
      window.localStorage.setItem("myjobid" , this.JID);
      this.db.database.ref("/jobs/" + this.JID).on(
        'value', (snapshot) => {
          this.job = snapshot.val();
          console.log(this.job);
          window.localStorage.setItem("jobInfo", JSON.stringify(this.job));
          this.date=this.job.date;
          this.time=this.job.time;
          console.log(this.time);
          this.budject=this.job.budject;
        }
      );

        
    } else {
      this.job = JSON.parse(window.localStorage.getItem("jobInfo"));
      console.log("params not working");
    }
  }

 continue() {

    this.date = this.formatDate(this.date);
    // this.time=this.formatTime(this.time);

    console.log(this.date);
    console.log(this.time);

    window.localStorage.setItem("provider_date", this.date);
    window.localStorage.setItem("provider_time", this.time);
    window.localStorage.setItem("provider_budject", this.budject);
    window.localStorage.setItem("provider_Estimate_Time", this.estimatetime);
    console.log("date : "+ this.date + "time :" + this.time + " budject" + this.budject + "hourse" + this.estimatetime);

    this.route.navigate(['./confirm-submit']);
  } 

  formatDate(dateStr: string){
    //date parsing
   let date = new Date(dateStr);

    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let day = date.getDate();

    return year + '-' + month + '-' + day;
  }


  formatTime(dateStr: string)
  {

    let date = new Date(dateStr);

    let minuts = date.getMinutes();
    let  hourse= date.getHours();

    return hourse + ':'+ minuts;
  }

}
