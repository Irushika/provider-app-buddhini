import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed
} from '@capacitor/core';
import { AlertController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';

const { PushNotifications } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class ApiFcmService {

  notifications = new BehaviorSubject(null);
  token: string;

  headers = new HttpHeaders();
  httpOptions: any
  
  constructor(
    private alertController: AlertController,
    private httpClient: HttpClient,
    private router: Router
  ) {
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Content-Type', 'Application/json');
    this.headers.append('X-Request-With', 'XMLHttpRequest');

    this.httpOptions = { headers: this.headers };

   }


  getPermissions() {
    return PushNotifications.requestPermission().then(
      result => {
        if (result.granted) {
          console.log(result)
          PushNotifications.register();
          return true;
        }
      }
    );
  }

  loadKey() {
   return this.getPermissions().then(
      res => {
        if (res) {
          PushNotifications.addListener('registration',
            (token: PushNotificationToken) => {
              //  alert('Push registration success, token: ' + token.value);
              // this.data.fcmKey = token.value;
              console.log('Push registration success, token: ', token.value);
              this.token = token.value;
              this.get();
              this.open();
            }
          );
          return true;
        } else {
          console.log('Vous ne pouvez pas de recevoir de notifications, Veuillez l\'activer dans les paramètres de votre appareil!');
          return true;
        }
      }
    );
  }

  get() {
    PushNotifications.addListener('pushNotificationReceived',
      (notification: PushNotification) => {
        // console.log('Push received: ', notification);
        this.notifications.next(notification);
        console.log('recieved notification!')
        this.presentAlert(notification.title, notification.body)
      }
    );
  }

  open() {
    // Method called when tapping on a notification
    PushNotifications.addListener('pushNotificationActionPerformed',
      (payload: PushNotificationActionPerformed) => {
        // console.log('Push action performed: ' + notification);
        this.notifications.next(payload.notification);
        // this.router.navigate(["appointment-info",payload.notification.data.JID])
        this.router.navigate(["my-appointments"]);
        // this.presentAlert(notification.title, notification.body)

      }
    );
  }


    async presentAlert(title, message) {
      const alert = await this.alertController.create({
        header: title,
        message,
        buttons: ['OK']
      });
    
      await alert.present();
    }

    postNotification(fcmToken, title, message) {
      this.httpClient.post('https://service-hub-pro.herokuapp.com/notify', { fcmToken, title, message}, this.httpOptions)
    }

    // notifyProviders(category){
    //   this.httpClient.post('http://localhost:8081/notify-providers', { category})
    // }


    // send notification to client
    notifyClient(JID){
      this.httpClient.post('https://service-hub-pro.herokuapp.com/notifyclient', { JID }, this.httpOptions).toPromise().then(
        res => console.log(res)
      ).catch(err => console.log(err));
    }

}






