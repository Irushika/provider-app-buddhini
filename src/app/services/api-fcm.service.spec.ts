import { TestBed } from '@angular/core/testing';

import { ApiFcmService } from './api-fcm.service';

describe('ApiFcmService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiFcmService = TestBed.get(ApiFcmService);
    expect(service).toBeTruthy();
  });
});
