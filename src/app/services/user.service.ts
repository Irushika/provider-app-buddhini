import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
@Injectable({
    providedIn: 'root'
})
export class UserService {
    // behaviour subject
    public _$userSubject = new BehaviorSubject<any>('');
    
    constructor() { }

    public getUserSubject(): Observable<any> {
        return this._$userSubject.asObservable();
    }
}
