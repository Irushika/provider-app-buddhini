import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';
import { request } from 'http';
import { ApiFcmService } from '../services/api-fcm.service';

@Component({
  selector: 'app-confirm-submit',
  templateUrl: './confirm-submit.page.html',
  styleUrls: ['./confirm-submit.page.scss'],
})
export class ConfirmSubmitPage implements OnInit {

  JID:any;
  requests: any[];
  date:string;
  time:string;
  address:string;
  budject:string;
  information:string;
  UID:String;
  category:String;
  public estimate:string;
  
  public name:String;

  public AID: String

  constructor( private storage: Storage,
    private route: Router,
    public db: AngularFireDatabase,
    private apiFcmService:ApiFcmService
    ) { }

  ngOnInit() {

    this.appointment_details()
    
  }

  appointment_details(){

    let date =window.localStorage.getItem("provider_date");
    let time =window.localStorage.getItem("provider_time");
    let budject =window.localStorage.getItem("provider_budject");
    let information =window.localStorage.getItem("information");
    let estimate=window.localStorage.getItem("provider_Estimate_Time");

   

    this.date=date;
    this.time=time;
    this.budject=budject;
    this.estimate=estimate;
    this.checkestimate(this.estimate);
    let jobInfo = JSON.parse( localStorage.getItem('jobInfo'));
   
    this.category=jobInfo.category;
    this.address=jobInfo.address;
    this.storage.get('user').then(
      user => {
        if (user) {
          this.UID = user.UID;
          this.db.database.ref("/users/" + user.UID).on(
            'value', (snapshot) => {
              const data = snapshot.val();
              this.name = data.name;
              
              console.log(data.name);
          
              // this.checkestimate(this.estimatetime);
              
            }
          );
        } 
      }
    )

  }

  checkestimate(estimate){
    if(this.estimate=='undefined'){
      return false;
    }
    else{
     return true;
    }
  }

  // checkestimate(estimatetime){
  //   if(estimatetime=undefined){
  //     return false;
  //   }
  //     return true;
  // }

  confirmappoinment(){
    this.JID = window.localStorage.getItem("myjobid");

    // get saved requests from db
    firebase.database().ref(`jobs/${this.JID}`).get().then(
      resuslt => {
      let  job =  resuslt.val();
      console.log(job);
        this.requests = job.requests;
        
        if (!this.requests) this.requests = [];
         // array push new data
    this.requests.push(
      {
        provider_UID:this.UID,
        provider_name:this.name,
        jid:this.JID,
        provider_date: this.date,
        provider_time: this.time,
        provider_budject:this.budject,
        estimatetime:this.estimate,
        status: 'pending'
      }
    );

    console.log(this.JID);
    
    firebase.database().ref(`jobs/${this.JID}`).update({ requests: this.requests, status:'requested'}).then(
      () => {
         //send notification to the category providers
         this.apiFcmService.notifyClient(this.JID);
      }
    )
    this.route.navigate(['./order-done']);
   

      }
    );
  }

}
