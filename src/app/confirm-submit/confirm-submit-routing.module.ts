import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfirmSubmitPage } from './confirm-submit.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmSubmitPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfirmSubmitPageRoutingModule {}
