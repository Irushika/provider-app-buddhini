import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmSubmitPageRoutingModule } from './confirm-submit-routing.module';

import { ConfirmSubmitPage } from './confirm-submit.page';

import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    ConfirmSubmitPageRoutingModule
  ],
  declarations: [ConfirmSubmitPage]
})
export class ConfirmSubmitPageModule {}
