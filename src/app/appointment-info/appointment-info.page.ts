import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-appointment-info',
  templateUrl: './appointment-info.page.html',
  styleUrls: ['./appointment-info.page.scss'],
})
export class AppointmentInfoPage implements OnInit {

  job: any;
  JID: any;
  requests: any[];
  index: Number;
  completedRequest: any;
  user: any;

  currentRequest: any;
  name:any;
 

  constructor(private route: Router,
    public activatedRoute: ActivatedRoute,
    public db: AngularFireDatabase,
    public alertController: AlertController) { }

  ngOnInit() {
    this.JID = this.activatedRoute.snapshot.paramMap.get('JID');
    this.loadJobDetails();
  }

  loadJobDetails() {

    if (this.JID) {
      console.log(this.JID);
      window.localStorage.setItem("myjobid", this.JID);
      this.db.database.ref("/jobs/" + this.JID).on(
        'value', (snapshot) => {
          this.job = snapshot.val();
          console.log(this.job);
          let clientUID=this.job.UID;
          console.log('client', clientUID);
          this.loadUserDetails(clientUID);

          let currentUID = window.localStorage.getItem('UID');
              this.currentRequest = this.job.requests.filter((request)=>{
                return currentUID == request.provider_UID
              })[0];

          window.localStorage.setItem("jobInfo", JSON.stringify(this.job));
          this.requests = this.job.requests;
          console.log(this.requests);
          // load provider details
          this.completedRequest = this.requests.find(request => request.Status == 'Completed');
          if (this.completedRequest) this.loadUserDetails(this.completedRequest.provider_UID);
        }
      );


    } else {
      this.job = JSON.parse(window.localStorage.getItem("jobInfo"));
      console.log("params not working");
    }
  }

  loadUserDetails(UID) {
    this.db.database.ref("/users/" + UID).on(
      'value', (snapshot) => {
        this.user = snapshot.val();
        this.name=this.user.name;
      }
    );
  }

  


}
