import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'welcome',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'sign-in',
    loadChildren: () => import('./sign-in/sign-in.module').then( m => m.SignInPageModule)
  },
  {
    path: 'sign-up',
    loadChildren: () => import('./sign-up/sign-up.module').then( m => m.SignUpPageModule)
  },
  {
    path: 'veirifcation/:no',
    loadChildren: () => import('./veirifcation/veirifcation.module').then( m => m.VeirifcationPageModule)
  },
  {
    path: 'select-task',
    loadChildren: () => import('./select-task/select-task.module').then( m => m.SelectTaskPageModule)
  },
  {
    path: 'select-location',
    loadChildren: () => import('./select-location/select-location.module').then( m => m.SelectLocationPageModule)
  },
  {
    path: 'location',
    loadChildren: () => import('./location/location.module').then( m => m.LocationPageModule)
  },
  {
    path: 'confirm',
    loadChildren: () => import('./confirm/confirm.module').then( m => m.ConfirmPageModule)
  },
  {
    path: 'order-done',
    loadChildren: () => import('./order-done/order-done.module').then( m => m.OrderDonePageModule)
  },
  {
    path: 'my-appointments',
    loadChildren: () => import('./my-appointments/my-appointments.module').then( m => m.MyAppointmentsPageModule)
  },
  
  {
    path: 'my-profile',
    loadChildren: () => import('./my-profile/my-profile.module').then( m => m.MyProfilePageModule)
  },
  {
    path: 'add-address',
    loadChildren: () => import('./add-address/add-address.module').then( m => m.AddAddressPageModule)
  },
  {
    path: 'contact-us',
    loadChildren: () => import('./contact-us/contact-us.module').then( m => m.ContactUsPageModule)
  },
  {
    path: 'faq',
    loadChildren: () => import('./faq/faq.module').then( m => m.FaqPageModule)
  },
  {
    path: 'tnc',
    loadChildren: () => import('./tnc/tnc.module').then( m => m.TncPageModule)
  },
  
  {
    path: 'select-date-time/:JID',
    loadChildren: () => import('./select-date-time/select-date-time.module').then( m => m.SelectDateTimePageModule)
  },
  {
    path: 'title',
    loadChildren: () => import('./title/title.module').then( m => m.TitlePageModule)
  },
  {
    path: 'select-language',
    loadChildren: () => import('./select-language/select-language.module').then( m => m.SelectLanguagePageModule)
  },
  {
    path: 'welcome',
    loadChildren: () => import('./welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  {
    path: 'requests',
    loadChildren: () => import('./requests/requests.module').then( m => m.RequestsPageModule)
  },
  
  
  {
    path: 'appoinment-submit/:JID',
    loadChildren: () => import('./appoinment-submit/appoinment-submit.module').then( m => m.AppoinmentSubmitPageModule)
  },
  {
    path: 'confirm-submit',
    loadChildren: () => import('./confirm-submit/confirm-submit.module').then( m => m.ConfirmSubmitPageModule)
  },
  {
    path: 'new-appoinment',
    loadChildren: () => import('./new-appoinment/new-appoinment.module').then( m => m.NewAppoinmentPageModule)
  },
  {
    path: 'star-nevigation',
    loadChildren: () => import('./star-nevigation/star-nevigation.module').then( m => m.StarNevigationPageModule)
  },
  {
    path: 'appointment-info/:JID',
    loadChildren: () => import('./appointment-info/appointment-info.module').then( m => m.AppointmentInfoPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'category-all',
    loadChildren: () => import('./category-all/category-all.module').then( m => m.CategoryAllPageModule)
  },
  {
    path: 'category-serch',
    loadChildren: () => import('./category-serch/category-serch.module').then( m => m.CategorySerchPageModule)
  },
  {
    path: 'navigation/:JID',
    loadChildren: () => import('./navigation/navigation.module').then( m => m.NavigationPageModule)
  },








 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
