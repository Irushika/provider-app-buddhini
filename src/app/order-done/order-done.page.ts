import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-order-done',
  templateUrl: './order-done.page.html',
  styleUrls: ['./order-done.page.scss'],
})
export class OrderDonePage implements OnInit {

  constructor(private navCtrl: NavController, private route: Router) { }

  ngOnInit() {
  }


 home() {
    this.navCtrl.navigateRoot(['./new-appoinment']);
  } 
	 
 my_appointments() {
    this.navCtrl.navigateRoot(['./my-appointments']);
  } 
	
 
   
}
