import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
 
import { IonicModule } from '@ionic/angular';

import { OrderDonePageRoutingModule } from './order-done-routing.module';

import { OrderDonePage } from './order-done.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
	TranslateModule,   
    OrderDonePageRoutingModule
  ],
  declarations: [OrderDonePage]
})
export class OrderDonePageModule {}
