import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { DomSanitizer } from '@angular/platform-browser';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import firebase from 'firebase/app';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  
  categories = [];
  categoryName: string;
 
  constructor(private route: Router,
    public db: AngularFireDatabase,
    public alertController: AlertController,
    public _sanitizer: DomSanitizer) { }

  ngOnInit() {
    let cat = this.db
      .list('/categories')
      .valueChanges()
      .subscribe((cat) => {
        this.categories = cat;
      });
  }

  getUrl(value){
    return this._sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${value})`);
  }

  adddetails(){
    window.localStorage.setItem("cate", this.categoryName);
    console.log("category name" + this.categoryName)
    this.route.navigate(['./select-date-time']);
  }

  
 appointment_info() {
    this.route.navigate(['./appointment-info']);
  } 
 view_all() {
    this.route.navigate(['./my-appointments']);
  } 
  view_cat() {
    this.route.navigate(['./category-all']);
  } 
 select_task() {
    this.route.navigate(['./select-task']);
  } 
}
