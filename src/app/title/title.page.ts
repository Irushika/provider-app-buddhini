import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import firebase from 'firebase';
import { UserService } from '../services/user.service';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-title',
  templateUrl: './title.page.html',
  styleUrls: ['./title.page.scss'],
})
export class TitlePage implements OnInit {
  
  address:string;
  addaddress:string;
  address_type:string;
  index = 0;
// addresses:  [ {address_type: 'home', address: ''}, {address_type: 'office', address: ''}}]
addresses =  [ {address_type: 'home', address: ''}, {address_type: 'office', address: ''}, {address_type: 'other', address: ''}];
  public UID: String;
  public phoneNumber: String;

  constructor(private modalController: ModalController,
    private userService: UserService,
    private storage: Storage,
    public db: AngularFireDatabase,) { }

  ngOnInit() {
    // get the user UID
    this.storage.get('user').then(
      user => {
        console.log('user', user);
        this.UID = user.UID;
      }
    )
  let data = JSON.parse( window.localStorage.getItem('addresses'));
  if( data) this.addresses = data;
   
  }

  saveaddress(){

    // this.address=this.addaddress;
    window.localStorage.setItem("addresses", JSON.stringify(this.addresses));
    // alert(this.address);
    alert('UID' + this.UID);
    firebase.database().ref().child(`users/${this.UID}`).update({ addresses: this.addresses})
   this.modalController.dismiss();
 }

}
