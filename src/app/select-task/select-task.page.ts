import { Component, OnInit } from '@angular/core';
 import { Router } from '@angular/router';

@Component({
  selector: 'app-select-task',
  templateUrl: './select-task.page.html',
  styleUrls: ['./select-task.page.scss'],
})
export class SelectTaskPage implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
  }
	

 continue() {
    this.route.navigate(['./select-date-time']);
  } 

}
