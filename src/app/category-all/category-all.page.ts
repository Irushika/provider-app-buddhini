import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-category-all',
  templateUrl: './category-all.page.html',
  styleUrls: ['./category-all.page.scss'],
})
export class CategoryAllPage implements OnInit {

  categories = [];
 
  constructor(private route: Router,
    public db: AngularFireDatabase,
    public alertController: AlertController,
    public _sanitizer: DomSanitizer) { }

  ngOnInit() {
    let cat = this.db
      .list('/categories')
      .valueChanges()
      .subscribe((cat) => {
        this.categories = cat;
      });
  }

  getUrl(value){
    return this._sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${value})`);
  }

  adddetails(){
    this.route.navigate(['./select-date-time']);
  }

}
