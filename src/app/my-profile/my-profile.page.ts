import { Component, OnInit } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { UserService } from "../services/user.service";
import firebase from 'firebase';

@Component({
  selector: "app-my-profile",
  templateUrl: "./my-profile.page.html",
  styleUrls: ["./my-profile.page.scss"],
})
export class MyProfilePage implements OnInit {
  user: any;
  public phoneNumber: String;
  public UID: String;
  public name: String;
  public email: string;
  public addresses: any[];
  mynumber: string;
  bio:String;
  category:String;
  city:String;

  constructor(
    private route: Router,
    public activatedRoute: ActivatedRoute,
    private userService: UserService,
    public db: AngularFireDatabase,
    public alertController: AlertController
  ) {}

  ngOnInit() {
    this.loaddetails();
  }

  loaddetails(){
    console.log("my-profile");
    let phone = window.localStorage.getItem("userPhone");
    if (phone) {
      console.log(phone);
      this.db
        .list("/users", (ref) => ref.orderByChild("phoneNumber").equalTo(phone))
        .valueChanges()
        .subscribe((user) => {
          if (user.length > 0) {
            this.user = user[0];
            this.name = this.user.name;
            this.email=this.user.email;
            this.bio=this.user.bio;
            this.category=this.user.category;
            this.city=this.user.city;
            // window.localStorage.setItem("userInfo", JSON.stringify(user[0]));
            
          } else {
            this.presentAlert();
          }
        });

        let data = JSON.parse(window.localStorage.getItem("addresses"));
      if (data) this.addresses = data;
    } else {
      this.user = JSON.parse(window.localStorage.getItem("userInfo"));
      console.log("params not working");
    }

     this.mynumber = window.localStorage.getItem("mynumber");
    console.log(this.mynumber);
  }

  updateDatabasename(){
    this.UID=localStorage.getItem('UID');
    console.log(this.UID);
    console.log(this.name);
    firebase.database().ref(`users/`).child(`${this.UID}`).update(
      {
        name: this.name, 
        
      }) 
      console.log('update done');
  }
  
  updateDatabaseemail(){
    this.UID=localStorage.getItem('UID');
    console.log(this.UID);
    console.log(this.email);
    firebase.database().ref(`users/`).child(`${this.UID}`).update(
      {
        email: this.email, 
        
      }) 
      console.log('update done');
  }

  updateDatabasebio(){
    this.UID=localStorage.getItem('UID');
    console.log(this.UID);
    console.log(this.bio);
    firebase.database().ref(`users/`).child(`${this.UID}`).update(
      {
        bio: this.bio, 
        
      }) 
      console.log('update done');
  }

  updateDatabasecity(){
    this.UID=localStorage.getItem('UID');
    console.log(this.UID);
    console.log(this.city);
    firebase.database().ref(`users/`).child(`${this.UID}`).update(
      {
        city: this.city, 
        
      }) 
      console.log('update done');
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: "Alert",
      message: "No user found.",
      buttons: ["OK"],
    });
    await alert.present();
  }

  addadress() {
    this.route.navigate(["./add-address"]);
  }
}
