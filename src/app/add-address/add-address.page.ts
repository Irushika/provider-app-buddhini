import {
  Component,
  ElementRef,
  NgZone,
  OnInit,
  ViewChild,
} from "@angular/core";
import { TitlePage } from "../title/title.page";
import { LoadingController, ModalController, Platform } from "@ionic/angular";
import { Router } from "@angular/router";
import {
  NativeGeocoder,
  NativeGeocoderResult,
  NativeGeocoderOptions,} from "@ionic-native/native-geocoder/ngx";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import {
  Geolocation,
  GeolocationOptions,
  Geoposition,
  PositionError,
} from "@ionic-native/geolocation/ngx";
import { Diagnostic } from "@ionic-native/diagnostic/ngx";
declare var google;

@Component({
  selector: "app-add-address",
  templateUrl: "./add-address.page.html",
  styleUrls: ["./add-address.page.scss"],
})
export class AddAddressPage implements OnInit {
  @ViewChild("map", { static: true }) mapEle: ElementRef;
  map: any;
  marker: any;
  lat: any;
  lng: any;
  address: any;
  house: any = "";
  landmark: any = "";
  title: any = "home";
  id: any;
  from: any;
  constructor(
    private modalController: ModalController,
    private router: Router,
    public geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public zone: NgZone,
    private platform: Platform,
    public loadingController: LoadingController,
    private androidPermissions: AndroidPermissions,
    private diagnostic: Diagnostic
  ) {}

  ngOnInit() {
    this.getLocation();
  }

  addressAdding() {
    this.modalController
      .create({ component: TitlePage })
      .then((modalElement) => {
        modalElement.present();
      });
  }
  
  searchAddress(event) {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5,
    };
    this.nativeGeocoder
      .forwardGeocode(event.target.value, options)
      .then((result: NativeGeocoderResult[]) => {
        console.log(
          "The coordinates are latitude=" +
            result[0].latitude +
            " and longitude=" +
            result[0].longitude
        );
        let location = new google.maps.LatLng(
          result[0].latitude,
          result[0].longitude
        );
        this.map.setCenter(
          new google.maps.LatLng(result[0].latitude, result[0].longitude)
        );
        this.addMarker(location);
      })
      .catch((error: any) => console.log(error));
  }
  getLocation() {
    this.platform.ready().then(() => {
      if (this.platform.is("android")) {
        this.androidPermissions
          .checkPermission(
            this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION
          )
          .then(
            (result) => console.log("Has permission?", result.hasPermission),
            (err) =>
              this.androidPermissions.requestPermission(
                this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION
              )
          );
        this.grantRequest();
      } else if (this.platform.is("ios")) {
        this.grantRequest();
      } else {
        this.geolocation
          .getCurrentPosition({
            maximumAge: 3000,
            timeout: 10000,
            enableHighAccuracy: false,
          })
          .then((resp) => {
            if (resp) {
              console.log("resp", resp);
              this.lat = resp.coords.latitude;
              this.lng = resp.coords.longitude;
              if (!this.map)
                this.loadmap(
                  resp.coords.latitude,
                  resp.coords.longitude,
                  this.mapEle
                );
              this.getAddress(this.lat, this.lng);
            }
          });
      }
    });
  }
  grantRequest() {
    this.diagnostic
      .isLocationEnabled()
      .then(
        (data) => {
          if (data) {
            this.geolocation
              .getCurrentPosition({
                maximumAge: 3000,
                timeout: 10000,
                enableHighAccuracy: false,
              })
              .then((resp) => {
                if (resp) {
                  console.log("resp", resp);
                  if (!this.map)
                    this.loadmap(
                      resp.coords.latitude,
                      resp.coords.longitude,
                      this.mapEle
                    );
                  const location = new google.maps.LatLng(
                    resp.coords.latitude,
                    resp.coords.longitude
                  );
                  this.map.setCenter(
                    new google.maps.LatLng(
                      resp.coords.latitude,
                      resp.coords.longitude
                    )
                  );
                  this.addMarker(location);
                  this.getAddress(resp.coords.latitude, resp.coords.longitude);
                }
              });
          } else {
            this.diagnostic.switchToLocationSettings();
            this.geolocation
              .getCurrentPosition({
                maximumAge: 3000,
                timeout: 10000,
                enableHighAccuracy: false,
              })
              .then((resp) => {
                if (resp) {
                  console.log("ress,", resp);
                  if (!this.map)
                    this.loadmap(
                      resp.coords.latitude,
                      resp.coords.longitude,
                      this.mapEle
                    );
                  this.getAddress(resp.coords.latitude, resp.coords.longitude);
                }
              });
          }
        },
          (error) => {
            // console.log('errir', error);
          }
      )
      .catch((error) => {
        if (!this.map) this.loadmap(0, 0, this.mapEle);
        // console.log('error', error);
      });
  }
  loadmap(lat, lng, mapElement) {
    const location = new google.maps.LatLng(lat, lng);
    const style = [
      {
        featureType: "all",
        elementType: "all",
        stylers: [{ saturation: -100 }],
      },
    ];

    const mapOptions = {
      zoom: 15,
      scaleControl: false,
      streetViewControl: false,
      zoomControl: false,
      overviewMapControl: false,
      center: location,
      mapTypeControl: false,
      mapTypeControlOptions: {
        mapTypeIds: [google.maps.MapTypeId.ROADMAP, "fire5"],
      },
    };
    this.map = new google.maps.Map(mapElement.nativeElement, mapOptions);
    var mapType = new google.maps.StyledMapType(style, { name: "Grayscale" });
    this.map.mapTypes.set("fire5", mapType);
    this.map.setMapTypeId("fire5");
    this.map.setCenter(
      new google.maps.LatLng(lat, lng)
    );
    this.addMarker(location);
  }
  getAddress(lat, lng) {
    const geocoder = new google.maps.Geocoder();
    const location = new google.maps.LatLng(lat, lng);
    geocoder.geocode({ location: location }, (results, status) => {
      console.log(results);
      this.address = results[0].formatted_address;
      this.lat = lat;
      this.lng = lng;
    });
  }
  addMarker(location) {
    if (this.marker) this.marker.setMap(null);
    console.log("location =>", location);
    const icon = {
      url: "assets/images/pin.jpg",
      scaledSize: new google.maps.Size(50, 50), // scaled size
    };
    this.marker = new google.maps.Marker({
      position: location,
      map: this.map,
      icon: icon,
      draggable: true,
      animation: google.maps.Animation.DROP,
    });
    google.maps.event.addListener(this.marker, "addfeature", () => {
      debugger;
      console.log(this.marker);
      this.getDragAddress(this.marker);
    });
    google.maps.event.addListener(this.marker, "dragend", () => {
      console.log(this.marker);
      this.getDragAddress(this.marker);
    });
  }
  getDragAddress(event) {
    const geocoder = new google.maps.Geocoder();
    const location = new google.maps.LatLng(
      event.position.lat(),
      event.position.lng()
    );
    geocoder.geocode({ location: location }, (results, status) => {
      console.log(results);
      this.address = results[0].formatted_address;
      this.lat = event.position.lat();
      this.lng = event.position.lng();
    });
  }
}
