import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategorySerchPage } from './category-serch.page';

const routes: Routes = [
  {
    path: '',
    component: CategorySerchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategorySerchPageRoutingModule {}
