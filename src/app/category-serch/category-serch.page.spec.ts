import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CategorySerchPage } from './category-serch.page';

describe('CategorySerchPage', () => {
  let component: CategorySerchPage;
  let fixture: ComponentFixture<CategorySerchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategorySerchPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CategorySerchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
