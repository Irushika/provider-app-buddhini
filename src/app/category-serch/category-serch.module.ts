import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { CategorySerchPageRoutingModule } from './category-serch-routing.module';

import { CategorySerchPage } from './category-serch.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,  
    CategorySerchPageRoutingModule
  ],
  declarations: [CategorySerchPage]
})
export class CategorySerchPageModule {}
