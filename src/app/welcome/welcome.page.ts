import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { NgModule }      from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  constructor(  private router: Router,
    public loadingController: LoadingController,
    public navCtrl: NavController) {}

  ngOnInit(){
  //   const loading =  this.loadingController.create({
  //     message: 'WELCOME.',
  //     duration: 100
  //   });
    this.entering();
  }

  async entering(){
    // await loading.present();
    //this.navCtrl.navigateForward('/sign-in');
    this.router.navigateByUrl("/sign-in")
  }

}
