import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams } from '@ionic/angular';
import { NavigationExtras, Router } from '@angular/router';
import { WindowReference } from '../services/window-reference.service';
import { UserService } from '../services/user.service';
import firebase from 'firebase';
import * as admin from "firebase-admin";

import { Storage } from '@ionic/storage';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.page.html',
  styleUrls: ['./sign-in.page.scss'],
})
export class SignInPage implements OnInit {

  public cellNumber: Number;
  windowRef: any;
  public otpScreen: Boolean = false;
  public otpNumber: any;
  phone:any;
  mynumber:any;

  constructor(private navCtrl: NavController,
    private route: Router,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private storage: Storage,
    private windowReference: WindowReference,
    private userService: UserService,
    public alertController: AlertController,
    public db: AngularFireDatabase,) { }

  send() {
    this.route.navigate(['./veirifcation']);
  }

  ngOnInit() {
    console.log('check', firebase.auth().currentUser);

    this.windowReference.storeWindowRef = this.windowReference.windowRef;
    this.windowReference.storeWindowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      size: 'invisible'
    });
    this.windowReference.storeWindowRef.recaptchaVerifier.render();
  }


  continue() {

    this.phone = '+94' + this.cellNumber.toString();

    this.mynumber='0'+this.cellNumber.toString();

    window.localStorage.setItem("mynumber", this.mynumber);
    console.log(this.phone)

    firebase.database().ref().child(`users`).orderByChild('phoneNumber').equalTo(this.phone).on('value', async ev => 
    {
      if (ev.val()) {
        let getUsers: any = Object.values(ev.val());
        console.log('getUsers', getUsers)
        
        if (getUsers.some(item => item.phoneNumber === this.phone)) {
          // if user is not provider, login , then alert you are not provider and can't log
          let user = getUsers[0];
          if (user.position == 'Provider') this.sendnumber();
          else this.presentAlert('you are not provider')
          this.storage.set('user', { UID: Object.keys(ev.val())[0] })
        } 

        else {
          this.route.navigate(['./veirifcation/' + this.phone]);

        }

      }  
       
      else if (!ev.val()) {
        this.route.navigate(['./veirifcation/' + this.phone]);
  
      }

    }
    );

}
 sendnumber(){

    window.localStorage.setItem("userPhone", this.phone);
    console.log("send number " + this.phone);

    this.phone.length > 0 ?
    this.route.navigate(['./new-appoinment']) :
      this.presentAlert('No user found.');
 }

  async sendCode() {
    console.log("Sending Code")
    const loader = await this.loadingCtrl.create();
    loader.present();
    const appVerifier = this.windowReference.storeWindowRef.recaptchaVerifier;
    const num: any = '+94' + this.cellNumber.toString();

    console.log("verifier", appVerifier)
    console.log("num", num)


    firebase.auth().signInWithPhoneNumber(num, appVerifier).then((result) => {
      this.windowReference.storeWindowRef.confirmationResult = result;
      this.otpScreen = true;
      loader.dismiss();
    }).catch((err) => {
      loader.dismiss();
      this.alertCtrl.create({
        header: 'Invalid Number',
        message: `${err}`,
        buttons: ['Ok']
      }).then((res) => {
        res.present();
      });
    });
  }
   async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message,
      buttons: ['OK']
    });
    await alert.present();
  }

  // checknumbers(){
  //   if(this.cellNumber.substr(0,9))
  //   {
  //     return true;
  //   }
  //   return false;
  // }
}
