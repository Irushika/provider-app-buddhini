import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class RegisterPage implements OnInit {

  categories = [];
  category:string;

  public phoneNumber: String
  public UID: String
  public name: String
  public email: String
  public position:String
  public address:String
  public bio:String
  public city:String

  constructor(private navCtrl: NavController, 
    private userService: UserService,
    private storage: Storage,
    public db: AngularFireDatabase,) {
   
  }

  ngOnInit() {
   
    this.phoneNumber=localStorage.getItem('mynumber');

    this.userService._$userSubject.subscribe(user => {
      if (user) {
        console.log(user, 'user')
        // this.phoneNumber = user.phoneNumber;
        this.UID = user.UID;
        // window.localStorage.setItem("myID", user.UID);
      }

    })

    

    let cat = this.db
    .list('/categories')
    .valueChanges()
    .subscribe((cat) => {
      this.categories = cat;
    });
  }
 
  registerUser() {

    console.log(this.category);
    localStorage.setItem('mycategory', this.category);
    console.log(this.category);
    this.storage.set(`${this.UID}_cat`, this.category);

    firebase.database().ref().child(`users/${this.UID}`).set(
    {
      name: this.name, 
      phoneNumber: this.phoneNumber, 
      email:this.email,
      position: 'Provider',
      address:'update',
      category: this.category,
      bio: this.bio,
      city:this.city
    }
		);
    this.navCtrl.navigateRoot(['./new-appoinment']);
  }


}
