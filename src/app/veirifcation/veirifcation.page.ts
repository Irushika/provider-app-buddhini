import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { UserService } from '../services/user.service';
import { WindowReference } from '../services/window-reference.service';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';
import * as admin from "firebase-admin";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-veirifcation',
  templateUrl: './veirifcation.page.html',
  styleUrls: ['./veirifcation.page.scss'],
})
export class VeirifcationPage implements OnInit {
  public otpNumber: any;
  public cellNumber: string;
  public otpScreen: Boolean = true;

  constructor(
    private navCtrl: NavController,
    private windowReference: WindowReference,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
     
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private storage: Storage,
  ) { }

  ngOnInit() {
    this.windowReference.storeWindowRef = this.windowReference.windowRef;
    this.windowReference.storeWindowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      size: 'invisible'
    });
    this.windowReference.storeWindowRef.recaptchaVerifier.render();
    
    this.cellNumber = this.activatedRoute.snapshot.paramMap.get('no');

    this.sendCode();
  }


   async verify() {
    const loader = await this.loadingCtrl.create();
    loader.present();
    console.log('otp', this.otpNumber)
    this.windowReference.storeWindowRef.confirmationResult.confirm(this.otpNumber)
      .then((result) => {
        loader.dismiss();
        console.log('resultt phone: ', result.user.phoneNumber);
        console.log('resultt UID: ', result.user.uid);
        let setUser = {
          phoneNumber: result.user.phoneNumber,
          UID: result.user.uid
        }
        // store the user information 
        this.storage.set('user', setUser);
        this.storage.set('isLoggedIn', true);
        this.userService._$userSubject.next(setUser)
        this.navCtrl.navigateRoot(['./sign-up']);

      }).catch((err) => {
        loader.dismiss();
        this.alertCtrl.create({
          header: 'Incorrect Code',
          message: `${err.message}`,
          buttons: ['Ok']
        }).then((res) => {
          res.present();
        });
      });
  }

  sendotp(){
    this.sendCode();
  }
  

  async sendCode() {
    console.log("Sending Code")
    const loader = await this.loadingCtrl.create();
    loader.present();
    const appVerifier = this.windowReference.storeWindowRef.recaptchaVerifier;
    const num: any = this.cellNumber.toString();

    console.log("verifier", appVerifier)
    console.log("num", num)
 

    firebase.auth().signInWithPhoneNumber(num, appVerifier).then((result) => {
      this.windowReference.storeWindowRef.confirmationResult = result;
      this.otpScreen = true;
      loader.dismiss();
    }).catch((err) => {
      loader.dismiss();
      this.alertCtrl.create({
        header: 'Invalid Number',
        message: `${err}`,
        buttons: ['Ok']
      }).then((res) => {
        res.present();
      });
    });
  }


}
