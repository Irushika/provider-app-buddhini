import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VeirifcationPage } from './veirifcation.page';

describe('VeirifcationPage', () => {
  let component: VeirifcationPage;
  let fixture: ComponentFixture<VeirifcationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VeirifcationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VeirifcationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
