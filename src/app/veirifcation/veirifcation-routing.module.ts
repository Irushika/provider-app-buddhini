import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VeirifcationPage } from './veirifcation.page';

const routes: Routes = [
  {
    path: '',
    component: VeirifcationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VeirifcationPageRoutingModule {}
