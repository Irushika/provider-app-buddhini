import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.page.html',
  styleUrls: ['./navigation.page.scss'],
})
export class NavigationPage implements OnInit {

  job:any;
  requests:any[];
  request:any;
  JID:any;
  category:any;
  index: Number;
  name:any;
  completedRequest: any;
  user: any;
  StartedRequest:any;
  currentRequest: any;
  provider_UID:any;

  constructor( public db: AngularFireDatabase,
    public alertController: AlertController,
    public activatedRoute: ActivatedRoute,) { }

    ngOnInit() {
   
      this.JID = this.activatedRoute.snapshot.paramMap.get('JID');
      this.loadJobDetails();
      }


      loadJobDetails() {

        if (this.JID) {
          console.log(this.JID);
          window.localStorage.setItem("myjobid", this.JID);
          this.db.database.ref("/jobs/" + this.JID).on(
            'value', (snapshot) => {
              this.job = snapshot.val();
              console.log(this.job);
              let userUID=this.job.UID;
              this.loadUserDetails(userUID)

              let currentUID = window.localStorage.getItem('UID');
              this.currentRequest = this.job.requests.filter((request)=>{
                return currentUID == request.provider_UID
              })[0];

             

              window.localStorage.setItem("jobInfo", JSON.stringify(this.job));
              this.requests = this.job.requests;
              console.log(this.requests);

              this.StartedRequest = this.requests.find(request => request.status == 'started');
              this.checkrequest(this.StartedRequest)
            }
          );
    
    
        } else {
          this.job = JSON.parse(window.localStorage.getItem("jobInfo"));
          console.log("params not working");
        }
      }

      checkrequest(requests){
        if (requests== "started")
        {
          return true;
        }return false;
      }

  
      async presentAlert() {
        const alert = await this.alertController.create({
        header: "Alert",
        message: "No Job found.",
        buttons: ["OK"],
        });
        await alert.present();
      }

      loadUserDetails(UID) {
        this.db.database.ref("/users/" + UID).on(
          'value', (snapshot) => {
            this.user = snapshot.val();
            this.name=this.user.name;
          }
        );
      }
}
