import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-my-appointments',
  templateUrl: './my-appointments.page.html',
  styleUrls: ['./my-appointments.page.scss'],
})
export class MyAppointmentsPage implements OnInit {

  appointment: any[];
  UID: any;

  constructor(private route: Router,
    public alertController: AlertController,
    public storage: Storage,
    public db: AngularFireDatabase,) { }

  ngOnInit() {
    this.loadingMyAppointment();
  }

  loadingMyAppointment(){
    console.log("my-profile");
    let userInfo = JSON.parse( localStorage.getItem('userInfo'));
    let category = userInfo.category;
    this.storage.get('user').then(
      user => {
        console.log(user)
        if (user) this.UID = user.UID;     
       }
    )
    this.db
      .list("/jobs")
      .valueChanges([],{idField: 'id'})
      .subscribe((appointment) => {
        console.log(appointment);
        this.appointment = appointment;
        console.log(this.appointment);
        window.localStorage.setItem("apointmentInfo", JSON.stringify(appointment[0]));

      });
  }


  loadingPastAppointment(){
    console.log("my-profile");
    let userInfo = JSON.parse( localStorage.getItem('userInfo'));
    let category = userInfo.category;
    this.storage.get('user').then(
      user => {
        console.log(user)
        if (user) this.UID = user.UID;     
       }
    )
    this.db
      .list("/jobs", (ref) => ref.orderByChild("requests.provider_UID").equalTo(this.UID))
      .valueChanges([],{idField: 'id'})
      .subscribe((appointment) => {
        console.log(appointment);
        this.appointment = appointment;
        window.localStorage.setItem("apointmentInfo", JSON.stringify(appointment[0]));

      });
  }


 past_appointment_info(JID) {
    this.route.navigate(['./appointment-info', JID]);
  } 

  findrequested(state){
    if(state=='requested'){
      return true;
    }
    else
    {
      return false;
    }
  }


  findstarted(state){
    if(state=='started'){
      return true;
    }
    else
    {
      return false;
    }
  }


  my_appointment_info(JID){
     this.route.navigate(['./navigation', JID]);
  }

 
}
