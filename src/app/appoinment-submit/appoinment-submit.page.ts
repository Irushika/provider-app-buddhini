import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';


import firebase from 'firebase';
import * as admin from "firebase-admin";

import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-appoinment-submit',
  templateUrl: './appoinment-submit.page.html',
  styleUrls: ['./appoinment-submit.page.scss'],
})
export class AppoinmentSubmitPage implements OnInit {

  job:any;
  JID:any;

  date:String;

  constructor(private route: Router,
    public activatedRoute: ActivatedRoute,
    private storage: Storage,
    public db: AngularFireDatabase,
    public alertController: AlertController) { }

  ngOnInit() {
   
  this.JID = this.activatedRoute.snapshot.paramMap.get('JID');
  this.loadJobDetails();
  }

  loadJobDetails(){
    
    if (this.JID) {
      console.log(this.JID);
      window.localStorage.setItem("myjobid" , this.JID);
      this.db.database.ref("/jobs/" + this.JID).on(
        'value', (snapshot) => {
          this.job = snapshot.val();
          console.log(this.job);
          window.localStorage.setItem("jobInfo", JSON.stringify(this.job));
          
        }
      );

        
    } else {
      this.job = JSON.parse(window.localStorage.getItem("jobInfo"));
      console.log("params not working");
    }
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: "Alert",
      message: "No job found.",
      buttons: ["OK"],
    });
    await alert.present();
  }


  submit(){
    this.route.navigate(['./select-date-time', this.JID]);
  }

}