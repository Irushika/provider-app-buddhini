import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { AppoinmentSubmitPageRoutingModule } from './appoinment-submit-routing.module';

import { AppoinmentSubmitPage } from './appoinment-submit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule, 
    
    AppoinmentSubmitPageRoutingModule
  ],
  declarations: [AppoinmentSubmitPage]
})
export class AppoinmentSubmitPageModule {}
