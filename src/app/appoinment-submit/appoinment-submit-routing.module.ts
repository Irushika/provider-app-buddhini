import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppoinmentSubmitPage } from './appoinment-submit.page';

const routes: Routes = [
  {
    path: '',
    component: AppoinmentSubmitPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppoinmentSubmitPageRoutingModule {}
