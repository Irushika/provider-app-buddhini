import { Component, OnInit, Inject } from '@angular/core';

import { Platform, NavController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { TranslateService } from '@ngx-translate/core';
import { APP_CONFIG, AppConfig } from './app.config';
import { MyEvent } from 'src/services/myevent.services';
import { Constants } from 'src/models/contants.models';

import { AngularFirestore } from '@angular/fire/firestore';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase } from '@angular/fire/database';

import { BackButtonEvent } from '@ionic/core';
import { Router, NavigationEnd } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { ApiFcmService } from './services/api-fcm.service';

const { App } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {

  rtlSide = "left";
  public selectedIndex = 0;
  UID: any;
  mycategory: any;
  name: string;


  public appPages = [
    {
      title: 'home',
      url: '/new-appoinment',
      icon: 'zmdi zmdi-home'
    },
    {
      title: 'my_appointments',
      url: '/my-appointments',
      icon: 'zmdi zmdi-assignment-check'
    },

    {
      title: 'my_profile',
      url: '/my-profile',
      icon: 'zmdi zmdi-account'
    },
    //  {
    //   title: 'change_language', 
    //   url: '/select-language',
    //   icon: 'zmdi zmdi-globe'
    // },

    {
      title: 'contact_us',
      url: '/contact-us',
      icon: 'zmdi zmdi-email'
    },

    {
      title: 'faq',
      url: '/faq',
      icon: 'zmdi zmdi-comment-text'
    },

    {
      title: 'terms_conditions',
      url: '/tnc',
      icon: 'zmdi zmdi-assignment'
    },

    {
      title: 'logout',
      url: '/sign-in',
      icon: 'zmdi zmdi-sign-in'
    },
  ];

  presentExit;
  currentUrl
  constructor(
    private storage: Storage,
    private router: Router,
    @Inject(APP_CONFIG) public config: AppConfig,
    private platform: Platform, private navCtrl: NavController,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private db: AngularFireDatabase,
    private alertController: AlertController,
    private translate: TranslateService,
    private myEvent: MyEvent,
    private apiFcmService: ApiFcmService
  ) {
    this.initializeApp();

    // const things = db.collection('things').valueChanges();
    // things.subscribe(console.log);


    this.myEvent.getLanguageObservable().subscribe(value => {
      this.navCtrl.navigateRoot(['./']);
      this.globalize(value);
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // load the token
      this.apiFcmService.loadKey();

      let defaultLang = window.localStorage.getItem(Constants.KEY_DEFAULT_LANGUAGE);
      this.globalize(defaultLang);
    });
  }

  globalize(languagePriority) {
    this.translate.setDefaultLang("en");
    let defaultLangCode = this.config.availableLanguages[0].code;
    this.translate.use(languagePriority && languagePriority.length ? languagePriority : defaultLangCode);
    this.setDirectionAccordingly(languagePriority && languagePriority.length ? languagePriority : defaultLangCode);
  }

  setDirectionAccordingly(lang: string) {
    switch (lang) {
      case 'ar': {
        this.rtlSide = "rtl";
        break;
      }
      default: {
        this.rtlSide = "ltr";
        break;
      }
    }
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }



    this.storage.get('user').then(
      user => {
        console.log('user', user);
        if (user) {
          this.UID = user.UID;
          console.log(user.UID);
          localStorage.setItem('UID', this.UID);
          // check for the user current token is the same as the loaded token;
          console.log('storage user :', user);
          console.log('loaded token :', this.apiFcmService.token);
          if (user.fcmToken != this.apiFcmService.token) {

            console.log('saving token');
            user.fcmToken = this.apiFcmService.token;
            // save new token in storage
            this.storage.set('user', user);

            // update the new token in the db.
            this.db.database.ref("/users/" + user.UID).update({ fcmToken: this.apiFcmService.token });
          }
          this.db.database.ref("/users/" + user.UID).on(
            'value', (snapshot) => {
              const data = snapshot.val();
              this.name = data.name;
              this.mycategory = data.category;
              localStorage.setItem('mycategory', this.mycategory);
            }
          );
        } else {
          // the user have not yet registred
          console.log('the user have not yet registred')
        }
      }
    );

    document.addEventListener('ionBackButton', (ev: BackButtonEvent) => {
      ev.detail.register(-1, () => {
        // const path = window.location.pathname;
        if (this.currentUrl == '/home') {
          if (!this.presentExit) this.presentAlertConfirmExit();
        } else {
          this.router.navigate(['/home']);
        }
      });
    });
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.currentUrl = event.url;
        console.log(this.currentUrl);
      }
    });


  }

  async presentAlertConfirmExit() {
    this.presentExit = true;
    const alert = await this.alertController.create({
      header: 'Attention!',
      message: 'Do you want to <strong>leave</strong> the application?',
      mode: 'ios',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel: blah');
            this.presentExit = false;
          }
        }, {
          text: 'Leave',
          handler: () => {
            console.log('Confirm Okay');
            App.exitApp();
          }
        }
      ]
    });

    await alert.present();
  }

  buyAppAction() {
    window.open("http://bit.ly/cc2_ShopHour", '_system', 'location=no');
  }


}



