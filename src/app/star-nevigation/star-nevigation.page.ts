import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AlertController } from '@ionic/angular';
import firebase from 'firebase';

@Component({
  selector: 'app-star-nevigation',
  templateUrl: './star-nevigation.page.html',
  styleUrls: ['./star-nevigation.page.scss'],
})
export class StarNevigationPage implements OnInit {

  job:any;

  constructor( public db: AngularFireDatabase,
    public alertController: AlertController) { }

  ngOnInit() {

    
    let JID = window.localStorage.getItem("getjobs");
    console.log(JID);
    if (JID) {
      console.log(JID);
      this.db
        .list("/jobs", (ref) => ref.orderByChild("phoneNumber").equalTo(JID))
        .valueChanges()
        .subscribe((job) => {
          if (job.length > 0) {
            this.job = job[0];
            window.localStorage.setItem("jobInfo", JSON.stringify(job[0]));
            
          } else {
            this.presentAlert();
          }
        });

        
    } else {
      this.job = JSON.parse(window.localStorage.getItem("jobInfo"));
      console.log("params not working");
    }
};

  async presentAlert() {
    const alert = await this.alertController.create({
      header: "Alert",
      message: "No user found.",
      buttons: ["OK"],
    });
    await alert.present();
  }
}
