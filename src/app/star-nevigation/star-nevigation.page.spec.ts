import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StarNevigationPage } from './star-nevigation.page';

describe('StarNevigationPage', () => {
  let component: StarNevigationPage;
  let fixture: ComponentFixture<StarNevigationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StarNevigationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StarNevigationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
