import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { StarNevigationPageRoutingModule } from './star-nevigation-routing.module';

import { StarNevigationPage } from './star-nevigation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    StarNevigationPageRoutingModule
  ],
  declarations: [StarNevigationPage]
})
export class StarNevigationPageModule {}
