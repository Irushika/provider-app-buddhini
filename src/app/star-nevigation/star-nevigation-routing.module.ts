import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StarNevigationPage } from './star-nevigation.page';

const routes: Routes = [
  {
    path: '',
    component: StarNevigationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StarNevigationPageRoutingModule {}
