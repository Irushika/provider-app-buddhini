import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyJobFilterPipe } from './my-job-filter.pipe';
import { SortdatePipe } from './sortdate.pipe';
import { JobFilterPipe } from './job-filter.pipe';
import { PastJobFilterPipe } from './past-job-filter.pipe';

const pipes = [MyJobFilterPipe, JobFilterPipe, SortdatePipe, PastJobFilterPipe];

@NgModule({
  declarations: [...pipes],
  imports: [
    CommonModule
  ],
  exports: [
    ...pipes
  ]
})
export class PipesModule { }
