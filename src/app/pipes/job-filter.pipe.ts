import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'jobFilter'
})
export class JobFilterPipe implements PipeTransform {

  transform(jobs: any[], UID: string): any[] {
    if (!jobs) return [];
    if (!UID) return jobs;
    
    return jobs.filter( job =>  
      {

        if(!job.requests) return true;

        else if (job.requests.length == 0) return true;

          else {
                  let request = job.requests.find( request => request.provider_UID == UID);
                  
                  if(request) return false;
                  else return true;
                }
      });
  }

}
