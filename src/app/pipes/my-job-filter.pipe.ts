import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myJobFilter'
})
export class MyJobFilterPipe implements PipeTransform {

  transform(jobs: any[], UID: string): any[] {
    if (!jobs) return [];

    if (!UID) return jobs;

    return jobs.filter( job =>  {
      if(!job.requests) return false;

      else if (job.requests.length == 0) return false;

       else {
         let request = job.requests.find( request => request.provider_UID == UID && (request.status == 'pending' || request.status == 'started'|| request.status == 'requested'));
         if(request) return true;
         else return false;
       }

    });
   }

}
