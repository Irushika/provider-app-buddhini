import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortdate'
})
export class SortdatePipe implements PipeTransform {

  transform(jobs: any[]): any[] {
    if (!jobs) return [];  
    return jobs.sort( (a, b) =>{
      let avalue: any =  new Date(a.date + " "  + a.time),
      bvalue: any = new Date(b.date + " "  + b.time);
      return avalue-bvalue ; 
    });
    }
   

}
