(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["appoinment-submit-appoinment-submit-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/appoinment-submit/appoinment-submit.page.html":
  /*!*****************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/appoinment-submit/appoinment-submit.page.html ***!
    \*****************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAppoinmentSubmitAppoinmentSubmitPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n\t<ion-toolbar>\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-back-button text=\"\" icon=\"chevron-back-outline\"></ion-back-button>\n\t\t</ion-buttons>\n\t\t<ion-title>\n\n\t\t</ion-title>\n\t</ion-toolbar>\n\t<h1>\n\t\t{{'Appointment Information' | translate}}\n\t\t<small>{{'Confirmed on' | translate}} {{job?.date| date:'EEEE, MMMM d, y'}}</small>\n\t</h1>\n</ion-header>\n\n\n<ion-content fullscreen>\n\t<div class=\"form\">\n\t\t<ion-list lines=\"none\">\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>{{'your_appointment_for' | translate}}</h2>\n\t\t\t\t\t<h3>{{job?.category}}</h3>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>{{'date_time' | translate}}</h2>\n\t\t\t\t\t<h3>{{job?.date| date:'EEEE, MMMM d, y'}}</h3><br><h3>{{job?.time}}</h3> \n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>{{'location_at' | translate}}</h2>\n\t\t\t\t\t<h3>{{job?.adrress_type}}</h3>\n\t\t\t\t\t<ion-textarea rows=\"1\" readonly> {{job?.address}} </ion-textarea>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\t\t\t\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>Additional Information</h2>\n\t\t\t\t\t<ion-textarea auto-grow=\"true\" rows=\"1\" readonly>{{job?.information}}</ion-textarea>\n\t\t\t\t\t<h4></h4>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\t\t\t\n\t\t</ion-list>\n\t</div>\n</ion-content>\n\n<ion-footer class=\"ion-no-border d-flex\">\n\t <ion-row>\n\t\t <ion-col size=\"12\">\n\t\t\t <ion-button size=\"large\" fill=\"outline\" shape=\"block\" class=\"btn end\" (click)=\"submit()\">{{'Submit Proposal' | translate}}</ion-button>\n\t\t </ion-col> \n\t</ion-row>\n\t\n</ion-footer>\n";
    /***/
  },

  /***/
  "./src/app/appoinment-submit/appoinment-submit-routing.module.ts":
  /*!***********************************************************************!*\
    !*** ./src/app/appoinment-submit/appoinment-submit-routing.module.ts ***!
    \***********************************************************************/

  /*! exports provided: AppoinmentSubmitPageRoutingModule */

  /***/
  function srcAppAppoinmentSubmitAppoinmentSubmitRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppoinmentSubmitPageRoutingModule", function () {
      return AppoinmentSubmitPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _appoinment_submit_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./appoinment-submit.page */
    "./src/app/appoinment-submit/appoinment-submit.page.ts");

    const routes = [{
      path: '',
      component: _appoinment_submit_page__WEBPACK_IMPORTED_MODULE_3__["AppoinmentSubmitPage"]
    }];
    let AppoinmentSubmitPageRoutingModule = class AppoinmentSubmitPageRoutingModule {};
    AppoinmentSubmitPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AppoinmentSubmitPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/appoinment-submit/appoinment-submit.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/appoinment-submit/appoinment-submit.module.ts ***!
    \***************************************************************/

  /*! exports provided: AppoinmentSubmitPageModule */

  /***/
  function srcAppAppoinmentSubmitAppoinmentSubmitModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppoinmentSubmitPageModule", function () {
      return AppoinmentSubmitPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _appoinment_submit_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./appoinment-submit-routing.module */
    "./src/app/appoinment-submit/appoinment-submit-routing.module.ts");
    /* harmony import */


    var _appoinment_submit_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./appoinment-submit.page */
    "./src/app/appoinment-submit/appoinment-submit.page.ts");

    let AppoinmentSubmitPageModule = class AppoinmentSubmitPageModule {};
    AppoinmentSubmitPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"], _appoinment_submit_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppoinmentSubmitPageRoutingModule"]],
      declarations: [_appoinment_submit_page__WEBPACK_IMPORTED_MODULE_7__["AppoinmentSubmitPage"]]
    })], AppoinmentSubmitPageModule);
    /***/
  },

  /***/
  "./src/app/appoinment-submit/appoinment-submit.page.scss":
  /*!***************************************************************!*\
    !*** ./src/app/appoinment-submit/appoinment-submit.page.scss ***!
    \***************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAppoinmentSubmitAppoinmentSubmitPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-header h1 small {\n  display: block;\n  color: var(--text-light);\n  font-weight: 400;\n  font-size: 0.95rem;\n  opacity: 0.7;\n  padding-top: 6px;\n}\n\n.form {\n  padding-top: 50px;\n}\n\n.form ion-textarea {\n  background-color: transparent;\n}\n\n.form ion-list ion-item {\n  margin-bottom: 25px;\n}\n\n.form ion-list ion-item h2 {\n  margin: 0;\n  color: var(--text-light);\n  font-size: 1.15rem;\n  padding-bottom: 10px;\n  font-weight: 700;\n  opacity: 0.7;\n}\n\n.form ion-list ion-item h3 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 700;\n  font-size: 1.35rem;\n  padding-bottom: 5px;\n}\n\n.form ion-list ion-item h4 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 500;\n  font-size: 1.15rem;\n  line-height: 23px;\n}\n\n.form ion-list ion-item ion-textarea {\n  font-size: 1rem !important;\n  letter-spacing: 0.5px !important;\n}\n\n.form ion-list ion-item ion-label {\n  color: var(--text-dark) !important;\n  font-weight: 700 !important;\n  font-size: 1.35rem !important;\n}\n\nion-footer {\n  background: var(--white);\n  padding: 10px 10px 20px 10px;\n}\n\nion-footer ion-row {\n  width: 100%;\n}\n\nion-footer ion-row ion-col {\n  padding: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwb2lubWVudC1zdWJtaXQvRTpcXEVcXHNlcnZpY2VcXGZpcmViYXNlIDA0XFxwcm92aWRlci1hcHAtYnVkZGhpbmkvc3JjXFxhcHBcXGFwcG9pbm1lbnQtc3VibWl0XFxhcHBvaW5tZW50LXN1Ym1pdC5wYWdlLnNjc3MiLCJzcmMvYXBwL2FwcG9pbm1lbnQtc3VibWl0L2FwcG9pbm1lbnQtc3VibWl0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFRTtFQUNDLGNBQUE7RUFDQSx3QkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNESDs7QURNQTtFQUNDLGlCQUFBO0FDSEQ7O0FES0M7RUFDQyw2QkFBQTtBQ0hGOztBRE1FO0VBQ0MsbUJBQUE7QUNKSDs7QURNRztFQUNDLFNBQUE7RUFDQSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUNKSjs7QURPRztFQUNDLFNBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ0xKOztBRFFHO0VBQ0MsU0FBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDTko7O0FEU0c7RUFDQywwQkFBQTtFQUNBLGdDQUFBO0FDUEo7O0FEVUc7RUFDQyxrQ0FBQTtFQUNBLDJCQUFBO0VBQ0EsNkJBQUE7QUNSSjs7QURjQTtFQUNDLHdCQUFBO0VBQ0EsNEJBQUE7QUNYRDs7QURhQztFQUNDLFdBQUE7QUNYRjs7QURhRTtFQUNDLGVBQUE7QUNYSCIsImZpbGUiOiJzcmMvYXBwL2FwcG9pbm1lbnQtc3VibWl0L2FwcG9pbm1lbnQtc3VibWl0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXIge1xyXG5cdGgxIHtcclxuXHRcdHNtYWxsIHtcclxuXHRcdFx0ZGlzcGxheTogYmxvY2s7XHJcblx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcclxuXHRcdFx0Zm9udC13ZWlnaHQ6IDQwMDtcclxuXHRcdFx0Zm9udC1zaXplOiAuOTVyZW07XHJcblx0XHRcdG9wYWNpdHk6IC43O1xyXG5cdFx0XHRwYWRkaW5nLXRvcDogNnB4O1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLmZvcm0ge1xyXG5cdHBhZGRpbmctdG9wOiA1MHB4O1xyXG5cclxuXHRpb24tdGV4dGFyZWF7XHJcblx0XHRiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuXHR9XHJcblx0aW9uLWxpc3Qge1xyXG5cdFx0aW9uLWl0ZW0ge1xyXG5cdFx0XHRtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG5cclxuXHRcdFx0aDIge1xyXG5cdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxLjE1cmVtO1xyXG5cdFx0XHRcdHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG5cdFx0XHRcdGZvbnQtd2VpZ2h0OiA3MDA7XHJcblx0XHRcdFx0b3BhY2l0eTogLjc7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGgzIHtcclxuXHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0Y29sb3I6IHZhcigtLXRleHQtZGFyayk7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IDcwMDtcclxuXHRcdFx0XHRmb250LXNpemU6IDEuMzVyZW07XHJcblx0XHRcdFx0cGFkZGluZy1ib3R0b206IDVweDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aDQge1xyXG5cdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcclxuXHRcdFx0XHRmb250LXdlaWdodDogNTAwO1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMS4xNXJlbTtcclxuXHRcdFx0XHRsaW5lLWhlaWdodDogMjNweDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aW9uLXRleHRhcmVhIHtcclxuXHRcdFx0XHRmb250LXNpemU6IDFyZW0gIWltcG9ydGFudDtcclxuXHRcdFx0XHRsZXR0ZXItc3BhY2luZzogLjVweCAhaW1wb3J0YW50O1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpb24tbGFiZWwge1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IDcwMCAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMS4zNXJlbSAhaW1wb3J0YW50O1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG5pb24tZm9vdGVyIHtcclxuXHRiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XHJcblx0cGFkZGluZzogMTBweCAxMHB4IDIwcHggMTBweDtcclxuXHJcblx0aW9uLXJvdyB7XHJcblx0XHR3aWR0aDogMTAwJTsgXHJcblxyXG5cdFx0aW9uLWNvbCB7XHJcblx0XHRcdHBhZGRpbmc6IDAgMTBweDtcclxuXHRcdH1cclxuXHR9XHJcbn0iLCJpb24taGVhZGVyIGgxIHNtYWxsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgZm9udC1zaXplOiAwLjk1cmVtO1xuICBvcGFjaXR5OiAwLjc7XG4gIHBhZGRpbmctdG9wOiA2cHg7XG59XG5cbi5mb3JtIHtcbiAgcGFkZGluZy10b3A6IDUwcHg7XG59XG4uZm9ybSBpb24tdGV4dGFyZWEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cbi5mb3JtIGlvbi1saXN0IGlvbi1pdGVtIHtcbiAgbWFyZ2luLWJvdHRvbTogMjVweDtcbn1cbi5mb3JtIGlvbi1saXN0IGlvbi1pdGVtIGgyIHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XG4gIGZvbnQtc2l6ZTogMS4xNXJlbTtcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIG9wYWNpdHk6IDAuNztcbn1cbi5mb3JtIGlvbi1saXN0IGlvbi1pdGVtIGgzIHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgZm9udC1zaXplOiAxLjM1cmVtO1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWl0ZW0gaDQge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xuICBmb250LXdlaWdodDogNTAwO1xuICBmb250LXNpemU6IDEuMTVyZW07XG4gIGxpbmUtaGVpZ2h0OiAyM3B4O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWl0ZW0gaW9uLXRleHRhcmVhIHtcbiAgZm9udC1zaXplOiAxcmVtICFpbXBvcnRhbnQ7XG4gIGxldHRlci1zcGFjaW5nOiAwLjVweCAhaW1wb3J0YW50O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWl0ZW0gaW9uLWxhYmVsIHtcbiAgY29sb3I6IHZhcigtLXRleHQtZGFyaykgIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDcwMCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDEuMzVyZW0gIWltcG9ydGFudDtcbn1cblxuaW9uLWZvb3RlciB7XG4gIGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcbiAgcGFkZGluZzogMTBweCAxMHB4IDIwcHggMTBweDtcbn1cbmlvbi1mb290ZXIgaW9uLXJvdyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuaW9uLWZvb3RlciBpb24tcm93IGlvbi1jb2wge1xuICBwYWRkaW5nOiAwIDEwcHg7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/appoinment-submit/appoinment-submit.page.ts":
  /*!*************************************************************!*\
    !*** ./src/app/appoinment-submit/appoinment-submit.page.ts ***!
    \*************************************************************/

  /*! exports provided: AppoinmentSubmitPage */

  /***/
  function srcAppAppoinmentSubmitAppoinmentSubmitPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppoinmentSubmitPage", function () {
      return AppoinmentSubmitPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/fire/database */
    "./node_modules/@angular/fire/fesm2015/angular-fire-database.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");

    let AppoinmentSubmitPage = class AppoinmentSubmitPage {
      constructor(route, activatedRoute, storage, db, alertController) {
        this.route = route;
        this.activatedRoute = activatedRoute;
        this.storage = storage;
        this.db = db;
        this.alertController = alertController;
      }

      ngOnInit() {
        this.JID = this.activatedRoute.snapshot.paramMap.get('JID');
        this.loadJobDetails();
      }

      loadJobDetails() {
        if (this.JID) {
          console.log(this.JID);
          window.localStorage.setItem("myjobid", this.JID);
          this.db.database.ref("/jobs/" + this.JID).on('value', snapshot => {
            this.job = snapshot.val();
            console.log(this.job);
            window.localStorage.setItem("jobInfo", JSON.stringify(this.job));
          });
        } else {
          this.job = JSON.parse(window.localStorage.getItem("jobInfo"));
          console.log("params not working");
        }
      }

      presentAlert() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
          const alert = yield this.alertController.create({
            header: "Alert",
            message: "No job found.",
            buttons: ["OK"]
          });
          yield alert.present();
        });
      }

      submit() {
        this.route.navigate(['./select-date-time', this.JID]);
      }

    };

    AppoinmentSubmitPage.ctorParameters = () => [{
      type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
    }, {
      type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
    }, {
      type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
    }, {
      type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
    }];

    AppoinmentSubmitPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-appoinment-submit',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./appoinment-submit.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/appoinment-submit/appoinment-submit.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./appoinment-submit.page.scss */
      "./src/app/appoinment-submit/appoinment-submit.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]])], AppoinmentSubmitPage);
    /***/
  }
}]);
//# sourceMappingURL=appoinment-submit-appoinment-submit-module-es5.js.map