(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["select-task-select-task-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/select-task/select-task.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/select-task/select-task.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSelectTaskSelectTaskPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n\t<ion-toolbar>\r\n\t\t<ion-buttons slot=\"start\">\r\n\t\t\t<ion-back-button text=\"\" icon=\"chevron-back-outline\"></ion-back-button>\r\n\t\t</ion-buttons>\r\n\t\t<ion-title>\r\n\t\t\t<ion-icon class=\"zmdi zmdi-circle active\"></ion-icon>\r\n\t\t\t<ion-icon class=\"zmdi zmdi-circle\"></ion-icon>\r\n\t\t\t<ion-icon class=\"zmdi zmdi-circle\"></ion-icon>\r\n\t\t</ion-title>\r\n\t</ion-toolbar>\r\n\t<h1>{{'select_task' | translate}}</h1>\r\n</ion-header>\r\n\r\n<ion-content fullscreen>\r\n\t<ion-list lines=\"none\">\r\n\t\t<ion-radio-group value=\"biff\">\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Refrigerator Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"1\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Television Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"2\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Air Conditioner Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"3\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Computer Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"4\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Microwave Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"5\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Cooler Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"6\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Laptop Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"7\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Audio Player Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"8\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Refrigerator Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"9\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Television Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"10\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Air Conditioner Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"11\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Computer Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"12\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Microwave Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"13\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Cooler Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"14\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Laptop Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"15\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Audio Player Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"16\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\r\n\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Refrigerator Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"17\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Television Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"18\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Air Conditioner Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"19\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Computer Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"20\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Microwave Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"21\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Cooler Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"22\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Laptop Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"23\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>Audio Player Repair</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"24\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\r\n\t\t</ion-radio-group>\r\n\r\n\t</ion-list>\r\n</ion-content>\r\n<ion-footer class=\"ion-no-border d-flex\">\r\n\t<div class=\"amount\">\r\n\t\t<h3>{{'amount' | translate}}</h3>\r\n\t\t<h2>$140.00</h2>\r\n\t</div>\r\n\t<ion-button size=\"large\" shape=\"block\" class=\"btn end\" (click)=\"continue()\">{{'continue' | translate}}</ion-button>\r\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/select-task/select-task-routing.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/select-task/select-task-routing.module.ts ***!
    \***********************************************************/

  /*! exports provided: SelectTaskPageRoutingModule */

  /***/
  function srcAppSelectTaskSelectTaskRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SelectTaskPageRoutingModule", function () {
      return SelectTaskPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _select_task_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./select-task.page */
    "./src/app/select-task/select-task.page.ts");

    const routes = [{
      path: '',
      component: _select_task_page__WEBPACK_IMPORTED_MODULE_3__["SelectTaskPage"]
    }];
    let SelectTaskPageRoutingModule = class SelectTaskPageRoutingModule {};
    SelectTaskPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], SelectTaskPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/select-task/select-task.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/select-task/select-task.module.ts ***!
    \***************************************************/

  /*! exports provided: SelectTaskPageModule */

  /***/
  function srcAppSelectTaskSelectTaskModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SelectTaskPageModule", function () {
      return SelectTaskPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _select_task_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./select-task-routing.module */
    "./src/app/select-task/select-task-routing.module.ts");
    /* harmony import */


    var _select_task_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./select-task.page */
    "./src/app/select-task/select-task.page.ts");

    let SelectTaskPageModule = class SelectTaskPageModule {};
    SelectTaskPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"], _select_task_routing_module__WEBPACK_IMPORTED_MODULE_6__["SelectTaskPageRoutingModule"]],
      declarations: [_select_task_page__WEBPACK_IMPORTED_MODULE_7__["SelectTaskPage"]]
    })], SelectTaskPageModule);
    /***/
  },

  /***/
  "./src/app/select-task/select-task.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/select-task/select-task.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSelectTaskSelectTaskPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-header ion-toolbar ion-title {\n  position: absolute !important;\n  top: 0;\n  left: 0;\n  width: 100%;\n  padding: 0 15px !important;\n  text-align: center;\n}\nion-header ion-toolbar ion-title ion-icon {\n  color: var(--text-light2);\n  margin: 0 5px;\n  font-size: 0.85rem;\n}\nion-header ion-toolbar ion-title ion-icon.active {\n  color: var(--primary);\n}\nion-header h1 {\n  font-size: 1.3rem;\n  padding: 0 20px;\n  padding-top: 17px;\n}\nion-list {\n  background: var(--transparent) !important;\n  margin: 0;\n  padding: 0;\n  width: calc(100% - 40px);\n  margin: 0 auto;\n  padding-top: 26px;\n}\nion-list ion-item {\n  padding: 0px 19px;\n  border-radius: 4px;\n  background: var(--white);\n  --inner-padding-end: 0px;\n  --inner-min-height: unset !important;\n  --padding-start: 0;\n  --highligh-color-focused: var(--transparent) !important;\n  --background: var(--transparent);\n  background: var(--input_filed_bg);\n  margin-bottom: 10px;\n  --min-height: 54px;\n}\nion-list ion-item ion-label {\n  position: relative;\n  margin: 0;\n  color: var(--text-light);\n  font-size: 1.12rem !important;\n  font-weight: 400 !important;\n}\nion-list ion-item ion-radio {\n  position: absolute;\n  z-index: 999;\n  width: 100%;\n  height: 100%;\n  margin: 0;\n  --color: var(--transparent) !important;\n  --color-checked: var(--transparent) !important;\n}\nion-list ion-item::before {\n  content: \"\";\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  width: 0;\n  height: 0;\n  margin: auto;\n  background: var(--primary);\n  -webkit-transition: all 0.3s;\n  transition: all 0.3s;\n  border-radius: 4px;\n}\nion-list ion-item.item-radio-checked {\n  border-color: var(--primary) !important;\n}\nion-list ion-item.item-radio-checked::before {\n  width: 100%;\n  height: 100%;\n}\nion-list ion-item.item-radio-checked ion-label {\n  color: var(--white);\n}\nion-footer {\n  background: var(--white);\n  padding: 10px 20px 20px 20px;\n}\nion-footer .amount {\n  min-width: 50%;\n  max-width: 50%;\n}\nion-footer .amount h3 {\n  margin: 0;\n  color: var(--text-light);\n  font-weight: 400;\n  font-size: 0.85rem;\n  padding-bottom: 5px;\n}\nion-footer .amount h2 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 600;\n  font-size: 1.2rem;\n}\nion-footer .button.btn {\n  min-width: 50%;\n  max-width: 50%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VsZWN0LXRhc2svRTpcXEVcXHNlcnZpY2VcXGZpcmViYXNlIDA0XFxwcm92aWRlci1hcHAtYnVkZGhpbmkvc3JjXFxhcHBcXHNlbGVjdC10YXNrXFxzZWxlY3QtdGFzay5wYWdlLnNjc3MiLCJzcmMvYXBwL3NlbGVjdC10YXNrL3NlbGVjdC10YXNrLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQztFQUNDLDZCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7RUFDQSxrQkFBQTtBQ0FGO0FERUU7RUFDQyx5QkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtBQ0FIO0FERUc7RUFDQyxxQkFBQTtBQ0FKO0FES0M7RUFDQyxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0hGO0FET0E7RUFDQyx5Q0FBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0Esd0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUNKRDtBRE1DO0VBQ0MsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHdCQUFBO0VBQ0Esd0JBQUE7RUFDQSxvQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsdURBQUE7RUFDQSxnQ0FBQTtFQUNBLGlDQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ0pGO0FET0U7RUFDQyxrQkFBQTtFQUNBLFNBQUE7RUFDQSx3QkFBQTtFQUNBLDZCQUFBO0VBQ0EsMkJBQUE7QUNMSDtBRFFFO0VBQ0Msa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0VBQ0Esc0NBQUE7RUFDQSw4Q0FBQTtBQ05IO0FEU0U7RUFDQyxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSw0QkFBQTtFQUFBLG9CQUFBO0VBQ0Esa0JBQUE7QUNQSDtBRFVFO0VBQ0MsdUNBQUE7QUNSSDtBRFVHO0VBQ0MsV0FBQTtFQUNBLFlBQUE7QUNSSjtBRFdHO0VBQ0MsbUJBQUE7QUNUSjtBRGdCQTtFQUNDLHdCQUFBO0VBQ0EsNEJBQUE7QUNiRDtBRGVDO0VBQ0MsY0FBQTtFQUNBLGNBQUE7QUNiRjtBRGVFO0VBQ0MsU0FBQTtFQUNBLHdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDYkg7QURnQkU7RUFDQyxTQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FDZEg7QURrQkM7RUFDQyxjQUFBO0VBQ0EsY0FBQTtBQ2hCRiIsImZpbGUiOiJzcmMvYXBwL3NlbGVjdC10YXNrL3NlbGVjdC10YXNrLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXIge1xyXG5cdGlvbi10b29sYmFyIGlvbi10aXRsZSB7XHJcblx0XHRwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcclxuXHRcdHRvcDogMDtcclxuXHRcdGxlZnQ6IDA7XHJcblx0XHR3aWR0aDogMTAwJTtcclxuXHRcdHBhZGRpbmc6IDAgMTVweCAhaW1wb3J0YW50O1xyXG5cdFx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuXHRcdGlvbi1pY29uIHtcclxuXHRcdFx0Y29sb3I6IHZhcigtLXRleHQtbGlnaHQyKTtcclxuXHRcdFx0bWFyZ2luOiAwIDVweDtcclxuXHRcdFx0Zm9udC1zaXplOiAwLjg1cmVtO1xyXG5cclxuXHRcdFx0Ji5hY3RpdmUge1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS1wcmltYXJ5KTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0aDEge1xyXG5cdFx0Zm9udC1zaXplOiAxLjNyZW07IFxyXG5cdFx0cGFkZGluZzogMCAyMHB4O1xyXG5cdFx0cGFkZGluZy10b3A6IDE3cHg7XHJcblx0fVxyXG59XHJcblxyXG5pb24tbGlzdCB7XHJcblx0YmFja2dyb3VuZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XHJcblx0bWFyZ2luOiAwO1xyXG5cdHBhZGRpbmc6IDA7XHJcblx0d2lkdGg6IGNhbGMoMTAwJSAtIDQwcHgpO1xyXG5cdG1hcmdpbjogMCBhdXRvO1xyXG5cdHBhZGRpbmctdG9wOiAyNnB4O1xyXG5cclxuXHRpb24taXRlbSB7XHJcblx0XHRwYWRkaW5nOiAwcHggMTlweDtcclxuXHRcdGJvcmRlci1yYWRpdXM6IDRweDtcclxuXHRcdGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcclxuXHRcdC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcclxuXHRcdC0taW5uZXItbWluLWhlaWdodDogdW5zZXQgIWltcG9ydGFudDtcclxuXHRcdC0tcGFkZGluZy1zdGFydDogMDtcclxuXHRcdC0taGlnaGxpZ2gtY29sb3ItZm9jdXNlZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XHJcblx0XHQtLWJhY2tncm91bmQ6IHZhcigtLXRyYW5zcGFyZW50KTtcclxuXHRcdGJhY2tncm91bmQ6IHZhcigtLWlucHV0X2ZpbGVkX2JnKTtcclxuXHRcdG1hcmdpbi1ib3R0b206IDEwcHg7XHJcblx0XHQtLW1pbi1oZWlnaHQ6IDU0cHg7XHJcblxyXG5cclxuXHRcdGlvbi1sYWJlbCB7XHJcblx0XHRcdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XHJcblx0XHRcdGZvbnQtc2l6ZTogMS4xMnJlbSAhaW1wb3J0YW50O1xyXG5cdFx0XHRmb250LXdlaWdodDogNDAwICFpbXBvcnRhbnQ7XHJcblx0XHR9XHJcblxyXG5cdFx0aW9uLXJhZGlvIHtcclxuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHR6LWluZGV4OiA5OTk7XHJcblx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRoZWlnaHQ6IDEwMCU7XHJcblx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0LS1jb2xvcjogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XHJcblx0XHRcdC0tY29sb3ItY2hlY2tlZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XHJcblx0XHR9XHJcblxyXG5cdFx0Jjo6YmVmb3JlIHtcclxuXHRcdFx0Y29udGVudDogJyc7XHJcblx0XHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdFx0dG9wOiAwO1xyXG5cdFx0XHRsZWZ0OiAwO1xyXG5cdFx0XHRib3R0b206IDA7XHJcblx0XHRcdHJpZ2h0OiAwO1xyXG5cdFx0XHR3aWR0aDogMDtcclxuXHRcdFx0aGVpZ2h0OiAwO1xyXG5cdFx0XHRtYXJnaW46IGF1dG87XHJcblx0XHRcdGJhY2tncm91bmQ6IHZhcigtLXByaW1hcnkpO1xyXG5cdFx0XHR0cmFuc2l0aW9uOiBhbGwgLjNzO1xyXG5cdFx0XHRib3JkZXItcmFkaXVzOiA0cHg7XHJcblx0XHR9XHJcblxyXG5cdFx0Ji5pdGVtLXJhZGlvLWNoZWNrZWQge1xyXG5cdFx0XHRib3JkZXItY29sb3I6IHZhcigtLXByaW1hcnkpICFpbXBvcnRhbnQ7XHJcblxyXG5cdFx0XHQmOjpiZWZvcmUge1xyXG5cdFx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRcdGhlaWdodDogMTAwJTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aW9uLWxhYmVsIHtcclxuXHRcdFx0XHRjb2xvcjogdmFyKC0td2hpdGUpO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuaW9uLWZvb3RlciB7XHJcblx0YmFja2dyb3VuZDogdmFyKC0td2hpdGUpO1xyXG5cdHBhZGRpbmc6IDEwcHggMjBweCAyMHB4IDIwcHg7XHJcblxyXG5cdC5hbW91bnQge1xyXG5cdFx0bWluLXdpZHRoOiA1MCU7XHJcblx0XHRtYXgtd2lkdGg6IDUwJTtcclxuXHJcblx0XHRoMyB7XHJcblx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0Y29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xyXG5cdFx0XHRmb250LXdlaWdodDogNDAwO1xyXG5cdFx0XHRmb250LXNpemU6IC44NXJlbTtcclxuXHRcdFx0cGFkZGluZy1ib3R0b206IDVweDtcclxuXHRcdH1cclxuXHJcblx0XHRoMiB7XHJcblx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0Y29sb3I6IHZhcigtLXRleHQtZGFyayk7XHJcblx0XHRcdGZvbnQtd2VpZ2h0OiA2MDA7XHJcblx0XHRcdGZvbnQtc2l6ZTogMS4ycmVtO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0LmJ1dHRvbi5idG4ge1xyXG5cdFx0bWluLXdpZHRoOiA1MCU7XHJcblx0XHRtYXgtd2lkdGg6IDUwJTtcclxuXHR9XHJcbn0iLCJpb24taGVhZGVyIGlvbi10b29sYmFyIGlvbi10aXRsZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAwIDE1cHggIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuaW9uLWhlYWRlciBpb24tdG9vbGJhciBpb24tdGl0bGUgaW9uLWljb24ge1xuICBjb2xvcjogdmFyKC0tdGV4dC1saWdodDIpO1xuICBtYXJnaW46IDAgNXB4O1xuICBmb250LXNpemU6IDAuODVyZW07XG59XG5pb24taGVhZGVyIGlvbi10b29sYmFyIGlvbi10aXRsZSBpb24taWNvbi5hY3RpdmUge1xuICBjb2xvcjogdmFyKC0tcHJpbWFyeSk7XG59XG5pb24taGVhZGVyIGgxIHtcbiAgZm9udC1zaXplOiAxLjNyZW07XG4gIHBhZGRpbmc6IDAgMjBweDtcbiAgcGFkZGluZy10b3A6IDE3cHg7XG59XG5cbmlvbi1saXN0IHtcbiAgYmFja2dyb3VuZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDQwcHgpO1xuICBtYXJnaW46IDAgYXV0bztcbiAgcGFkZGluZy10b3A6IDI2cHg7XG59XG5pb24tbGlzdCBpb24taXRlbSB7XG4gIHBhZGRpbmc6IDBweCAxOXB4O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcbiAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xuICAtLWlubmVyLW1pbi1oZWlnaHQ6IHVuc2V0ICFpbXBvcnRhbnQ7XG4gIC0tcGFkZGluZy1zdGFydDogMDtcbiAgLS1oaWdobGlnaC1jb2xvci1mb2N1c2VkOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCk7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlucHV0X2ZpbGVkX2JnKTtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgLS1taW4taGVpZ2h0OiA1NHB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gaW9uLWxhYmVsIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcbiAgZm9udC1zaXplOiAxLjEycmVtICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIGlvbi1yYWRpbyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogOTk5O1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBtYXJnaW46IDA7XG4gIC0tY29sb3I6IHZhcigtLXRyYW5zcGFyZW50KSAhaW1wb3J0YW50O1xuICAtLWNvbG9yLWNoZWNrZWQ6IHZhcigtLXRyYW5zcGFyZW50KSAhaW1wb3J0YW50O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW06OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiAwO1xuICB3aWR0aDogMDtcbiAgaGVpZ2h0OiAwO1xuICBtYXJnaW46IGF1dG87XG4gIGJhY2tncm91bmQ6IHZhcigtLXByaW1hcnkpO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcztcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0uaXRlbS1yYWRpby1jaGVja2VkIHtcbiAgYm9yZGVyLWNvbG9yOiB2YXIoLS1wcmltYXJ5KSAhaW1wb3J0YW50O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0uaXRlbS1yYWRpby1jaGVja2VkOjpiZWZvcmUge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0uaXRlbS1yYWRpby1jaGVja2VkIGlvbi1sYWJlbCB7XG4gIGNvbG9yOiB2YXIoLS13aGl0ZSk7XG59XG5cbmlvbi1mb290ZXIge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XG4gIHBhZGRpbmc6IDEwcHggMjBweCAyMHB4IDIwcHg7XG59XG5pb24tZm9vdGVyIC5hbW91bnQge1xuICBtaW4td2lkdGg6IDUwJTtcbiAgbWF4LXdpZHRoOiA1MCU7XG59XG5pb24tZm9vdGVyIC5hbW91bnQgaDMge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgZm9udC1zaXplOiAwLjg1cmVtO1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuaW9uLWZvb3RlciAuYW1vdW50IGgyIHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAxLjJyZW07XG59XG5pb24tZm9vdGVyIC5idXR0b24uYnRuIHtcbiAgbWluLXdpZHRoOiA1MCU7XG4gIG1heC13aWR0aDogNTAlO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/select-task/select-task.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/select-task/select-task.page.ts ***!
    \*************************************************/

  /*! exports provided: SelectTaskPage */

  /***/
  function srcAppSelectTaskSelectTaskPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SelectTaskPage", function () {
      return SelectTaskPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    let SelectTaskPage = class SelectTaskPage {
      constructor(route) {
        this.route = route;
      }

      ngOnInit() {}

      continue() {
        this.route.navigate(['./select-date-time']);
      }

    };

    SelectTaskPage.ctorParameters = () => [{
      type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
    }];

    SelectTaskPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-select-task',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./select-task.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/select-task/select-task.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./select-task.page.scss */
      "./src/app/select-task/select-task.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])], SelectTaskPage);
    /***/
  }
}]);
//# sourceMappingURL=select-task-select-task-module-es5.js.map