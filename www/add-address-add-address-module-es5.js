(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-address-add-address-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/add-address/add-address.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/add-address/add-address.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAddAddressAddAddressPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n\t<ion-toolbar>\r\n\t\t<ion-buttons slot=\"start\">\r\n\t\t\t<ion-back-button text=\"\" icon=\"chevron-back-outline\"></ion-back-button>\r\n\t\t</ion-buttons>\r\n\t\t<ion-title>\r\n\t\t\t{{\"cancel\" | translate}}\r\n\r\n\t\t\t<ion-button class=\"ion-float-end cart_icon btn\" (click)=\"addressAdding()\">\r\n\t\t\t\t{{\"continue\" | translate}}\r\n\t\t\t</ion-button>\r\n\t\t</ion-title>\r\n\t</ion-toolbar>\r\n\r\n\t<div class=\"search_box d-flex\">\r\n\t\t<ion-icon class=\"zmdi zmdi-search ion-text-start\"></ion-icon>\r\n\t\t<ion-searchbar class=\"ion-no-padding\" (ionChange)=\"searchAddress($event)\" searchIcon=\"hide\" placeholder=\"{{'search_location' | translate}}\"></ion-searchbar>\r\n\t\t<ion-icon (click)=\"getLocation()\" class=\"zmdi zmdi-gps-dot ion-text-end\"></ion-icon>\r\n\t</div>\r\n</ion-header>\r\n\r\n<ion-content class=\"bg_color\">\r\n\t\r\n\t<!-- <div class=\"map\" (click)=\"title()\">\r\n\t\t<img src=\"assets/images/map.png\">\r\n\t\t<ion-icon class=\"zmdi zmdi-pin\" style=\"top:38%; right: 50%;\"></ion-icon>\r\n\t</div> -->\r\n\r\n\t<div #map id=\"map\"></div>\r\n\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/add-address/add-address-routing.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/add-address/add-address-routing.module.ts ***!
    \***********************************************************/

  /*! exports provided: AddAddressPageRoutingModule */

  /***/
  function srcAppAddAddressAddAddressRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddAddressPageRoutingModule", function () {
      return AddAddressPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _add_address_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./add-address.page */
    "./src/app/add-address/add-address.page.ts");

    const routes = [{
      path: '',
      component: _add_address_page__WEBPACK_IMPORTED_MODULE_3__["AddAddressPage"]
    }];
    let AddAddressPageRoutingModule = class AddAddressPageRoutingModule {};
    AddAddressPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AddAddressPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/add-address/add-address.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/add-address/add-address.module.ts ***!
    \***************************************************/

  /*! exports provided: AddAddressPageModule */

  /***/
  function srcAppAddAddressAddAddressModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddAddressPageModule", function () {
      return AddAddressPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _add_address_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./add-address-routing.module */
    "./src/app/add-address/add-address-routing.module.ts");
    /* harmony import */


    var _add_address_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./add-address.page */
    "./src/app/add-address/add-address.page.ts");

    let AddAddressPageModule = class AddAddressPageModule {};
    AddAddressPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"], _add_address_routing_module__WEBPACK_IMPORTED_MODULE_6__["AddAddressPageRoutingModule"]],
      declarations: [_add_address_page__WEBPACK_IMPORTED_MODULE_7__["AddAddressPage"]]
    })], AddAddressPageModule);
    /***/
  },

  /***/
  "./src/app/add-address/add-address.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/add-address/add-address.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAddAddressAddAddressPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-header ion-toolbar ion-title ion-button.btn {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  height: 55px;\n  font-size: 1.1rem;\n  font-weight: 400;\n  --background: var(--transparent);\n  --padding-end: 0;\n  --padding-start: 0;\n  padding: 0 0px;\n  --color: var(--primary) !important;\n  font-weight: 700;\n  letter-spacing: 0 !important;\n  font-size: 1.5rem;\n}\nion-header .search_box {\n  background: var(--input_filed_bg);\n  width: calc(100% - 0px);\n  border-radius: 3px;\n  margin: 0 auto;\n  padding: 0 15px;\n  position: relative;\n  overflow: hidden;\n  min-height: 55px;\n  z-index: 99;\n}\nion-header .search_box ion-icon {\n  color: var(--text-dark2);\n  font-size: 1.4rem;\n  min-width: 35px;\n  height: 35px;\n  line-height: 35px;\n  z-index: 99;\n}\nion-header .search_box ion-icon.zmdi-gps-dot {\n  font-size: 1.5rem;\n}\nion-header .search_box ion-searchbar {\n  --background: var(--transparent) !important;\n  --color: var(--text-light);\n  --placeholder-opacity: 1;\n  --placeholder-font-weight: 400 !important;\n  --box-shadow: none !important;\n  font-size: 1.4rem;\n}\n#map {\n  height: 500px;\n}\n.map {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  overflow: hidden;\n}\n.map img {\n  width: 100%;\n  height: 100%;\n}\n.map ion-icon {\n  position: absolute;\n  z-index: 99;\n  color: var(--primary);\n  font-size: 2.5rem;\n  width: 24px;\n  overflow: hidden;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRkLWFkZHJlc3MvRTpcXEVcXHNlcnZpY2VcXGZpcmViYXNlIDA0XFxwcm92aWRlci1hcHAtYnVkZGhpbmkvc3JjXFxhcHBcXGFkZC1hZGRyZXNzXFxhZGQtYWRkcmVzcy5wYWdlLnNjc3MiLCJzcmMvYXBwL2FkZC1hZGRyZXNzL2FkZC1hZGRyZXNzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFRTtFQUNDLDBCQUFBO0VBQUEsdUJBQUE7RUFBQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGtDQUFBO0VBQ0EsZ0JBQUE7RUFDQSw0QkFBQTtFQUNBLGlCQUFBO0FDREg7QURLQztFQUNDLGlDQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQ0hGO0FES0U7RUFDQyx3QkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7QUNISDtBREtHO0VBQ0MsaUJBQUE7QUNISjtBRE9FO0VBQ0MsMkNBQUE7RUFDQSwwQkFBQTtFQUNBLHdCQUFBO0VBQ0EseUNBQUE7RUFDQSw2QkFBQTtFQUNBLGlCQUFBO0FDTEg7QURTQTtFQUNDLGFBQUE7QUNORDtBRFFBO0VBQ0Msa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNMRDtBRE9DO0VBQ0MsV0FBQTtFQUNBLFlBQUE7QUNMRjtBRFFDO0VBQ0Msa0JBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ05GIiwiZmlsZSI6InNyYy9hcHAvYWRkLWFkZHJlc3MvYWRkLWFkZHJlc3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlciB7XHJcblx0aW9uLXRvb2xiYXIgaW9uLXRpdGxlIHtcclxuXHRcdGlvbi1idXR0b24uYnRuIHtcclxuXHRcdFx0d2lkdGg6IGZpdC1jb250ZW50O1xyXG5cdFx0XHRoZWlnaHQ6IDU1cHg7XHJcblx0XHRcdGZvbnQtc2l6ZTogMS4xcmVtO1xyXG5cdFx0XHRmb250LXdlaWdodDogNDAwO1xyXG5cdFx0XHQtLWJhY2tncm91bmQ6IHZhcigtLXRyYW5zcGFyZW50KTtcclxuXHRcdFx0LS1wYWRkaW5nLWVuZDogMDtcclxuXHRcdFx0LS1wYWRkaW5nLXN0YXJ0OiAwO1xyXG5cdFx0XHRwYWRkaW5nOiAwIDBweDtcclxuXHRcdFx0LS1jb2xvcjogdmFyKC0tcHJpbWFyeSkgIWltcG9ydGFudDtcclxuXHRcdFx0Zm9udC13ZWlnaHQ6IDcwMDtcclxuXHRcdFx0bGV0dGVyLXNwYWNpbmc6IDAgIWltcG9ydGFudDtcclxuXHRcdFx0Zm9udC1zaXplOiAxLjVyZW07XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQuc2VhcmNoX2JveCB7XHJcblx0XHRiYWNrZ3JvdW5kOiB2YXIoLS1pbnB1dF9maWxlZF9iZyk7XHJcblx0XHR3aWR0aDogY2FsYygxMDAlIC0gMHB4KTtcclxuXHRcdGJvcmRlci1yYWRpdXM6IDNweDtcclxuXHRcdG1hcmdpbjogMCBhdXRvO1xyXG5cdFx0cGFkZGluZzogMCAxNXB4O1xyXG5cdFx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdFx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHRcdG1pbi1oZWlnaHQ6IDU1cHg7XHJcblx0XHR6LWluZGV4OiA5OTtcclxuXHJcblx0XHRpb24taWNvbiB7XHJcblx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmsyKTtcclxuXHRcdFx0Zm9udC1zaXplOiAxLjRyZW07XHJcblx0XHRcdG1pbi13aWR0aDogMzVweDtcclxuXHRcdFx0aGVpZ2h0OiAzNXB4O1xyXG5cdFx0XHRsaW5lLWhlaWdodDogMzVweDtcclxuXHRcdFx0ei1pbmRleDogOTk7XHJcblxyXG5cdFx0XHQmLnptZGktZ3BzLWRvdCB7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxLjVyZW07IFxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0aW9uLXNlYXJjaGJhciB7XHJcblx0XHRcdC0tYmFja2dyb3VuZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XHJcblx0XHRcdC0tY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xyXG5cdFx0XHQtLXBsYWNlaG9sZGVyLW9wYWNpdHk6IDE7XHJcblx0XHRcdC0tcGxhY2Vob2xkZXItZm9udC13ZWlnaHQ6IDQwMCAhaW1wb3J0YW50O1xyXG5cdFx0XHQtLWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuXHRcdFx0Zm9udC1zaXplOiAxLjRyZW07XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiNtYXB7XHJcblx0aGVpZ2h0OiA1MDBweDtcclxufVxyXG4ubWFwIHtcclxuXHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0dG9wOiAwO1xyXG5cdGxlZnQ6IDA7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0aGVpZ2h0OiAxMDAlO1xyXG5cdG92ZXJmbG93OiBoaWRkZW47XHJcblxyXG5cdGltZyB7XHJcblx0XHR3aWR0aDogMTAwJTtcclxuXHRcdGhlaWdodDogMTAwJTtcclxuXHR9XHJcblxyXG5cdGlvbi1pY29uIHtcclxuXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdHotaW5kZXg6IDk5O1xyXG5cdFx0Y29sb3I6IHZhcigtLXByaW1hcnkpO1xyXG5cdFx0Zm9udC1zaXplOiAyLjVyZW07XHJcblx0XHR3aWR0aDogMjRweDtcclxuXHRcdG92ZXJmbG93OiBoaWRkZW47XHJcblx0fVxyXG59IiwiaW9uLWhlYWRlciBpb24tdG9vbGJhciBpb24tdGl0bGUgaW9uLWJ1dHRvbi5idG4ge1xuICB3aWR0aDogZml0LWNvbnRlbnQ7XG4gIGhlaWdodDogNTVweDtcbiAgZm9udC1zaXplOiAxLjFyZW07XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tdHJhbnNwYXJlbnQpO1xuICAtLXBhZGRpbmctZW5kOiAwO1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIHBhZGRpbmc6IDAgMHB4O1xuICAtLWNvbG9yOiB2YXIoLS1wcmltYXJ5KSAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogNzAwO1xuICBsZXR0ZXItc3BhY2luZzogMCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDEuNXJlbTtcbn1cbmlvbi1oZWFkZXIgLnNlYXJjaF9ib3gge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pbnB1dF9maWxlZF9iZyk7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSAwcHgpO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBwYWRkaW5nOiAwIDE1cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgbWluLWhlaWdodDogNTVweDtcbiAgei1pbmRleDogOTk7XG59XG5pb24taGVhZGVyIC5zZWFyY2hfYm94IGlvbi1pY29uIHtcbiAgY29sb3I6IHZhcigtLXRleHQtZGFyazIpO1xuICBmb250LXNpemU6IDEuNHJlbTtcbiAgbWluLXdpZHRoOiAzNXB4O1xuICBoZWlnaHQ6IDM1cHg7XG4gIGxpbmUtaGVpZ2h0OiAzNXB4O1xuICB6LWluZGV4OiA5OTtcbn1cbmlvbi1oZWFkZXIgLnNlYXJjaF9ib3ggaW9uLWljb24uem1kaS1ncHMtZG90IHtcbiAgZm9udC1zaXplOiAxLjVyZW07XG59XG5pb24taGVhZGVyIC5zZWFyY2hfYm94IGlvbi1zZWFyY2hiYXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLXRyYW5zcGFyZW50KSAhaW1wb3J0YW50O1xuICAtLWNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcbiAgLS1wbGFjZWhvbGRlci1vcGFjaXR5OiAxO1xuICAtLXBsYWNlaG9sZGVyLWZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcbiAgLS1ib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMS40cmVtO1xufVxuXG4jbWFwIHtcbiAgaGVpZ2h0OiA1MDBweDtcbn1cblxuLm1hcCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLm1hcCBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLm1hcCBpb24taWNvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogOTk7XG4gIGNvbG9yOiB2YXIoLS1wcmltYXJ5KTtcbiAgZm9udC1zaXplOiAyLjVyZW07XG4gIHdpZHRoOiAyNHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/add-address/add-address.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/add-address/add-address.page.ts ***!
    \*************************************************/

  /*! exports provided: AddAddressPage */

  /***/
  function srcAppAddAddressAddAddressPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AddAddressPage", function () {
      return AddAddressPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _title_title_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../title/title.page */
    "./src/app/title/title.page.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic-native/native-geocoder/ngx */
    "./node_modules/@ionic-native/native-geocoder/ngx/index.js");
    /* harmony import */


    var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic-native/android-permissions/ngx */
    "./node_modules/@ionic-native/android-permissions/ngx/index.js");
    /* harmony import */


    var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/ngx/index.js");
    /* harmony import */


    var _ionic_native_diagnostic_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic-native/diagnostic/ngx */
    "./node_modules/@ionic-native/diagnostic/ngx/index.js");

    let AddAddressPage = class AddAddressPage {
      constructor(modalController, router, geolocation, nativeGeocoder, zone, platform, loadingController, androidPermissions, diagnostic) {
        this.modalController = modalController;
        this.router = router;
        this.geolocation = geolocation;
        this.nativeGeocoder = nativeGeocoder;
        this.zone = zone;
        this.platform = platform;
        this.loadingController = loadingController;
        this.androidPermissions = androidPermissions;
        this.diagnostic = diagnostic;
        this.house = "";
        this.landmark = "";
        this.title = "home";
      }

      ngOnInit() {
        this.getLocation();
      }

      addressAdding() {
        this.modalController.create({
          component: _title_title_page__WEBPACK_IMPORTED_MODULE_2__["TitlePage"]
        }).then(modalElement => {
          modalElement.present();
        });
      }

      searchAddress(event) {
        let options = {
          useLocale: true,
          maxResults: 5
        };
        this.nativeGeocoder.forwardGeocode(event.target.value, options).then(result => {
          console.log("The coordinates are latitude=" + result[0].latitude + " and longitude=" + result[0].longitude);
          let location = new google.maps.LatLng(result[0].latitude, result[0].longitude);
          this.map.setCenter(new google.maps.LatLng(result[0].latitude, result[0].longitude));
          this.addMarker(location);
        }).catch(error => console.log(error));
      }

      getLocation() {
        this.platform.ready().then(() => {
          if (this.platform.is("android")) {
            this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(result => console.log("Has permission?", result.hasPermission), err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION));
            this.grantRequest();
          } else if (this.platform.is("ios")) {
            this.grantRequest();
          } else {
            this.geolocation.getCurrentPosition({
              maximumAge: 3000,
              timeout: 10000,
              enableHighAccuracy: false
            }).then(resp => {
              if (resp) {
                console.log("resp", resp);
                this.lat = resp.coords.latitude;
                this.lng = resp.coords.longitude;
                if (!this.map) this.loadmap(resp.coords.latitude, resp.coords.longitude, this.mapEle);
                this.getAddress(this.lat, this.lng);
              }
            });
          }
        });
      }

      grantRequest() {
        this.diagnostic.isLocationEnabled().then(data => {
          if (data) {
            this.geolocation.getCurrentPosition({
              maximumAge: 3000,
              timeout: 10000,
              enableHighAccuracy: false
            }).then(resp => {
              if (resp) {
                console.log("resp", resp);
                if (!this.map) this.loadmap(resp.coords.latitude, resp.coords.longitude, this.mapEle);
                const location = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
                this.map.setCenter(new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude));
                this.addMarker(location);
                this.getAddress(resp.coords.latitude, resp.coords.longitude);
              }
            });
          } else {
            this.diagnostic.switchToLocationSettings();
            this.geolocation.getCurrentPosition({
              maximumAge: 3000,
              timeout: 10000,
              enableHighAccuracy: false
            }).then(resp => {
              if (resp) {
                console.log("ress,", resp);
                if (!this.map) this.loadmap(resp.coords.latitude, resp.coords.longitude, this.mapEle);
                this.getAddress(resp.coords.latitude, resp.coords.longitude);
              }
            });
          }
        }, error => {// console.log('errir', error);
        }).catch(error => {
          if (!this.map) this.loadmap(0, 0, this.mapEle); // console.log('error', error);
        });
      }

      loadmap(lat, lng, mapElement) {
        const location = new google.maps.LatLng(lat, lng);
        const style = [{
          featureType: "all",
          elementType: "all",
          stylers: [{
            saturation: -100
          }]
        }];
        const mapOptions = {
          zoom: 15,
          scaleControl: false,
          streetViewControl: false,
          zoomControl: false,
          overviewMapControl: false,
          center: location,
          mapTypeControl: false,
          mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, "fire5"]
          }
        };
        this.map = new google.maps.Map(mapElement.nativeElement, mapOptions);
        var mapType = new google.maps.StyledMapType(style, {
          name: "Grayscale"
        });
        this.map.mapTypes.set("fire5", mapType);
        this.map.setMapTypeId("fire5");
        this.map.setCenter(new google.maps.LatLng(lat, lng));
        this.addMarker(location);
      }

      getAddress(lat, lng) {
        const geocoder = new google.maps.Geocoder();
        const location = new google.maps.LatLng(lat, lng);
        geocoder.geocode({
          location: location
        }, (results, status) => {
          console.log(results);
          this.address = results[0].formatted_address;
          this.lat = lat;
          this.lng = lng;
        });
      }

      addMarker(location) {
        if (this.marker) this.marker.setMap(null);
        console.log("location =>", location);
        const icon = {
          url: "assets/images/pin.jpg",
          scaledSize: new google.maps.Size(50, 50)
        };
        this.marker = new google.maps.Marker({
          position: location,
          map: this.map,
          icon: icon,
          draggable: true,
          animation: google.maps.Animation.DROP
        });
        google.maps.event.addListener(this.marker, "addfeature", () => {
          debugger;
          console.log(this.marker);
          this.getDragAddress(this.marker);
        });
        google.maps.event.addListener(this.marker, "dragend", () => {
          console.log(this.marker);
          this.getDragAddress(this.marker);
        });
      }

      getDragAddress(event) {
        const geocoder = new google.maps.Geocoder();
        const location = new google.maps.LatLng(event.position.lat(), event.position.lng());
        geocoder.geocode({
          location: location
        }, (results, status) => {
          console.log(results);
          this.address = results[0].formatted_address;
          this.lat = event.position.lat();
          this.lng = event.position.lng();
        });
      }

    };

    AddAddressPage.ctorParameters = () => [{
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
    }, {
      type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
    }, {
      type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_7__["Geolocation"]
    }, {
      type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__["NativeGeocoder"]
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
    }, {
      type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_6__["AndroidPermissions"]
    }, {
      type: _ionic_native_diagnostic_ngx__WEBPACK_IMPORTED_MODULE_8__["Diagnostic"]
    }];

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("map", {
      static: true
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], AddAddressPage.prototype, "mapEle", void 0);
    AddAddressPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: "app-add-address",
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./add-address.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/add-address/add-address.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./add-address.page.scss */
      "./src/app/add-address/add-address.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_7__["Geolocation"], _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_5__["NativeGeocoder"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_6__["AndroidPermissions"], _ionic_native_diagnostic_ngx__WEBPACK_IMPORTED_MODULE_8__["Diagnostic"]])], AddAddressPage);
    /***/
  }
}]);
//# sourceMappingURL=add-address-add-address-module-es5.js.map