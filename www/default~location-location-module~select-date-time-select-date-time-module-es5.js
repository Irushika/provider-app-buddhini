(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~location-location-module~select-date-time-select-date-time-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/location/location.page.html":
  /*!***********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/location/location.page.html ***!
    \***********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppLocationLocationPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<!--\r\n<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>location</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n-->\r\n\r\n<ion-content (click)=\"dismiss()\">\r\n\r\n</ion-content>\r\n<ion-footer class=\"ion-no-border\">\r\n\t<ion-list lines=\"none\">\r\n\t\t<div class=\"list_header\">\r\n\t\t\t<h2 class=\"d-flex\"><span (click)=\"dismiss()\">{{'cencel' | translate}}</span> <span class=\"end\" (click)=\"dismiss()\">{{'done' | translate}}</span></h2>\r\n\t\t</div>\r\n\t\t<ion-radio-group>\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>\r\n\t\t\t\t\t<h3>Home</h3>\r\n\t\t\t\t\t<p> 345, New York Business center,<br>Centerl Park, New York, USA </p>\r\n\t\t\t\t</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"2\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t \r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>\r\n\t\t\t\t\t<h3>Office</h3>\r\n\t\t\t\t\t<p>1124, Red fort Villas, Hemilton <br>Road, New York, USA </p>\r\n\t\t\t\t</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"3\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>\r\n\t\t\t\t\t<h3>Farm House</h3>\r\n\t\t\t\t\t<p> 345, New York Business center,<br>Centerl Park, New York, USA </p>\r\n\t\t\t\t</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"4\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t\t\r\n\t\t\t<ion-item>\r\n\t\t\t\t<ion-label>\r\n\t\t\t\t\t<h3>Other</h3>\r\n\t\t\t\t\t<p>1124, Red fort Villas, Hemilton <br>Road, New York, USA </p>\r\n\t\t\t\t</ion-label>\r\n\t\t\t\t<ion-radio slot=\"start\" value=\"5\"></ion-radio>\r\n\t\t\t</ion-item>\r\n\t\t</ion-radio-group>\r\n\t</ion-list>\r\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/location/location.page.scss":
  /*!*********************************************!*\
    !*** ./src/app/location/location.page.scss ***!
    \*********************************************/

  /*! exports provided: default */

  /***/
  function srcAppLocationLocationPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content {\n  --background: rgba(0, 0, 0, 0) !important;\n}\n\nion-footer {\n  background: var(--white);\n}\n\nion-footer ion-list {\n  background: var(--transparent) !important;\n  margin: 0;\n  padding: 0;\n}\n\nion-footer ion-list .list_header {\n  background: var(--input_filed_bg);\n  min-height: 54px;\n  height: unset;\n  padding: 0 25px;\n}\n\nion-footer ion-list .list_header h2 {\n  margin: 0;\n  min-height: 54px;\n  line-height: 54px;\n  text-transform: capitalize;\n  font-size: 1.35rem;\n  color: var(--text-dark);\n  font-weight: 700;\n}\n\nion-footer ion-list .list_header h2 span.end {\n  color: var(--primary);\n}\n\nion-footer ion-list ion-radio-group {\n  max-height: 260px;\n  overflow: hidden;\n  display: block;\n  overflow-y: scroll;\n}\n\nion-footer ion-list ion-item {\n  padding: 15px 18px;\n  border-radius: 4px;\n  background: var(--white);\n  --inner-padding-end: 0px;\n  --inner-min-height: unset !important;\n  --padding-start: 0;\n  --highligh-color-focused: var(--transparent) !important;\n  --background: var(--transparent);\n  background: var(--input_filed_bg);\n  --min-height: 54px;\n  width: calc(100% - 40px);\n  margin: 0 auto;\n  margin-bottom: 10px;\n}\n\nion-footer ion-list ion-item ion-label {\n  position: relative;\n  margin: 0;\n}\n\nion-footer ion-list ion-item ion-label h3 {\n  margin: 0;\n  text-transform: uppercase;\n  font-size: 1rem;\n  font-weight: 600;\n  letter-spacing: 1.5px;\n  padding-bottom: 10px;\n  color: var(--text-black);\n}\n\nion-footer ion-list ion-item ion-label p {\n  color: var(--text-light);\n  font-size: 1.12rem !important;\n  font-weight: 400 !important;\n  margin: 0;\n}\n\nion-footer ion-list ion-item ion-radio {\n  position: absolute;\n  z-index: 999;\n  width: 100%;\n  height: 100%;\n  margin: 0;\n  --color: var(--transparent) !important;\n  --color-checked: var(--transparent) !important;\n}\n\nion-footer ion-list ion-item::before {\n  content: \"\";\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  width: 0;\n  height: 0;\n  margin: auto;\n  background: var(--primary);\n  -webkit-transition: all 0.3s;\n  transition: all 0.3s;\n  border-radius: 4px;\n}\n\nion-footer ion-list ion-item.item-radio-checked {\n  border-color: var(--primary) !important;\n}\n\nion-footer ion-list ion-item.item-radio-checked::before {\n  width: 100%;\n  height: 100%;\n}\n\nion-footer ion-list ion-item.item-radio-checked ion-label h3 {\n  color: var(--white);\n  font-weight: 500;\n}\n\nion-footer ion-list ion-item.item-radio-checked ion-label p {\n  color: var(--white);\n}\n\nion-footer ion-list ion-item:first-child {\n  margin-top: 27px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9jYXRpb24vRTpcXEVcXHNlcnZpY2VcXGZpcmViYXNlIDA0XFxwcm92aWRlci1hcHAtYnVkZGhpbmkvc3JjXFxhcHBcXGxvY2F0aW9uXFxsb2NhdGlvbi5wYWdlLnNjc3MiLCJzcmMvYXBwL2xvY2F0aW9uL2xvY2F0aW9uLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLHlDQUFBO0FDQ0Q7O0FERUE7RUFDQyx3QkFBQTtBQ0NEOztBRENDO0VBQ0MseUNBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtBQ0NGOztBRENFO0VBQ0MsaUNBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0FDQ0g7O0FEQ0c7RUFDQyxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLDBCQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FERUs7RUFDQyxxQkFBQTtBQ0FOOztBRE1FO0VBQ0MsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQ0pIOztBRE9FO0VBQ0Msa0JBQUE7RUFDQSxrQkFBQTtFQUNBLHdCQUFBO0VBQ0Esd0JBQUE7RUFDQSxvQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsdURBQUE7RUFDQSxnQ0FBQTtFQUNBLGlDQUFBO0VBQ0Esa0JBQUE7RUFDQSx3QkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQ0xIOztBRFFHO0VBQ0Msa0JBQUE7RUFDQSxTQUFBO0FDTko7O0FEUUk7RUFDQyxTQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNBLG9CQUFBO0VBQ0Esd0JBQUE7QUNOTDs7QURTSTtFQUNDLHdCQUFBO0VBQ0EsNkJBQUE7RUFDQSwyQkFBQTtFQUNBLFNBQUE7QUNQTDs7QURXRztFQUNDLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLHNDQUFBO0VBQ0EsOENBQUE7QUNUSjs7QURZRztFQUNDLFdBQUE7RUFDQSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7RUFDQSwwQkFBQTtFQUNBLDRCQUFBO0VBQUEsb0JBQUE7RUFDQSxrQkFBQTtBQ1ZKOztBRGFHO0VBQ0MsdUNBQUE7QUNYSjs7QURhSTtFQUNDLFdBQUE7RUFDQSxZQUFBO0FDWEw7O0FEZUs7RUFDQyxtQkFBQTtFQUNBLGdCQUFBO0FDYk47O0FEZ0JLO0VBQ0MsbUJBQUE7QUNkTjs7QURvQkc7RUFDQyxnQkFBQTtBQ2xCSiIsImZpbGUiOiJzcmMvYXBwL2xvY2F0aW9uL2xvY2F0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuXHQtLWJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMCkgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWZvb3RlciB7XHJcblx0YmFja2dyb3VuZDogdmFyKC0td2hpdGUpO1xyXG5cclxuXHRpb24tbGlzdCB7XHJcblx0XHRiYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcclxuXHRcdG1hcmdpbjogMDtcclxuXHRcdHBhZGRpbmc6IDA7XHJcblxyXG5cdFx0Lmxpc3RfaGVhZGVyIHtcclxuXHRcdFx0YmFja2dyb3VuZDogdmFyKC0taW5wdXRfZmlsZWRfYmcpO1xyXG5cdFx0XHRtaW4taGVpZ2h0OiA1NHB4O1xyXG5cdFx0XHRoZWlnaHQ6IHVuc2V0O1xyXG5cdFx0XHRwYWRkaW5nOiAwIDI1cHg7XHJcblxyXG5cdFx0XHRoMiB7XHJcblx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdG1pbi1oZWlnaHQ6IDU0cHg7XHJcblx0XHRcdFx0bGluZS1oZWlnaHQ6IDU0cHg7XHJcblx0XHRcdFx0dGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxLjM1cmVtO1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xyXG5cdFx0XHRcdGZvbnQtd2VpZ2h0OiA3MDA7XHJcblxyXG5cdFx0XHRcdHNwYW4ge1xyXG5cdFx0XHRcdFx0Ji5lbmQge1xyXG5cdFx0XHRcdFx0XHRjb2xvcjogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0aW9uLXJhZGlvLWdyb3VwIHtcclxuXHRcdFx0bWF4LWhlaWdodDogMjYwcHg7XHJcblx0XHRcdG92ZXJmbG93OiBoaWRkZW47XHJcblx0XHRcdGRpc3BsYXk6IGJsb2NrO1xyXG5cdFx0XHRvdmVyZmxvdy15OiBzY3JvbGw7XHJcblx0XHR9XHJcblxyXG5cdFx0aW9uLWl0ZW0ge1xyXG5cdFx0XHRwYWRkaW5nOiAxNXB4IDE4cHg7XHJcblx0XHRcdGJvcmRlci1yYWRpdXM6IDRweDtcclxuXHRcdFx0YmFja2dyb3VuZDogdmFyKC0td2hpdGUpO1xyXG5cdFx0XHQtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XHJcblx0XHRcdC0taW5uZXItbWluLWhlaWdodDogdW5zZXQgIWltcG9ydGFudDtcclxuXHRcdFx0LS1wYWRkaW5nLXN0YXJ0OiAwO1xyXG5cdFx0XHQtLWhpZ2hsaWdoLWNvbG9yLWZvY3VzZWQ6IHZhcigtLXRyYW5zcGFyZW50KSAhaW1wb3J0YW50O1xyXG5cdFx0XHQtLWJhY2tncm91bmQ6IHZhcigtLXRyYW5zcGFyZW50KTtcclxuXHRcdFx0YmFja2dyb3VuZDogdmFyKC0taW5wdXRfZmlsZWRfYmcpO1xyXG5cdFx0XHQtLW1pbi1oZWlnaHQ6IDU0cHg7XHJcblx0XHRcdHdpZHRoOiBjYWxjKDEwMCUgLSA0MHB4KTtcclxuXHRcdFx0bWFyZ2luOiAwIGF1dG87XHJcblx0XHRcdG1hcmdpbi1ib3R0b206IDEwcHg7XHJcblxyXG5cclxuXHRcdFx0aW9uLWxhYmVsIHtcclxuXHRcdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cclxuXHRcdFx0XHRoMyB7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHR0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAxcmVtO1xyXG5cdFx0XHRcdFx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHRcdFx0XHRcdGxldHRlci1zcGFjaW5nOiAxLjVweDtcclxuXHRcdFx0XHRcdHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG5cdFx0XHRcdFx0Y29sb3I6IHZhcigtLXRleHQtYmxhY2spO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0cCB7XHJcblx0XHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XHJcblx0XHRcdFx0XHRmb250LXNpemU6IDEuMTJyZW0gIWltcG9ydGFudDtcclxuXHRcdFx0XHRcdGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcclxuXHRcdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlvbi1yYWRpbyB7XHJcblx0XHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHRcdHotaW5kZXg6IDk5OTtcclxuXHRcdFx0XHR3aWR0aDogMTAwJTtcclxuXHRcdFx0XHRoZWlnaHQ6IDEwMCU7XHJcblx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdC0tY29sb3I6IHZhcigtLXRyYW5zcGFyZW50KSAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdC0tY29sb3ItY2hlY2tlZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdCY6OmJlZm9yZSB7XHJcblx0XHRcdFx0Y29udGVudDogJyc7XHJcblx0XHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHRcdHRvcDogMDtcclxuXHRcdFx0XHRsZWZ0OiAwO1xyXG5cdFx0XHRcdGJvdHRvbTogMDtcclxuXHRcdFx0XHRyaWdodDogMDtcclxuXHRcdFx0XHR3aWR0aDogMDtcclxuXHRcdFx0XHRoZWlnaHQ6IDA7XHJcblx0XHRcdFx0bWFyZ2luOiBhdXRvO1xyXG5cdFx0XHRcdGJhY2tncm91bmQ6IHZhcigtLXByaW1hcnkpO1xyXG5cdFx0XHRcdHRyYW5zaXRpb246IGFsbCAuM3M7XHJcblx0XHRcdFx0Ym9yZGVyLXJhZGl1czogNHB4O1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQmLml0ZW0tcmFkaW8tY2hlY2tlZCB7XHJcblx0XHRcdFx0Ym9yZGVyLWNvbG9yOiB2YXIoLS1wcmltYXJ5KSAhaW1wb3J0YW50O1xyXG5cclxuXHRcdFx0XHQmOjpiZWZvcmUge1xyXG5cdFx0XHRcdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRcdFx0XHRoZWlnaHQ6IDEwMCU7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRpb24tbGFiZWwge1xyXG5cdFx0XHRcdFx0aDMge1xyXG5cdFx0XHRcdFx0XHRjb2xvcjogdmFyKC0td2hpdGUpO1xyXG5cdFx0XHRcdFx0XHRmb250LXdlaWdodDogNTAwO1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdHAge1xyXG5cdFx0XHRcdFx0XHRjb2xvcjogdmFyKC0td2hpdGUpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdH1cclxuXHJcblx0XHRcdCY6Zmlyc3QtY2hpbGQge1xyXG5cdFx0XHRcdG1hcmdpbi10b3A6IDI3cHg7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn0iLCJpb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwKSAhaW1wb3J0YW50O1xufVxuXG5pb24tZm9vdGVyIHtcbiAgYmFja2dyb3VuZDogdmFyKC0td2hpdGUpO1xufVxuaW9uLWZvb3RlciBpb24tbGlzdCB7XG4gIGJhY2tncm91bmQ6IHZhcigtLXRyYW5zcGFyZW50KSAhaW1wb3J0YW50O1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDA7XG59XG5pb24tZm9vdGVyIGlvbi1saXN0IC5saXN0X2hlYWRlciB7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlucHV0X2ZpbGVkX2JnKTtcbiAgbWluLWhlaWdodDogNTRweDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgcGFkZGluZzogMCAyNXB4O1xufVxuaW9uLWZvb3RlciBpb24tbGlzdCAubGlzdF9oZWFkZXIgaDIge1xuICBtYXJnaW46IDA7XG4gIG1pbi1oZWlnaHQ6IDU0cHg7XG4gIGxpbmUtaGVpZ2h0OiA1NHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgZm9udC1zaXplOiAxLjM1cmVtO1xuICBjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cbmlvbi1mb290ZXIgaW9uLWxpc3QgLmxpc3RfaGVhZGVyIGgyIHNwYW4uZW5kIHtcbiAgY29sb3I6IHZhcigtLXByaW1hcnkpO1xufVxuaW9uLWZvb3RlciBpb24tbGlzdCBpb24tcmFkaW8tZ3JvdXAge1xuICBtYXgtaGVpZ2h0OiAyNjBweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG92ZXJmbG93LXk6IHNjcm9sbDtcbn1cbmlvbi1mb290ZXIgaW9uLWxpc3QgaW9uLWl0ZW0ge1xuICBwYWRkaW5nOiAxNXB4IDE4cHg7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgYmFja2dyb3VuZDogdmFyKC0td2hpdGUpO1xuICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XG4gIC0taW5uZXItbWluLWhlaWdodDogdW5zZXQgIWltcG9ydGFudDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICAtLWhpZ2hsaWdoLWNvbG9yLWZvY3VzZWQ6IHZhcigtLXRyYW5zcGFyZW50KSAhaW1wb3J0YW50O1xuICAtLWJhY2tncm91bmQ6IHZhcigtLXRyYW5zcGFyZW50KTtcbiAgYmFja2dyb3VuZDogdmFyKC0taW5wdXRfZmlsZWRfYmcpO1xuICAtLW1pbi1oZWlnaHQ6IDU0cHg7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA0MHB4KTtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5pb24tZm9vdGVyIGlvbi1saXN0IGlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luOiAwO1xufVxuaW9uLWZvb3RlciBpb24tbGlzdCBpb24taXRlbSBpb24tbGFiZWwgaDMge1xuICBtYXJnaW46IDA7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDEuNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgY29sb3I6IHZhcigtLXRleHQtYmxhY2spO1xufVxuaW9uLWZvb3RlciBpb24tbGlzdCBpb24taXRlbSBpb24tbGFiZWwgcCB7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcbiAgZm9udC1zaXplOiAxLjEycmVtICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwO1xufVxuaW9uLWZvb3RlciBpb24tbGlzdCBpb24taXRlbSBpb24tcmFkaW8ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDk5OTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgbWFyZ2luOiAwO1xuICAtLWNvbG9yOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcbiAgLS1jb2xvci1jaGVja2VkOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcbn1cbmlvbi1mb290ZXIgaW9uLWxpc3QgaW9uLWl0ZW06OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiAwO1xuICB3aWR0aDogMDtcbiAgaGVpZ2h0OiAwO1xuICBtYXJnaW46IGF1dG87XG4gIGJhY2tncm91bmQ6IHZhcigtLXByaW1hcnkpO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcztcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xufVxuaW9uLWZvb3RlciBpb24tbGlzdCBpb24taXRlbS5pdGVtLXJhZGlvLWNoZWNrZWQge1xuICBib3JkZXItY29sb3I6IHZhcigtLXByaW1hcnkpICFpbXBvcnRhbnQ7XG59XG5pb24tZm9vdGVyIGlvbi1saXN0IGlvbi1pdGVtLml0ZW0tcmFkaW8tY2hlY2tlZDo6YmVmb3JlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cbmlvbi1mb290ZXIgaW9uLWxpc3QgaW9uLWl0ZW0uaXRlbS1yYWRpby1jaGVja2VkIGlvbi1sYWJlbCBoMyB7XG4gIGNvbG9yOiB2YXIoLS13aGl0ZSk7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5pb24tZm9vdGVyIGlvbi1saXN0IGlvbi1pdGVtLml0ZW0tcmFkaW8tY2hlY2tlZCBpb24tbGFiZWwgcCB7XG4gIGNvbG9yOiB2YXIoLS13aGl0ZSk7XG59XG5pb24tZm9vdGVyIGlvbi1saXN0IGlvbi1pdGVtOmZpcnN0LWNoaWxkIHtcbiAgbWFyZ2luLXRvcDogMjdweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/location/location.page.ts":
  /*!*******************************************!*\
    !*** ./src/app/location/location.page.ts ***!
    \*******************************************/

  /*! exports provided: LocationPage */

  /***/
  function srcAppLocationLocationPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LocationPage", function () {
      return LocationPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

    let LocationPage = class LocationPage {
      constructor(modalController) {
        this.modalController = modalController;
      }

      ngOnInit() {}

      dismiss() {
        this.modalController.dismiss();
      }

    };

    LocationPage.ctorParameters = () => [{
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
    }];

    LocationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-location',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./location.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/location/location.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./location.page.scss */
      "./src/app/location/location.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])], LocationPage);
    /***/
  }
}]);
//# sourceMappingURL=default~location-location-module~select-date-time-select-date-time-module-es5.js.map