(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["confirm-submit-confirm-submit-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/confirm-submit/confirm-submit.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/confirm-submit/confirm-submit.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n\t<ion-toolbar>\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-back-button text=\"\" icon=\"chevron-back-outline\"></ion-back-button>\n\t\t</ion-buttons>\n\t\t<ion-title>\n\t\t\t<ion-icon class=\"zmdi zmdi-circle active\"></ion-icon>\n\t\t\t<ion-icon class=\"zmdi zmdi-circle active\"></ion-icon>\n\t\t\t<ion-icon class=\"zmdi zmdi-circle\"></ion-icon>\n\t\t</ion-title>\n\t</ion-toolbar>\n\t<h1>Confirm Information</h1>\n</ion-header>\n\n\n<ion-content fullscreen>\n\t<div class=\"form\">\n\t\t<ion-list lines=\"none\">\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>{{'your_appointment_for' | translate}}</h2>\n\t\t\t\t\t<h3><label>{{category}}</label></h3>\n\t\t\t\t\t<h4><label></label></h4>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>{{'Confirm for' | translate}}</h2>\n\t\t\t\t\t<label>{{date| date:'EEEE, MMMM d, y'}}</label><br><label>{{time}}</label>\n\t\t\t\t</div>\n\t\t\t\t\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>{{'location_at' | translate}}</h2>\n\t\t\t\t\t<label>{{address}}</label>\n\t\t\t\t\t\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\t\t\t\n\t\t\t\n\t\t\t\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>{{'Confirmed Budject' | translate}}</h2>\n\t\t\t\t\t<label> {{budject| currency:'Rs. '}}</label>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\" *ngIf=checkestimate()>\n\t\t\t\t\t<h2 *ngIf=checkestimate()>{{'Estimated Hourses' | translate}}</h2>\n\t\t\t\t\t<label *ngIf=checkestimate()> {{estimate}}</label>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t \n\t\t</ion-list>\n\t</div>\n</ion-content>\n\n<ion-footer class=\"ion-no-border d-flex\">\n\t<ion-row>\n\t\t <ion-col size=\"12\">\n\t\t\t <ion-button size=\"large\" fill=\"outline\" shape=\"block\" class=\"btn end\" (click)=confirmappoinment()>Confirm</ion-button>\n\t\t </ion-col>\n\t</ion-row>\n\t\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/confirm-submit/confirm-submit-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/confirm-submit/confirm-submit-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: ConfirmSubmitPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmSubmitPageRoutingModule", function() { return ConfirmSubmitPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _confirm_submit_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./confirm-submit.page */ "./src/app/confirm-submit/confirm-submit.page.ts");




const routes = [
    {
        path: '',
        component: _confirm_submit_page__WEBPACK_IMPORTED_MODULE_3__["ConfirmSubmitPage"]
    }
];
let ConfirmSubmitPageRoutingModule = class ConfirmSubmitPageRoutingModule {
};
ConfirmSubmitPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ConfirmSubmitPageRoutingModule);



/***/ }),

/***/ "./src/app/confirm-submit/confirm-submit.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/confirm-submit/confirm-submit.module.ts ***!
  \*********************************************************/
/*! exports provided: ConfirmSubmitPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmSubmitPageModule", function() { return ConfirmSubmitPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _confirm_submit_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./confirm-submit-routing.module */ "./src/app/confirm-submit/confirm-submit-routing.module.ts");
/* harmony import */ var _confirm_submit_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./confirm-submit.page */ "./src/app/confirm-submit/confirm-submit.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");








let ConfirmSubmitPageModule = class ConfirmSubmitPageModule {
};
ConfirmSubmitPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _confirm_submit_routing_module__WEBPACK_IMPORTED_MODULE_5__["ConfirmSubmitPageRoutingModule"]
        ],
        declarations: [_confirm_submit_page__WEBPACK_IMPORTED_MODULE_6__["ConfirmSubmitPage"]]
    })
], ConfirmSubmitPageModule);



/***/ }),

/***/ "./src/app/confirm-submit/confirm-submit.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/confirm-submit/confirm-submit.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header h1 small {\n  display: block;\n  color: var(--text-light);\n  font-weight: 400;\n  font-size: 0.95rem;\n  opacity: 0.7;\n  padding-top: 6px;\n}\nion-header ion-toolbar ion-title {\n  position: absolute !important;\n  top: 0;\n  left: 0;\n  width: 100%;\n  padding: 0 15px !important;\n  text-align: center;\n}\nion-header ion-toolbar ion-title ion-icon {\n  color: var(--text-light2);\n  margin: 0 5px;\n  font-size: 0.85rem;\n}\nion-header ion-toolbar ion-title ion-icon.active {\n  color: var(--primary);\n}\n.form {\n  padding-top: 50px;\n}\n.form ion-list ion-item {\n  margin-bottom: 25px;\n}\n.form ion-list ion-item h2 {\n  margin: 0;\n  color: var(--text-light);\n  font-size: 1.15rem;\n  padding-bottom: 10px;\n  font-weight: 700;\n  opacity: 0.7;\n}\n.form ion-list ion-item h3 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 700;\n  font-size: 1.35rem;\n  padding-bottom: 5px;\n}\n.form ion-list ion-item h4 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 500;\n  font-size: 1.15rem;\n  line-height: 23px;\n}\n.form ion-list ion-item ion-textarea {\n  font-size: 1rem !important;\n  letter-spacing: 0.5px !important;\n}\n.form ion-list ion-item ion-label {\n  color: var(--text-dark) !important;\n  font-weight: 700 !important;\n  font-size: 1.35rem !important;\n}\nion-footer {\n  background: var(--white);\n  padding: 10px 10px 20px 10px;\n}\nion-footer ion-row {\n  width: 100%;\n}\nion-footer ion-row ion-col {\n  padding: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29uZmlybS1zdWJtaXQvRTpcXEVcXHNlcnZpY2VcXGZpcmViYXNlIDA0XFxwcm92aWRlci1hcHAtYnVkZGhpbmkvc3JjXFxhcHBcXGNvbmZpcm0tc3VibWl0XFxjb25maXJtLXN1Ym1pdC5wYWdlLnNjc3MiLCJzcmMvYXBwL2NvbmZpcm0tc3VibWl0L2NvbmZpcm0tc3VibWl0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFRTtFQUNDLGNBQUE7RUFDQSx3QkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNESDtBREtDO0VBQ0MsNkJBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtFQUNBLGtCQUFBO0FDSEY7QURLRTtFQUNDLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0FDSEg7QURLRztFQUNDLHFCQUFBO0FDSEo7QURTQTtFQUNDLGlCQUFBO0FDTkQ7QURTRTtFQUNDLG1CQUFBO0FDUEg7QURTRztFQUNDLFNBQUE7RUFDQSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUNQSjtBRFVHO0VBQ0MsU0FBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDUko7QURXRztFQUNDLFNBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ1RKO0FEWUc7RUFDQywwQkFBQTtFQUNBLGdDQUFBO0FDVko7QURhRztFQUNDLGtDQUFBO0VBQ0EsMkJBQUE7RUFDQSw2QkFBQTtBQ1hKO0FEaUJBO0VBQ0Msd0JBQUE7RUFDQSw0QkFBQTtBQ2REO0FEZ0JDO0VBQ0MsV0FBQTtBQ2RGO0FEZ0JFO0VBQ0MsZUFBQTtBQ2RIIiwiZmlsZSI6InNyYy9hcHAvY29uZmlybS1zdWJtaXQvY29uZmlybS1zdWJtaXQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlciB7XHJcblx0aDEge1xyXG5cdFx0c21hbGwge1xyXG5cdFx0XHRkaXNwbGF5OiBibG9jaztcclxuXHRcdFx0Y29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xyXG5cdFx0XHRmb250LXdlaWdodDogNDAwO1xyXG5cdFx0XHRmb250LXNpemU6IC45NXJlbTtcclxuXHRcdFx0b3BhY2l0eTogLjc7XHJcblx0XHRcdHBhZGRpbmctdG9wOiA2cHg7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRpb24tdG9vbGJhciBpb24tdGl0bGUge1xyXG5cdFx0cG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XHJcblx0XHR0b3A6IDA7XHJcblx0XHRsZWZ0OiAwO1xyXG5cdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRwYWRkaW5nOiAwIDE1cHggIWltcG9ydGFudDtcclxuXHRcdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcblx0XHRpb24taWNvbiB7XHJcblx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0Mik7XHJcblx0XHRcdG1hcmdpbjogMCA1cHg7XHJcblx0XHRcdGZvbnQtc2l6ZTogMC44NXJlbTtcclxuXHJcblx0XHRcdCYuYWN0aXZlIHtcclxuXHRcdFx0XHRjb2xvcjogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbi5mb3JtIHtcclxuXHRwYWRkaW5nLXRvcDogNTBweDtcclxuXHJcblx0aW9uLWxpc3Qge1xyXG5cdFx0aW9uLWl0ZW0ge1xyXG5cdFx0XHRtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG5cclxuXHRcdFx0aDIge1xyXG5cdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxLjE1cmVtO1xyXG5cdFx0XHRcdHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG5cdFx0XHRcdGZvbnQtd2VpZ2h0OiA3MDA7XHJcblx0XHRcdFx0b3BhY2l0eTogLjc7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGgzIHtcclxuXHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0Y29sb3I6IHZhcigtLXRleHQtZGFyayk7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IDcwMDtcclxuXHRcdFx0XHRmb250LXNpemU6IDEuMzVyZW07XHJcblx0XHRcdFx0cGFkZGluZy1ib3R0b206IDVweDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aDQge1xyXG5cdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcclxuXHRcdFx0XHRmb250LXdlaWdodDogNTAwO1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMS4xNXJlbTtcclxuXHRcdFx0XHRsaW5lLWhlaWdodDogMjNweDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aW9uLXRleHRhcmVhIHtcclxuXHRcdFx0XHRmb250LXNpemU6IDFyZW0gIWltcG9ydGFudDtcclxuXHRcdFx0XHRsZXR0ZXItc3BhY2luZzogLjVweCAhaW1wb3J0YW50O1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpb24tbGFiZWwge1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IDcwMCAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMS4zNXJlbSAhaW1wb3J0YW50O1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG5pb24tZm9vdGVyIHtcclxuXHRiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XHJcblx0cGFkZGluZzogMTBweCAxMHB4IDIwcHggMTBweDtcclxuXHJcblx0aW9uLXJvdyB7XHJcblx0XHR3aWR0aDogMTAwJTsgXHJcblxyXG5cdFx0aW9uLWNvbCB7XHJcblx0XHRcdHBhZGRpbmc6IDAgMTBweDtcclxuXHRcdH1cclxuXHR9XHJcbn0iLCJpb24taGVhZGVyIGgxIHNtYWxsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgZm9udC1zaXplOiAwLjk1cmVtO1xuICBvcGFjaXR5OiAwLjc7XG4gIHBhZGRpbmctdG9wOiA2cHg7XG59XG5pb24taGVhZGVyIGlvbi10b29sYmFyIGlvbi10aXRsZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAwIDE1cHggIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuaW9uLWhlYWRlciBpb24tdG9vbGJhciBpb24tdGl0bGUgaW9uLWljb24ge1xuICBjb2xvcjogdmFyKC0tdGV4dC1saWdodDIpO1xuICBtYXJnaW46IDAgNXB4O1xuICBmb250LXNpemU6IDAuODVyZW07XG59XG5pb24taGVhZGVyIGlvbi10b29sYmFyIGlvbi10aXRsZSBpb24taWNvbi5hY3RpdmUge1xuICBjb2xvcjogdmFyKC0tcHJpbWFyeSk7XG59XG5cbi5mb3JtIHtcbiAgcGFkZGluZy10b3A6IDUwcHg7XG59XG4uZm9ybSBpb24tbGlzdCBpb24taXRlbSB7XG4gIG1hcmdpbi1ib3R0b206IDI1cHg7XG59XG4uZm9ybSBpb24tbGlzdCBpb24taXRlbSBoMiB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuICBmb250LXNpemU6IDEuMTVyZW07XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICBmb250LXdlaWdodDogNzAwO1xuICBvcGFjaXR5OiAwLjc7XG59XG4uZm9ybSBpb24tbGlzdCBpb24taXRlbSBoMyB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtZGFyayk7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGZvbnQtc2l6ZTogMS4zNXJlbTtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cbi5mb3JtIGlvbi1saXN0IGlvbi1pdGVtIGg0IHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgZm9udC1zaXplOiAxLjE1cmVtO1xuICBsaW5lLWhlaWdodDogMjNweDtcbn1cbi5mb3JtIGlvbi1saXN0IGlvbi1pdGVtIGlvbi10ZXh0YXJlYSB7XG4gIGZvbnQtc2l6ZTogMXJlbSAhaW1wb3J0YW50O1xuICBsZXR0ZXItc3BhY2luZzogMC41cHggIWltcG9ydGFudDtcbn1cbi5mb3JtIGlvbi1saXN0IGlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA3MDAgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxLjM1cmVtICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1mb290ZXIge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XG4gIHBhZGRpbmc6IDEwcHggMTBweCAyMHB4IDEwcHg7XG59XG5pb24tZm9vdGVyIGlvbi1yb3cge1xuICB3aWR0aDogMTAwJTtcbn1cbmlvbi1mb290ZXIgaW9uLXJvdyBpb24tY29sIHtcbiAgcGFkZGluZzogMCAxMHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/confirm-submit/confirm-submit.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/confirm-submit/confirm-submit.page.ts ***!
  \*******************************************************/
/*! exports provided: ConfirmSubmitPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmSubmitPage", function() { return ConfirmSubmitPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/fesm2015/angular-fire-database.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.esm.js");
/* harmony import */ var _services_api_fcm_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/api-fcm.service */ "./src/app/services/api-fcm.service.ts");







let ConfirmSubmitPage = class ConfirmSubmitPage {
    constructor(storage, route, db, apiFcmService) {
        this.storage = storage;
        this.route = route;
        this.db = db;
        this.apiFcmService = apiFcmService;
    }
    ngOnInit() {
        this.appointment_details();
    }
    appointment_details() {
        let date = window.localStorage.getItem("provider_date");
        let time = window.localStorage.getItem("provider_time");
        let budject = window.localStorage.getItem("provider_budject");
        let information = window.localStorage.getItem("information");
        let estimate = window.localStorage.getItem("provider_Estimate_Time");
        this.date = date;
        this.time = time;
        this.budject = budject;
        this.estimate = estimate;
        this.checkestimate(this.estimate);
        let jobInfo = JSON.parse(localStorage.getItem('jobInfo'));
        this.category = jobInfo.category;
        this.address = jobInfo.address;
        this.storage.get('user').then(user => {
            if (user) {
                this.UID = user.UID;
                this.db.database.ref("/users/" + user.UID).on('value', (snapshot) => {
                    const data = snapshot.val();
                    this.name = data.name;
                    console.log(data.name);
                    // this.checkestimate(this.estimatetime);
                });
            }
        });
    }
    checkestimate(estimate) {
        if (this.estimate == 'undefined') {
            return false;
        }
        else {
            return true;
        }
    }
    // checkestimate(estimatetime){
    //   if(estimatetime=undefined){
    //     return false;
    //   }
    //     return true;
    // }
    confirmappoinment() {
        this.JID = window.localStorage.getItem("myjobid");
        // get saved requests from db
        firebase__WEBPACK_IMPORTED_MODULE_5__["default"].database().ref(`jobs/${this.JID}`).get().then(resuslt => {
            let job = resuslt.val();
            console.log(job);
            this.requests = job.requests;
            if (!this.requests)
                this.requests = [];
            // array push new data
            this.requests.push({
                provider_UID: this.UID,
                provider_name: this.name,
                jid: this.JID,
                provider_date: this.date,
                provider_time: this.time,
                provider_budject: this.budject,
                estimatetime: this.estimate,
                status: 'pending'
            });
            console.log(this.JID);
            firebase__WEBPACK_IMPORTED_MODULE_5__["default"].database().ref(`jobs/${this.JID}`).update({ requests: this.requests, status: 'requested' }).then(() => {
                //send notification to the category providers
                this.apiFcmService.notifyClient(this.JID);
            });
            this.route.navigate(['./order-done']);
        });
    }
};
ConfirmSubmitPage.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] },
    { type: _services_api_fcm_service__WEBPACK_IMPORTED_MODULE_6__["ApiFcmService"] }
];
ConfirmSubmitPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-confirm-submit',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./confirm-submit.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/confirm-submit/confirm-submit.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./confirm-submit.page.scss */ "./src/app/confirm-submit/confirm-submit.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"],
        _services_api_fcm_service__WEBPACK_IMPORTED_MODULE_6__["ApiFcmService"]])
], ConfirmSubmitPage);



/***/ })

}]);
//# sourceMappingURL=confirm-submit-confirm-submit-module-es2015.js.map