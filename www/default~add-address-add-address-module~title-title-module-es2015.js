(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~add-address-add-address-module~title-title-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/title/title.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/title/title.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--\r\n<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>title</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n-->\r\n<ion-content (click)=\"dismiss()\">\r\n\r\n</ion-content>\r\n<ion-footer no-border>\r\n\t<div class=\"form\"> \r\n\t\t<ion-list lines=\"none\">\r\n\t\t\t<h1>{{'select_address_type' | translate}}</h1>\r\n\t\t\t<ion-radio-group [(ngModel)]=\"index\" >\r\n\t\t\t\t<ion-row>\r\n\t\t\t\t\t<ion-col size=\"4\" *ngFor=\"let item of addresses; let i = index\">\r\n\t\t\t\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t\t\t\t<ion-radio slot=\"start\" [value]=\"i\"></ion-radio>\r\n\t\t\t\t\t\t\t<ion-label class=\"d-flex\">\r\n\t\t\t\t\t\t\t\t{{ item.address_type.toUpperCase() }}\r\n\t\t\t\t\t\t\t</ion-label>\r\n\t\t\t\t\t\t</ion-item>\r\n\t\t\t\t\t</ion-col>\r\n\t\t\t\t\r\n\t\t\t\t</ion-row>\r\n\t\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t\t<ion-textarea rows=\"2\" type=\"text\" value=\"\" placeholder=\"Address\" [(ngModel)]=\"addresses[index].address\"></ion-textarea>\r\n\t\t\t\t</ion-item>\r\n\t\t\t</ion-radio-group>\r\n\t\t\t<ion-button size=\"large\" shape=\"block\" class=\"btn\" (click)=\"saveaddress()\">{{'save_address' | translate}}</ion-button>\r\n\t\t</ion-list>\r\n\t</div>\r\n\r\n</ion-footer>");

/***/ }),

/***/ "./src/app/services/user.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/user.service.ts ***!
  \******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");



let UserService = class UserService {
    constructor() {
        // behaviour subject
        this._$userSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]('');
    }
    getUserSubject() {
        return this._$userSubject.asObservable();
    }
};
UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], UserService);



/***/ }),

/***/ "./src/app/title/title.page.scss":
/*!***************************************!*\
  !*** ./src/app/title/title.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: rgba(0, 0, 0, 0) !important;\n}\n\nion-footer {\n  background: var(--white);\n  padding: 20px 0;\n  box-shadow: 0 -6px 10px 10px rgba(0, 0, 0, 0.1);\n}\n\nion-footer .form h1 {\n  margin: 0;\n  font-size: 1.35rem;\n  font-weight: 700;\n  padding-bottom: 20px;\n}\n\nion-footer .form ion-list ion-item ion-textarea {\n  color: var(--text-light) !important;\n  font-size: 1.14rem !important;\n  font-weight: 400 !important;\n  background: var(--input_filed_bg) !important;\n  margin: 0 !important;\n}\n\nion-footer .form ion-list ion-row {\n  margin: 0 -10px;\n}\n\nion-footer .form ion-list ion-row ion-col {\n  padding: 0 10px;\n}\n\nion-footer .form ion-list ion-row ion-col ion-item {\n  padding: 0px 8px;\n  border-radius: 4px;\n  --min-height: 54px;\n  margin: 0 auto;\n  margin-bottom: 17px;\n  background: var(--input_filed_bg) !important;\n}\n\nion-footer .form ion-list ion-row ion-col ion-item ion-label {\n  position: relative;\n  margin: 0;\n  color: var(--text-light) !important;\n  font-size: 1.14rem !important;\n  font-weight: 400 !important;\n  margin: 0;\n  padding: 0;\n  display: block !important;\n  text-align: center;\n}\n\nion-footer .form ion-list ion-row ion-col ion-item ion-radio {\n  position: absolute;\n  z-index: 999;\n  width: 100%;\n  height: 100%;\n  margin: 0;\n  --color: var(--transparent) !important;\n  --color-checked: var(--transparent) !important;\n}\n\nion-footer .form ion-list ion-row ion-col ion-item::before {\n  content: \"\";\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  width: 0;\n  height: 0;\n  margin: auto;\n  background: var(--primary);\n  -webkit-transition: all 0.3s;\n  transition: all 0.3s;\n  border-radius: 4px;\n}\n\nion-footer .form ion-list ion-row ion-col ion-item.item-radio-checked {\n  border-color: var(--primary) !important;\n}\n\nion-footer .form ion-list ion-row ion-col ion-item.item-radio-checked::before {\n  width: 100%;\n  height: 100%;\n}\n\nion-footer .form ion-list ion-row ion-col ion-item.item-radio-checked ion-label {\n  color: var(--white) !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGl0bGUvRTpcXEVcXHNlcnZpY2VcXGZpcmViYXNlIDA0XFxwcm92aWRlci1hcHAtYnVkZGhpbmkvc3JjXFxhcHBcXHRpdGxlXFx0aXRsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3RpdGxlL3RpdGxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLHlDQUFBO0FDQ0Q7O0FERUE7RUFDQyx3QkFBQTtFQUNBLGVBQUE7RUFDQSwrQ0FBQTtBQ0NEOztBREVFO0VBQ0MsU0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtBQ0FIOztBREtJO0VBQ0MsbUNBQUE7RUFDQSw2QkFBQTtFQUNBLDJCQUFBO0VBQ0EsNENBQUE7RUFDQSxvQkFBQTtBQ0hMOztBRE9HO0VBQ0MsZUFBQTtBQ0xKOztBRE9JO0VBQ0MsZUFBQTtBQ0xMOztBRE9LO0VBQ0MsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsNENBQUE7QUNMTjs7QURPTTtFQUNDLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLG1DQUFBO0VBQ0EsNkJBQUE7RUFDQSwyQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQ0xQOztBRFFNO0VBQ0Msa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0VBQ0Esc0NBQUE7RUFDQSw4Q0FBQTtBQ05QOztBRFNNO0VBQ0MsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLDBCQUFBO0VBQ0EsNEJBQUE7RUFBQSxvQkFBQTtFQUNBLGtCQUFBO0FDUFA7O0FEVU07RUFDQyx1Q0FBQTtBQ1JQOztBRFVPO0VBQ0MsV0FBQTtFQUNBLFlBQUE7QUNSUjs7QURXTztFQUNDLDhCQUFBO0FDVFIiLCJmaWxlIjoic3JjL2FwcC90aXRsZS90aXRsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcblx0LS1iYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDApICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlvbi1mb290ZXIge1xyXG5cdGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcclxuXHRwYWRkaW5nOiAyMHB4IDA7XHJcblx0Ym94LXNoYWRvdzogMCAtNnB4IDEwcHggMTBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XHJcblxyXG5cdC5mb3JtIHtcclxuXHRcdGgxIHtcclxuXHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRmb250LXNpemU6IDEuMzVyZW07XHJcblx0XHRcdGZvbnQtd2VpZ2h0OiA3MDA7XHJcblx0XHRcdHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG5cdFx0fVxyXG5cclxuXHRcdGlvbi1saXN0IHtcclxuXHRcdFx0aW9uLWl0ZW0ge1xyXG5cdFx0XHRcdGlvbi10ZXh0YXJlYSB7XHJcblx0XHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1saWdodCkgIWltcG9ydGFudDtcclxuXHRcdFx0XHRcdGZvbnQtc2l6ZTogMS4xNHJlbSAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdFx0Zm9udC13ZWlnaHQ6IDQwMCAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdFx0YmFja2dyb3VuZDogdmFyKC0taW5wdXRfZmlsZWRfYmcpICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0XHRtYXJnaW46IDAgIWltcG9ydGFudDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlvbi1yb3cge1xyXG5cdFx0XHRcdG1hcmdpbjogMCAtMTBweDtcclxuXHJcblx0XHRcdFx0aW9uLWNvbCB7XHJcblx0XHRcdFx0XHRwYWRkaW5nOiAwIDEwcHg7XHJcblxyXG5cdFx0XHRcdFx0aW9uLWl0ZW0ge1xyXG5cdFx0XHRcdFx0XHRwYWRkaW5nOiAwcHggOHB4O1xyXG5cdFx0XHRcdFx0XHRib3JkZXItcmFkaXVzOiA0cHg7XHJcblx0XHRcdFx0XHRcdC0tbWluLWhlaWdodDogNTRweDtcclxuXHRcdFx0XHRcdFx0bWFyZ2luOiAwIGF1dG87XHJcblx0XHRcdFx0XHRcdG1hcmdpbi1ib3R0b206IDE3cHg7XHJcblx0XHRcdFx0XHRcdGJhY2tncm91bmQ6IHZhcigtLWlucHV0X2ZpbGVkX2JnKSAhaW1wb3J0YW50O1xyXG5cclxuXHRcdFx0XHRcdFx0aW9uLWxhYmVsIHtcclxuXHRcdFx0XHRcdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHRcdFx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KSAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdFx0XHRcdGZvbnQtc2l6ZTogMS4xNHJlbSAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdFx0XHRcdGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcclxuXHRcdFx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRcdFx0cGFkZGluZzogMDtcclxuXHRcdFx0XHRcdFx0XHRkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdFx0XHRcdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRcdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdFx0aW9uLXJhZGlvIHtcclxuXHRcdFx0XHRcdFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRcdFx0XHRcdFx0ei1pbmRleDogOTk5O1xyXG5cdFx0XHRcdFx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRcdFx0XHRcdGhlaWdodDogMTAwJTtcclxuXHRcdFx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRcdFx0LS1jb2xvcjogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0XHRcdFx0LS1jb2xvci1jaGVja2VkOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcclxuXHRcdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdFx0Jjo6YmVmb3JlIHtcclxuXHRcdFx0XHRcdFx0XHRjb250ZW50OiAnJztcclxuXHRcdFx0XHRcdFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRcdFx0XHRcdFx0dG9wOiAwO1xyXG5cdFx0XHRcdFx0XHRcdGxlZnQ6IDA7XHJcblx0XHRcdFx0XHRcdFx0Ym90dG9tOiAwO1xyXG5cdFx0XHRcdFx0XHRcdHJpZ2h0OiAwO1xyXG5cdFx0XHRcdFx0XHRcdHdpZHRoOiAwO1xyXG5cdFx0XHRcdFx0XHRcdGhlaWdodDogMDtcclxuXHRcdFx0XHRcdFx0XHRtYXJnaW46IGF1dG87XHJcblx0XHRcdFx0XHRcdFx0YmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRcdFx0XHRcdFx0dHJhbnNpdGlvbjogYWxsIC4zcztcclxuXHRcdFx0XHRcdFx0XHRib3JkZXItcmFkaXVzOiA0cHg7XHJcblx0XHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRcdCYuaXRlbS1yYWRpby1jaGVja2VkIHtcclxuXHRcdFx0XHRcdFx0XHRib3JkZXItY29sb3I6IHZhcigtLXByaW1hcnkpICFpbXBvcnRhbnQ7XHJcblxyXG5cdFx0XHRcdFx0XHRcdCY6OmJlZm9yZSB7XHJcblx0XHRcdFx0XHRcdFx0XHR3aWR0aDogMTAwJTtcclxuXHRcdFx0XHRcdFx0XHRcdGhlaWdodDogMTAwJTtcclxuXHRcdFx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0XHRcdGlvbi1sYWJlbCB7XHJcblx0XHRcdFx0XHRcdFx0XHRjb2xvcjogdmFyKC0td2hpdGUpICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsImlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDApICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1mb290ZXIge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XG4gIHBhZGRpbmc6IDIwcHggMDtcbiAgYm94LXNoYWRvdzogMCAtNnB4IDEwcHggMTBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG59XG5pb24tZm9vdGVyIC5mb3JtIGgxIHtcbiAgbWFyZ2luOiAwO1xuICBmb250LXNpemU6IDEuMzVyZW07XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xufVxuaW9uLWZvb3RlciAuZm9ybSBpb24tbGlzdCBpb24taXRlbSBpb24tdGV4dGFyZWEge1xuICBjb2xvcjogdmFyKC0tdGV4dC1saWdodCkgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxLjE0cmVtICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZDogdmFyKC0taW5wdXRfZmlsZWRfYmcpICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMCAhaW1wb3J0YW50O1xufVxuaW9uLWZvb3RlciAuZm9ybSBpb24tbGlzdCBpb24tcm93IHtcbiAgbWFyZ2luOiAwIC0xMHB4O1xufVxuaW9uLWZvb3RlciAuZm9ybSBpb24tbGlzdCBpb24tcm93IGlvbi1jb2wge1xuICBwYWRkaW5nOiAwIDEwcHg7XG59XG5pb24tZm9vdGVyIC5mb3JtIGlvbi1saXN0IGlvbi1yb3cgaW9uLWNvbCBpb24taXRlbSB7XG4gIHBhZGRpbmc6IDBweCA4cHg7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgLS1taW4taGVpZ2h0OiA1NHB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgbWFyZ2luLWJvdHRvbTogMTdweDtcbiAgYmFja2dyb3VuZDogdmFyKC0taW5wdXRfZmlsZWRfYmcpICFpbXBvcnRhbnQ7XG59XG5pb24tZm9vdGVyIC5mb3JtIGlvbi1saXN0IGlvbi1yb3cgaW9uLWNvbCBpb24taXRlbSBpb24tbGFiZWwge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtbGlnaHQpICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMS4xNHJlbSAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogNDAwICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcbiAgZGlzcGxheTogYmxvY2sgIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuaW9uLWZvb3RlciAuZm9ybSBpb24tbGlzdCBpb24tcm93IGlvbi1jb2wgaW9uLWl0ZW0gaW9uLXJhZGlvIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiA5OTk7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIG1hcmdpbjogMDtcbiAgLS1jb2xvcjogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XG4gIC0tY29sb3ItY2hlY2tlZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XG59XG5pb24tZm9vdGVyIC5mb3JtIGlvbi1saXN0IGlvbi1yb3cgaW9uLWNvbCBpb24taXRlbTo6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGJvdHRvbTogMDtcbiAgcmlnaHQ6IDA7XG4gIHdpZHRoOiAwO1xuICBoZWlnaHQ6IDA7XG4gIG1hcmdpbjogYXV0bztcbiAgYmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG59XG5pb24tZm9vdGVyIC5mb3JtIGlvbi1saXN0IGlvbi1yb3cgaW9uLWNvbCBpb24taXRlbS5pdGVtLXJhZGlvLWNoZWNrZWQge1xuICBib3JkZXItY29sb3I6IHZhcigtLXByaW1hcnkpICFpbXBvcnRhbnQ7XG59XG5pb24tZm9vdGVyIC5mb3JtIGlvbi1saXN0IGlvbi1yb3cgaW9uLWNvbCBpb24taXRlbS5pdGVtLXJhZGlvLWNoZWNrZWQ6OmJlZm9yZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG5pb24tZm9vdGVyIC5mb3JtIGlvbi1saXN0IGlvbi1yb3cgaW9uLWNvbCBpb24taXRlbS5pdGVtLXJhZGlvLWNoZWNrZWQgaW9uLWxhYmVsIHtcbiAgY29sb3I6IHZhcigtLXdoaXRlKSAhaW1wb3J0YW50O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/title/title.page.ts":
/*!*************************************!*\
  !*** ./src/app/title/title.page.ts ***!
  \*************************************/
/*! exports provided: TitlePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TitlePage", function() { return TitlePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.esm.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/fesm2015/angular-fire-database.js");







let TitlePage = class TitlePage {
    constructor(modalController, userService, storage, db) {
        this.modalController = modalController;
        this.userService = userService;
        this.storage = storage;
        this.db = db;
        this.index = 0;
        // addresses:  [ {address_type: 'home', address: ''}, {address_type: 'office', address: ''}}]
        this.addresses = [{ address_type: 'home', address: '' }, { address_type: 'office', address: '' }, { address_type: 'other', address: '' }];
    }
    ngOnInit() {
        // get the user UID
        this.storage.get('user').then(user => {
            console.log('user', user);
            this.UID = user.UID;
        });
        let data = JSON.parse(window.localStorage.getItem('addresses'));
        if (data)
            this.addresses = data;
    }
    saveaddress() {
        // this.address=this.addaddress;
        window.localStorage.setItem("addresses", JSON.stringify(this.addresses));
        // alert(this.address);
        alert('UID' + this.UID);
        firebase__WEBPACK_IMPORTED_MODULE_3__["default"].database().ref().child(`users/${this.UID}`).update({ addresses: this.addresses });
        this.modalController.dismiss();
    }
};
TitlePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_6__["AngularFireDatabase"] }
];
TitlePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-title',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./title.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/title/title.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./title.page.scss */ "./src/app/title/title.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
        _angular_fire_database__WEBPACK_IMPORTED_MODULE_6__["AngularFireDatabase"]])
], TitlePage);



/***/ })

}]);
//# sourceMappingURL=default~add-address-add-address-module~title-title-module-es2015.js.map