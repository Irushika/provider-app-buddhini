(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["confirm-confirm-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/confirm/confirm.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/confirm/confirm.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppConfirmConfirmPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n\t<ion-toolbar>\r\n\t\t<ion-buttons slot=\"start\">\r\n\t\t\t<ion-back-button text=\"\" icon=\"chevron-back-outline\"></ion-back-button>\r\n\t\t</ion-buttons>\r\n\t\t<ion-title>\r\n\t\t\t<ion-icon class=\"zmdi zmdi-circle active\"></ion-icon>\r\n\t\t\t<ion-icon class=\"zmdi zmdi-circle active\"></ion-icon>\r\n\t\t\t<ion-icon class=\"zmdi zmdi-circle active\"></ion-icon>\r\n\t\t</ion-title>\r\n\t</ion-toolbar>\r\n\t<h1>{{'confirm_info' | translate}}</h1>\r\n</ion-header>\r\n\r\n\r\n<ion-content fullscreen>\r\n\t<div class=\"form\">\r\n\t\t<ion-list lines=\"none\">\r\n\t\t\t<ion-item>\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t<h2>{{'your_appointment_for' | translate}}</h2>\r\n\t\t\t\t<h3>Fix my Appliances</h3>\r\n\t\t\t\t<h4>Air Conditioner</h4>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t\t<ion-item>\r\n\t\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t<h2>{{'confirmed_for' | translate}}</h2>\r\n\t\t\t\t<label>{{date}}</label>\r\n\t\t\t\t<h4>11:00 am</h4>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t\t<ion-item>\r\n\t\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t<h2>{{'location_at' | translate}}</h2>\r\n\t\t\t\t<h3>Home</h3>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<ion-label position=\"fixed\">{{'additionla_information' | translate}}</ion-label>\r\n\t\t\t\t\t<ion-textarea rows=\"5\" type=\"name\" value=\"\" placeholder=\"{{'elebrate_your_issue' | translate}}\"></ion-textarea>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\t\t</ion-list>\r\n\t</div>\r\n</ion-content>\r\n\r\n<ion-footer class=\"ion-no-border d-flex\">\r\n\t<div class=\"amount\">\r\n\t\t<h3>{{'amount' | translate}}</h3>\r\n\t\t<h2>$140.00</h2>\r\n\t</div>\r\n\t<ion-button size=\"large\" shape=\"block\" class=\"btn end\" (click)=\"continue()\">{{'continue' | translate}}</ion-button>\r\n</ion-footer>\r\n";
    /***/
  },

  /***/
  "./src/app/confirm/confirm-routing.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/confirm/confirm-routing.module.ts ***!
    \***************************************************/

  /*! exports provided: ConfirmPageRoutingModule */

  /***/
  function srcAppConfirmConfirmRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ConfirmPageRoutingModule", function () {
      return ConfirmPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _confirm_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./confirm.page */
    "./src/app/confirm/confirm.page.ts");

    const routes = [{
      path: '',
      component: _confirm_page__WEBPACK_IMPORTED_MODULE_3__["ConfirmPage"]
    }];
    let ConfirmPageRoutingModule = class ConfirmPageRoutingModule {};
    ConfirmPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ConfirmPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/confirm/confirm.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/confirm/confirm.module.ts ***!
    \*******************************************/

  /*! exports provided: ConfirmPageModule */

  /***/
  function srcAppConfirmConfirmModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ConfirmPageModule", function () {
      return ConfirmPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _confirm_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./confirm-routing.module */
    "./src/app/confirm/confirm-routing.module.ts");
    /* harmony import */


    var _confirm_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./confirm.page */
    "./src/app/confirm/confirm.page.ts");

    let ConfirmPageModule = class ConfirmPageModule {};
    ConfirmPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"], _confirm_routing_module__WEBPACK_IMPORTED_MODULE_6__["ConfirmPageRoutingModule"]],
      declarations: [_confirm_page__WEBPACK_IMPORTED_MODULE_7__["ConfirmPage"]]
    })], ConfirmPageModule);
    /***/
  },

  /***/
  "./src/app/confirm/confirm.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/confirm/confirm.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppConfirmConfirmPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-header ion-toolbar ion-title {\n  position: absolute !important;\n  top: 0;\n  left: 0;\n  width: 100%;\n  padding: 0 15px !important;\n  text-align: center;\n}\nion-header ion-toolbar ion-title ion-icon {\n  color: var(--text-light2);\n  margin: 0 5px;\n  font-size: 0.85rem;\n}\nion-header ion-toolbar ion-title ion-icon.active {\n  color: var(--primary);\n}\nion-header h1 {\n  font-size: 1.3rem;\n  padding: 0 20px;\n  padding-top: 17px;\n}\n.form {\n  padding-top: 26px;\n}\n.form ion-list ion-item {\n  margin-bottom: 30px;\n}\n.form ion-list ion-item h2 {\n  margin: 0;\n  color: var(--text-light);\n  font-size: 1.15rem;\n  padding-bottom: 10px;\n  font-weight: 700;\n  opacity: 0.7;\n}\n.form ion-list ion-item h3 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 700;\n  font-size: 1.35rem;\n  padding-bottom: 5px;\n}\n.form ion-list ion-item h4 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 600;\n  font-size: 1.1rem;\n}\n.form ion-list ion-item ion-textarea {\n  font-size: 1rem !important;\n  letter-spacing: 0.5px !important;\n}\n.form ion-list ion-item ion-label {\n  color: var(--text-dark) !important;\n  font-weight: 700 !important;\n  font-size: 1.35rem !important;\n}\nion-footer {\n  background: var(--white);\n  padding: 10px 20px 20px 20px;\n}\nion-footer .amount {\n  min-width: 50%;\n  max-width: 50%;\n}\nion-footer .amount h3 {\n  margin: 0;\n  color: var(--text-light);\n  font-weight: 400;\n  font-size: 0.85rem;\n  padding-bottom: 5px;\n}\nion-footer .amount h2 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 600;\n  font-size: 1.2rem;\n}\nion-footer .button.btn {\n  min-width: 50%;\n  max-width: 50%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29uZmlybS9FOlxcRVxcc2VydmljZVxcZmlyZWJhc2UgMDRcXHByb3ZpZGVyLWFwcC1idWRkaGluaS9zcmNcXGFwcFxcY29uZmlybVxcY29uZmlybS5wYWdlLnNjc3MiLCJzcmMvYXBwL2NvbmZpcm0vY29uZmlybS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0M7RUFDQyw2QkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0VBQ0Esa0JBQUE7QUNBRjtBREVFO0VBQ0MseUJBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7QUNBSDtBREVHO0VBQ0MscUJBQUE7QUNBSjtBREtDO0VBQ0MsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNIRjtBRE9BO0VBQ0MsaUJBQUE7QUNKRDtBRE9FO0VBQ0MsbUJBQUE7QUNMSDtBRE9HO0VBQ0MsU0FBQTtFQUNBLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtBQ0xKO0FEUUc7RUFDQyxTQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNOSjtBRFNHO0VBQ0MsU0FBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQ1BKO0FEVUc7RUFDQywwQkFBQTtFQUNBLGdDQUFBO0FDUko7QURXRztFQUNDLGtDQUFBO0VBQ0EsMkJBQUE7RUFDQSw2QkFBQTtBQ1RKO0FEZUE7RUFDQyx3QkFBQTtFQUNBLDRCQUFBO0FDWkQ7QURjQztFQUNDLGNBQUE7RUFDQSxjQUFBO0FDWkY7QURjRTtFQUNDLFNBQUE7RUFDQSx3QkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ1pIO0FEZUU7RUFDQyxTQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FDYkg7QURpQkM7RUFDQyxjQUFBO0VBQ0EsY0FBQTtBQ2ZGIiwiZmlsZSI6InNyYy9hcHAvY29uZmlybS9jb25maXJtLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXIge1xyXG5cdGlvbi10b29sYmFyIGlvbi10aXRsZSB7XHJcblx0XHRwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcclxuXHRcdHRvcDogMDtcclxuXHRcdGxlZnQ6IDA7XHJcblx0XHR3aWR0aDogMTAwJTtcclxuXHRcdHBhZGRpbmc6IDAgMTVweCAhaW1wb3J0YW50O1xyXG5cdFx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuXHRcdGlvbi1pY29uIHtcclxuXHRcdFx0Y29sb3I6IHZhcigtLXRleHQtbGlnaHQyKTtcclxuXHRcdFx0bWFyZ2luOiAwIDVweDtcclxuXHRcdFx0Zm9udC1zaXplOiAwLjg1cmVtO1xyXG5cclxuXHRcdFx0Ji5hY3RpdmUge1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS1wcmltYXJ5KTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0aDEge1xyXG5cdFx0Zm9udC1zaXplOiAxLjNyZW07XHJcblx0XHRwYWRkaW5nOiAwIDIwcHg7XHJcblx0XHRwYWRkaW5nLXRvcDogMTdweDtcclxuXHR9XHJcbn1cclxuXHJcbi5mb3JtIHtcclxuXHRwYWRkaW5nLXRvcDogMjZweDtcclxuXHJcblx0aW9uLWxpc3Qge1xyXG5cdFx0aW9uLWl0ZW0ge1xyXG5cdFx0XHRtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG5cclxuXHRcdFx0aDIge1xyXG5cdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxLjE1cmVtO1xyXG5cdFx0XHRcdHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG5cdFx0XHRcdGZvbnQtd2VpZ2h0OiA3MDA7XHJcblx0XHRcdFx0b3BhY2l0eTogLjc7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGgzIHtcclxuXHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0Y29sb3I6IHZhcigtLXRleHQtZGFyayk7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IDcwMDtcclxuXHRcdFx0XHRmb250LXNpemU6IDEuMzVyZW07XHJcblx0XHRcdFx0cGFkZGluZy1ib3R0b206IDVweDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aDQge1xyXG5cdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcclxuXHRcdFx0XHRmb250LXdlaWdodDogNjAwO1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMS4xcmVtO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpb24tdGV4dGFyZWEge1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMXJlbSAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdGxldHRlci1zcGFjaW5nOiAuNXB4ICFpbXBvcnRhbnQ7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlvbi1sYWJlbCB7XHJcblx0XHRcdFx0Y29sb3I6IHZhcigtLXRleHQtZGFyaykgIWltcG9ydGFudDtcclxuXHRcdFx0XHRmb250LXdlaWdodDogNzAwICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxLjM1cmVtICFpbXBvcnRhbnQ7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbmlvbi1mb290ZXIge1xyXG5cdGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcclxuXHRwYWRkaW5nOiAxMHB4IDIwcHggMjBweCAyMHB4O1xyXG5cclxuXHQuYW1vdW50IHtcclxuXHRcdG1pbi13aWR0aDogNTAlO1xyXG5cdFx0bWF4LXdpZHRoOiA1MCU7XHJcblxyXG5cdFx0aDMge1xyXG5cdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcclxuXHRcdFx0Zm9udC13ZWlnaHQ6IDQwMDtcclxuXHRcdFx0Zm9udC1zaXplOiAuODVyZW07XHJcblx0XHRcdHBhZGRpbmctYm90dG9tOiA1cHg7XHJcblx0XHR9XHJcblxyXG5cdFx0aDIge1xyXG5cdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xyXG5cdFx0XHRmb250LXdlaWdodDogNjAwO1xyXG5cdFx0XHRmb250LXNpemU6IDEuMnJlbTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdC5idXR0b24uYnRuIHtcclxuXHRcdG1pbi13aWR0aDogNTAlO1xyXG5cdFx0bWF4LXdpZHRoOiA1MCU7XHJcblx0fVxyXG59IiwiaW9uLWhlYWRlciBpb24tdG9vbGJhciBpb24tdGl0bGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMCAxNXB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbmlvbi1oZWFkZXIgaW9uLXRvb2xiYXIgaW9uLXRpdGxlIGlvbi1pY29uIHtcbiAgY29sb3I6IHZhcigtLXRleHQtbGlnaHQyKTtcbiAgbWFyZ2luOiAwIDVweDtcbiAgZm9udC1zaXplOiAwLjg1cmVtO1xufVxuaW9uLWhlYWRlciBpb24tdG9vbGJhciBpb24tdGl0bGUgaW9uLWljb24uYWN0aXZlIHtcbiAgY29sb3I6IHZhcigtLXByaW1hcnkpO1xufVxuaW9uLWhlYWRlciBoMSB7XG4gIGZvbnQtc2l6ZTogMS4zcmVtO1xuICBwYWRkaW5nOiAwIDIwcHg7XG4gIHBhZGRpbmctdG9wOiAxN3B4O1xufVxuXG4uZm9ybSB7XG4gIHBhZGRpbmctdG9wOiAyNnB4O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWl0ZW0ge1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWl0ZW0gaDIge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcbiAgZm9udC1zaXplOiAxLjE1cmVtO1xuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgb3BhY2l0eTogMC43O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWl0ZW0gaDMge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xuICBmb250LXdlaWdodDogNzAwO1xuICBmb250LXNpemU6IDEuMzVyZW07XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4uZm9ybSBpb24tbGlzdCBpb24taXRlbSBoNCB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtZGFyayk7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc2l6ZTogMS4xcmVtO1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWl0ZW0gaW9uLXRleHRhcmVhIHtcbiAgZm9udC1zaXplOiAxcmVtICFpbXBvcnRhbnQ7XG4gIGxldHRlci1zcGFjaW5nOiAwLjVweCAhaW1wb3J0YW50O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWl0ZW0gaW9uLWxhYmVsIHtcbiAgY29sb3I6IHZhcigtLXRleHQtZGFyaykgIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDcwMCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDEuMzVyZW0gIWltcG9ydGFudDtcbn1cblxuaW9uLWZvb3RlciB7XG4gIGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcbiAgcGFkZGluZzogMTBweCAyMHB4IDIwcHggMjBweDtcbn1cbmlvbi1mb290ZXIgLmFtb3VudCB7XG4gIG1pbi13aWR0aDogNTAlO1xuICBtYXgtd2lkdGg6IDUwJTtcbn1cbmlvbi1mb290ZXIgLmFtb3VudCBoMyB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuICBmb250LXdlaWdodDogNDAwO1xuICBmb250LXNpemU6IDAuODVyZW07XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG5pb24tZm9vdGVyIC5hbW91bnQgaDIge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXNpemU6IDEuMnJlbTtcbn1cbmlvbi1mb290ZXIgLmJ1dHRvbi5idG4ge1xuICBtaW4td2lkdGg6IDUwJTtcbiAgbWF4LXdpZHRoOiA1MCU7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/confirm/confirm.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/confirm/confirm.page.ts ***!
    \*****************************************/

  /*! exports provided: ConfirmPage */

  /***/
  function srcAppConfirmConfirmPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ConfirmPage", function () {
      return ConfirmPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    let ConfirmPage = class ConfirmPage {
      constructor(route) {
        this.route = route;
      }

      ngOnInit() {}

      continue() {
        this.route.navigate(['./order-done']);
      }

    };

    ConfirmPage.ctorParameters = () => [{
      type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
    }];

    ConfirmPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-confirm',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./confirm.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/confirm/confirm.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./confirm.page.scss */
      "./src/app/confirm/confirm.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])], ConfirmPage);
    /***/
  }
}]);
//# sourceMappingURL=confirm-confirm-module-es5.js.map