(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["veirifcation-veirifcation-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/veirifcation/veirifcation.page.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/veirifcation/veirifcation.page.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppVeirifcationVeirifcationPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n\t<ion-toolbar>\r\n\t\t<ion-buttons slot=\"start\">\r\n\t\t\t<ion-back-button text=\"\" icon=\"chevron-back-outline\"></ion-back-button>\r\n\t\t</ion-buttons>\r\n\t\t<ion-title></ion-title>\r\n\t\t<!-- <ion-title *ngIf=\"otpScreen\"></ion-title> -->\r\n\t</ion-toolbar>\r\n\t<h1>{{'verification' | translate}}</h1>\r\n</ion-header>\r\n\r\n<ion-content fullscreen>\r\n\t<div class=\"form\">\r\n\t\t\r\n\t\t<ion-list lines=\"none\">\r\n\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<ion-label position=\"fixed\">{{'enter_otp' | translate}}</ion-label>\r\n\t\t\t\t\t<ion-input type=\"text\" value=\"\" placeholder=\"{{'enter_6_digit_otp' | translate}}\" [(ngModel)]=\"otpNumber\"></ion-input>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-button size=\"large\" shape=\"block\" class=\"btn\" (click)=\"verify()\">{{'continue' | translate}}</ion-button>\r\n\t\t</ion-list>\r\n\t\t<h2 class=\"d-flex\">\r\n\t\t\r\n\t\t\t<span class=\"end\" (click)=\"sendotp()\">{{'send_again' | translate}}</span>\r\n\t\t</h2>\r\n\t</div>\r\n</ion-content>\r\n<div id=\"recaptcha-container\"></div>";
    /***/
  },

  /***/
  "./src/app/services/user.service.ts":
  /*!******************************************!*\
    !*** ./src/app/services/user.service.ts ***!
    \******************************************/

  /*! exports provided: UserService */

  /***/
  function srcAppServicesUserServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserService", function () {
      return UserService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    let UserService = class UserService {
      constructor() {
        // behaviour subject
        this._$userSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]('');
      }

      getUserSubject() {
        return this._$userSubject.asObservable();
      }

    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], UserService);
    /***/
  },

  /***/
  "./src/app/veirifcation/veirifcation-routing.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/veirifcation/veirifcation-routing.module.ts ***!
    \*************************************************************/

  /*! exports provided: VeirifcationPageRoutingModule */

  /***/
  function srcAppVeirifcationVeirifcationRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "VeirifcationPageRoutingModule", function () {
      return VeirifcationPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _veirifcation_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./veirifcation.page */
    "./src/app/veirifcation/veirifcation.page.ts");

    const routes = [{
      path: '',
      component: _veirifcation_page__WEBPACK_IMPORTED_MODULE_3__["VeirifcationPage"]
    }];
    let VeirifcationPageRoutingModule = class VeirifcationPageRoutingModule {};
    VeirifcationPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], VeirifcationPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/veirifcation/veirifcation.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/veirifcation/veirifcation.module.ts ***!
    \*****************************************************/

  /*! exports provided: VeirifcationPageModule */

  /***/
  function srcAppVeirifcationVeirifcationModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "VeirifcationPageModule", function () {
      return VeirifcationPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _veirifcation_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./veirifcation-routing.module */
    "./src/app/veirifcation/veirifcation-routing.module.ts");
    /* harmony import */


    var _veirifcation_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./veirifcation.page */
    "./src/app/veirifcation/veirifcation.page.ts");

    let VeirifcationPageModule = class VeirifcationPageModule {};
    VeirifcationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"], _veirifcation_routing_module__WEBPACK_IMPORTED_MODULE_6__["VeirifcationPageRoutingModule"]],
      declarations: [_veirifcation_page__WEBPACK_IMPORTED_MODULE_7__["VeirifcationPage"]]
    })], VeirifcationPageModule);
    /***/
  },

  /***/
  "./src/app/veirifcation/veirifcation.page.scss":
  /*!*****************************************************!*\
    !*** ./src/app/veirifcation/veirifcation.page.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppVeirifcationVeirifcationPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".form {\n  padding-top: 20px;\n}\n.form p {\n  margin: 0;\n  color: var(--text-light2);\n  font-weight: 500;\n  font-size: 1.23rem;\n  padding-bottom: 50px;\n}\n.form h2 {\n  margin: 0;\n  font-weight: 500;\n  color: var(--text-dark2);\n  font-size: 1.25rem;\n  font-weight: 600;\n  padding-top: 30px;\n}\n.form h2 span {\n  color: var(--primary);\n  font-size: 1.3rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmVpcmlmY2F0aW9uL0U6XFxFXFxzZXJ2aWNlXFxmaXJlYmFzZSAwNFxccHJvdmlkZXItYXBwLWJ1ZGRoaW5pL3NyY1xcYXBwXFx2ZWlyaWZjYXRpb25cXHZlaXJpZmNhdGlvbi5wYWdlLnNjc3MiLCJzcmMvYXBwL3ZlaXJpZmNhdGlvbi92ZWlyaWZjYXRpb24ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsaUJBQUE7QUNDRDtBRENDO0VBQ0MsU0FBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0FDQ0Y7QURFQztFQUNDLFNBQUE7RUFDQSxnQkFBQTtFQUNBLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FDQUY7QURFRTtFQUNDLHFCQUFBO0VBQ0EsaUJBQUE7QUNBSCIsImZpbGUiOiJzcmMvYXBwL3ZlaXJpZmNhdGlvbi92ZWlyaWZjYXRpb24ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcm0ge1xyXG5cdHBhZGRpbmctdG9wOiAyMHB4O1xyXG5cclxuXHRwIHtcclxuXHRcdG1hcmdpbjogMDtcclxuXHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0Mik7XHJcblx0XHRmb250LXdlaWdodDogNTAwO1xyXG5cdFx0Zm9udC1zaXplOiAxLjIzcmVtO1xyXG5cdFx0cGFkZGluZy1ib3R0b206IDUwcHg7XHJcblx0fVxyXG5cclxuXHRoMiB7XHJcblx0XHRtYXJnaW46IDA7XHJcblx0XHRmb250LXdlaWdodDogNTAwO1xyXG5cdFx0Y29sb3I6IHZhcigtLXRleHQtZGFyazIpO1xyXG5cdFx0Zm9udC1zaXplOiAxLjI1cmVtO1xyXG5cdFx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHRcdHBhZGRpbmctdG9wOiAzMHB4O1xyXG5cclxuXHRcdHNwYW4ge1xyXG5cdFx0XHRjb2xvcjogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRcdGZvbnQtc2l6ZTogMS4zcmVtO1xyXG5cdFx0fVxyXG5cdH1cclxufSIsIi5mb3JtIHtcbiAgcGFkZGluZy10b3A6IDIwcHg7XG59XG4uZm9ybSBwIHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1saWdodDIpO1xuICBmb250LXdlaWdodDogNTAwO1xuICBmb250LXNpemU6IDEuMjNyZW07XG4gIHBhZGRpbmctYm90dG9tOiA1MHB4O1xufVxuLmZvcm0gaDIge1xuICBtYXJnaW46IDA7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWRhcmsyKTtcbiAgZm9udC1zaXplOiAxLjI1cmVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBwYWRkaW5nLXRvcDogMzBweDtcbn1cbi5mb3JtIGgyIHNwYW4ge1xuICBjb2xvcjogdmFyKC0tcHJpbWFyeSk7XG4gIGZvbnQtc2l6ZTogMS4zcmVtO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/veirifcation/veirifcation.page.ts":
  /*!***************************************************!*\
    !*** ./src/app/veirifcation/veirifcation.page.ts ***!
    \***************************************************/

  /*! exports provided: VeirifcationPage */

  /***/
  function srcAppVeirifcationVeirifcationPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "VeirifcationPage", function () {
      return VeirifcationPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../services/user.service */
    "./src/app/services/user.service.ts");
    /* harmony import */


    var _services_window_reference_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../services/window-reference.service */
    "./src/app/services/window-reference.service.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var firebase__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! firebase */
    "./node_modules/firebase/dist/index.esm.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    let VeirifcationPage = class VeirifcationPage {
      constructor(navCtrl, windowReference, userService, activatedRoute, loadingCtrl, alertCtrl, storage) {
        this.navCtrl = navCtrl;
        this.windowReference = windowReference;
        this.userService = userService;
        this.activatedRoute = activatedRoute;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.otpScreen = true;
      }

      ngOnInit() {
        this.windowReference.storeWindowRef = this.windowReference.windowRef;
        this.windowReference.storeWindowRef.recaptchaVerifier = new firebase__WEBPACK_IMPORTED_MODULE_6__["default"].auth.RecaptchaVerifier('recaptcha-container', {
          size: 'invisible'
        });
        this.windowReference.storeWindowRef.recaptchaVerifier.render();
        this.cellNumber = this.activatedRoute.snapshot.paramMap.get('no');
        this.sendCode();
      }

      verify() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
          const loader = yield this.loadingCtrl.create();
          loader.present();
          console.log('otp', this.otpNumber);
          this.windowReference.storeWindowRef.confirmationResult.confirm(this.otpNumber).then(result => {
            loader.dismiss();
            console.log('resultt phone: ', result.user.phoneNumber);
            console.log('resultt UID: ', result.user.uid);
            let setUser = {
              phoneNumber: result.user.phoneNumber,
              UID: result.user.uid
            }; // store the user information 

            this.storage.set('user', setUser);
            this.storage.set('isLoggedIn', true);

            this.userService._$userSubject.next(setUser);

            this.navCtrl.navigateRoot(['./sign-up']);
          }).catch(err => {
            loader.dismiss();
            this.alertCtrl.create({
              header: 'Incorrect Code',
              message: "".concat(err.message),
              buttons: ['Ok']
            }).then(res => {
              res.present();
            });
          });
        });
      }

      sendotp() {
        this.sendCode();
      }

      sendCode() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
          console.log("Sending Code");
          const loader = yield this.loadingCtrl.create();
          loader.present();
          const appVerifier = this.windowReference.storeWindowRef.recaptchaVerifier;
          const num = this.cellNumber.toString();
          console.log("verifier", appVerifier);
          console.log("num", num);
          firebase__WEBPACK_IMPORTED_MODULE_6__["default"].auth().signInWithPhoneNumber(num, appVerifier).then(result => {
            this.windowReference.storeWindowRef.confirmationResult = result;
            this.otpScreen = true;
            loader.dismiss();
          }).catch(err => {
            loader.dismiss();
            this.alertCtrl.create({
              header: 'Invalid Number',
              message: "".concat(err),
              buttons: ['Ok']
            }).then(res => {
              res.present();
            });
          });
        });
      }

    };

    VeirifcationPage.ctorParameters = () => [{
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
    }, {
      type: _services_window_reference_service__WEBPACK_IMPORTED_MODULE_4__["WindowReference"]
    }, {
      type: _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]
    }, {
      type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
    }, {
      type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
    }];

    VeirifcationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-veirifcation',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./veirifcation.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/veirifcation/veirifcation.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./veirifcation.page.scss */
      "./src/app/veirifcation/veirifcation.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _services_window_reference_service__WEBPACK_IMPORTED_MODULE_4__["WindowReference"], _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]])], VeirifcationPage);
    /***/
  }
}]);
//# sourceMappingURL=veirifcation-veirifcation-module-es5.js.map