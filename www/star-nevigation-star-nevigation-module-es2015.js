(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["star-nevigation-star-nevigation-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/star-nevigation/star-nevigation.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/star-nevigation/star-nevigation.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n\t<ion-toolbar>\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-back-button text=\"\" icon=\"chevron-back-outline\"></ion-back-button>\n\t\t</ion-buttons>\n\t\t<ion-title>\n\n\t\t</ion-title>\n\t</ion-toolbar>\n\t<h1>\n\t\t{{'appointment_info' | translate}}\n\t\t<small>{{'booked_on' | translate}} 9 June 2020 11:40 am</small>\n\t</h1>\n</ion-header>\n\n\n<ion-content fullscreen>\n\t<div class=\"form\">\n\t\t<ion-list lines=\"none\">\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>{{'your_appointment_for' | translate}}</h2>\n\t\t\t\t\t<h3>{{jobs?.category}}</h3>\n\t\t\t\t\t<h4>Air Conditioner</h4>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>{{'date_time' | translate}}</h2>\n\t\t\t\t\t<h3>13 June 2020 11:00</h3> \n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>{{'location_at' | translate}}</h2>\n\t\t\t\t\t<h3>Home</h3>\n\t\t\t\t\t<h4> 345, New York Business center,<br>Centerl Park, New York, USA </h4>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\t\t\t\n\t\t\t\n\t\t\t\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>{{'additionla_information' | translate}}</h2>\n\t\t\t\t \n\t\t\t\t\t<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.   </h4>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t \n\t\t</ion-list>\n\t</div>\n</ion-content>\n\n<ion-footer class=\"ion-no-border d-flex\">\n\t <ion-row>\n\t\t <ion-col size=\"12\">\n\t\t\t <ion-button size=\"large\" fill=\"outline\" shape=\"block\" class=\"btn end\">{{'Start Navigation' | translate}}</ion-button>\n\t\t </ion-col>\n\t\t \n\t\t \n\t</ion-row>\n\t\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/star-nevigation/star-nevigation-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/star-nevigation/star-nevigation-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: StarNevigationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StarNevigationPageRoutingModule", function() { return StarNevigationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _star_nevigation_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./star-nevigation.page */ "./src/app/star-nevigation/star-nevigation.page.ts");




const routes = [
    {
        path: '',
        component: _star_nevigation_page__WEBPACK_IMPORTED_MODULE_3__["StarNevigationPage"]
    }
];
let StarNevigationPageRoutingModule = class StarNevigationPageRoutingModule {
};
StarNevigationPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], StarNevigationPageRoutingModule);



/***/ }),

/***/ "./src/app/star-nevigation/star-nevigation.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/star-nevigation/star-nevigation.module.ts ***!
  \***********************************************************/
/*! exports provided: StarNevigationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StarNevigationPageModule", function() { return StarNevigationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _star_nevigation_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./star-nevigation-routing.module */ "./src/app/star-nevigation/star-nevigation-routing.module.ts");
/* harmony import */ var _star_nevigation_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./star-nevigation.page */ "./src/app/star-nevigation/star-nevigation.page.ts");








let StarNevigationPageModule = class StarNevigationPageModule {
};
StarNevigationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"],
            _star_nevigation_routing_module__WEBPACK_IMPORTED_MODULE_6__["StarNevigationPageRoutingModule"]
        ],
        declarations: [_star_nevigation_page__WEBPACK_IMPORTED_MODULE_7__["StarNevigationPage"]]
    })
], StarNevigationPageModule);



/***/ }),

/***/ "./src/app/star-nevigation/star-nevigation.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/star-nevigation/star-nevigation.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header h1 small {\n  display: block;\n  color: var(--text-light);\n  font-weight: 400;\n  font-size: 0.95rem;\n  opacity: 0.7;\n  padding-top: 6px;\n}\n\n.form {\n  padding-top: 50px;\n}\n\n.form ion-list ion-item {\n  margin-bottom: 25px;\n}\n\n.form ion-list ion-item h2 {\n  margin: 0;\n  color: var(--text-light);\n  font-size: 1.15rem;\n  padding-bottom: 10px;\n  font-weight: 700;\n  opacity: 0.7;\n}\n\n.form ion-list ion-item h3 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 700;\n  font-size: 1.35rem;\n  padding-bottom: 5px;\n}\n\n.form ion-list ion-item h4 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 500;\n  font-size: 1.15rem;\n  line-height: 23px;\n}\n\n.form ion-list ion-item ion-textarea {\n  font-size: 1rem !important;\n  letter-spacing: 0.5px !important;\n}\n\n.form ion-list ion-item ion-label {\n  color: var(--text-dark) !important;\n  font-weight: 700 !important;\n  font-size: 1.35rem !important;\n}\n\nion-footer {\n  background: var(--white);\n  padding: 10px 10px 20px 10px;\n}\n\nion-footer ion-row {\n  width: 100%;\n}\n\nion-footer ion-row ion-col {\n  padding: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3Rhci1uZXZpZ2F0aW9uL0U6XFxFXFxzZXJ2aWNlXFxmaXJlYmFzZSAwNFxccHJvdmlkZXItYXBwLWJ1ZGRoaW5pL3NyY1xcYXBwXFxzdGFyLW5ldmlnYXRpb25cXHN0YXItbmV2aWdhdGlvbi5wYWdlLnNjc3MiLCJzcmMvYXBwL3N0YXItbmV2aWdhdGlvbi9zdGFyLW5ldmlnYXRpb24ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVFO0VBQ0MsY0FBQTtFQUNBLHdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQ0RIOztBRE1BO0VBQ0MsaUJBQUE7QUNIRDs7QURNRTtFQUNDLG1CQUFBO0FDSkg7O0FETUc7RUFDQyxTQUFBO0VBQ0Esd0JBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FDSko7O0FET0c7RUFDQyxTQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNMSjs7QURRRztFQUNDLFNBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ05KOztBRFNHO0VBQ0MsMEJBQUE7RUFDQSxnQ0FBQTtBQ1BKOztBRFVHO0VBQ0Msa0NBQUE7RUFDQSwyQkFBQTtFQUNBLDZCQUFBO0FDUko7O0FEY0E7RUFDQyx3QkFBQTtFQUNBLDRCQUFBO0FDWEQ7O0FEYUM7RUFDQyxXQUFBO0FDWEY7O0FEYUU7RUFDQyxlQUFBO0FDWEgiLCJmaWxlIjoic3JjL2FwcC9zdGFyLW5ldmlnYXRpb24vc3Rhci1uZXZpZ2F0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXIge1xyXG5cdGgxIHtcclxuXHRcdHNtYWxsIHtcclxuXHRcdFx0ZGlzcGxheTogYmxvY2s7XHJcblx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcclxuXHRcdFx0Zm9udC13ZWlnaHQ6IDQwMDtcclxuXHRcdFx0Zm9udC1zaXplOiAuOTVyZW07XHJcblx0XHRcdG9wYWNpdHk6IC43O1xyXG5cdFx0XHRwYWRkaW5nLXRvcDogNnB4O1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLmZvcm0ge1xyXG5cdHBhZGRpbmctdG9wOiA1MHB4O1xyXG5cclxuXHRpb24tbGlzdCB7XHJcblx0XHRpb24taXRlbSB7XHJcblx0XHRcdG1hcmdpbi1ib3R0b206IDI1cHg7XHJcblxyXG5cdFx0XHRoMiB7XHJcblx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcclxuXHRcdFx0XHRmb250LXNpemU6IDEuMTVyZW07XHJcblx0XHRcdFx0cGFkZGluZy1ib3R0b206IDEwcHg7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IDcwMDtcclxuXHRcdFx0XHRvcGFjaXR5OiAuNztcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aDMge1xyXG5cdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcclxuXHRcdFx0XHRmb250LXdlaWdodDogNzAwO1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMS4zNXJlbTtcclxuXHRcdFx0XHRwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRoNCB7XHJcblx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xyXG5cdFx0XHRcdGZvbnQtd2VpZ2h0OiA1MDA7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxLjE1cmVtO1xyXG5cdFx0XHRcdGxpbmUtaGVpZ2h0OiAyM3B4O1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpb24tdGV4dGFyZWEge1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMXJlbSAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdGxldHRlci1zcGFjaW5nOiAuNXB4ICFpbXBvcnRhbnQ7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlvbi1sYWJlbCB7XHJcblx0XHRcdFx0Y29sb3I6IHZhcigtLXRleHQtZGFyaykgIWltcG9ydGFudDtcclxuXHRcdFx0XHRmb250LXdlaWdodDogNzAwICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxLjM1cmVtICFpbXBvcnRhbnQ7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbmlvbi1mb290ZXIge1xyXG5cdGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcclxuXHRwYWRkaW5nOiAxMHB4IDEwcHggMjBweCAxMHB4O1xyXG5cclxuXHRpb24tcm93IHtcclxuXHRcdHdpZHRoOiAxMDAlOyBcclxuXHJcblx0XHRpb24tY29sIHtcclxuXHRcdFx0cGFkZGluZzogMCAxMHB4O1xyXG5cdFx0fVxyXG5cdH1cclxufSIsImlvbi1oZWFkZXIgaDEgc21hbGwge1xuICBkaXNwbGF5OiBibG9jaztcbiAgY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuICBmb250LXdlaWdodDogNDAwO1xuICBmb250LXNpemU6IDAuOTVyZW07XG4gIG9wYWNpdHk6IDAuNztcbiAgcGFkZGluZy10b3A6IDZweDtcbn1cblxuLmZvcm0ge1xuICBwYWRkaW5nLXRvcDogNTBweDtcbn1cbi5mb3JtIGlvbi1saXN0IGlvbi1pdGVtIHtcbiAgbWFyZ2luLWJvdHRvbTogMjVweDtcbn1cbi5mb3JtIGlvbi1saXN0IGlvbi1pdGVtIGgyIHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XG4gIGZvbnQtc2l6ZTogMS4xNXJlbTtcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIG9wYWNpdHk6IDAuNztcbn1cbi5mb3JtIGlvbi1saXN0IGlvbi1pdGVtIGgzIHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgZm9udC1zaXplOiAxLjM1cmVtO1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWl0ZW0gaDQge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xuICBmb250LXdlaWdodDogNTAwO1xuICBmb250LXNpemU6IDEuMTVyZW07XG4gIGxpbmUtaGVpZ2h0OiAyM3B4O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWl0ZW0gaW9uLXRleHRhcmVhIHtcbiAgZm9udC1zaXplOiAxcmVtICFpbXBvcnRhbnQ7XG4gIGxldHRlci1zcGFjaW5nOiAwLjVweCAhaW1wb3J0YW50O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWl0ZW0gaW9uLWxhYmVsIHtcbiAgY29sb3I6IHZhcigtLXRleHQtZGFyaykgIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDcwMCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDEuMzVyZW0gIWltcG9ydGFudDtcbn1cblxuaW9uLWZvb3RlciB7XG4gIGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcbiAgcGFkZGluZzogMTBweCAxMHB4IDIwcHggMTBweDtcbn1cbmlvbi1mb290ZXIgaW9uLXJvdyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuaW9uLWZvb3RlciBpb24tcm93IGlvbi1jb2wge1xuICBwYWRkaW5nOiAwIDEwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/star-nevigation/star-nevigation.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/star-nevigation/star-nevigation.page.ts ***!
  \*********************************************************/
/*! exports provided: StarNevigationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StarNevigationPage", function() { return StarNevigationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/fesm2015/angular-fire-database.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");




let StarNevigationPage = class StarNevigationPage {
    constructor(db, alertController) {
        this.db = db;
        this.alertController = alertController;
    }
    ngOnInit() {
        let JID = window.localStorage.getItem("getjobs");
        console.log(JID);
        if (JID) {
            console.log(JID);
            this.db
                .list("/jobs", (ref) => ref.orderByChild("phoneNumber").equalTo(JID))
                .valueChanges()
                .subscribe((job) => {
                if (job.length > 0) {
                    this.job = job[0];
                    window.localStorage.setItem("jobInfo", JSON.stringify(job[0]));
                }
                else {
                    this.presentAlert();
                }
            });
        }
        else {
            this.job = JSON.parse(window.localStorage.getItem("jobInfo"));
            console.log("params not working");
        }
    }
    ;
    presentAlert() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: "Alert",
                message: "No user found.",
                buttons: ["OK"],
            });
            yield alert.present();
        });
    }
};
StarNevigationPage.ctorParameters = () => [
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] }
];
StarNevigationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-star-nevigation',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./star-nevigation.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/star-nevigation/star-nevigation.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./star-nevigation.page.scss */ "./src/app/star-nevigation/star-nevigation.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]])
], StarNevigationPage);



/***/ })

}]);
//# sourceMappingURL=star-nevigation-star-nevigation-module-es2015.js.map