(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["my-appointments-my-appointments-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/my-appointments/my-appointments.page.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/my-appointments/my-appointments.page.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppMyAppointmentsMyAppointmentsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n\t<ion-toolbar>\r\n\t\t<ion-buttons slot=\"start\">\r\n\t\t\t<ion-menu-button></ion-menu-button>\r\n\t\t</ion-buttons>\r\n\t\t<ion-title></ion-title>\r\n\t</ion-toolbar>\r\n\t<h1>{{'my_appointments' | translate}}</h1>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\t<ion-list lines=\"none\">\r\n\t\t\t<ion-item (click)=\"my_appointment_info(item.id)\" *ngFor=\"let item of appointment | myJobFilter: UID | sortdate\">\r\n\t\t\t\t<div class=\"item_inner d-flex\">\r\n\t\t\t\t\t<div class=\"date_time\">\r\n\t\t\t\t\t\t<h4>{{item.date| date:'MMMM d'}}</h4>\r\n\t\t\t\t\t\t<h4>{{item.time}}</h4>\r\n\t\t\t\t\t\t<h5 class=\"d-flex\">\r\n\t\t\t\t\t\t\t<ion-icon class=\"zmdi zmdi-pin ion-text-start\"></ion-icon>\r\n\t\t\t\t\t\t\t{{item.adrress_type}}\r\n\t\t\t\t\t\t</h5>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"appointment_details\">\r\n\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t<h3> {{item.category}} </h3>\r\n\t\t\t\t\t\t\t\t<ion-badge color=\"secondary\" slot=\"start\" *ngIf=findrequested(item.status)>{{item.status}}</ion-badge>\r\n\t\t\t\t\t\t\t\t<ion-badge color=\"tertiary\" slot=\"start\" *ngIf=findstarted(item.status)>{{item.status}}</ion-badge>\r\n\t\t\t\t\t\t\t<h5>{{item.budject| currency:'Rs. '}}</h5>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\t\t\t\r\n\t\t<div class=\"past_appointment\">\r\n\t\t\t<h2>{{'past_appointment' | translate}}</h2>\r\n\t\t\t<ion-item (click)=\"past_appointment_info(item.id)\" *ngFor=\"let item of appointment |pastJobFilter: UID  | sortdate\">\r\n\t\t\t\t<div class=\"item_inner d-flex\">\r\n\t\t\t\t\t<div class=\"date_time\">\r\n\t\t\t\t\t\t<h4>{{item.date| date:'MMMM d'}}</h4>\r\n\t\t\t\t\t\t<h4>{{item.time}}</h4>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t<h5 class=\"d-flex\">\r\n\t\t\t\t\t\t\t<ion-icon class=\"zmdi zmdi-pin ion-text-start\"></ion-icon>\r\n\t\t\t\t\t\t\t{{item.adrress_type}}\r\n\t\t\t\t\t\t</h5>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"appointment_details\">\r\n\t\t\t\t\t\t<h3> {{item.category}} </h3>\r\n\t\t\t\t\t\t<ion-badge color=\"danger\" slot=\"start\">{{item.status}}</ion-badge>\r\n\t\t\t\t\t\t<h5>{{item.budject| currency:'Rs. '}}</h5>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t</div>\r\n\t</ion-list>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/my-appointments/my-appointments-routing.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/my-appointments/my-appointments-routing.module.ts ***!
    \*******************************************************************/

  /*! exports provided: MyAppointmentsPageRoutingModule */

  /***/
  function srcAppMyAppointmentsMyAppointmentsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyAppointmentsPageRoutingModule", function () {
      return MyAppointmentsPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _my_appointments_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./my-appointments.page */
    "./src/app/my-appointments/my-appointments.page.ts");

    const routes = [{
      path: '',
      component: _my_appointments_page__WEBPACK_IMPORTED_MODULE_3__["MyAppointmentsPage"]
    }];
    let MyAppointmentsPageRoutingModule = class MyAppointmentsPageRoutingModule {};
    MyAppointmentsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], MyAppointmentsPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/my-appointments/my-appointments.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/my-appointments/my-appointments.module.ts ***!
    \***********************************************************/

  /*! exports provided: MyAppointmentsPageModule */

  /***/
  function srcAppMyAppointmentsMyAppointmentsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyAppointmentsPageModule", function () {
      return MyAppointmentsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _my_appointments_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./my-appointments-routing.module */
    "./src/app/my-appointments/my-appointments-routing.module.ts");
    /* harmony import */


    var _my_appointments_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./my-appointments.page */
    "./src/app/my-appointments/my-appointments.page.ts");
    /* harmony import */


    var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../pipes/pipes.module */
    "./src/app/pipes/pipes.module.ts");

    let MyAppointmentsPageModule = class MyAppointmentsPageModule {};
    MyAppointmentsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"], _my_appointments_routing_module__WEBPACK_IMPORTED_MODULE_6__["MyAppointmentsPageRoutingModule"], _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"]],
      declarations: [_my_appointments_page__WEBPACK_IMPORTED_MODULE_7__["MyAppointmentsPage"]]
    })], MyAppointmentsPageModule);
    /***/
  },

  /***/
  "./src/app/my-appointments/my-appointments.page.scss":
  /*!***********************************************************!*\
    !*** ./src/app/my-appointments/my-appointments.page.scss ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppMyAppointmentsMyAppointmentsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-list {\n  background: var(--transparent) !important;\n  margin: 0;\n  padding: 0;\n  width: calc(100% - 40px);\n  margin: 0 auto;\n  padding-top: 26px;\n}\nion-list h2 {\n  margin: 0;\n  color: var(--text-light);\n  font-size: 1rem;\n  font-weight: 500;\n  padding-bottom: 18px;\n}\nion-list ion-item {\n  padding: 0;\n  background: var(--white);\n  --inner-padding-end: 0px;\n  --inner-min-height: unset !important;\n  --padding-start: 0;\n  --highligh-color-focused: var(--transparent) !important;\n  margin-bottom: 10px;\n}\nion-list ion-item .item_inner {\n  width: 100%;\n  overflow: hidden;\n}\nion-list ion-item .item_inner .date_time {\n  background: var(--primary);\n  padding: 10px 12px;\n  min-width: 100px;\n  border-radius: 4px 0px 0px 4px;\n}\nion-list ion-item .item_inner .date_time h3 {\n  margin: 0;\n  color: var(--white);\n  font-size: 1.15rem;\n  padding-bottom: 8px;\n}\nion-list ion-item .item_inner .date_time h4 {\n  margin: 0;\n  color: #cdd6ed;\n  font-size: 0.77rem;\n  padding-bottom: 11px;\n}\nion-list ion-item .item_inner .date_time h5 {\n  margin: 0;\n  color: var(--white);\n  font-size: 0.77rem;\n}\nion-list ion-item .item_inner .date_time h5 ion-icon {\n  color: #cdd6ed;\n}\nion-list ion-item .item_inner .appointment_details {\n  padding: 9px 12px;\n  border: 1px solid #ccc;\n  border-left: 0;\n  width: 100%;\n  border-radius: 0px 4px 4px 0px;\n}\nion-list ion-item .item_inner .appointment_details h3 {\n  margin: 0;\n  color: var(--text-dark);\n  font-size: 1.2rem;\n  padding-bottom: 5px;\n  font-weight: 700;\n}\nion-list ion-item .item_inner .appointment_details h4 {\n  margin: 0;\n  color: var(--text-light);\n  font-size: 0.77rem;\n  padding-bottom: 7px;\n}\nion-list ion-item .item_inner .appointment_details h5 {\n  margin: 0;\n  color: var(--text-dark);\n  font-size: 0.77rem;\n  font-weight: 600;\n}\nion-list .past_appointment {\n  padding-top: 27px;\n}\nion-list .past_appointment ion-item .item_inner .date_time {\n  background: var(--bg-dark);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbXktYXBwb2ludG1lbnRzL0U6XFxFXFxzZXJ2aWNlXFxmaXJlYmFzZSAwNFxccHJvdmlkZXItYXBwLWJ1ZGRoaW5pL3NyY1xcYXBwXFxteS1hcHBvaW50bWVudHNcXG15LWFwcG9pbnRtZW50cy5wYWdlLnNjc3MiLCJzcmMvYXBwL215LWFwcG9pbnRtZW50cy9teS1hcHBvaW50bWVudHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MseUNBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLHdCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FDQ0Q7QURDQztFQUNDLFNBQUE7RUFDQSx3QkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0FDQ0Y7QURFQztFQUNDLFVBQUE7RUFDQSx3QkFBQTtFQUNBLHdCQUFBO0VBQ0Esb0NBQUE7RUFDQSxrQkFBQTtFQUNBLHVEQUFBO0VBQ0EsbUJBQUE7QUNBRjtBREVFO0VBQ0MsV0FBQTtFQUNBLGdCQUFBO0FDQUg7QURFRztFQUNDLDBCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLDhCQUFBO0FDQUo7QURFSTtFQUNDLFNBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNBTDtBREdJO0VBQ0MsU0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0FDREw7QURJSTtFQUNDLFNBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDRkw7QURJSztFQUNDLGNBQUE7QUNGTjtBRFFHO0VBQ0MsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsOEJBQUE7QUNOSjtBRFFJO0VBQ0MsU0FBQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FDTkw7QURTSTtFQUNDLFNBQUE7RUFDQSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNQTDtBRFVJO0VBQ0MsU0FBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ1JMO0FEYUM7RUFDQyxpQkFBQTtBQ1hGO0FEWUU7RUFDQywwQkFBQTtBQ1ZIIiwiZmlsZSI6InNyYy9hcHAvbXktYXBwb2ludG1lbnRzL215LWFwcG9pbnRtZW50cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tbGlzdCB7XHJcblx0YmFja2dyb3VuZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XHJcblx0bWFyZ2luOiAwO1xyXG5cdHBhZGRpbmc6IDA7XHJcblx0d2lkdGg6IGNhbGMoMTAwJSAtIDQwcHgpO1xyXG5cdG1hcmdpbjogMCBhdXRvO1xyXG5cdHBhZGRpbmctdG9wOiAyNnB4O1xyXG5cclxuXHRoMiB7XHJcblx0XHRtYXJnaW46IDA7XHJcblx0XHRjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XHJcblx0XHRmb250LXNpemU6IDFyZW07XHJcblx0XHRmb250LXdlaWdodDogNTAwO1xyXG5cdFx0cGFkZGluZy1ib3R0b206IDE4cHg7XHJcblx0fVxyXG5cclxuXHRpb24taXRlbSB7XHJcblx0XHRwYWRkaW5nOiAwO1xyXG5cdFx0YmFja2dyb3VuZDogdmFyKC0td2hpdGUpO1xyXG5cdFx0LS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xyXG5cdFx0LS1pbm5lci1taW4taGVpZ2h0OiB1bnNldCAhaW1wb3J0YW50O1xyXG5cdFx0LS1wYWRkaW5nLXN0YXJ0OiAwO1xyXG5cdFx0LS1oaWdobGlnaC1jb2xvci1mb2N1c2VkOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcclxuXHRcdG1hcmdpbi1ib3R0b206IDEwcHg7XHJcblxyXG5cdFx0Lml0ZW1faW5uZXIge1xyXG5cdFx0XHR3aWR0aDogMTAwJTtcclxuXHRcdFx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHJcblx0XHRcdC5kYXRlX3RpbWUge1xyXG5cdFx0XHRcdGJhY2tncm91bmQ6IHZhcigtLXByaW1hcnkpO1xyXG5cdFx0XHRcdHBhZGRpbmc6IDEwcHggMTJweDtcclxuXHRcdFx0XHRtaW4td2lkdGg6IDEwMHB4O1xyXG5cdFx0XHRcdGJvcmRlci1yYWRpdXM6IDRweCAwcHggMHB4IDRweDtcclxuXHJcblx0XHRcdFx0aDMge1xyXG5cdFx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdFx0Y29sb3I6IHZhcigtLXdoaXRlKTtcclxuXHRcdFx0XHRcdGZvbnQtc2l6ZTogMS4xNXJlbTtcclxuXHRcdFx0XHRcdHBhZGRpbmctYm90dG9tOiA4cHg7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRoNCB7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRjb2xvcjogI2NkZDZlZDtcclxuXHRcdFx0XHRcdGZvbnQtc2l6ZTogLjc3cmVtO1xyXG5cdFx0XHRcdFx0cGFkZGluZy1ib3R0b206IDExcHg7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRoNSB7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRjb2xvcjogdmFyKC0td2hpdGUpO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAuNzdyZW07XHJcblxyXG5cdFx0XHRcdFx0aW9uLWljb24ge1xyXG5cdFx0XHRcdFx0XHRjb2xvcjogI2NkZDZlZDtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQuYXBwb2ludG1lbnRfZGV0YWlscyB7XHJcblx0XHRcdFx0cGFkZGluZzogOXB4IDEycHg7XHJcblx0XHRcdFx0Ym9yZGVyOiAxcHggc29saWQgI2NjYztcclxuXHRcdFx0XHRib3JkZXItbGVmdDogMDtcclxuXHRcdFx0XHR3aWR0aDogMTAwJTtcclxuXHRcdFx0XHRib3JkZXItcmFkaXVzOiAwcHggNHB4IDRweCAwcHg7XHJcblxyXG5cdFx0XHRcdGgzIHtcclxuXHRcdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAxLjJyZW07XHJcblx0XHRcdFx0XHRwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG5cdFx0XHRcdFx0Zm9udC13ZWlnaHQ6IDcwMDtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdGg0IHtcclxuXHRcdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcclxuXHRcdFx0XHRcdGZvbnQtc2l6ZTogLjc3cmVtO1xyXG5cdFx0XHRcdFx0cGFkZGluZy1ib3R0b206IDdweDtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdGg1IHtcclxuXHRcdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAuNzdyZW07XHJcblx0XHRcdFx0XHRmb250LXdlaWdodDogNjAwO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHQucGFzdF9hcHBvaW50bWVudHtcclxuXHRcdHBhZGRpbmctdG9wOiAyN3B4O1xyXG5cdFx0aW9uLWl0ZW0gLml0ZW1faW5uZXIgLmRhdGVfdGltZXtcclxuXHRcdFx0YmFja2dyb3VuZDogdmFyKC0tYmctZGFyayk7XHJcblx0XHR9XHJcblxyXG5cdH1cclxufSIsImlvbi1saXN0IHtcbiAgYmFja2dyb3VuZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDQwcHgpO1xuICBtYXJnaW46IDAgYXV0bztcbiAgcGFkZGluZy10b3A6IDI2cHg7XG59XG5pb24tbGlzdCBoMiB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuICBmb250LXNpemU6IDFyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIHBhZGRpbmctYm90dG9tOiAxOHB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0ge1xuICBwYWRkaW5nOiAwO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcbiAgLS1pbm5lci1taW4taGVpZ2h0OiB1bnNldCAhaW1wb3J0YW50O1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIC0taGlnaGxpZ2gtY29sb3ItZm9jdXNlZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5pb24tbGlzdCBpb24taXRlbSAuaXRlbV9pbm5lciB7XG4gIHdpZHRoOiAxMDAlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmRhdGVfdGltZSB7XG4gIGJhY2tncm91bmQ6IHZhcigtLXByaW1hcnkpO1xuICBwYWRkaW5nOiAxMHB4IDEycHg7XG4gIG1pbi13aWR0aDogMTAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDRweCAwcHggMHB4IDRweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC5kYXRlX3RpbWUgaDMge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS13aGl0ZSk7XG4gIGZvbnQtc2l6ZTogMS4xNXJlbTtcbiAgcGFkZGluZy1ib3R0b206IDhweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC5kYXRlX3RpbWUgaDQge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiAjY2RkNmVkO1xuICBmb250LXNpemU6IDAuNzdyZW07XG4gIHBhZGRpbmctYm90dG9tOiAxMXB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmRhdGVfdGltZSBoNSB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXdoaXRlKTtcbiAgZm9udC1zaXplOiAwLjc3cmVtO1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmRhdGVfdGltZSBoNSBpb24taWNvbiB7XG4gIGNvbG9yOiAjY2RkNmVkO1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmFwcG9pbnRtZW50X2RldGFpbHMge1xuICBwYWRkaW5nOiA5cHggMTJweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgYm9yZGVyLWxlZnQ6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiAwcHggNHB4IDRweCAwcHg7XG59XG5pb24tbGlzdCBpb24taXRlbSAuaXRlbV9pbm5lciAuYXBwb2ludG1lbnRfZGV0YWlscyBoMyB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtZGFyayk7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICBmb250LXdlaWdodDogNzAwO1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmFwcG9pbnRtZW50X2RldGFpbHMgaDQge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcbiAgZm9udC1zaXplOiAwLjc3cmVtO1xuICBwYWRkaW5nLWJvdHRvbTogN3B4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmFwcG9pbnRtZW50X2RldGFpbHMgaDUge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xuICBmb250LXNpemU6IDAuNzdyZW07XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5pb24tbGlzdCAucGFzdF9hcHBvaW50bWVudCB7XG4gIHBhZGRpbmctdG9wOiAyN3B4O1xufVxuaW9uLWxpc3QgLnBhc3RfYXBwb2ludG1lbnQgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmRhdGVfdGltZSB7XG4gIGJhY2tncm91bmQ6IHZhcigtLWJnLWRhcmspO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/my-appointments/my-appointments.page.ts":
  /*!*********************************************************!*\
    !*** ./src/app/my-appointments/my-appointments.page.ts ***!
    \*********************************************************/

  /*! exports provided: MyAppointmentsPage */

  /***/
  function srcAppMyAppointmentsMyAppointmentsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyAppointmentsPage", function () {
      return MyAppointmentsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/fire/database */
    "./node_modules/@angular/fire/fesm2015/angular-fire-database.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");

    let MyAppointmentsPage = class MyAppointmentsPage {
      constructor(route, alertController, storage, db) {
        this.route = route;
        this.alertController = alertController;
        this.storage = storage;
        this.db = db;
      }

      ngOnInit() {
        this.loadingMyAppointment();
      }

      loadingMyAppointment() {
        console.log("my-profile");
        let userInfo = JSON.parse(localStorage.getItem('userInfo'));
        let category = userInfo.category;
        this.storage.get('user').then(user => {
          console.log(user);
          if (user) this.UID = user.UID;
        });
        this.db.list("/jobs").valueChanges([], {
          idField: 'id'
        }).subscribe(appointment => {
          console.log(appointment);
          this.appointment = appointment;
          console.log(this.appointment);
          window.localStorage.setItem("apointmentInfo", JSON.stringify(appointment[0]));
        });
      }

      loadingPastAppointment() {
        console.log("my-profile");
        let userInfo = JSON.parse(localStorage.getItem('userInfo'));
        let category = userInfo.category;
        this.storage.get('user').then(user => {
          console.log(user);
          if (user) this.UID = user.UID;
        });
        this.db.list("/jobs", ref => ref.orderByChild("requests.provider_UID").equalTo(this.UID)).valueChanges([], {
          idField: 'id'
        }).subscribe(appointment => {
          console.log(appointment);
          this.appointment = appointment;
          window.localStorage.setItem("apointmentInfo", JSON.stringify(appointment[0]));
        });
      }

      past_appointment_info(JID) {
        this.route.navigate(['./appointment-info', JID]);
      }

      findrequested(state) {
        if (state == 'requested') {
          return true;
        } else {
          return false;
        }
      }

      findstarted(state) {
        if (state == 'started') {
          return true;
        } else {
          return false;
        }
      }

      my_appointment_info(JID) {
        this.route.navigate(['./navigation', JID]);
      }

    };

    MyAppointmentsPage.ctorParameters = () => [{
      type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
    }, {
      type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
    }, {
      type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]
    }];

    MyAppointmentsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-my-appointments',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./my-appointments.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/my-appointments/my-appointments.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./my-appointments.page.scss */
      "./src/app/my-appointments/my-appointments.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]])], MyAppointmentsPage);
    /***/
  }
}]);
//# sourceMappingURL=my-appointments-my-appointments-module-es5.js.map