(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["navigation-navigation-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/navigation/navigation.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/navigation/navigation.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n\t<ion-toolbar>\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-back-button text=\"\" icon=\"chevron-back-outline\"></ion-back-button>\n\t\t</ion-buttons>\n\t\t<ion-title>\n\n\t\t</ion-title>\n\t</ion-toolbar>\n\t<h1>\n\t\tAppointment Information\n\t\t<small>{{'booked_on' | translate}} {{currentRequest?.provider_date| date:'EEEE, MMMM d, y'}}</small>\n\t</h1>\n</ion-header>\n\n\n<ion-content fullscreen>\n\t<div class=\"form\">\n\t\t<ion-list lines=\"none\">\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>{{'your_appointment_for' | translate}}</h2>\n\t\t\t\t\t<h3>{{job?.category}}</h3>\n\t\t\t\t\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>Name</h2>\n\t\t\t\t\t<h3>{{name}}</h3> \n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>{{'date_time' | translate}}</h2>\n\t\t\t\t\t<h3>{{currentRequest?.provider_date| date:'EEEE, MMMM d, y'}}</h3> \n\t\t\t\t\t<h3> {{currentRequest?.provider_time}}</h3>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>{{'location_at' | translate}}</h2>\n\t\t\t\t\t<h3>{{job?.adrress_type}}</h3>\n\t\t\t\t\t<ion-textarea auto-grow=\"true\" rows=\"1\" readonly>{{job?.address}}</ion-textarea>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\t\t\t\n\t\t\t\n\t\t\t\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_inner\">\n\t\t\t\t\t<h2>Additional Information</h2>\n\t\t\t\t\t<ion-textarea auto-grow=\"true\" rows=\"1\" readonly>{{job?.information}}</ion-textarea>\n\t\t\t\t\t\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t \n\t\t</ion-list>\n\t</div>\n</ion-content>\n\n<ion-footer class=\"ion-no-border d-flex\">\n\t <ion-row>\n\t\t <ion-col size=\"12\">\n\t\t\t <ion-button *ngIf=\"checkrequest(job?.status)\" size=\"large\" fill=\"outline\" shape=\"block\" class=\"btn end\">{{'Start Navigation' | translate}}</ion-button>\n\t\t\t \n\t\t </ion-col>\n\t\t \n\t\t \n\t</ion-row>\n\t\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/navigation/navigation-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/navigation/navigation-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: NavigationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationPageRoutingModule", function() { return NavigationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _navigation_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./navigation.page */ "./src/app/navigation/navigation.page.ts");




const routes = [
    {
        path: '',
        component: _navigation_page__WEBPACK_IMPORTED_MODULE_3__["NavigationPage"]
    }
];
let NavigationPageRoutingModule = class NavigationPageRoutingModule {
};
NavigationPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NavigationPageRoutingModule);



/***/ }),

/***/ "./src/app/navigation/navigation.module.ts":
/*!*************************************************!*\
  !*** ./src/app/navigation/navigation.module.ts ***!
  \*************************************************/
/*! exports provided: NavigationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationPageModule", function() { return NavigationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _navigation_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./navigation-routing.module */ "./src/app/navigation/navigation-routing.module.ts");
/* harmony import */ var _navigation_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./navigation.page */ "./src/app/navigation/navigation.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");








let NavigationPageModule = class NavigationPageModule {
};
NavigationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _navigation_routing_module__WEBPACK_IMPORTED_MODULE_5__["NavigationPageRoutingModule"]
        ],
        declarations: [_navigation_page__WEBPACK_IMPORTED_MODULE_6__["NavigationPage"]]
    })
], NavigationPageModule);



/***/ }),

/***/ "./src/app/navigation/navigation.page.scss":
/*!*************************************************!*\
  !*** ./src/app/navigation/navigation.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header h1 small {\n  display: block;\n  color: var(--text-light);\n  font-weight: 400;\n  font-size: 0.95rem;\n  opacity: 0.7;\n  padding-top: 6px;\n}\n\n.form {\n  padding-top: 50px;\n}\n\n.form ion-textarea {\n  background-color: transparent;\n}\n\n.form ion-list ion-item {\n  margin-bottom: 25px;\n}\n\n.form ion-list ion-item h2 {\n  margin: 0;\n  color: var(--text-light);\n  font-size: 1.15rem;\n  padding-bottom: 10px;\n  font-weight: 700;\n  opacity: 0.7;\n}\n\n.form ion-list ion-item h3 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 700;\n  font-size: 1.35rem;\n  padding-bottom: 5px;\n}\n\n.form ion-list ion-item h4 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 500;\n  font-size: 1.15rem;\n  line-height: 23px;\n}\n\n.form ion-list ion-item ion-textarea {\n  font-size: 1rem !important;\n  letter-spacing: 0.5px !important;\n}\n\n.form ion-list ion-item ion-label {\n  color: var(--text-dark) !important;\n  font-weight: 700 !important;\n  font-size: 1.35rem !important;\n}\n\nion-footer {\n  background: var(--white);\n  padding: 10px 10px 20px 10px;\n}\n\nion-footer ion-row {\n  width: 100%;\n}\n\nion-footer ion-row ion-col {\n  padding: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmF2aWdhdGlvbi9FOlxcRVxcc2VydmljZVxcZmlyZWJhc2UgMDRcXHByb3ZpZGVyLWFwcC1idWRkaGluaS9zcmNcXGFwcFxcbmF2aWdhdGlvblxcbmF2aWdhdGlvbi5wYWdlLnNjc3MiLCJzcmMvYXBwL25hdmlnYXRpb24vbmF2aWdhdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUU7RUFDQyxjQUFBO0VBQ0Esd0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FDREg7O0FETUE7RUFDQyxpQkFBQTtBQ0hEOztBREtDO0VBQ0MsNkJBQUE7QUNIRjs7QURNRTtFQUNDLG1CQUFBO0FDSkg7O0FETUc7RUFDQyxTQUFBO0VBQ0Esd0JBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FDSko7O0FET0c7RUFDQyxTQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNMSjs7QURRRztFQUNDLFNBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ05KOztBRFNHO0VBQ0MsMEJBQUE7RUFDQSxnQ0FBQTtBQ1BKOztBRFVHO0VBQ0Msa0NBQUE7RUFDQSwyQkFBQTtFQUNBLDZCQUFBO0FDUko7O0FEY0E7RUFDQyx3QkFBQTtFQUNBLDRCQUFBO0FDWEQ7O0FEYUM7RUFDQyxXQUFBO0FDWEY7O0FEYUU7RUFDQyxlQUFBO0FDWEgiLCJmaWxlIjoic3JjL2FwcC9uYXZpZ2F0aW9uL25hdmlnYXRpb24ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlciB7XHJcblx0aDEge1xyXG5cdFx0c21hbGwge1xyXG5cdFx0XHRkaXNwbGF5OiBibG9jaztcclxuXHRcdFx0Y29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xyXG5cdFx0XHRmb250LXdlaWdodDogNDAwO1xyXG5cdFx0XHRmb250LXNpemU6IC45NXJlbTtcclxuXHRcdFx0b3BhY2l0eTogLjc7XHJcblx0XHRcdHBhZGRpbmctdG9wOiA2cHg7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG4uZm9ybSB7XHJcblx0cGFkZGluZy10b3A6IDUwcHg7XHJcblxyXG5cdGlvbi10ZXh0YXJlYXtcclxuXHRcdGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG5cdH1cclxuXHRpb24tbGlzdCB7XHJcblx0XHRpb24taXRlbSB7XHJcblx0XHRcdG1hcmdpbi1ib3R0b206IDI1cHg7XHJcblxyXG5cdFx0XHRoMiB7XHJcblx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcclxuXHRcdFx0XHRmb250LXNpemU6IDEuMTVyZW07XHJcblx0XHRcdFx0cGFkZGluZy1ib3R0b206IDEwcHg7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IDcwMDtcclxuXHRcdFx0XHRvcGFjaXR5OiAuNztcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aDMge1xyXG5cdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcclxuXHRcdFx0XHRmb250LXdlaWdodDogNzAwO1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMS4zNXJlbTtcclxuXHRcdFx0XHRwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRoNCB7XHJcblx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xyXG5cdFx0XHRcdGZvbnQtd2VpZ2h0OiA1MDA7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxLjE1cmVtO1xyXG5cdFx0XHRcdGxpbmUtaGVpZ2h0OiAyM3B4O1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpb24tdGV4dGFyZWEge1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMXJlbSAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdGxldHRlci1zcGFjaW5nOiAuNXB4ICFpbXBvcnRhbnQ7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlvbi1sYWJlbCB7XHJcblx0XHRcdFx0Y29sb3I6IHZhcigtLXRleHQtZGFyaykgIWltcG9ydGFudDtcclxuXHRcdFx0XHRmb250LXdlaWdodDogNzAwICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxLjM1cmVtICFpbXBvcnRhbnQ7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbmlvbi1mb290ZXIge1xyXG5cdGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcclxuXHRwYWRkaW5nOiAxMHB4IDEwcHggMjBweCAxMHB4O1xyXG5cclxuXHRpb24tcm93IHtcclxuXHRcdHdpZHRoOiAxMDAlOyBcclxuXHJcblx0XHRpb24tY29sIHtcclxuXHRcdFx0cGFkZGluZzogMCAxMHB4O1xyXG5cdFx0fVxyXG5cdH1cclxufSIsImlvbi1oZWFkZXIgaDEgc21hbGwge1xuICBkaXNwbGF5OiBibG9jaztcbiAgY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuICBmb250LXdlaWdodDogNDAwO1xuICBmb250LXNpemU6IDAuOTVyZW07XG4gIG9wYWNpdHk6IDAuNztcbiAgcGFkZGluZy10b3A6IDZweDtcbn1cblxuLmZvcm0ge1xuICBwYWRkaW5nLXRvcDogNTBweDtcbn1cbi5mb3JtIGlvbi10ZXh0YXJlYSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWl0ZW0ge1xuICBtYXJnaW4tYm90dG9tOiAyNXB4O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWl0ZW0gaDIge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcbiAgZm9udC1zaXplOiAxLjE1cmVtO1xuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgb3BhY2l0eTogMC43O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWl0ZW0gaDMge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xuICBmb250LXdlaWdodDogNzAwO1xuICBmb250LXNpemU6IDEuMzVyZW07XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4uZm9ybSBpb24tbGlzdCBpb24taXRlbSBoNCB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtZGFyayk7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGZvbnQtc2l6ZTogMS4xNXJlbTtcbiAgbGluZS1oZWlnaHQ6IDIzcHg7XG59XG4uZm9ybSBpb24tbGlzdCBpb24taXRlbSBpb24tdGV4dGFyZWEge1xuICBmb250LXNpemU6IDFyZW0gIWltcG9ydGFudDtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4ICFpbXBvcnRhbnQ7XG59XG4uZm9ybSBpb24tbGlzdCBpb24taXRlbSBpb24tbGFiZWwge1xuICBjb2xvcjogdmFyKC0tdGV4dC1kYXJrKSAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogNzAwICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMS4zNXJlbSAhaW1wb3J0YW50O1xufVxuXG5pb24tZm9vdGVyIHtcbiAgYmFja2dyb3VuZDogdmFyKC0td2hpdGUpO1xuICBwYWRkaW5nOiAxMHB4IDEwcHggMjBweCAxMHB4O1xufVxuaW9uLWZvb3RlciBpb24tcm93IHtcbiAgd2lkdGg6IDEwMCU7XG59XG5pb24tZm9vdGVyIGlvbi1yb3cgaW9uLWNvbCB7XG4gIHBhZGRpbmc6IDAgMTBweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/navigation/navigation.page.ts":
/*!***********************************************!*\
  !*** ./src/app/navigation/navigation.page.ts ***!
  \***********************************************/
/*! exports provided: NavigationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationPage", function() { return NavigationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/fesm2015/angular-fire-database.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");





let NavigationPage = class NavigationPage {
    constructor(db, alertController, activatedRoute) {
        this.db = db;
        this.alertController = alertController;
        this.activatedRoute = activatedRoute;
    }
    ngOnInit() {
        this.JID = this.activatedRoute.snapshot.paramMap.get('JID');
        this.loadJobDetails();
    }
    loadJobDetails() {
        if (this.JID) {
            console.log(this.JID);
            window.localStorage.setItem("myjobid", this.JID);
            this.db.database.ref("/jobs/" + this.JID).on('value', (snapshot) => {
                this.job = snapshot.val();
                console.log(this.job);
                let userUID = this.job.UID;
                this.loadUserDetails(userUID);
                let currentUID = window.localStorage.getItem('UID');
                this.currentRequest = this.job.requests.filter((request) => {
                    return currentUID == request.provider_UID;
                })[0];
                window.localStorage.setItem("jobInfo", JSON.stringify(this.job));
                this.requests = this.job.requests;
                console.log(this.requests);
                this.StartedRequest = this.requests.find(request => request.status == 'started');
                this.checkrequest(this.StartedRequest);
            });
        }
        else {
            this.job = JSON.parse(window.localStorage.getItem("jobInfo"));
            console.log("params not working");
        }
    }
    checkrequest(requests) {
        if (requests == "started") {
            return true;
        }
        return false;
    }
    presentAlert() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: "Alert",
                message: "No Job found.",
                buttons: ["OK"],
            });
            yield alert.present();
        });
    }
    loadUserDetails(UID) {
        this.db.database.ref("/users/" + UID).on('value', (snapshot) => {
            this.user = snapshot.val();
            this.name = this.user.name;
        });
    }
};
NavigationPage.ctorParameters = () => [
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
];
NavigationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-navigation',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./navigation.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/navigation/navigation.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./navigation.page.scss */ "./src/app/navigation/navigation.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
], NavigationPage);



/***/ })

}]);
//# sourceMappingURL=navigation-navigation-module-es2015.js.map