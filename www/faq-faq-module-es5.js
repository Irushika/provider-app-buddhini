(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["faq-faq-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/faq/faq.page.html":
  /*!*************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/faq/faq.page.html ***!
    \*************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppFaqFaqPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n\t<ion-toolbar>\r\n\t\t<ion-buttons slot=\"start\">\r\n\t\t\t<ion-menu-button></ion-menu-button>\r\n\t\t</ion-buttons>\r\n\t\t<ion-title>\r\n\t\t</ion-title>\r\n\t</ion-toolbar>\r\n\t<h1>{{'faq' | translate}}</h1>\r\n</ion-header>\r\n\r\n\r\n<ion-content>\r\n\t<ion-list lines=\"none\">\r\n\t\t<ion-item [ngClass]=\"faqExpand1 ? 'active' : '' \" (click)=\"faqExpandToggle1()\">\r\n\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t<h2>1. How to Login to App?</h2>\r\n\t\t\t\t<div class=\"text_box\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</ion-item>\r\n\t\t\r\n\t\t<ion-item [ngClass]=\"faqExpand2 ? 'active' : '' \" (click)=\"faqExpandToggle2()\">\r\n\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t<h2>2. How  to book an Appointment?</h2>\r\n\t\t\t\t<div class=\"text_box\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</ion-item>\r\n\t\t\r\n\t\t<ion-item [ngClass]=\"faqExpand3 ? 'active' : '' \" (click)=\"faqExpandToggle3()\">\r\n\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t<h2>3. How to cancel an Appointment?</h2>\r\n\t\t\t\t<div class=\"text_box\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</ion-item>\r\n\t\t\r\n\t\t<ion-item [ngClass]=\"faqExpand4 ? 'active' : '' \" (click)=\"faqExpandToggle4()\">\r\n\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t<h2>4.What if I failed to book?</h2>\r\n\t\t\t\t<div class=\"text_box\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</ion-item>\r\n\t\t\r\n\t\t<ion-item [ngClass]=\"faqExpand5 ? 'active' : '' \" (click)=\"faqExpandToggle5()\">\r\n\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t<h2>5. How  to payment?</h2>\r\n\t\t\t\t<div class=\"text_box\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</ion-item>\r\n\t\t\r\n\t\t\r\n\t\t<ion-item [ngClass]=\"faqExpand6 ? 'active' : '' \" (click)=\"faqExpandToggle6()\">\r\n\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t<h2>6. Payment modes available?</h2>\r\n\t\t\t\t<div class=\"text_box\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</ion-item>\r\n\t\t\r\n\t\t\r\n\t\t<ion-item [ngClass]=\"faqExpand7 ? 'active' : '' \" (click)=\"faqExpandToggle7()\">\r\n\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t<h2>7. What if failed to book?</h2>\r\n\t\t\t\t<div class=\"text_box\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</ion-item>\r\n\t\t\r\n\t\t\r\n\t\t<ion-item [ngClass]=\"faqExpand8 ? 'active' : '' \" (click)=\"faqExpandToggle8()\">\r\n\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t<h2>8. How  to Payment?</h2>\r\n\t\t\t\t<div class=\"text_box\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</ion-item>\r\n\t\t\r\n\t\t\r\n\t\t<ion-item [ngClass]=\"faqExpand9 ? 'active' : '' \" (click)=\"faqExpandToggle9()\">\r\n\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t<h2>9. Payment modes available?</h2>\r\n\t\t\t\t<div class=\"text_box\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</ion-item>\r\n\t\t\r\n\t\t\r\n\t\t<ion-item [ngClass]=\"faqExpand10 ? 'active' : '' \" (click)=\"faqExpandToggle10()\">\r\n\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t<h2>2. What if failed to book?</h2>\r\n\t\t\t\t<div class=\"text_box\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</ion-item>\r\n\t</ion-list>\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/faq/faq-routing.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/faq/faq-routing.module.ts ***!
    \*******************************************/

  /*! exports provided: FaqPageRoutingModule */

  /***/
  function srcAppFaqFaqRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FaqPageRoutingModule", function () {
      return FaqPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _faq_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./faq.page */
    "./src/app/faq/faq.page.ts");

    const routes = [{
      path: '',
      component: _faq_page__WEBPACK_IMPORTED_MODULE_3__["FaqPage"]
    }];
    let FaqPageRoutingModule = class FaqPageRoutingModule {};
    FaqPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], FaqPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/faq/faq.module.ts":
  /*!***********************************!*\
    !*** ./src/app/faq/faq.module.ts ***!
    \***********************************/

  /*! exports provided: FaqPageModule */

  /***/
  function srcAppFaqFaqModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FaqPageModule", function () {
      return FaqPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _faq_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./faq-routing.module */
    "./src/app/faq/faq-routing.module.ts");
    /* harmony import */


    var _faq_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./faq.page */
    "./src/app/faq/faq.page.ts");

    let FaqPageModule = class FaqPageModule {};
    FaqPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"], _faq_routing_module__WEBPACK_IMPORTED_MODULE_6__["FaqPageRoutingModule"]],
      declarations: [_faq_page__WEBPACK_IMPORTED_MODULE_7__["FaqPage"]]
    })], FaqPageModule);
    /***/
  },

  /***/
  "./src/app/faq/faq.page.scss":
  /*!***********************************!*\
    !*** ./src/app/faq/faq.page.scss ***!
    \***********************************/

  /*! exports provided: default */

  /***/
  function srcAppFaqFaqPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-list {\n  background: var(--transparent) !important;\n  margin: 0;\n  padding: 0;\n  padding-top: 26px;\n}\nion-list ion-item {\n  padding: 5px 23px;\n  min-height: unset !important;\n  --inner-padding-end: 0px;\n  --inner-min-height: unset !important;\n  --padding-start: 0;\n  --highligh-color-focused: var(--transparent) !important;\n  margin-bottom: 5px;\n  background: var(--white);\n  -webkit-transition: all 0.5s;\n  transition: all 0.5s;\n  --min-height: unset !important;\n}\nion-list ion-item .item_inner h2 {\n  margin: 0;\n  margin: 0;\n  font-size: 1.22rem;\n  font-weight: 600;\n  padding: 10px 0;\n}\nion-list ion-item .item_inner .text_box {\n  padding-left: 20px;\n  height: 0px;\n  overflow: hidden;\n  -webkit-transition: all 0.5s;\n  transition: all 0.5s;\n}\nion-list ion-item .item_inner .text_box p {\n  margin: 0;\n  font-size: 1.1rem;\n  font-weight: 500;\n  padding-bottom: 13px;\n  line-height: 23px;\n  padding-top: 4px;\n}\nion-list ion-item.active {\n  -webkit-transition: all 0.5s;\n  transition: all 0.5s;\n}\nion-list ion-item.active .item_inner .text_box {\n  height: auto;\n  -webkit-transition: all 0.5s;\n  transition: all 0.5s;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmFxL0U6XFxFXFxzZXJ2aWNlXFxmaXJlYmFzZSAwNFxccHJvdmlkZXItYXBwLWJ1ZGRoaW5pL3NyY1xcYXBwXFxmYXFcXGZhcS5wYWdlLnNjc3MiLCJzcmMvYXBwL2ZhcS9mYXEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MseUNBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0FDQ0Q7QURDQztFQUNDLGlCQUFBO0VBQ0EsNEJBQUE7RUFDQSx3QkFBQTtFQUNBLG9DQUFBO0VBQ0Esa0JBQUE7RUFDQSx1REFBQTtFQUNBLGtCQUFBO0VBQ0Esd0JBQUE7RUFDQSw0QkFBQTtFQUFBLG9CQUFBO0VBQ0EsOEJBQUE7QUNDRjtBREVHO0VBQ0MsU0FBQTtFQUNBLFNBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQ0FKO0FER0c7RUFDQyxrQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLDRCQUFBO0VBQUEsb0JBQUE7QUNESjtBREdJO0VBQ0MsU0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUNETDtBRE9FO0VBQ0MsNEJBQUE7RUFBQSxvQkFBQTtBQ0xIO0FET0c7RUFDQyxZQUFBO0VBQ0EsNEJBQUE7RUFBQSxvQkFBQTtBQ0xKIiwiZmlsZSI6InNyYy9hcHAvZmFxL2ZhcS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tbGlzdCB7XHJcblx0YmFja2dyb3VuZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XHJcblx0bWFyZ2luOiAwO1xyXG5cdHBhZGRpbmc6IDA7XHJcblx0cGFkZGluZy10b3A6IDI2cHg7XHJcblxyXG5cdGlvbi1pdGVtIHtcclxuXHRcdHBhZGRpbmc6IDVweCAyM3B4O1xyXG5cdFx0bWluLWhlaWdodDogdW5zZXQgIWltcG9ydGFudDtcclxuXHRcdC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcclxuXHRcdC0taW5uZXItbWluLWhlaWdodDogdW5zZXQgIWltcG9ydGFudDtcclxuXHRcdC0tcGFkZGluZy1zdGFydDogMDtcclxuXHRcdC0taGlnaGxpZ2gtY29sb3ItZm9jdXNlZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XHJcblx0XHRtYXJnaW4tYm90dG9tOiA1cHg7XHJcblx0XHRiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XHJcblx0XHR0cmFuc2l0aW9uOiBhbGwgLjVzO1xyXG5cdFx0LS1taW4taGVpZ2h0OiB1bnNldCAhaW1wb3J0YW50O1xyXG5cclxuXHRcdC5pdGVtX2lubmVyIHtcclxuXHRcdFx0aDIge1xyXG5cdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxLjIycmVtO1xyXG5cdFx0XHRcdGZvbnQtd2VpZ2h0OiA2MDA7XHJcblx0XHRcdFx0cGFkZGluZzogMTBweCAwO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQudGV4dF9ib3gge1xyXG5cdFx0XHRcdHBhZGRpbmctbGVmdDogMjBweDtcclxuXHRcdFx0XHRoZWlnaHQ6IDBweDtcclxuXHRcdFx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdFx0XHRcdHRyYW5zaXRpb246IGFsbCAuNXM7XHJcblxyXG5cdFx0XHRcdHAge1xyXG5cdFx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAxLjFyZW07XHJcblx0XHRcdFx0XHRmb250LXdlaWdodDogNTAwO1xyXG5cdFx0XHRcdFx0cGFkZGluZy1ib3R0b206IDEzcHg7XHJcblx0XHRcdFx0XHRsaW5lLWhlaWdodDogMjNweDtcclxuXHRcdFx0XHRcdHBhZGRpbmctdG9wOiA0cHg7XHJcblxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdCYuYWN0aXZlIHtcclxuXHRcdFx0dHJhbnNpdGlvbjogYWxsIC41cztcclxuXHJcblx0XHRcdC5pdGVtX2lubmVyIC50ZXh0X2JveCB7XHJcblx0XHRcdFx0aGVpZ2h0OiBhdXRvO1xyXG5cdFx0XHRcdHRyYW5zaXRpb246IGFsbCAuNXM7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuIiwiaW9uLWxpc3Qge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBwYWRkaW5nLXRvcDogMjZweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIHtcbiAgcGFkZGluZzogNXB4IDIzcHg7XG4gIG1pbi1oZWlnaHQ6IHVuc2V0ICFpbXBvcnRhbnQ7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcbiAgLS1pbm5lci1taW4taGVpZ2h0OiB1bnNldCAhaW1wb3J0YW50O1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIC0taGlnaGxpZ2gtY29sb3ItZm9jdXNlZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgYmFja2dyb3VuZDogdmFyKC0td2hpdGUpO1xuICB0cmFuc2l0aW9uOiBhbGwgMC41cztcbiAgLS1taW4taGVpZ2h0OiB1bnNldCAhaW1wb3J0YW50O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgaDIge1xuICBtYXJnaW46IDA7XG4gIG1hcmdpbjogMDtcbiAgZm9udC1zaXplOiAxLjIycmVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBwYWRkaW5nOiAxMHB4IDA7XG59XG5pb24tbGlzdCBpb24taXRlbSAuaXRlbV9pbm5lciAudGV4dF9ib3gge1xuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gIGhlaWdodDogMHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB0cmFuc2l0aW9uOiBhbGwgMC41cztcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC50ZXh0X2JveCBwIHtcbiAgbWFyZ2luOiAwO1xuICBmb250LXNpemU6IDEuMXJlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgcGFkZGluZy1ib3R0b206IDEzcHg7XG4gIGxpbmUtaGVpZ2h0OiAyM3B4O1xuICBwYWRkaW5nLXRvcDogNHB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0uYWN0aXZlIHtcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXM7XG59XG5pb24tbGlzdCBpb24taXRlbS5hY3RpdmUgLml0ZW1faW5uZXIgLnRleHRfYm94IHtcbiAgaGVpZ2h0OiBhdXRvO1xuICB0cmFuc2l0aW9uOiBhbGwgMC41cztcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/faq/faq.page.ts":
  /*!*********************************!*\
    !*** ./src/app/faq/faq.page.ts ***!
    \*********************************/

  /*! exports provided: FaqPage */

  /***/
  function srcAppFaqFaqPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FaqPage", function () {
      return FaqPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    let FaqPage = class FaqPage {
      constructor() {}

      ngOnInit() {}

      reset() {
        this.faqExpand1 = false;
        this.faqExpand2 = false;
        this.faqExpand3 = false;
        this.faqExpand4 = false;
        this.faqExpand5 = false;
        this.faqExpand6 = false;
        this.faqExpand7 = false;
        this.faqExpand8 = false;
        this.faqExpand9 = false;
        this.faqExpand10 = false;
      }

      faqExpandToggle1() {
        this.reset();
        this.faqExpand1 = !this.faqExpand1;
      }

      faqExpandToggle2() {
        this.reset();
        this.faqExpand2 = !this.faqExpand2;
      }

      faqExpandToggle3() {
        this.reset();
        this.faqExpand3 = !this.faqExpand3;
      }

      faqExpandToggle4() {
        this.reset();
        this.faqExpand4 = !this.faqExpand4;
      }

      faqExpandToggle5() {
        this.reset();
        this.faqExpand5 = !this.faqExpand5;
      }

      faqExpandToggle6() {
        this.reset();
        this.faqExpand6 = !this.faqExpand6;
      }

      faqExpandToggle7() {
        this.reset();
        this.faqExpand7 = !this.faqExpand7;
      }

      faqExpandToggle8() {
        this.reset();
        this.faqExpand8 = !this.faqExpand8;
      }

      faqExpandToggle9() {
        this.reset();
        this.faqExpand9 = !this.faqExpand9;
      }

      faqExpandToggle10() {
        this.reset();
        this.faqExpand10 = !this.faqExpand10;
      }

    };
    FaqPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-faq',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./faq.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/faq/faq.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./faq.page.scss */
      "./src/app/faq/faq.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], FaqPage);
    /***/
  }
}]);
//# sourceMappingURL=faq-faq-module-es5.js.map