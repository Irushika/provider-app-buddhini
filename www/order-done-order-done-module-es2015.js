(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["order-done-order-done-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/order-done/order-done.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/order-done/order-done.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--\r\n<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>order-done</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n-->\r\n\r\n<ion-content>\r\n\t<div class=\"container ion-text-center\">\r\n\t\t<div class=\"img_box\">\r\n\t\t\t<img src=\"assets/images/successful.png\">\r\n\t\t</div>\r\n\t\t<h1>Proposal<br>Submited</h1>\r\n\t\t<h2>Your Proposal has been<br>Submitted for client review.</h2>\r\n\t</div>\r\n</ion-content>\r\n\r\n<ion-footer class=\"ion-no-border\">\r\n\t\t<ion-button size=\"large\" shape=\"block\" class=\"btn end\" (click)=\"my_appointments()\">{{'View Appointments' | translate}}</ion-button>\r\n\t\t<ion-button size=\"large\" fill=\"outline\" shape=\"block\"  class=\"btn end\" (click)=\"home()\">{{'home' | translate}}</ion-button>\r\n</ion-footer>");

/***/ }),

/***/ "./src/app/order-done/order-done-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/order-done/order-done-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: OrderDonePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDonePageRoutingModule", function() { return OrderDonePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _order_done_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-done.page */ "./src/app/order-done/order-done.page.ts");




const routes = [
    {
        path: '',
        component: _order_done_page__WEBPACK_IMPORTED_MODULE_3__["OrderDonePage"]
    }
];
let OrderDonePageRoutingModule = class OrderDonePageRoutingModule {
};
OrderDonePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OrderDonePageRoutingModule);



/***/ }),

/***/ "./src/app/order-done/order-done.module.ts":
/*!*************************************************!*\
  !*** ./src/app/order-done/order-done.module.ts ***!
  \*************************************************/
/*! exports provided: OrderDonePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDonePageModule", function() { return OrderDonePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _order_done_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./order-done-routing.module */ "./src/app/order-done/order-done-routing.module.ts");
/* harmony import */ var _order_done_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./order-done.page */ "./src/app/order-done/order-done.page.ts");








let OrderDonePageModule = class OrderDonePageModule {
};
OrderDonePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"],
            _order_done_routing_module__WEBPACK_IMPORTED_MODULE_6__["OrderDonePageRoutingModule"]
        ],
        declarations: [_order_done_page__WEBPACK_IMPORTED_MODULE_7__["OrderDonePage"]]
    })
], OrderDonePageModule);



/***/ }),

/***/ "./src/app/order-done/order-done.page.scss":
/*!*************************************************!*\
  !*** ./src/app/order-done/order-done.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  padding: 0 20px;\n  position: absolute;\n  top: -30px;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  margin: auto;\n  width: 100%;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n}\n.container .img_box {\n  width: 250px;\n  margin: 0 auto;\n}\n.container h1 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 700;\n  font-size: 2rem;\n  padding-bottom: 35px;\n  margin-bottom: 10px;\n}\n.container h2 {\n  margin: 0;\n  color: var(--text-light);\n  font-weight: 600;\n  font-size: 1.15rem;\n  opacity: 0.7;\n}\nion-footer {\n  padding: 0 20px 8px 20px;\n}\nion-footer .button.btn {\n  margin-bottom: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvb3JkZXItZG9uZS9FOlxcRVxcc2VydmljZVxcZmlyZWJhc2UgMDRcXHByb3ZpZGVyLWFwcC1idWRkaGluaS9zcmNcXGFwcFxcb3JkZXItZG9uZVxcb3JkZXItZG9uZS5wYWdlLnNjc3MiLCJzcmMvYXBwL29yZGVyLWRvbmUvb3JkZXItZG9uZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSwyQkFBQTtFQUFBLHdCQUFBO0VBQUEsbUJBQUE7QUNDRDtBRENDO0VBQ0MsWUFBQTtFQUNBLGNBQUE7QUNDRjtBREVDO0VBQ0MsU0FBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQkFBQTtBQ0FGO0FER0M7RUFDQyxTQUFBO0VBQ0Esd0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ0RGO0FES0E7RUFDQyx3QkFBQTtBQ0ZEO0FESUM7RUFDQyxtQkFBQTtBQ0ZGIiwiZmlsZSI6InNyYy9hcHAvb3JkZXItZG9uZS9vcmRlci1kb25lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXIge1xyXG5cdHBhZGRpbmc6IDAgMjBweDtcclxuXHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0dG9wOiAtMzBweDtcclxuXHRib3R0b206IDA7XHJcblx0bGVmdDogMDtcclxuXHRyaWdodDogMDtcclxuXHRtYXJnaW46IGF1dG87XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0aGVpZ2h0OiBmaXQtY29udGVudDtcclxuXHJcblx0LmltZ19ib3gge1xyXG5cdFx0d2lkdGg6IDI1MHB4O1xyXG5cdFx0bWFyZ2luOiAwIGF1dG87XHJcblx0fVxyXG5cclxuXHRoMSB7XHJcblx0XHRtYXJnaW46IDA7XHJcblx0XHRjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcclxuXHRcdGZvbnQtd2VpZ2h0OiA3MDA7XHJcblx0XHRmb250LXNpemU6IDJyZW07XHJcblx0XHRwYWRkaW5nLWJvdHRvbTogMzVweDtcclxuXHRcdG1hcmdpbi1ib3R0b206IDEwcHg7XHJcblx0fVxyXG5cclxuXHRoMiB7XHJcblx0XHRtYXJnaW46IDA7XHJcblx0XHRjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XHJcblx0XHRmb250LXdlaWdodDogNjAwO1xyXG5cdFx0Zm9udC1zaXplOiAxLjE1cmVtO1xyXG5cdFx0b3BhY2l0eTogLjc7XHJcblx0fVxyXG59XHJcblxyXG5pb24tZm9vdGVyIHtcclxuXHRwYWRkaW5nOiAwIDIwcHggOHB4IDIwcHg7XHJcblxyXG5cdC5idXR0b24uYnRuIHtcclxuXHRcdG1hcmdpbi1ib3R0b206IDIwcHg7XHJcblx0fVxyXG59IiwiLmNvbnRhaW5lciB7XG4gIHBhZGRpbmc6IDAgMjBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IC0zMHB4O1xuICBib3R0b206IDA7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICBtYXJnaW46IGF1dG87XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGZpdC1jb250ZW50O1xufVxuLmNvbnRhaW5lciAuaW1nX2JveCB7XG4gIHdpZHRoOiAyNTBweDtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG4uY29udGFpbmVyIGgxIHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgZm9udC1zaXplOiAycmVtO1xuICBwYWRkaW5nLWJvdHRvbTogMzVweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cbi5jb250YWluZXIgaDIge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAxLjE1cmVtO1xuICBvcGFjaXR5OiAwLjc7XG59XG5cbmlvbi1mb290ZXIge1xuICBwYWRkaW5nOiAwIDIwcHggOHB4IDIwcHg7XG59XG5pb24tZm9vdGVyIC5idXR0b24uYnRuIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/order-done/order-done.page.ts":
/*!***********************************************!*\
  !*** ./src/app/order-done/order-done.page.ts ***!
  \***********************************************/
/*! exports provided: OrderDonePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDonePage", function() { return OrderDonePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let OrderDonePage = class OrderDonePage {
    constructor(navCtrl, route) {
        this.navCtrl = navCtrl;
        this.route = route;
    }
    ngOnInit() {
    }
    home() {
        this.navCtrl.navigateRoot(['./new-appoinment']);
    }
    my_appointments() {
        this.navCtrl.navigateRoot(['./my-appointments']);
    }
};
OrderDonePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
OrderDonePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order-done',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./order-done.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/order-done/order-done.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./order-done.page.scss */ "./src/app/order-done/order-done.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], OrderDonePage);



/***/ })

}]);
//# sourceMappingURL=order-done-order-done-module-es2015.js.map