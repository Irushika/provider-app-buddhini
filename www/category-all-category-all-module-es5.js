(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["category-all-category-all-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/category-all/category-all.page.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/category-all/category-all.page.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCategoryAllCategoryAllPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n</ion-header>\n\n<ion-content>\n\t<div class=\"services\">\n\t\t\n\t\t<h2>{{'All Categories' | translate}}</h2>\n\t\t\n\t\t<div class=\"container\">\n\t\t\t<div class=\"item\" *ngFor=\"let cat of categories;index as i\" >\n\t\t\t  <div \n\t\t\t\t    class=\"container-wrapper\" [style.background-image]=\"getUrl(cat.img)\"\n\t\t\t\t    style=\"background-size: cover;background-position: center;\"\n\t\t\t\t\t(click)=\"adddetails()\"\n\t\t\t  >\n\t\t\t  <div class=\"overlay\">\n\t\t\t\t  <div class=\"service-title\">{{cat?.name}}</div>\n\t\t\t\t</div>\n        \n\t\t\t  </div>\n\t\t  </div>\n\t  </div>\n  </div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/category-all/category-all-routing.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/category-all/category-all-routing.module.ts ***!
    \*************************************************************/

  /*! exports provided: CategoryAllPageRoutingModule */

  /***/
  function srcAppCategoryAllCategoryAllRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CategoryAllPageRoutingModule", function () {
      return CategoryAllPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _category_all_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./category-all.page */
    "./src/app/category-all/category-all.page.ts");

    const routes = [{
      path: '',
      component: _category_all_page__WEBPACK_IMPORTED_MODULE_3__["CategoryAllPage"]
    }];
    let CategoryAllPageRoutingModule = class CategoryAllPageRoutingModule {};
    CategoryAllPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], CategoryAllPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/category-all/category-all.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/category-all/category-all.module.ts ***!
    \*****************************************************/

  /*! exports provided: CategoryAllPageModule */

  /***/
  function srcAppCategoryAllCategoryAllModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CategoryAllPageModule", function () {
      return CategoryAllPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _category_all_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./category-all-routing.module */
    "./src/app/category-all/category-all-routing.module.ts");
    /* harmony import */


    var _category_all_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./category-all.page */
    "./src/app/category-all/category-all.page.ts");

    let CategoryAllPageModule = class CategoryAllPageModule {};
    CategoryAllPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"], _category_all_routing_module__WEBPACK_IMPORTED_MODULE_6__["CategoryAllPageRoutingModule"]],
      declarations: [_category_all_page__WEBPACK_IMPORTED_MODULE_7__["CategoryAllPage"]]
    })], CategoryAllPageModule);
    /***/
  },

  /***/
  "./src/app/category-all/category-all.page.scss":
  /*!*****************************************************!*\
    !*** ./src/app/category-all/category-all.page.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppCategoryAllCategoryAllPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-header ion-toolbar ion-buttons ion-menu-button {\n  color: var(--white) !important;\n}\nion-header ion-toolbar ion-title {\n  position: absolute !important;\n  width: 100%;\n  top: 0;\n  left: 0;\n  padding: 0 15px !important;\n  text-align: center !important;\n}\nion-header ion-toolbar ion-title img {\n  width: 110px;\n  position: relative;\n  top: 5px;\n}\nion-header .header_bg {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100px;\n  overflow: hidden;\n}\nion-header .search_box {\n  background: var(--input_filed_bg);\n  border-radius: 4px;\n  margin: 0 auto;\n  padding: 0 15px;\n  position: relative;\n  overflow: hidden;\n  z-index: 99;\n  width: calc(100% - 30px);\n  min-height: 52px;\n  margin-top: 14px;\n}\nion-header .search_box ion-icon {\n  color: var(--text-light);\n  font-size: 1.4rem;\n  min-width: 35px;\n  height: 35px;\n  line-height: 35px;\n  z-index: 99;\n}\nion-header .search_box ion-searchbar {\n  --background: var(--transparent) !important;\n  --color: var(--text-light);\n  --placeholder-opacity: 1;\n  --placeholder-font-weight: 400 !important;\n  --box-shadow: none !important;\n}\n.services {\n  width: calc(100% - 40px);\n  margin: 0 auto;\n  padding-top: 30px;\n  padding-bottom: 35px;\n}\n.services h2 {\n  margin: 0;\n  font-size: 1.2rem;\n  font-weight: 600;\n  margin-bottom: 7px;\n}\n.services ion-row {\n  margin: 0 -10px;\n}\n.services ion-row ion-col {\n  padding: 10px;\n}\n.services ion-row ion-col .services_box {\n  position: relative;\n  width: 100%;\n  height: 110px;\n  border-radius: 4px;\n  overflow: hidden;\n}\n.services ion-row ion-col .services_box .img_box {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n}\n.services ion-row ion-col .services_box::after {\n  content: \"\";\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.48);\n}\n.services ion-row ion-col .services_box h3 {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  width: 100%;\n  color: var(--white);\n  z-index: 99;\n  margin: 0;\n  font-size: 1.1rem;\n  padding: 12px;\n  letter-spacing: 0.5px;\n  line-height: 21px;\n}\nion-list {\n  background: var(--transparent) !important;\n  margin: 0;\n  padding: 0;\n  width: calc(100% - 40px);\n  margin: 0 auto;\n}\nion-list h2 {\n  margin: 0;\n  font-size: 1.2rem;\n  font-weight: 600;\n  margin-bottom: 7px;\n}\nion-list h2 span {\n  color: var(--text-light);\n  font-size: 0.85rem;\n  font-weight: 500;\n  min-width: 80px;\n}\nion-list ion-item {\n  padding: 12px 0;\n  background: var(--white);\n  --inner-padding-end: 0px;\n  --inner-min-height: unset !important;\n  --padding-start: 0;\n  --highligh-color-focused: var(--transparent) !important;\n  margin-bottom: 8px;\n}\nion-list ion-item .item_inner {\n  width: 100%;\n  overflow: hidden;\n}\nion-list ion-item .item_inner .date_time {\n  background: var(--primary);\n  padding: 10px 12px;\n  min-width: 100px;\n  border-radius: 4px 0px 0px 4px;\n}\nion-list ion-item .item_inner .date_time h3 {\n  margin: 0;\n  color: var(--white);\n  font-size: 1.15rem;\n  padding-bottom: 5px;\n}\nion-list ion-item .item_inner .date_time h4 {\n  margin: 0;\n  color: #cdd6ed;\n  font-size: 0.77rem;\n  padding-bottom: 7px;\n}\nion-list ion-item .item_inner .date_time h5 {\n  margin: 0;\n  color: var(--white);\n  font-size: 0.77rem;\n}\nion-list ion-item .item_inner .date_time h5 ion-icon {\n  color: #cdd6ed;\n}\nion-list ion-item .item_inner .appointment_details {\n  padding: 9px 12px;\n  border: 1px solid #ccc;\n  border-left: 0;\n  width: 100%;\n  border-radius: 0px 4px 4px 0px;\n}\nion-list ion-item .item_inner .appointment_details h3 {\n  margin: 0;\n  color: var(--text-dark);\n  font-size: 1.2rem;\n  padding-bottom: 5px;\n  font-weight: 700;\n}\nion-list ion-item .item_inner .appointment_details h4 {\n  margin: 0;\n  color: var(--text-light);\n  font-size: 0.77rem;\n  padding-bottom: 7px;\n}\nion-list ion-item .item_inner .appointment_details h5 {\n  margin: 0;\n  color: var(--text-dark);\n  font-size: 0.77rem;\n  font-weight: 600;\n}\n.container {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-flow: row wrap;\n  height: 100%;\n}\n.container .item {\n  width: 50%;\n  position: relative;\n}\n.container .item .container-wrapper {\n  height: 115px;\n  overflow: hidden;\n  box-shadow: 0px 0px 7px 0px rgba(0, 0, 0, 0.75);\n  border-radius: 10px;\n  margin: 10px;\n}\n.container .item .container-wrapper .overlay {\n  background-color: rgba(0, 0, 0, 0.15);\n  height: 100%;\n  z-index: 2;\n}\n.container .item .service-title {\n  position: absolute;\n  bottom: 20px;\n  left: 20px;\n  color: #fff;\n  font-weight: 600;\n  line-height: 25px;\n  width: -webkit-min-content;\n  width: -moz-min-content;\n  width: min-content;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2F0ZWdvcnktYWxsL0U6XFxFXFxzZXJ2aWNlXFxmaXJlYmFzZSAwNFxccHJvdmlkZXItYXBwLWJ1ZGRoaW5pL3NyY1xcYXBwXFxjYXRlZ29yeS1hbGxcXGNhdGVnb3J5LWFsbC5wYWdlLnNjc3MiLCJzcmMvYXBwL2NhdGVnb3J5LWFsbC9jYXRlZ29yeS1hbGwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdFO0VBQ0MsOEJBQUE7QUNGSDtBREtFO0VBQ0MsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSwwQkFBQTtFQUNBLDZCQUFBO0FDSEg7QURLRztFQUNDLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7QUNISjtBRFNDO0VBQ0Msa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUNQRjtBRFVDO0VBQ0MsaUNBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSx3QkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNSRjtBRFVFO0VBQ0Msd0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0FDUkg7QURXRTtFQUNDLDJDQUFBO0VBQ0EsMEJBQUE7RUFDQSx3QkFBQTtFQUNBLHlDQUFBO0VBQ0EsNkJBQUE7QUNUSDtBRGNBO0VBQ0Msd0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtBQ1hEO0FEYUM7RUFDQyxTQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDWEY7QURjQztFQUNDLGVBQUE7QUNaRjtBRGNFO0VBQ0MsYUFBQTtBQ1pIO0FEY0c7RUFDQyxrQkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ1pKO0FEY0k7RUFDQyxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNaTDtBRGdCSTtFQUNDLFdBQUE7RUFDQSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSwrQkFBQTtBQ2RMO0FEaUJJO0VBQ0Msa0JBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtBQ2ZMO0FEc0JBO0VBQ0MseUNBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLHdCQUFBO0VBQ0EsY0FBQTtBQ25CRDtBRHFCQztFQUNDLFNBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNuQkY7QURxQkU7RUFDQyx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDbkJIO0FEdUJDO0VBQ0MsZUFBQTtFQUNBLHdCQUFBO0VBQ0Esd0JBQUE7RUFDQSxvQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsdURBQUE7RUFDQSxrQkFBQTtBQ3JCRjtBRHVCRTtFQUNDLFdBQUE7RUFDQSxnQkFBQTtBQ3JCSDtBRHVCRztFQUNDLDBCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLDhCQUFBO0FDckJKO0FEdUJJO0VBQ0MsU0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ3JCTDtBRHdCSTtFQUNDLFNBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ3RCTDtBRHlCSTtFQUNDLFNBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDdkJMO0FEeUJLO0VBQ0MsY0FBQTtBQ3ZCTjtBRDZCRztFQUNDLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0FDM0JKO0FENkJJO0VBQ0MsU0FBQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FDM0JMO0FEOEJJO0VBQ0MsU0FBQTtFQUNBLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQzVCTDtBRCtCSTtFQUNDLFNBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUM3Qkw7QURxQ0E7RUFDQyxvQkFBQTtFQUFBLGFBQUE7RUFDQSw4QkFBQTtFQUFBLDZCQUFBO1VBQUEsbUJBQUE7RUFDQSxZQUFBO0FDbENEO0FEbUNDO0VBQ0UsVUFBQTtFQUNBLGtCQUFBO0FDakNIO0FEa0NHO0VBQ0QsYUFBQTtFQUNBLGdCQUFBO0VBR0EsK0NBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNoQ0Y7QURpQ0U7RUFDRSxxQ0FBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0FDL0JKO0FEa0NHO0VBQ0Qsa0JBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsMEJBQUE7RUFBQSx1QkFBQTtFQUFBLGtCQUFBO0FDaENGIiwiZmlsZSI6InNyYy9hcHAvY2F0ZWdvcnktYWxsL2NhdGVnb3J5LWFsbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taGVhZGVyIHtcclxuXHJcblx0aW9uLXRvb2xiYXIge1xyXG5cdFx0aW9uLWJ1dHRvbnMgaW9uLW1lbnUtYnV0dG9uIHtcclxuXHRcdFx0Y29sb3I6IHZhcigtLXdoaXRlKSAhaW1wb3J0YW50O1xyXG5cdFx0fVxyXG5cclxuXHRcdGlvbi10aXRsZSB7XHJcblx0XHRcdHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xyXG5cdFx0XHR3aWR0aDogMTAwJTtcclxuXHRcdFx0dG9wOiAwO1xyXG5cdFx0XHRsZWZ0OiAwO1xyXG5cdFx0XHRwYWRkaW5nOiAwIDE1cHggIWltcG9ydGFudDtcclxuXHRcdFx0dGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XHJcblxyXG5cdFx0XHRpbWcge1xyXG5cdFx0XHRcdHdpZHRoOiAxMTBweDtcclxuXHRcdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHRcdFx0dG9wOiA1cHg7XHJcblx0XHRcdH1cclxuXHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQuaGVhZGVyX2JnIHtcclxuXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdHRvcDogMDtcclxuXHRcdGxlZnQ6IDA7XHJcblx0XHR3aWR0aDogMTAwJTtcclxuXHRcdGhlaWdodDogMTAwcHg7XHJcblx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdH1cclxuXHJcblx0LnNlYXJjaF9ib3gge1xyXG5cdFx0YmFja2dyb3VuZDogdmFyKC0taW5wdXRfZmlsZWRfYmcpO1xyXG5cdFx0Ym9yZGVyLXJhZGl1czogNHB4O1xyXG5cdFx0bWFyZ2luOiAwIGF1dG87XHJcblx0XHRwYWRkaW5nOiAwIDE1cHg7XHJcblx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdFx0ei1pbmRleDogOTk7XHJcblx0XHR3aWR0aDogY2FsYygxMDAlIC0gMzBweCk7XHJcblx0XHRtaW4taGVpZ2h0OiA1MnB4O1xyXG5cdFx0bWFyZ2luLXRvcDogMTRweDtcclxuXHJcblx0XHRpb24taWNvbiB7XHJcblx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcclxuXHRcdFx0Zm9udC1zaXplOiAxLjRyZW07XHJcblx0XHRcdG1pbi13aWR0aDogMzVweDtcclxuXHRcdFx0aGVpZ2h0OiAzNXB4O1xyXG5cdFx0XHRsaW5lLWhlaWdodDogMzVweDtcclxuXHRcdFx0ei1pbmRleDogOTk7XHJcblx0XHR9XHJcblxyXG5cdFx0aW9uLXNlYXJjaGJhciB7XHJcblx0XHRcdC0tYmFja2dyb3VuZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XHJcblx0XHRcdC0tY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xyXG5cdFx0XHQtLXBsYWNlaG9sZGVyLW9wYWNpdHk6IDE7XHJcblx0XHRcdC0tcGxhY2Vob2xkZXItZm9udC13ZWlnaHQ6IDQwMCAhaW1wb3J0YW50O1xyXG5cdFx0XHQtLWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbi5zZXJ2aWNlcyB7XHJcblx0d2lkdGg6IGNhbGMoMTAwJSAtIDQwcHgpO1xyXG5cdG1hcmdpbjogMCBhdXRvO1xyXG5cdHBhZGRpbmctdG9wOiAzMHB4O1xyXG5cdHBhZGRpbmctYm90dG9tOiAzNXB4O1xyXG5cclxuXHRoMiB7XHJcblx0XHRtYXJnaW46IDA7XHJcblx0XHRmb250LXNpemU6IDEuMnJlbTtcclxuXHRcdGZvbnQtd2VpZ2h0OiA2MDA7XHJcblx0XHRtYXJnaW4tYm90dG9tOiA3cHg7XHJcblx0fVxyXG5cclxuXHRpb24tcm93IHtcclxuXHRcdG1hcmdpbjogMCAtMTBweDtcclxuXHJcblx0XHRpb24tY29sIHtcclxuXHRcdFx0cGFkZGluZzogMTBweDtcclxuXHJcblx0XHRcdC5zZXJ2aWNlc19ib3gge1xyXG5cdFx0XHRcdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRcdFx0XHR3aWR0aDogMTAwJTtcclxuXHRcdFx0XHRoZWlnaHQ6IDExMHB4O1xyXG5cdFx0XHRcdGJvcmRlci1yYWRpdXM6IDRweDtcclxuXHRcdFx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cclxuXHRcdFx0XHQuaW1nX2JveCB7XHJcblx0XHRcdFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRcdFx0XHR0b3A6IDA7XHJcblx0XHRcdFx0XHRsZWZ0OiAwO1xyXG5cdFx0XHRcdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRcdFx0XHRoZWlnaHQ6IDEwMCU7XHJcblxyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0Jjo6YWZ0ZXIge1xyXG5cdFx0XHRcdFx0Y29udGVudDogJyc7XHJcblx0XHRcdFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRcdFx0XHR0b3A6IDA7XHJcblx0XHRcdFx0XHRsZWZ0OiAwO1xyXG5cdFx0XHRcdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRcdFx0XHRoZWlnaHQ6IDEwMCU7XHJcblx0XHRcdFx0XHRiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNDgpO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aDMge1xyXG5cdFx0XHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHRcdFx0Ym90dG9tOiAwO1xyXG5cdFx0XHRcdFx0bGVmdDogMDtcclxuXHRcdFx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRcdFx0Y29sb3I6IHZhcigtLXdoaXRlKTtcclxuXHRcdFx0XHRcdHotaW5kZXg6IDk5O1xyXG5cdFx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAxLjFyZW07XHJcblx0XHRcdFx0XHRwYWRkaW5nOiAxMnB4O1xyXG5cdFx0XHRcdFx0bGV0dGVyLXNwYWNpbmc6IC41cHg7XHJcblx0XHRcdFx0XHRsaW5lLWhlaWdodDogMjFweDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbmlvbi1saXN0IHtcclxuXHRiYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcclxuXHRtYXJnaW46IDA7XHJcblx0cGFkZGluZzogMDtcclxuXHR3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XHJcblx0bWFyZ2luOiAwIGF1dG87XHJcblxyXG5cdGgyIHtcclxuXHRcdG1hcmdpbjogMDtcclxuXHRcdGZvbnQtc2l6ZTogMS4ycmVtO1xyXG5cdFx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHRcdG1hcmdpbi1ib3R0b206IDdweDtcclxuXHJcblx0XHRzcGFuIHtcclxuXHRcdFx0Y29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xyXG5cdFx0XHRmb250LXNpemU6IC44NXJlbTtcclxuXHRcdFx0Zm9udC13ZWlnaHQ6IDUwMDtcclxuXHRcdFx0bWluLXdpZHRoOiA4MHB4O1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0aW9uLWl0ZW0ge1xyXG5cdFx0cGFkZGluZzogMTJweCAwO1xyXG5cdFx0YmFja2dyb3VuZDogdmFyKC0td2hpdGUpO1xyXG5cdFx0LS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xyXG5cdFx0LS1pbm5lci1taW4taGVpZ2h0OiB1bnNldCAhaW1wb3J0YW50O1xyXG5cdFx0LS1wYWRkaW5nLXN0YXJ0OiAwO1xyXG5cdFx0LS1oaWdobGlnaC1jb2xvci1mb2N1c2VkOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcclxuXHRcdG1hcmdpbi1ib3R0b206IDhweDtcclxuXHJcblx0XHQuaXRlbV9pbm5lciB7XHJcblx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cclxuXHRcdFx0LmRhdGVfdGltZSB7XHJcblx0XHRcdFx0YmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRcdFx0cGFkZGluZzogMTBweCAxMnB4O1xyXG5cdFx0XHRcdG1pbi13aWR0aDogMTAwcHg7XHJcblx0XHRcdFx0Ym9yZGVyLXJhZGl1czogNHB4IDBweCAwcHggNHB4O1xyXG5cclxuXHRcdFx0XHRoMyB7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRjb2xvcjogdmFyKC0td2hpdGUpO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAxLjE1cmVtO1xyXG5cdFx0XHRcdFx0cGFkZGluZy1ib3R0b206IDVweDtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdGg0IHtcclxuXHRcdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRcdGNvbG9yOiAjY2RkNmVkO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAuNzdyZW07XHJcblx0XHRcdFx0XHRwYWRkaW5nLWJvdHRvbTogN3B4O1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aDUge1xyXG5cdFx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdFx0Y29sb3I6IHZhcigtLXdoaXRlKTtcclxuXHRcdFx0XHRcdGZvbnQtc2l6ZTogLjc3cmVtO1xyXG5cclxuXHRcdFx0XHRcdGlvbi1pY29uIHtcclxuXHRcdFx0XHRcdFx0Y29sb3I6ICNjZGQ2ZWQ7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0LmFwcG9pbnRtZW50X2RldGFpbHMge1xyXG5cdFx0XHRcdHBhZGRpbmc6IDlweCAxMnB4O1xyXG5cdFx0XHRcdGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcblx0XHRcdFx0Ym9yZGVyLWxlZnQ6IDA7XHJcblx0XHRcdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRcdFx0Ym9yZGVyLXJhZGl1czogMHB4IDRweCA0cHggMHB4O1xyXG5cclxuXHRcdFx0XHRoMyB7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcclxuXHRcdFx0XHRcdGZvbnQtc2l6ZTogMS4ycmVtO1xyXG5cdFx0XHRcdFx0cGFkZGluZy1ib3R0b206IDVweDtcclxuXHRcdFx0XHRcdGZvbnQtd2VpZ2h0OiA3MDA7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRoNCB7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XHJcblx0XHRcdFx0XHRmb250LXNpemU6IC43N3JlbTtcclxuXHRcdFx0XHRcdHBhZGRpbmctYm90dG9tOiA3cHg7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRoNSB7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcclxuXHRcdFx0XHRcdGZvbnQtc2l6ZTogLjc3cmVtO1xyXG5cdFx0XHRcdFx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcblxyXG4uY29udGFpbmVyIHtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGZsZXgtZmxvdzogcm93IHdyYXA7XHJcblx0aGVpZ2h0OiAxMDAlO1xyXG5cdC5pdGVtIHtcclxuXHQgIHdpZHRoOiA1MCU7XHJcblx0ICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0ICAuY29udGFpbmVyLXdyYXBwZXIge1xyXG5cdFx0aGVpZ2h0OiAxMTVweDtcclxuXHRcdG92ZXJmbG93OiBoaWRkZW47XHJcblx0XHQtd2Via2l0LWJveC1zaGFkb3c6IDBweCAwcHggN3B4IDBweCByZ2JhKDAsIDAsIDAsIDAuNzUpO1xyXG5cdFx0LW1vei1ib3gtc2hhZG93OiAwcHggMHB4IDdweCAwcHggcmdiYSgwLCAwLCAwLCAwLjc1KTtcclxuXHRcdGJveC1zaGFkb3c6IDBweCAwcHggN3B4IDBweCByZ2JhKDAsIDAsIDAsIDAuNzUpO1xyXG5cdFx0Ym9yZGVyLXJhZGl1czogMTBweDtcclxuXHRcdG1hcmdpbjogMTBweDtcclxuXHRcdC5vdmVybGF5IHtcclxuXHRcdCAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjE1KTtcclxuXHRcdCAgaGVpZ2h0OiAxMDAlO1xyXG5cdFx0ICB6LWluZGV4OiAyO1xyXG5cdFx0fVxyXG5cdCAgfVxyXG5cdCAgLnNlcnZpY2UtdGl0bGUge1xyXG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0Ym90dG9tOiAyMHB4O1xyXG5cdFx0bGVmdDogMjBweDtcclxuXHRcdGNvbG9yOiAjZmZmO1xyXG5cdFx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHRcdGxpbmUtaGVpZ2h0OiAyNXB4O1xyXG5cdFx0d2lkdGg6IG1pbi1jb250ZW50O1xyXG5cdCAgfVxyXG5cdH1cclxuICB9XHJcbiAgXHJcbiIsImlvbi1oZWFkZXIgaW9uLXRvb2xiYXIgaW9uLWJ1dHRvbnMgaW9uLW1lbnUtYnV0dG9uIHtcbiAgY29sb3I6IHZhcigtLXdoaXRlKSAhaW1wb3J0YW50O1xufVxuaW9uLWhlYWRlciBpb24tdG9vbGJhciBpb24tdGl0bGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcbiAgd2lkdGg6IDEwMCU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgcGFkZGluZzogMCAxNXB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xufVxuaW9uLWhlYWRlciBpb24tdG9vbGJhciBpb24tdGl0bGUgaW1nIHtcbiAgd2lkdGg6IDExMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogNXB4O1xufVxuaW9uLWhlYWRlciAuaGVhZGVyX2JnIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuaW9uLWhlYWRlciAuc2VhcmNoX2JveCB7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlucHV0X2ZpbGVkX2JnKTtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgcGFkZGluZzogMCAxNXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHotaW5kZXg6IDk5O1xuICB3aWR0aDogY2FsYygxMDAlIC0gMzBweCk7XG4gIG1pbi1oZWlnaHQ6IDUycHg7XG4gIG1hcmdpbi10b3A6IDE0cHg7XG59XG5pb24taGVhZGVyIC5zZWFyY2hfYm94IGlvbi1pY29uIHtcbiAgY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuICBmb250LXNpemU6IDEuNHJlbTtcbiAgbWluLXdpZHRoOiAzNXB4O1xuICBoZWlnaHQ6IDM1cHg7XG4gIGxpbmUtaGVpZ2h0OiAzNXB4O1xuICB6LWluZGV4OiA5OTtcbn1cbmlvbi1oZWFkZXIgLnNlYXJjaF9ib3ggaW9uLXNlYXJjaGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XG4gIC0tY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuICAtLXBsYWNlaG9sZGVyLW9wYWNpdHk6IDE7XG4gIC0tcGxhY2Vob2xkZXItZm9udC13ZWlnaHQ6IDQwMCAhaW1wb3J0YW50O1xuICAtLWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLnNlcnZpY2VzIHtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDQwcHgpO1xuICBtYXJnaW46IDAgYXV0bztcbiAgcGFkZGluZy10b3A6IDMwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAzNXB4O1xufVxuLnNlcnZpY2VzIGgyIHtcbiAgbWFyZ2luOiAwO1xuICBmb250LXNpemU6IDEuMnJlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbWFyZ2luLWJvdHRvbTogN3B4O1xufVxuLnNlcnZpY2VzIGlvbi1yb3cge1xuICBtYXJnaW46IDAgLTEwcHg7XG59XG4uc2VydmljZXMgaW9uLXJvdyBpb24tY29sIHtcbiAgcGFkZGluZzogMTBweDtcbn1cbi5zZXJ2aWNlcyBpb24tcm93IGlvbi1jb2wgLnNlcnZpY2VzX2JveCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5zZXJ2aWNlcyBpb24tcm93IGlvbi1jb2wgLnNlcnZpY2VzX2JveCAuaW1nX2JveCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLnNlcnZpY2VzIGlvbi1yb3cgaW9uLWNvbCAuc2VydmljZXNfYm94OjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNDgpO1xufVxuLnNlcnZpY2VzIGlvbi1yb3cgaW9uLWNvbCAuc2VydmljZXNfYm94IGgzIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDA7XG4gIGxlZnQ6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogdmFyKC0td2hpdGUpO1xuICB6LWluZGV4OiA5OTtcbiAgbWFyZ2luOiAwO1xuICBmb250LXNpemU6IDEuMXJlbTtcbiAgcGFkZGluZzogMTJweDtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xuICBsaW5lLWhlaWdodDogMjFweDtcbn1cblxuaW9uLWxpc3Qge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICB3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuaW9uLWxpc3QgaDIge1xuICBtYXJnaW46IDA7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBtYXJnaW4tYm90dG9tOiA3cHg7XG59XG5pb24tbGlzdCBoMiBzcGFuIHtcbiAgY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuICBmb250LXNpemU6IDAuODVyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1pbi13aWR0aDogODBweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIHtcbiAgcGFkZGluZzogMTJweCAwO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcbiAgLS1pbm5lci1taW4taGVpZ2h0OiB1bnNldCAhaW1wb3J0YW50O1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIC0taGlnaGxpZ2gtY29sb3ItZm9jdXNlZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDhweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5pb24tbGlzdCBpb24taXRlbSAuaXRlbV9pbm5lciAuZGF0ZV90aW1lIHtcbiAgYmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XG4gIHBhZGRpbmc6IDEwcHggMTJweDtcbiAgbWluLXdpZHRoOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogNHB4IDBweCAwcHggNHB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmRhdGVfdGltZSBoMyB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXdoaXRlKTtcbiAgZm9udC1zaXplOiAxLjE1cmVtO1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmRhdGVfdGltZSBoNCB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6ICNjZGQ2ZWQ7XG4gIGZvbnQtc2l6ZTogMC43N3JlbTtcbiAgcGFkZGluZy1ib3R0b206IDdweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC5kYXRlX3RpbWUgaDUge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS13aGl0ZSk7XG4gIGZvbnQtc2l6ZTogMC43N3JlbTtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC5kYXRlX3RpbWUgaDUgaW9uLWljb24ge1xuICBjb2xvcjogI2NkZDZlZDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC5hcHBvaW50bWVudF9kZXRhaWxzIHtcbiAgcGFkZGluZzogOXB4IDEycHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIGJvcmRlci1sZWZ0OiAwO1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogMHB4IDRweCA0cHggMHB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmFwcG9pbnRtZW50X2RldGFpbHMgaDMge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xuICBmb250LXNpemU6IDEuMnJlbTtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC5hcHBvaW50bWVudF9kZXRhaWxzIGg0IHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XG4gIGZvbnQtc2l6ZTogMC43N3JlbTtcbiAgcGFkZGluZy1ib3R0b206IDdweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC5hcHBvaW50bWVudF9kZXRhaWxzIGg1IHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcbiAgZm9udC1zaXplOiAwLjc3cmVtO1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4uY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3cgd3JhcDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmNvbnRhaW5lciAuaXRlbSB7XG4gIHdpZHRoOiA1MCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jb250YWluZXIgLml0ZW0gLmNvbnRhaW5lci13cmFwcGVyIHtcbiAgaGVpZ2h0OiAxMTVweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMHB4IDdweCAwcHggcmdiYSgwLCAwLCAwLCAwLjc1KTtcbiAgLW1vei1ib3gtc2hhZG93OiAwcHggMHB4IDdweCAwcHggcmdiYSgwLCAwLCAwLCAwLjc1KTtcbiAgYm94LXNoYWRvdzogMHB4IDBweCA3cHggMHB4IHJnYmEoMCwgMCwgMCwgMC43NSk7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIG1hcmdpbjogMTBweDtcbn1cbi5jb250YWluZXIgLml0ZW0gLmNvbnRhaW5lci13cmFwcGVyIC5vdmVybGF5IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjE1KTtcbiAgaGVpZ2h0OiAxMDAlO1xuICB6LWluZGV4OiAyO1xufVxuLmNvbnRhaW5lciAuaXRlbSAuc2VydmljZS10aXRsZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAyMHB4O1xuICBsZWZ0OiAyMHB4O1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbGluZS1oZWlnaHQ6IDI1cHg7XG4gIHdpZHRoOiBtaW4tY29udGVudDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/category-all/category-all.page.ts":
  /*!***************************************************!*\
    !*** ./src/app/category-all/category-all.page.ts ***!
    \***************************************************/

  /*! exports provided: CategoryAllPage */

  /***/
  function srcAppCategoryAllCategoryAllPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CategoryAllPage", function () {
      return CategoryAllPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/fire/database */
    "./node_modules/@angular/fire/fesm2015/angular-fire-database.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

    let CategoryAllPage = class CategoryAllPage {
      constructor(route, db, alertController, _sanitizer) {
        this.route = route;
        this.db = db;
        this.alertController = alertController;
        this._sanitizer = _sanitizer;
        this.categories = [];
      }

      ngOnInit() {
        let cat = this.db.list('/categories').valueChanges().subscribe(cat => {
          this.categories = cat;
        });
      }

      getUrl(value) {
        return this._sanitizer.bypassSecurityTrustStyle("linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(".concat(value, ")"));
      }

      adddetails() {
        this.route.navigate(['./select-date-time']);
      }

    };

    CategoryAllPage.ctorParameters = () => [{
      type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
    }, {
      type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"]
    }, {
      type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]
    }];

    CategoryAllPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-category-all',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./category-all.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/category-all/category-all.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./category-all.page.scss */
      "./src/app/category-all/category-all.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]])], CategoryAllPage);
    /***/
  }
}]);
//# sourceMappingURL=category-all-category-all-module-es5.js.map