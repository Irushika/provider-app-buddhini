(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["appointment-info-appointment-info-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/appointment-info/appointment-info.page.html":
  /*!***************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/appointment-info/appointment-info.page.html ***!
    \***************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAppointmentInfoAppointmentInfoPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n\t<ion-toolbar>\r\n\t\t<ion-buttons slot=\"start\">\r\n\t\t\t<ion-toolbar>\r\n\t\t\t\t<ion-back-button  text=\"Back\" icon=\"chevron-back-outline\"></ion-back-button>\r\n\t\t\t\t\r\n\t\t\t</ion-toolbar>\r\n\t\t</ion-buttons>\r\n\t\t<ion-title>\r\n\r\n\t\t</ion-title>\r\n\t</ion-toolbar>\r\n\t<h1>\r\n\t\t{{'appointment_info' | translate}}\r\n\t\t<small>{{'booked_on' | translate}} {{currentRequest?.provider_date| date:'EEEE, MMMM d, y'}}</small>\r\n\t</h1>\r\n</ion-header>\r\n\r\n\r\n<ion-content fullscreen>\r\n\t<div class=\"form\">\r\n\t\t<ion-list lines=\"none\">\r\n\t\t\t<ion-item>\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<h2>{{'your_appointment_for' | translate}}</h2>\r\n\t\t\t\t\t<h3>{{job?.category}}</h3>\r\n\t\t\t\t\t\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t\t<ion-item>\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<h2>Name</h2>\r\n\t\t\t\t\t<h3>{{name}}</h3> \r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t\t<ion-item>\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<h2>{{'date_time' | translate}}</h2>\r\n\t\t\t\t\t<h3>{{currentRequest?.provider_date| date:'EEEE, MMMM d, y'}}</h3><br>\r\n\t\t\t\t\t<h3>{{currentRequest?.provider_time}}</h3>  \r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t\t<ion-item>\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<h2>{{'location_at' | translate}}</h2>\r\n\t\t\t\t\t<h3>{{job?.adrress_type}}</h3>\r\n\t\t\t\t\t<h4> {{job?.address}} </h4>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\t\t\t\r\n\t\t\t\r\n\t\t\t\r\n\t\t\t<ion-item>\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<h2>Additional Information</h2>\r\n\t\t\t\t \r\n\t\t\t\t\t<h4>{{job?.information}}</h4>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t\t<ion-item>\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<h2>Payment Information</h2>\r\n\t\t\t\t\t<h3>Paid</h3>\r\n\t\t\t\t\t<h4> card payment(viza),<br>2021.01.03 </h4>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t</ion-list>\r\n\t</div>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/appointment-info/appointment-info-routing.module.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/appointment-info/appointment-info-routing.module.ts ***!
    \*********************************************************************/

  /*! exports provided: AppointmentInfoPageRoutingModule */

  /***/
  function srcAppAppointmentInfoAppointmentInfoRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppointmentInfoPageRoutingModule", function () {
      return AppointmentInfoPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _appointment_info_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./appointment-info.page */
    "./src/app/appointment-info/appointment-info.page.ts");

    const routes = [{
      path: '',
      component: _appointment_info_page__WEBPACK_IMPORTED_MODULE_3__["AppointmentInfoPage"]
    }];
    let AppointmentInfoPageRoutingModule = class AppointmentInfoPageRoutingModule {};
    AppointmentInfoPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AppointmentInfoPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/appointment-info/appointment-info.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/appointment-info/appointment-info.module.ts ***!
    \*************************************************************/

  /*! exports provided: AppointmentInfoPageModule */

  /***/
  function srcAppAppointmentInfoAppointmentInfoModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppointmentInfoPageModule", function () {
      return AppointmentInfoPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _appointment_info_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./appointment-info-routing.module */
    "./src/app/appointment-info/appointment-info-routing.module.ts");
    /* harmony import */


    var _appointment_info_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./appointment-info.page */
    "./src/app/appointment-info/appointment-info.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");

    let AppointmentInfoPageModule = class AppointmentInfoPageModule {};
    AppointmentInfoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"], _appointment_info_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppointmentInfoPageRoutingModule"]],
      declarations: [_appointment_info_page__WEBPACK_IMPORTED_MODULE_6__["AppointmentInfoPage"]]
    })], AppointmentInfoPageModule);
    /***/
  },

  /***/
  "./src/app/appointment-info/appointment-info.page.scss":
  /*!*************************************************************!*\
    !*** ./src/app/appointment-info/appointment-info.page.scss ***!
    \*************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAppointmentInfoAppointmentInfoPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-header h1 small {\n  display: block;\n  color: var(--text-light);\n  font-weight: 400;\n  font-size: 0.95rem;\n  opacity: 0.7;\n  padding-top: 6px;\n}\n\n.form {\n  padding-top: 50px;\n}\n\n.form ion-list ion-item {\n  margin-bottom: 25px;\n}\n\n.form ion-list ion-item h2 {\n  margin: 0;\n  color: var(--text-light);\n  font-size: 1.15rem;\n  padding-bottom: 10px;\n  font-weight: 700;\n  opacity: 0.7;\n}\n\n.form ion-list ion-item h3 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 700;\n  font-size: 1.35rem;\n  padding-bottom: 5px;\n}\n\n.form ion-list ion-item h4 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 500;\n  font-size: 1.15rem;\n  line-height: 23px;\n}\n\n.form ion-list ion-item ion-textarea {\n  font-size: 1rem !important;\n  letter-spacing: 0.5px !important;\n}\n\n.form ion-list ion-item ion-label {\n  color: var(--text-dark) !important;\n  font-weight: 700 !important;\n  font-size: 1.35rem !important;\n}\n\nion-footer {\n  background: var(--white);\n  padding: 10px 10px 20px 10px;\n}\n\nion-footer ion-row {\n  width: 100%;\n}\n\nion-footer ion-row ion-col {\n  padding: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwb2ludG1lbnQtaW5mby9FOlxcRVxcc2VydmljZVxcZmlyZWJhc2UgMDRcXHByb3ZpZGVyLWFwcC1idWRkaGluaS9zcmNcXGFwcFxcYXBwb2ludG1lbnQtaW5mb1xcYXBwb2ludG1lbnQtaW5mby5wYWdlLnNjc3MiLCJzcmMvYXBwL2FwcG9pbnRtZW50LWluZm8vYXBwb2ludG1lbnQtaW5mby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUU7RUFDQyxjQUFBO0VBQ0Esd0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FDREg7O0FETUE7RUFDQyxpQkFBQTtBQ0hEOztBRE1FO0VBQ0MsbUJBQUE7QUNKSDs7QURNRztFQUNDLFNBQUE7RUFDQSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUNKSjs7QURPRztFQUNDLFNBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ0xKOztBRFFHO0VBQ0MsU0FBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDTko7O0FEU0c7RUFDQywwQkFBQTtFQUNBLGdDQUFBO0FDUEo7O0FEVUc7RUFDQyxrQ0FBQTtFQUNBLDJCQUFBO0VBQ0EsNkJBQUE7QUNSSjs7QURjQTtFQUNDLHdCQUFBO0VBQ0EsNEJBQUE7QUNYRDs7QURhQztFQUNDLFdBQUE7QUNYRjs7QURhRTtFQUNDLGVBQUE7QUNYSCIsImZpbGUiOiJzcmMvYXBwL2FwcG9pbnRtZW50LWluZm8vYXBwb2ludG1lbnQtaW5mby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taGVhZGVyIHtcclxuXHRoMSB7XHJcblx0XHRzbWFsbCB7XHJcblx0XHRcdGRpc3BsYXk6IGJsb2NrO1xyXG5cdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XHJcblx0XHRcdGZvbnQtd2VpZ2h0OiA0MDA7XHJcblx0XHRcdGZvbnQtc2l6ZTogLjk1cmVtO1xyXG5cdFx0XHRvcGFjaXR5OiAuNztcclxuXHRcdFx0cGFkZGluZy10b3A6IDZweDtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbi5mb3JtIHtcclxuXHRwYWRkaW5nLXRvcDogNTBweDtcclxuXHJcblx0aW9uLWxpc3Qge1xyXG5cdFx0aW9uLWl0ZW0ge1xyXG5cdFx0XHRtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG5cclxuXHRcdFx0aDIge1xyXG5cdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxLjE1cmVtO1xyXG5cdFx0XHRcdHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG5cdFx0XHRcdGZvbnQtd2VpZ2h0OiA3MDA7XHJcblx0XHRcdFx0b3BhY2l0eTogLjc7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGgzIHtcclxuXHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0Y29sb3I6IHZhcigtLXRleHQtZGFyayk7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IDcwMDtcclxuXHRcdFx0XHRmb250LXNpemU6IDEuMzVyZW07XHJcblx0XHRcdFx0cGFkZGluZy1ib3R0b206IDVweDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aDQge1xyXG5cdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcclxuXHRcdFx0XHRmb250LXdlaWdodDogNTAwO1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMS4xNXJlbTtcclxuXHRcdFx0XHRsaW5lLWhlaWdodDogMjNweDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aW9uLXRleHRhcmVhIHtcclxuXHRcdFx0XHRmb250LXNpemU6IDFyZW0gIWltcG9ydGFudDtcclxuXHRcdFx0XHRsZXR0ZXItc3BhY2luZzogLjVweCAhaW1wb3J0YW50O1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpb24tbGFiZWwge1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IDcwMCAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMS4zNXJlbSAhaW1wb3J0YW50O1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG5pb24tZm9vdGVyIHtcclxuXHRiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XHJcblx0cGFkZGluZzogMTBweCAxMHB4IDIwcHggMTBweDtcclxuXHJcblx0aW9uLXJvdyB7XHJcblx0XHR3aWR0aDogMTAwJTsgXHJcblxyXG5cdFx0aW9uLWNvbCB7XHJcblx0XHRcdHBhZGRpbmc6IDAgMTBweDtcclxuXHRcdH1cclxuXHR9XHJcbn0iLCJpb24taGVhZGVyIGgxIHNtYWxsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgZm9udC1zaXplOiAwLjk1cmVtO1xuICBvcGFjaXR5OiAwLjc7XG4gIHBhZGRpbmctdG9wOiA2cHg7XG59XG5cbi5mb3JtIHtcbiAgcGFkZGluZy10b3A6IDUwcHg7XG59XG4uZm9ybSBpb24tbGlzdCBpb24taXRlbSB7XG4gIG1hcmdpbi1ib3R0b206IDI1cHg7XG59XG4uZm9ybSBpb24tbGlzdCBpb24taXRlbSBoMiB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuICBmb250LXNpemU6IDEuMTVyZW07XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICBmb250LXdlaWdodDogNzAwO1xuICBvcGFjaXR5OiAwLjc7XG59XG4uZm9ybSBpb24tbGlzdCBpb24taXRlbSBoMyB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtZGFyayk7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGZvbnQtc2l6ZTogMS4zNXJlbTtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cbi5mb3JtIGlvbi1saXN0IGlvbi1pdGVtIGg0IHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgZm9udC1zaXplOiAxLjE1cmVtO1xuICBsaW5lLWhlaWdodDogMjNweDtcbn1cbi5mb3JtIGlvbi1saXN0IGlvbi1pdGVtIGlvbi10ZXh0YXJlYSB7XG4gIGZvbnQtc2l6ZTogMXJlbSAhaW1wb3J0YW50O1xuICBsZXR0ZXItc3BhY2luZzogMC41cHggIWltcG9ydGFudDtcbn1cbi5mb3JtIGlvbi1saXN0IGlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA3MDAgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxLjM1cmVtICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1mb290ZXIge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XG4gIHBhZGRpbmc6IDEwcHggMTBweCAyMHB4IDEwcHg7XG59XG5pb24tZm9vdGVyIGlvbi1yb3cge1xuICB3aWR0aDogMTAwJTtcbn1cbmlvbi1mb290ZXIgaW9uLXJvdyBpb24tY29sIHtcbiAgcGFkZGluZzogMCAxMHB4O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/appointment-info/appointment-info.page.ts":
  /*!***********************************************************!*\
    !*** ./src/app/appointment-info/appointment-info.page.ts ***!
    \***********************************************************/

  /*! exports provided: AppointmentInfoPage */

  /***/
  function srcAppAppointmentInfoAppointmentInfoPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppointmentInfoPage", function () {
      return AppointmentInfoPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/fire/database */
    "./node_modules/@angular/fire/fesm2015/angular-fire-database.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

    let AppointmentInfoPage = class AppointmentInfoPage {
      constructor(route, activatedRoute, db, alertController) {
        this.route = route;
        this.activatedRoute = activatedRoute;
        this.db = db;
        this.alertController = alertController;
      }

      ngOnInit() {
        this.JID = this.activatedRoute.snapshot.paramMap.get('JID');
        this.loadJobDetails();
      }

      loadJobDetails() {
        if (this.JID) {
          console.log(this.JID);
          window.localStorage.setItem("myjobid", this.JID);
          this.db.database.ref("/jobs/" + this.JID).on('value', snapshot => {
            this.job = snapshot.val();
            console.log(this.job);
            let clientUID = this.job.UID;
            console.log('client', clientUID);
            this.loadUserDetails(clientUID);
            let currentUID = window.localStorage.getItem('UID');
            this.currentRequest = this.job.requests.filter(request => {
              return currentUID == request.provider_UID;
            })[0];
            window.localStorage.setItem("jobInfo", JSON.stringify(this.job));
            this.requests = this.job.requests;
            console.log(this.requests); // load provider details

            this.completedRequest = this.requests.find(request => request.Status == 'Completed');
            if (this.completedRequest) this.loadUserDetails(this.completedRequest.provider_UID);
          });
        } else {
          this.job = JSON.parse(window.localStorage.getItem("jobInfo"));
          console.log("params not working");
        }
      }

      loadUserDetails(UID) {
        this.db.database.ref("/users/" + UID).on('value', snapshot => {
          this.user = snapshot.val();
          this.name = this.user.name;
        });
      }

    };

    AppointmentInfoPage.ctorParameters = () => [{
      type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
    }, {
      type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
    }, {
      type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
    }];

    AppointmentInfoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-appointment-info',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./appointment-info.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/appointment-info/appointment-info.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./appointment-info.page.scss */
      "./src/app/appointment-info/appointment-info.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]])], AppointmentInfoPage);
    /***/
  }
}]);
//# sourceMappingURL=appointment-info-appointment-info-module-es5.js.map