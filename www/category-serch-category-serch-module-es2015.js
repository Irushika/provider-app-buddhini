(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["category-serch-category-serch-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/category-serch/category-serch.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/category-serch/category-serch.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"bg_transparent\">\n\t<ion-toolbar>\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-menu-button></ion-menu-button>\n\t\t</ion-buttons>\n\t\t<ion-title><img src=\"assets/images/light_logo.png\"></ion-title>\n\t</ion-toolbar>\n\n\t<div class=\"search_box d-flex\">\n\t\t<ion-icon class=\"zmdi zmdi-search ion-text-start\"></ion-icon>\n\t\t<ion-searchbar class=\"ion-no-padding\" searchIcon=\"hide\" placeholder=\"{{'search_text' | translate}}\"></ion-searchbar>\n\t</div>\n</ion-header>\n\n<ion-content>\n\t<div class=\"services\">\n\t\t\n\t\t<h2>{{'services' | translate}}</h2>\n\t\t<div class=\"container\">\n\t\t\t<div class=\"item\" *ngFor=\"let cat of categories;index as i\">\n\t\t\t  <div\n\t\t\t\tclass=\"container-wrapper\" [style.background-image]=\"getUrl(cat.img)\"\n\t\t\t\tstyle=\"background-size: cover;\n\t\t\tbackground-position: center;\"\n      \n\t\t\t  >\n\t\t\t\t<div class=\"overlay\">\n\t\t\t\t  <div class=\"service-title\">{{cat?.name}}</div>\n\t\t\t\t</div>\n\t\t\t  </div>\n\t\t\t</div>\n\t\t  </div>\n\n\t</div>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/category-serch/category-serch-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/category-serch/category-serch-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: CategorySerchPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategorySerchPageRoutingModule", function() { return CategorySerchPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _category_serch_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./category-serch.page */ "./src/app/category-serch/category-serch.page.ts");




const routes = [
    {
        path: '',
        component: _category_serch_page__WEBPACK_IMPORTED_MODULE_3__["CategorySerchPage"]
    }
];
let CategorySerchPageRoutingModule = class CategorySerchPageRoutingModule {
};
CategorySerchPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CategorySerchPageRoutingModule);



/***/ }),

/***/ "./src/app/category-serch/category-serch.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/category-serch/category-serch.module.ts ***!
  \*********************************************************/
/*! exports provided: CategorySerchPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategorySerchPageModule", function() { return CategorySerchPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _category_serch_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./category-serch-routing.module */ "./src/app/category-serch/category-serch-routing.module.ts");
/* harmony import */ var _category_serch_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./category-serch.page */ "./src/app/category-serch/category-serch.page.ts");








let CategorySerchPageModule = class CategorySerchPageModule {
};
CategorySerchPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"],
            _category_serch_routing_module__WEBPACK_IMPORTED_MODULE_6__["CategorySerchPageRoutingModule"]
        ],
        declarations: [_category_serch_page__WEBPACK_IMPORTED_MODULE_7__["CategorySerchPage"]]
    })
], CategorySerchPageModule);



/***/ }),

/***/ "./src/app/category-serch/category-serch.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/category-serch/category-serch.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NhdGVnb3J5LXNlcmNoL2NhdGVnb3J5LXNlcmNoLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/category-serch/category-serch.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/category-serch/category-serch.page.ts ***!
  \*******************************************************/
/*! exports provided: CategorySerchPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategorySerchPage", function() { return CategorySerchPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let CategorySerchPage = class CategorySerchPage {
    constructor() { }
    ngOnInit() {
    }
    adddetails() {
    }
};
CategorySerchPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-category-serch',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./category-serch.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/category-serch/category-serch.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./category-serch.page.scss */ "./src/app/category-serch/category-serch.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], CategorySerchPage);



/***/ })

}]);
//# sourceMappingURL=category-serch-category-serch-module-es2015.js.map