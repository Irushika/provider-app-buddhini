(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["requests-requests-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/requests/requests.page.html":
  /*!***********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/requests/requests.page.html ***!
    \***********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppRequestsRequestsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n\t<ion-toolbar>\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-menu-button></ion-menu-button>\n\t\t</ion-buttons>\n\t\t<ion-title></ion-title>\n\t</ion-toolbar>\n\t<h1>{{'Requests' | translate}}</h1>\n</ion-header>\n\n<ion-content>\n\t<ion-list lines=\"none\">\n\n\t\t<ion-item (click)=\"appointment_info()\">\n\t\t\t<div class=\"item_inner d-flex\">\n\t\t\t\t<div class=\"date_time\">\n\t\t\t\t\t<h3>13 June</h3>\n\t\t\t\t\t<h4>11:00 am</h4>\n\t\t\t\t\t<h5 class=\"d-flex\">\n\t\t\t\t\t\t<ion-icon class=\"zmdi zmdi-pin ion-text-start\"></ion-icon>\n\t\t\t\t\t\tRequests\n\t\t\t\t\t</h5>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"appointment_details\">\n\t\t\t\t\t<h3> Fix my Appliances </h3>\n\t\t\t\t\t<h4>Air Conditionar</h4>\n\t\t\t\t\t<h5>$140.00</h5>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</ion-item>\n\n\t\t\n\t</ion-list>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/requests/requests-routing.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/requests/requests-routing.module.ts ***!
    \*****************************************************/

  /*! exports provided: RequestsPageRoutingModule */

  /***/
  function srcAppRequestsRequestsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RequestsPageRoutingModule", function () {
      return RequestsPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _requests_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./requests.page */
    "./src/app/requests/requests.page.ts");

    const routes = [{
      path: '',
      component: _requests_page__WEBPACK_IMPORTED_MODULE_3__["RequestsPage"]
    }];
    let RequestsPageRoutingModule = class RequestsPageRoutingModule {};
    RequestsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], RequestsPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/requests/requests.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/requests/requests.module.ts ***!
    \*********************************************/

  /*! exports provided: RequestsPageModule */

  /***/
  function srcAppRequestsRequestsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RequestsPageModule", function () {
      return RequestsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _requests_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./requests-routing.module */
    "./src/app/requests/requests-routing.module.ts");
    /* harmony import */


    var _requests_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./requests.page */
    "./src/app/requests/requests.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");

    let RequestsPageModule = class RequestsPageModule {};
    RequestsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _requests_routing_module__WEBPACK_IMPORTED_MODULE_5__["RequestsPageRoutingModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"]],
      declarations: [_requests_page__WEBPACK_IMPORTED_MODULE_6__["RequestsPage"]]
    })], RequestsPageModule);
    /***/
  },

  /***/
  "./src/app/requests/requests.page.scss":
  /*!*********************************************!*\
    !*** ./src/app/requests/requests.page.scss ***!
    \*********************************************/

  /*! exports provided: default */

  /***/
  function srcAppRequestsRequestsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-list {\n  background: var(--transparent) !important;\n  margin: 0;\n  padding: 0;\n  width: calc(100% - 40px);\n  margin: 0 auto;\n  padding-top: 26px;\n}\nion-list h2 {\n  margin: 0;\n  color: var(--text-light);\n  font-size: 1rem;\n  font-weight: 500;\n  padding-bottom: 18px;\n}\nion-list ion-item {\n  padding: 0;\n  background: var(--white);\n  --inner-padding-end: 0px;\n  --inner-min-height: unset !important;\n  --padding-start: 0;\n  --highligh-color-focused: var(--transparent) !important;\n  margin-bottom: 10px;\n}\nion-list ion-item .item_inner {\n  width: 100%;\n  overflow: hidden;\n}\nion-list ion-item .item_inner .date_time {\n  background: var(--primary);\n  padding: 10px 12px;\n  min-width: 100px;\n  border-radius: 4px 0px 0px 4px;\n}\nion-list ion-item .item_inner .date_time h3 {\n  margin: 0;\n  color: var(--white);\n  font-size: 1.15rem;\n  padding-bottom: 5px;\n}\nion-list ion-item .item_inner .date_time h4 {\n  margin: 0;\n  color: #cdd6ed;\n  font-size: 0.77rem;\n  padding-bottom: 7px;\n}\nion-list ion-item .item_inner .date_time h5 {\n  margin: 0;\n  color: var(--white);\n  font-size: 0.77rem;\n}\nion-list ion-item .item_inner .date_time h5 ion-icon {\n  color: #cdd6ed;\n}\nion-list ion-item .item_inner .appointment_details {\n  padding: 9px 12px;\n  border: 1px solid #ccc;\n  border-left: 0;\n  width: 100%;\n  border-radius: 0px 4px 4px 0px;\n}\nion-list ion-item .item_inner .appointment_details h3 {\n  margin: 0;\n  color: var(--text-dark);\n  font-size: 1.2rem;\n  padding-bottom: 5px;\n  font-weight: 700;\n}\nion-list ion-item .item_inner .appointment_details h4 {\n  margin: 0;\n  color: var(--text-light);\n  font-size: 0.77rem;\n  padding-bottom: 7px;\n}\nion-list ion-item .item_inner .appointment_details h5 {\n  margin: 0;\n  color: var(--text-dark);\n  font-size: 0.77rem;\n  font-weight: 600;\n}\nion-list .past_appointment {\n  padding-top: 27px;\n}\nion-list .past_appointment ion-item .item_inner .date_time {\n  background: var(--bg-dark);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVxdWVzdHMvRTpcXEVcXHNlcnZpY2VcXGZpcmViYXNlIDA0XFxwcm92aWRlci1hcHAtYnVkZGhpbmkvc3JjXFxhcHBcXHJlcXVlc3RzXFxyZXF1ZXN0cy5wYWdlLnNjc3MiLCJzcmMvYXBwL3JlcXVlc3RzL3JlcXVlc3RzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLHlDQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSx3QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQ0NEO0FEQ0M7RUFDQyxTQUFBO0VBQ0Esd0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtBQ0NGO0FERUM7RUFDQyxVQUFBO0VBQ0Esd0JBQUE7RUFDQSx3QkFBQTtFQUNBLG9DQUFBO0VBQ0Esa0JBQUE7RUFDQSx1REFBQTtFQUNBLG1CQUFBO0FDQUY7QURFRTtFQUNDLFdBQUE7RUFDQSxnQkFBQTtBQ0FIO0FERUc7RUFDQywwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSw4QkFBQTtBQ0FKO0FERUk7RUFDQyxTQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDQUw7QURHSTtFQUNDLFNBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ0RMO0FESUk7RUFDQyxTQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ0ZMO0FESUs7RUFDQyxjQUFBO0FDRk47QURRRztFQUNDLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0FDTko7QURRSTtFQUNDLFNBQUE7RUFDQSx1QkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQ05MO0FEU0k7RUFDQyxTQUFBO0VBQ0Esd0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDUEw7QURVSTtFQUNDLFNBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNSTDtBRGFDO0VBQ0MsaUJBQUE7QUNYRjtBRFlFO0VBQ0MsMEJBQUE7QUNWSCIsImZpbGUiOiJzcmMvYXBwL3JlcXVlc3RzL3JlcXVlc3RzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1saXN0IHtcclxuXHRiYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcclxuXHRtYXJnaW46IDA7XHJcblx0cGFkZGluZzogMDtcclxuXHR3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XHJcblx0bWFyZ2luOiAwIGF1dG87XHJcblx0cGFkZGluZy10b3A6IDI2cHg7XHJcblxyXG5cdGgyIHtcclxuXHRcdG1hcmdpbjogMDtcclxuXHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcclxuXHRcdGZvbnQtc2l6ZTogMXJlbTtcclxuXHRcdGZvbnQtd2VpZ2h0OiA1MDA7XHJcblx0XHRwYWRkaW5nLWJvdHRvbTogMThweDtcclxuXHR9XHJcblxyXG5cdGlvbi1pdGVtIHtcclxuXHRcdHBhZGRpbmc6IDA7XHJcblx0XHRiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XHJcblx0XHQtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XHJcblx0XHQtLWlubmVyLW1pbi1oZWlnaHQ6IHVuc2V0ICFpbXBvcnRhbnQ7XHJcblx0XHQtLXBhZGRpbmctc3RhcnQ6IDA7XHJcblx0XHQtLWhpZ2hsaWdoLWNvbG9yLWZvY3VzZWQ6IHZhcigtLXRyYW5zcGFyZW50KSAhaW1wb3J0YW50O1xyXG5cdFx0bWFyZ2luLWJvdHRvbTogMTBweDtcclxuXHJcblx0XHQuaXRlbV9pbm5lciB7XHJcblx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cclxuXHRcdFx0LmRhdGVfdGltZSB7XHJcblx0XHRcdFx0YmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRcdFx0cGFkZGluZzogMTBweCAxMnB4O1xyXG5cdFx0XHRcdG1pbi13aWR0aDogMTAwcHg7XHJcblx0XHRcdFx0Ym9yZGVyLXJhZGl1czogNHB4IDBweCAwcHggNHB4O1xyXG5cclxuXHRcdFx0XHRoMyB7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRjb2xvcjogdmFyKC0td2hpdGUpO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAxLjE1cmVtO1xyXG5cdFx0XHRcdFx0cGFkZGluZy1ib3R0b206IDVweDtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdGg0IHtcclxuXHRcdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRcdGNvbG9yOiAjY2RkNmVkO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAuNzdyZW07XHJcblx0XHRcdFx0XHRwYWRkaW5nLWJvdHRvbTogN3B4O1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aDUge1xyXG5cdFx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdFx0Y29sb3I6IHZhcigtLXdoaXRlKTtcclxuXHRcdFx0XHRcdGZvbnQtc2l6ZTogLjc3cmVtO1xyXG5cclxuXHRcdFx0XHRcdGlvbi1pY29uIHtcclxuXHRcdFx0XHRcdFx0Y29sb3I6ICNjZGQ2ZWQ7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0LmFwcG9pbnRtZW50X2RldGFpbHMge1xyXG5cdFx0XHRcdHBhZGRpbmc6IDlweCAxMnB4O1xyXG5cdFx0XHRcdGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcblx0XHRcdFx0Ym9yZGVyLWxlZnQ6IDA7XHJcblx0XHRcdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRcdFx0Ym9yZGVyLXJhZGl1czogMHB4IDRweCA0cHggMHB4O1xyXG5cclxuXHRcdFx0XHRoMyB7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcclxuXHRcdFx0XHRcdGZvbnQtc2l6ZTogMS4ycmVtO1xyXG5cdFx0XHRcdFx0cGFkZGluZy1ib3R0b206IDVweDtcclxuXHRcdFx0XHRcdGZvbnQtd2VpZ2h0OiA3MDA7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRoNCB7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XHJcblx0XHRcdFx0XHRmb250LXNpemU6IC43N3JlbTtcclxuXHRcdFx0XHRcdHBhZGRpbmctYm90dG9tOiA3cHg7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRoNSB7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcclxuXHRcdFx0XHRcdGZvbnQtc2l6ZTogLjc3cmVtO1xyXG5cdFx0XHRcdFx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblx0LnBhc3RfYXBwb2ludG1lbnR7XHJcblx0XHRwYWRkaW5nLXRvcDogMjdweDtcclxuXHRcdGlvbi1pdGVtIC5pdGVtX2lubmVyIC5kYXRlX3RpbWV7XHJcblx0XHRcdGJhY2tncm91bmQ6IHZhcigtLWJnLWRhcmspO1xyXG5cdFx0fVxyXG5cclxuXHR9XHJcbn0iLCJpb24tbGlzdCB7XG4gIGJhY2tncm91bmQ6IHZhcigtLXRyYW5zcGFyZW50KSAhaW1wb3J0YW50O1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDA7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA0MHB4KTtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHBhZGRpbmctdG9wOiAyNnB4O1xufVxuaW9uLWxpc3QgaDIge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBmb250LXdlaWdodDogNTAwO1xuICBwYWRkaW5nLWJvdHRvbTogMThweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIHtcbiAgcGFkZGluZzogMDtcbiAgYmFja2dyb3VuZDogdmFyKC0td2hpdGUpO1xuICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XG4gIC0taW5uZXItbWluLWhlaWdodDogdW5zZXQgIWltcG9ydGFudDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICAtLWhpZ2hsaWdoLWNvbG9yLWZvY3VzZWQ6IHZhcigtLXRyYW5zcGFyZW50KSAhaW1wb3J0YW50O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIge1xuICB3aWR0aDogMTAwJTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC5kYXRlX3RpbWUge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1wcmltYXJ5KTtcbiAgcGFkZGluZzogMTBweCAxMnB4O1xuICBtaW4td2lkdGg6IDEwMHB4O1xuICBib3JkZXItcmFkaXVzOiA0cHggMHB4IDBweCA0cHg7XG59XG5pb24tbGlzdCBpb24taXRlbSAuaXRlbV9pbm5lciAuZGF0ZV90aW1lIGgzIHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0td2hpdGUpO1xuICBmb250LXNpemU6IDEuMTVyZW07XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG5pb24tbGlzdCBpb24taXRlbSAuaXRlbV9pbm5lciAuZGF0ZV90aW1lIGg0IHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogI2NkZDZlZDtcbiAgZm9udC1zaXplOiAwLjc3cmVtO1xuICBwYWRkaW5nLWJvdHRvbTogN3B4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmRhdGVfdGltZSBoNSB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXdoaXRlKTtcbiAgZm9udC1zaXplOiAwLjc3cmVtO1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmRhdGVfdGltZSBoNSBpb24taWNvbiB7XG4gIGNvbG9yOiAjY2RkNmVkO1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmFwcG9pbnRtZW50X2RldGFpbHMge1xuICBwYWRkaW5nOiA5cHggMTJweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgYm9yZGVyLWxlZnQ6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiAwcHggNHB4IDRweCAwcHg7XG59XG5pb24tbGlzdCBpb24taXRlbSAuaXRlbV9pbm5lciAuYXBwb2ludG1lbnRfZGV0YWlscyBoMyB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtZGFyayk7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICBmb250LXdlaWdodDogNzAwO1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmFwcG9pbnRtZW50X2RldGFpbHMgaDQge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcbiAgZm9udC1zaXplOiAwLjc3cmVtO1xuICBwYWRkaW5nLWJvdHRvbTogN3B4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmFwcG9pbnRtZW50X2RldGFpbHMgaDUge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xuICBmb250LXNpemU6IDAuNzdyZW07XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5pb24tbGlzdCAucGFzdF9hcHBvaW50bWVudCB7XG4gIHBhZGRpbmctdG9wOiAyN3B4O1xufVxuaW9uLWxpc3QgLnBhc3RfYXBwb2ludG1lbnQgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmRhdGVfdGltZSB7XG4gIGJhY2tncm91bmQ6IHZhcigtLWJnLWRhcmspO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/requests/requests.page.ts":
  /*!*******************************************!*\
    !*** ./src/app/requests/requests.page.ts ***!
    \*******************************************/

  /*! exports provided: RequestsPage */

  /***/
  function srcAppRequestsRequestsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RequestsPage", function () {
      return RequestsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    let RequestsPage = class RequestsPage {
      constructor() {}

      ngOnInit() {}

    };
    RequestsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-requests',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./requests.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/requests/requests.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./requests.page.scss */
      "./src/app/requests/requests.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], RequestsPage);
    /***/
  }
}]);
//# sourceMappingURL=requests-requests-module-es5.js.map