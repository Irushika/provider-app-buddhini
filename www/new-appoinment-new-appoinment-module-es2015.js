(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["new-appoinment-new-appoinment-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/new-appoinment/new-appoinment.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/new-appoinment/new-appoinment.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n\t<ion-toolbar>\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-menu-button></ion-menu-button>\n\t\t</ion-buttons>\n\t\t<ion-title></ion-title>\n\t</ion-toolbar>\n\t<h1>{{'New Requests' | translate}}</h1>\n</ion-header>\n\n<ion-content>\n\t<ion-list lines=\"none\">\n\t\t<ion-item (click)=\"new_appointment_info(item.id)\" *ngFor=\"let item of appointment | jobFilter: UID| sortdate\">\n\t\t\t<div class=\"item_inner d-flex\">\n\t\t\t\t<div class=\"date_time\">\n\t\t\t\t\t<h4>{{item.date| date:'MMMM d'}}</h4>\n\t\t\t\t\t<h4>{{item.time}}</h4>\n\t\t\t\t\t\n\t\t\t\t\t<h5 class=\"d-flex\">\n\t\t\t\t\t\t<ion-icon class=\"zmdi zmdi-pin ion-text-start\"></ion-icon>\n\t\t\t\t\t\t{{item.adrress_type}}\n\t\t\t\t\t</h5>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"appointment_details\">\n\t\t\t\t\t<h3> {{item.category}} </h3>\n\t\t\t\t\t<h4>{{item.information.substr(0, 30)}}</h4>\n\t\t\t\t\t<h5>{{item.budject| currency:'Rs. '}}</h5>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</ion-item>\n\t</ion-list>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/new-appoinment/new-appoinment-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/new-appoinment/new-appoinment-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: NewAppoinmentPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewAppoinmentPageRoutingModule", function() { return NewAppoinmentPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _new_appoinment_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./new-appoinment.page */ "./src/app/new-appoinment/new-appoinment.page.ts");




const routes = [
    {
        path: '',
        component: _new_appoinment_page__WEBPACK_IMPORTED_MODULE_3__["NewAppoinmentPage"]
    }
];
let NewAppoinmentPageRoutingModule = class NewAppoinmentPageRoutingModule {
};
NewAppoinmentPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NewAppoinmentPageRoutingModule);



/***/ }),

/***/ "./src/app/new-appoinment/new-appoinment.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/new-appoinment/new-appoinment.module.ts ***!
  \*********************************************************/
/*! exports provided: NewAppoinmentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewAppoinmentPageModule", function() { return NewAppoinmentPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _new_appoinment_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./new-appoinment-routing.module */ "./src/app/new-appoinment/new-appoinment-routing.module.ts");
/* harmony import */ var _new_appoinment_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./new-appoinment.page */ "./src/app/new-appoinment/new-appoinment.page.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");









let NewAppoinmentPageModule = class NewAppoinmentPageModule {
};
NewAppoinmentPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"],
            _new_appoinment_routing_module__WEBPACK_IMPORTED_MODULE_6__["NewAppoinmentPageRoutingModule"],
            _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"]
        ],
        declarations: [_new_appoinment_page__WEBPACK_IMPORTED_MODULE_7__["NewAppoinmentPage"]]
    })
], NewAppoinmentPageModule);



/***/ }),

/***/ "./src/app/new-appoinment/new-appoinment.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/new-appoinment/new-appoinment.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-list {\n  background: var(--transparent) !important;\n  margin: 0;\n  padding: 0;\n  width: calc(100% - 40px);\n  margin: 0 auto;\n  padding-top: 26px;\n}\nion-list h2 {\n  margin: 0;\n  color: var(--text-light);\n  font-size: 1rem;\n  font-weight: 500;\n  padding-bottom: 18px;\n}\nion-list ion-item {\n  padding: 0;\n  background: var(--white);\n  --inner-padding-end: 0px;\n  --inner-min-height: unset !important;\n  --padding-start: 0;\n  --highligh-color-focused: var(--transparent) !important;\n  margin-bottom: 10px;\n}\nion-list ion-item .item_inner {\n  width: 100%;\n  overflow: hidden;\n}\nion-list ion-item .item_inner .date_time {\n  background: var(--primary);\n  padding: 10px 12px;\n  min-width: 100px;\n  border-radius: 4px 0px 0px 4px;\n}\nion-list ion-item .item_inner .date_time h3 {\n  margin: 0;\n  color: var(--white);\n  font-size: 1.15rem;\n  padding-bottom: 8px;\n}\nion-list ion-item .item_inner .date_time h4 {\n  margin: 0;\n  color: #cdd6ed;\n  font-size: 0.77rem;\n  padding-bottom: 10px;\n}\nion-list ion-item .item_inner .date_time h5 {\n  margin: 0;\n  color: var(--white);\n  font-size: 0.77rem;\n}\nion-list ion-item .item_inner .date_time h5 ion-icon {\n  color: #cdd6ed;\n}\nion-list ion-item .item_inner .appointment_details {\n  padding: 9px 12px;\n  border: 1px solid #ccc;\n  border-left: 0;\n  width: 100%;\n  border-radius: 0px 4px 4px 0px;\n}\nion-list ion-item .item_inner .appointment_details h3 {\n  margin: 0;\n  color: var(--text-dark);\n  font-size: 1.2rem;\n  padding-bottom: 5px;\n  font-weight: 700;\n}\nion-list ion-item .item_inner .appointment_details h4 {\n  margin: 0;\n  color: var(--text-light);\n  font-size: 0.77rem;\n  padding-bottom: 7px;\n}\nion-list ion-item .item_inner .appointment_details h5 {\n  margin: 0;\n  color: var(--text-dark);\n  font-size: 0.77rem;\n  font-weight: 600;\n}\nion-list .past_appointment {\n  padding-top: 27px;\n}\nion-list .past_appointment ion-item .item_inner .date_time {\n  background: var(--bg-dark);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmV3LWFwcG9pbm1lbnQvRTpcXEVcXHNlcnZpY2VcXGZpcmViYXNlIDA0XFxwcm92aWRlci1hcHAtYnVkZGhpbmkvc3JjXFxhcHBcXG5ldy1hcHBvaW5tZW50XFxuZXctYXBwb2lubWVudC5wYWdlLnNjc3MiLCJzcmMvYXBwL25ldy1hcHBvaW5tZW50L25ldy1hcHBvaW5tZW50LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLHlDQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSx3QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQ0NEO0FEQ0M7RUFDQyxTQUFBO0VBQ0Esd0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtBQ0NGO0FERUM7RUFDQyxVQUFBO0VBQ0Esd0JBQUE7RUFDQSx3QkFBQTtFQUNBLG9DQUFBO0VBQ0Esa0JBQUE7RUFDQSx1REFBQTtFQUNBLG1CQUFBO0FDQUY7QURFRTtFQUNDLFdBQUE7RUFDQSxnQkFBQTtBQ0FIO0FERUc7RUFDQywwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSw4QkFBQTtBQ0FKO0FERUk7RUFDQyxTQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDQUw7QURHSTtFQUNDLFNBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtBQ0RMO0FESUk7RUFDQyxTQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ0ZMO0FESUs7RUFDQyxjQUFBO0FDRk47QURRRztFQUNDLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0FDTko7QURRSTtFQUNDLFNBQUE7RUFDQSx1QkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQ05MO0FEU0k7RUFDQyxTQUFBO0VBQ0Esd0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDUEw7QURVSTtFQUNDLFNBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNSTDtBRGFDO0VBQ0MsaUJBQUE7QUNYRjtBRFlFO0VBQ0MsMEJBQUE7QUNWSCIsImZpbGUiOiJzcmMvYXBwL25ldy1hcHBvaW5tZW50L25ldy1hcHBvaW5tZW50LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1saXN0IHtcclxuXHRiYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcclxuXHRtYXJnaW46IDA7XHJcblx0cGFkZGluZzogMDtcclxuXHR3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XHJcblx0bWFyZ2luOiAwIGF1dG87XHJcblx0cGFkZGluZy10b3A6IDI2cHg7XHJcblxyXG5cdGgyIHtcclxuXHRcdG1hcmdpbjogMDtcclxuXHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcclxuXHRcdGZvbnQtc2l6ZTogMXJlbTtcclxuXHRcdGZvbnQtd2VpZ2h0OiA1MDA7XHJcblx0XHRwYWRkaW5nLWJvdHRvbTogMThweDtcclxuXHR9XHJcblxyXG5cdGlvbi1pdGVtIHtcclxuXHRcdHBhZGRpbmc6IDA7XHJcblx0XHRiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XHJcblx0XHQtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XHJcblx0XHQtLWlubmVyLW1pbi1oZWlnaHQ6IHVuc2V0ICFpbXBvcnRhbnQ7XHJcblx0XHQtLXBhZGRpbmctc3RhcnQ6IDA7XHJcblx0XHQtLWhpZ2hsaWdoLWNvbG9yLWZvY3VzZWQ6IHZhcigtLXRyYW5zcGFyZW50KSAhaW1wb3J0YW50O1xyXG5cdFx0bWFyZ2luLWJvdHRvbTogMTBweDtcclxuXHJcblx0XHQuaXRlbV9pbm5lciB7XHJcblx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cclxuXHRcdFx0LmRhdGVfdGltZSB7XHJcblx0XHRcdFx0YmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRcdFx0cGFkZGluZzogMTBweCAxMnB4O1xyXG5cdFx0XHRcdG1pbi13aWR0aDogMTAwcHg7XHJcblx0XHRcdFx0Ym9yZGVyLXJhZGl1czogNHB4IDBweCAwcHggNHB4O1xyXG5cclxuXHRcdFx0XHRoMyB7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRjb2xvcjogdmFyKC0td2hpdGUpO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAxLjE1cmVtO1xyXG5cdFx0XHRcdFx0cGFkZGluZy1ib3R0b206IDhweDtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdGg0IHtcclxuXHRcdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRcdGNvbG9yOiAjY2RkNmVkO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAuNzdyZW07XHJcblx0XHRcdFx0XHRwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdGg1IHtcclxuXHRcdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRcdGNvbG9yOiB2YXIoLS13aGl0ZSk7XHJcblx0XHRcdFx0XHRmb250LXNpemU6IC43N3JlbTtcclxuXHJcblx0XHRcdFx0XHRpb24taWNvbiB7XHJcblx0XHRcdFx0XHRcdGNvbG9yOiAjY2RkNmVkO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdH1cclxuXHJcblx0XHRcdC5hcHBvaW50bWVudF9kZXRhaWxzIHtcclxuXHRcdFx0XHRwYWRkaW5nOiA5cHggMTJweDtcclxuXHRcdFx0XHRib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG5cdFx0XHRcdGJvcmRlci1sZWZ0OiAwO1xyXG5cdFx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRcdGJvcmRlci1yYWRpdXM6IDBweCA0cHggNHB4IDBweDtcclxuXHJcblx0XHRcdFx0aDMge1xyXG5cdFx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdFx0Y29sb3I6IHZhcigtLXRleHQtZGFyayk7XHJcblx0XHRcdFx0XHRmb250LXNpemU6IDEuMnJlbTtcclxuXHRcdFx0XHRcdHBhZGRpbmctYm90dG9tOiA1cHg7XHJcblx0XHRcdFx0XHRmb250LXdlaWdodDogNzAwO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aDQge1xyXG5cdFx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdFx0Y29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAuNzdyZW07XHJcblx0XHRcdFx0XHRwYWRkaW5nLWJvdHRvbTogN3B4O1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aDUge1xyXG5cdFx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdFx0Y29sb3I6IHZhcigtLXRleHQtZGFyayk7XHJcblx0XHRcdFx0XHRmb250LXNpemU6IC43N3JlbTtcclxuXHRcdFx0XHRcdGZvbnQtd2VpZ2h0OiA2MDA7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cdC5wYXN0X2FwcG9pbnRtZW50e1xyXG5cdFx0cGFkZGluZy10b3A6IDI3cHg7XHJcblx0XHRpb24taXRlbSAuaXRlbV9pbm5lciAuZGF0ZV90aW1le1xyXG5cdFx0XHRiYWNrZ3JvdW5kOiB2YXIoLS1iZy1kYXJrKTtcclxuXHRcdH1cclxuXHJcblx0fVxyXG59IiwiaW9uLWxpc3Qge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICB3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBwYWRkaW5nLXRvcDogMjZweDtcbn1cbmlvbi1saXN0IGgyIHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgcGFkZGluZy1ib3R0b206IDE4cHg7XG59XG5pb24tbGlzdCBpb24taXRlbSB7XG4gIHBhZGRpbmc6IDA7XG4gIGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcbiAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xuICAtLWlubmVyLW1pbi1oZWlnaHQ6IHVuc2V0ICFpbXBvcnRhbnQ7XG4gIC0tcGFkZGluZy1zdGFydDogMDtcbiAgLS1oaWdobGlnaC1jb2xvci1mb2N1c2VkOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5pb24tbGlzdCBpb24taXRlbSAuaXRlbV9pbm5lciAuZGF0ZV90aW1lIHtcbiAgYmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XG4gIHBhZGRpbmc6IDEwcHggMTJweDtcbiAgbWluLXdpZHRoOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogNHB4IDBweCAwcHggNHB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmRhdGVfdGltZSBoMyB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXdoaXRlKTtcbiAgZm9udC1zaXplOiAxLjE1cmVtO1xuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmRhdGVfdGltZSBoNCB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6ICNjZGQ2ZWQ7XG4gIGZvbnQtc2l6ZTogMC43N3JlbTtcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XG59XG5pb24tbGlzdCBpb24taXRlbSAuaXRlbV9pbm5lciAuZGF0ZV90aW1lIGg1IHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0td2hpdGUpO1xuICBmb250LXNpemU6IDAuNzdyZW07XG59XG5pb24tbGlzdCBpb24taXRlbSAuaXRlbV9pbm5lciAuZGF0ZV90aW1lIGg1IGlvbi1pY29uIHtcbiAgY29sb3I6ICNjZGQ2ZWQ7XG59XG5pb24tbGlzdCBpb24taXRlbSAuaXRlbV9pbm5lciAuYXBwb2ludG1lbnRfZGV0YWlscyB7XG4gIHBhZGRpbmc6IDlweCAxMnB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBib3JkZXItbGVmdDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDBweCA0cHggNHB4IDBweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC5hcHBvaW50bWVudF9kZXRhaWxzIGgzIHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcbiAgZm9udC1zaXplOiAxLjJyZW07XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5pb24tbGlzdCBpb24taXRlbSAuaXRlbV9pbm5lciAuYXBwb2ludG1lbnRfZGV0YWlscyBoNCB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuICBmb250LXNpemU6IDAuNzdyZW07XG4gIHBhZGRpbmctYm90dG9tOiA3cHg7XG59XG5pb24tbGlzdCBpb24taXRlbSAuaXRlbV9pbm5lciAuYXBwb2ludG1lbnRfZGV0YWlscyBoNSB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtZGFyayk7XG4gIGZvbnQtc2l6ZTogMC43N3JlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbmlvbi1saXN0IC5wYXN0X2FwcG9pbnRtZW50IHtcbiAgcGFkZGluZy10b3A6IDI3cHg7XG59XG5pb24tbGlzdCAucGFzdF9hcHBvaW50bWVudCBpb24taXRlbSAuaXRlbV9pbm5lciAuZGF0ZV90aW1lIHtcbiAgYmFja2dyb3VuZDogdmFyKC0tYmctZGFyayk7XG59Il19 */");

/***/ }),

/***/ "./src/app/new-appoinment/new-appoinment.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/new-appoinment/new-appoinment.page.ts ***!
  \*******************************************************/
/*! exports provided: NewAppoinmentPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewAppoinmentPage", function() { return NewAppoinmentPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/fesm2015/angular-fire-database.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");






let NewAppoinmentPage = class NewAppoinmentPage {
    constructor(navCtrl, route, db, alertController, storage) {
        this.navCtrl = navCtrl;
        this.route = route;
        this.db = db;
        this.alertController = alertController;
        this.storage = storage;
    }
    ngOnInit() {
        this.loaddetails();
    }
    loadingAppointment() {
        let userInfo = JSON.parse(localStorage.getItem('userInfo'));
        console.log(userInfo);
        this.category = userInfo.category;
        console.log("cat 2", this.category);
        this.storage.get('user').then(user => {
            console.log(user);
            if (user) {
                this.UID = user.UID;
            }
        });
        // let mycategory=localStorage.getItem(this.mycategory);
        this.db
            .list("/jobs", (ref) => ref.orderByChild("category").equalTo(this.category))
            .valueChanges([], { idField: 'id' })
            .subscribe((appointment) => {
            console.log(appointment);
            this.appointment = appointment;
            window.localStorage.setItem("apointmentInfo", JSON.stringify(appointment[0]));
        });
    }
    presentAlert() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: "Alert",
                message: "No appointment found.",
                buttons: ["OK"],
            });
            yield alert.present();
        });
    }
    new_appointment_info(JID) {
        console.log(JID);
        this.route.navigate(['./appoinment-submit', JID]);
    }
    loaddetails() {
        console.log("my-profile");
        let phone = window.localStorage.getItem("userPhone");
        if (phone) {
            console.log(phone);
            this.db
                .list("/users", (ref) => ref.orderByChild("phoneNumber").equalTo(phone))
                .valueChanges()
                .subscribe((user) => {
                if (user.length > 0) {
                    this.user = user[0];
                    this.category = this.user.category;
                    console.log('cat 1', this.category);
                    window.localStorage.setItem("userInfo", JSON.stringify(user[0]));
                    window.localStorage.setItem("myCategory", this.category);
                    // window.localStorage.setItem("UID", this.UID);
                    //Now load Appointements 
                    this.loadingAppointment();
                }
                else {
                    this.presentAlert();
                }
            });
        }
    }
};
NewAppoinmentPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] }
];
NewAppoinmentPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-new-appoinment',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./new-appoinment.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/new-appoinment/new-appoinment.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./new-appoinment.page.scss */ "./src/app/new-appoinment/new-appoinment.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]])
], NewAppoinmentPage);



/***/ })

}]);
//# sourceMappingURL=new-appoinment-new-appoinment-module-es2015.js.map