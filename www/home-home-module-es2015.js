(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"bg_transparent\">\n\t<ion-toolbar>\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-menu-button></ion-menu-button>\n\t\t</ion-buttons>\n\t\t<ion-title><img src=\"assets/images/light_logo.png\"></ion-title>\n\t</ion-toolbar>\n\t<div class=\"header_bg\">\n\t\t<img src=\"assets/images/header_bg.png\">\n\t</div>\n\t<div class=\"search_box d-flex\">\n\t\t<ion-icon class=\"zmdi zmdi-search ion-text-start\"></ion-icon>\n\t\t<ion-searchbar class=\"ion-no-padding\" searchIcon=\"hide\" placeholder=\"{{'search_text' | translate}}\"></ion-searchbar>\n\t</div>\n</ion-header>\n\n<ion-content>\n\t<div class=\"services\">\n\t\t\n\t\t<h2>{{'services' | translate}}</h2>\n\t\t<h2 class=\"d-flex\"> <span class=\"end ion-text-end\" (click)=\"view_cat()\">{{'view_all' | translate}}</span></h2>\n\t\t<div class=\"container\">\n\t\t\t<div class=\"item\" *ngFor=\"let cat of categories;index as i\">\n\t\t\t  <div *ngIf=\"cat?.main\"\n\t\t\t\tclass=\"container-wrapper\" [style.background-image]=\"getUrl(cat.img)\"\n\t\t\t\tstyle=\"background-size: cover;\n\t\t\tbackground-position: center;\"\n\t\t\t(click)=\"adddetails()\"\n\t\t\t  >\n\t\t\t\t<div class=\"overlay\">\n\t\t\t\t  <div class=\"service-title\">{{cat?.name}}</div>\n\t\t\t\t</div>\n\t\t\t  </div>\n\t\t\t</div>\n\t\t  </div>\n\n\t</div>\n\n\t<ion-list lines=\"none\">\n\t\t<h2 class=\"d-flex\">{{'upcoming_appointment' | translate}} <span class=\"end ion-text-end\" (click)=\"view_all()\">{{'view_all' | translate}}</span></h2>\n\t\t<ion-item (click)=\"appointment_info()\"> \n\t\t\t<div class=\"item_inner d-flex\">\n\t\t\t\t<div class=\"date_time\">\n\t\t\t\t\t<h3>13 June</h3>\n\t\t\t\t\t<h4>11:00 am</h4>\n\t\t\t\t\t<h5 class=\"d-flex\">\n\t\t\t\t\t\t<ion-icon class=\"zmdi zmdi-pin ion-text-start\"></ion-icon>\n\t\t\t\t\t\tHome\n\t\t\t\t\t</h5>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"appointment_details\">\n\t\t\t\t\t<h3> Fix my Appliances </h3>\n\t\t\t\t\t<h4>Air Conditionar</h4>\n\t\t\t\t\t<h5>$140.00</h5>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</ion-item>\n\t</ion-list>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/home/home-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"]
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/home/home-routing.module.ts");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");








let HomePageModule = class HomePageModule {
};
HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_6__["HomePageRoutingModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_7__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header ion-toolbar ion-buttons ion-menu-button {\n  color: var(--white) !important;\n}\nion-header ion-toolbar ion-title {\n  position: absolute !important;\n  width: 100%;\n  top: 0;\n  left: 0;\n  padding: 0 15px !important;\n  text-align: center !important;\n}\nion-header ion-toolbar ion-title img {\n  width: 110px;\n  position: relative;\n  top: 5px;\n}\nion-header .header_bg {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100px;\n  overflow: hidden;\n}\nion-header .search_box {\n  background: var(--input_filed_bg);\n  border-radius: 4px;\n  margin: 0 auto;\n  padding: 0 15px;\n  position: relative;\n  overflow: hidden;\n  z-index: 99;\n  width: calc(100% - 30px);\n  min-height: 52px;\n  margin-top: 14px;\n}\nion-header .search_box ion-icon {\n  color: var(--text-light);\n  font-size: 1.4rem;\n  min-width: 35px;\n  height: 35px;\n  line-height: 35px;\n  z-index: 99;\n}\nion-header .search_box ion-searchbar {\n  --background: var(--transparent) !important;\n  --color: var(--text-light);\n  --placeholder-opacity: 1;\n  --placeholder-font-weight: 400 !important;\n  --box-shadow: none !important;\n}\n.services {\n  width: calc(100% - 40px);\n  margin: 0 auto;\n  padding-top: 30px;\n  padding-bottom: 35px;\n}\n.services h2 {\n  margin: 0;\n  font-size: 1.2rem;\n  font-weight: 600;\n  margin-bottom: 7px;\n}\n.services ion-row {\n  margin: 0 -10px;\n}\n.services ion-row ion-col {\n  padding: 10px;\n}\n.services ion-row ion-col .services_box {\n  position: relative;\n  width: 100%;\n  height: 110px;\n  border-radius: 4px;\n  overflow: hidden;\n}\n.services ion-row ion-col .services_box .img_box {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n}\n.services ion-row ion-col .services_box::after {\n  content: \"\";\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.48);\n}\n.services ion-row ion-col .services_box h3 {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  width: 100%;\n  color: var(--white);\n  z-index: 99;\n  margin: 0;\n  font-size: 1.1rem;\n  padding: 12px;\n  letter-spacing: 0.5px;\n  line-height: 21px;\n}\nion-list {\n  background: var(--transparent) !important;\n  margin: 0;\n  padding: 0;\n  width: calc(100% - 40px);\n  margin: 0 auto;\n}\nion-list h2 {\n  margin: 0;\n  font-size: 1.2rem;\n  font-weight: 600;\n  margin-bottom: 7px;\n}\nion-list h2 span {\n  color: var(--text-light);\n  font-size: 0.85rem;\n  font-weight: 500;\n  min-width: 80px;\n}\nion-list ion-item {\n  padding: 12px 0;\n  background: var(--white);\n  --inner-padding-end: 0px;\n  --inner-min-height: unset !important;\n  --padding-start: 0;\n  --highligh-color-focused: var(--transparent) !important;\n  margin-bottom: 8px;\n}\nion-list ion-item .item_inner {\n  width: 100%;\n  overflow: hidden;\n}\nion-list ion-item .item_inner .date_time {\n  background: var(--primary);\n  padding: 10px 12px;\n  min-width: 100px;\n  border-radius: 4px 0px 0px 4px;\n}\nion-list ion-item .item_inner .date_time h3 {\n  margin: 0;\n  color: var(--white);\n  font-size: 1.15rem;\n  padding-bottom: 5px;\n}\nion-list ion-item .item_inner .date_time h4 {\n  margin: 0;\n  color: #cdd6ed;\n  font-size: 0.77rem;\n  padding-bottom: 7px;\n}\nion-list ion-item .item_inner .date_time h5 {\n  margin: 0;\n  color: var(--white);\n  font-size: 0.77rem;\n}\nion-list ion-item .item_inner .date_time h5 ion-icon {\n  color: #cdd6ed;\n}\nion-list ion-item .item_inner .appointment_details {\n  padding: 9px 12px;\n  border: 1px solid #ccc;\n  border-left: 0;\n  width: 100%;\n  border-radius: 0px 4px 4px 0px;\n}\nion-list ion-item .item_inner .appointment_details h3 {\n  margin: 0;\n  color: var(--text-dark);\n  font-size: 1.2rem;\n  padding-bottom: 5px;\n  font-weight: 700;\n}\nion-list ion-item .item_inner .appointment_details h4 {\n  margin: 0;\n  color: var(--text-light);\n  font-size: 0.77rem;\n  padding-bottom: 7px;\n}\nion-list ion-item .item_inner .appointment_details h5 {\n  margin: 0;\n  color: var(--text-dark);\n  font-size: 0.77rem;\n  font-weight: 600;\n}\n.container {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-flow: row wrap;\n  height: 100%;\n}\n.container .item {\n  width: 50%;\n  position: relative;\n}\n.container .item .container-wrapper {\n  height: 115px;\n  overflow: hidden;\n  box-shadow: 0px 0px 7px 0px rgba(0, 0, 0, 0.75);\n  border-radius: 10px;\n  margin: 10px;\n}\n.container .item .container-wrapper .overlay {\n  background-color: rgba(0, 0, 0, 0.15);\n  height: 100%;\n  z-index: 2;\n}\n.container .item .service-title {\n  position: absolute;\n  bottom: 20px;\n  left: 20px;\n  color: #fff;\n  font-weight: 600;\n  line-height: 25px;\n  width: -webkit-min-content;\n  width: -moz-min-content;\n  width: min-content;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9FOlxcRVxcc2VydmljZVxcZmlyZWJhc2UgMDRcXHByb3ZpZGVyLWFwcC1idWRkaGluaS9zcmNcXGFwcFxcaG9tZVxcaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR0U7RUFDQyw4QkFBQTtBQ0ZIO0FES0U7RUFDQyw2QkFBQTtFQUNBLFdBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLDBCQUFBO0VBQ0EsNkJBQUE7QUNISDtBREtHO0VBQ0MsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtBQ0hKO0FEU0M7RUFDQyxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQ1BGO0FEVUM7RUFDQyxpQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLHdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQ1JGO0FEVUU7RUFDQyx3QkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7QUNSSDtBRFdFO0VBQ0MsMkNBQUE7RUFDQSwwQkFBQTtFQUNBLHdCQUFBO0VBQ0EseUNBQUE7RUFDQSw2QkFBQTtBQ1RIO0FEY0E7RUFDQyx3QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0FDWEQ7QURhQztFQUNDLFNBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNYRjtBRGNDO0VBQ0MsZUFBQTtBQ1pGO0FEY0U7RUFDQyxhQUFBO0FDWkg7QURjRztFQUNDLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDWko7QURjSTtFQUNDLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ1pMO0FEZ0JJO0VBQ0MsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0FDZEw7QURpQkk7RUFDQyxrQkFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtFQUNBLGlCQUFBO0FDZkw7QURzQkE7RUFDQyx5Q0FBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0Esd0JBQUE7RUFDQSxjQUFBO0FDbkJEO0FEcUJDO0VBQ0MsU0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ25CRjtBRHFCRTtFQUNDLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNuQkg7QUR1QkM7RUFDQyxlQUFBO0VBQ0Esd0JBQUE7RUFDQSx3QkFBQTtFQUNBLG9DQUFBO0VBQ0Esa0JBQUE7RUFDQSx1REFBQTtFQUNBLGtCQUFBO0FDckJGO0FEdUJFO0VBQ0MsV0FBQTtFQUNBLGdCQUFBO0FDckJIO0FEdUJHO0VBQ0MsMEJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsOEJBQUE7QUNyQko7QUR1Qkk7RUFDQyxTQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDckJMO0FEd0JJO0VBQ0MsU0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDdEJMO0FEeUJJO0VBQ0MsU0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUN2Qkw7QUR5Qks7RUFDQyxjQUFBO0FDdkJOO0FENkJHO0VBQ0MsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsOEJBQUE7QUMzQko7QUQ2Qkk7RUFDQyxTQUFBO0VBQ0EsdUJBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUMzQkw7QUQ4Qkk7RUFDQyxTQUFBO0VBQ0Esd0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDNUJMO0FEK0JJO0VBQ0MsU0FBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQzdCTDtBRHFDQTtFQUNDLG9CQUFBO0VBQUEsYUFBQTtFQUNBLDhCQUFBO0VBQUEsNkJBQUE7VUFBQSxtQkFBQTtFQUNBLFlBQUE7QUNsQ0Q7QURtQ0M7RUFDRSxVQUFBO0VBQ0Esa0JBQUE7QUNqQ0g7QURrQ0c7RUFDRCxhQUFBO0VBQ0EsZ0JBQUE7RUFHQSwrQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ2hDRjtBRGlDRTtFQUNFLHFDQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7QUMvQko7QURrQ0c7RUFDRCxrQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSwwQkFBQTtFQUFBLHVCQUFBO0VBQUEsa0JBQUE7QUNoQ0YiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlciB7XG5cblx0aW9uLXRvb2xiYXIge1xuXHRcdGlvbi1idXR0b25zIGlvbi1tZW51LWJ1dHRvbiB7XG5cdFx0XHRjb2xvcjogdmFyKC0td2hpdGUpICFpbXBvcnRhbnQ7XG5cdFx0fVxuXG5cdFx0aW9uLXRpdGxlIHtcblx0XHRcdHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuXHRcdFx0d2lkdGg6IDEwMCU7XG5cdFx0XHR0b3A6IDA7XG5cdFx0XHRsZWZ0OiAwO1xuXHRcdFx0cGFkZGluZzogMCAxNXB4ICFpbXBvcnRhbnQ7XG5cdFx0XHR0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcblxuXHRcdFx0aW1nIHtcblx0XHRcdFx0d2lkdGg6IDExMHB4O1xuXHRcdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XG5cdFx0XHRcdHRvcDogNXB4O1xuXHRcdFx0fVxuXG5cdFx0fVxuXHR9XG5cblx0LmhlYWRlcl9iZyB7XG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xuXHRcdHRvcDogMDtcblx0XHRsZWZ0OiAwO1xuXHRcdHdpZHRoOiAxMDAlO1xuXHRcdGhlaWdodDogMTAwcHg7XG5cdFx0b3ZlcmZsb3c6IGhpZGRlbjtcblx0fVxuXG5cdC5zZWFyY2hfYm94IHtcblx0XHRiYWNrZ3JvdW5kOiB2YXIoLS1pbnB1dF9maWxlZF9iZyk7XG5cdFx0Ym9yZGVyLXJhZGl1czogNHB4O1xuXHRcdG1hcmdpbjogMCBhdXRvO1xuXHRcdHBhZGRpbmc6IDAgMTVweDtcblx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XG5cdFx0b3ZlcmZsb3c6IGhpZGRlbjtcblx0XHR6LWluZGV4OiA5OTtcblx0XHR3aWR0aDogY2FsYygxMDAlIC0gMzBweCk7XG5cdFx0bWluLWhlaWdodDogNTJweDtcblx0XHRtYXJnaW4tdG9wOiAxNHB4O1xuXG5cdFx0aW9uLWljb24ge1xuXHRcdFx0Y29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuXHRcdFx0Zm9udC1zaXplOiAxLjRyZW07XG5cdFx0XHRtaW4td2lkdGg6IDM1cHg7XG5cdFx0XHRoZWlnaHQ6IDM1cHg7XG5cdFx0XHRsaW5lLWhlaWdodDogMzVweDtcblx0XHRcdHotaW5kZXg6IDk5O1xuXHRcdH1cblxuXHRcdGlvbi1zZWFyY2hiYXIge1xuXHRcdFx0LS1iYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcblx0XHRcdC0tY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuXHRcdFx0LS1wbGFjZWhvbGRlci1vcGFjaXR5OiAxO1xuXHRcdFx0LS1wbGFjZWhvbGRlci1mb250LXdlaWdodDogNDAwICFpbXBvcnRhbnQ7XG5cdFx0XHQtLWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcblx0XHR9XG5cdH1cbn1cblxuLnNlcnZpY2VzIHtcblx0d2lkdGg6IGNhbGMoMTAwJSAtIDQwcHgpO1xuXHRtYXJnaW46IDAgYXV0bztcblx0cGFkZGluZy10b3A6IDMwcHg7XG5cdHBhZGRpbmctYm90dG9tOiAzNXB4O1xuXG5cdGgyIHtcblx0XHRtYXJnaW46IDA7XG5cdFx0Zm9udC1zaXplOiAxLjJyZW07XG5cdFx0Zm9udC13ZWlnaHQ6IDYwMDtcblx0XHRtYXJnaW4tYm90dG9tOiA3cHg7XG5cdH1cblxuXHRpb24tcm93IHtcblx0XHRtYXJnaW46IDAgLTEwcHg7XG5cblx0XHRpb24tY29sIHtcblx0XHRcdHBhZGRpbmc6IDEwcHg7XG5cblx0XHRcdC5zZXJ2aWNlc19ib3gge1xuXHRcdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XG5cdFx0XHRcdHdpZHRoOiAxMDAlO1xuXHRcdFx0XHRoZWlnaHQ6IDExMHB4O1xuXHRcdFx0XHRib3JkZXItcmFkaXVzOiA0cHg7XG5cdFx0XHRcdG92ZXJmbG93OiBoaWRkZW47XG5cblx0XHRcdFx0LmltZ19ib3gge1xuXHRcdFx0XHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcblx0XHRcdFx0XHR0b3A6IDA7XG5cdFx0XHRcdFx0bGVmdDogMDtcblx0XHRcdFx0XHR3aWR0aDogMTAwJTtcblx0XHRcdFx0XHRoZWlnaHQ6IDEwMCU7XG5cblx0XHRcdFx0fVxuXG5cdFx0XHRcdCY6OmFmdGVyIHtcblx0XHRcdFx0XHRjb250ZW50OiAnJztcblx0XHRcdFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XG5cdFx0XHRcdFx0dG9wOiAwO1xuXHRcdFx0XHRcdGxlZnQ6IDA7XG5cdFx0XHRcdFx0d2lkdGg6IDEwMCU7XG5cdFx0XHRcdFx0aGVpZ2h0OiAxMDAlO1xuXHRcdFx0XHRcdGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC40OCk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRoMyB7XG5cdFx0XHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xuXHRcdFx0XHRcdGJvdHRvbTogMDtcblx0XHRcdFx0XHRsZWZ0OiAwO1xuXHRcdFx0XHRcdHdpZHRoOiAxMDAlO1xuXHRcdFx0XHRcdGNvbG9yOiB2YXIoLS13aGl0ZSk7XG5cdFx0XHRcdFx0ei1pbmRleDogOTk7XG5cdFx0XHRcdFx0bWFyZ2luOiAwO1xuXHRcdFx0XHRcdGZvbnQtc2l6ZTogMS4xcmVtO1xuXHRcdFx0XHRcdHBhZGRpbmc6IDEycHg7XG5cdFx0XHRcdFx0bGV0dGVyLXNwYWNpbmc6IC41cHg7XG5cdFx0XHRcdFx0bGluZS1oZWlnaHQ6IDIxcHg7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cdH1cbn1cblxuaW9uLWxpc3Qge1xuXHRiYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcblx0bWFyZ2luOiAwO1xuXHRwYWRkaW5nOiAwO1xuXHR3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XG5cdG1hcmdpbjogMCBhdXRvO1xuXG5cdGgyIHtcblx0XHRtYXJnaW46IDA7XG5cdFx0Zm9udC1zaXplOiAxLjJyZW07XG5cdFx0Zm9udC13ZWlnaHQ6IDYwMDtcblx0XHRtYXJnaW4tYm90dG9tOiA3cHg7XG5cblx0XHRzcGFuIHtcblx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcblx0XHRcdGZvbnQtc2l6ZTogLjg1cmVtO1xuXHRcdFx0Zm9udC13ZWlnaHQ6IDUwMDtcblx0XHRcdG1pbi13aWR0aDogODBweDtcblx0XHR9XG5cdH1cblxuXHRpb24taXRlbSB7XG5cdFx0cGFkZGluZzogMTJweCAwO1xuXHRcdGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcblx0XHQtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XG5cdFx0LS1pbm5lci1taW4taGVpZ2h0OiB1bnNldCAhaW1wb3J0YW50O1xuXHRcdC0tcGFkZGluZy1zdGFydDogMDtcblx0XHQtLWhpZ2hsaWdoLWNvbG9yLWZvY3VzZWQ6IHZhcigtLXRyYW5zcGFyZW50KSAhaW1wb3J0YW50O1xuXHRcdG1hcmdpbi1ib3R0b206IDhweDtcblxuXHRcdC5pdGVtX2lubmVyIHtcblx0XHRcdHdpZHRoOiAxMDAlO1xuXHRcdFx0b3ZlcmZsb3c6IGhpZGRlbjtcblxuXHRcdFx0LmRhdGVfdGltZSB7XG5cdFx0XHRcdGJhY2tncm91bmQ6IHZhcigtLXByaW1hcnkpO1xuXHRcdFx0XHRwYWRkaW5nOiAxMHB4IDEycHg7XG5cdFx0XHRcdG1pbi13aWR0aDogMTAwcHg7XG5cdFx0XHRcdGJvcmRlci1yYWRpdXM6IDRweCAwcHggMHB4IDRweDtcblxuXHRcdFx0XHRoMyB7XG5cdFx0XHRcdFx0bWFyZ2luOiAwO1xuXHRcdFx0XHRcdGNvbG9yOiB2YXIoLS13aGl0ZSk7XG5cdFx0XHRcdFx0Zm9udC1zaXplOiAxLjE1cmVtO1xuXHRcdFx0XHRcdHBhZGRpbmctYm90dG9tOiA1cHg7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRoNCB7XG5cdFx0XHRcdFx0bWFyZ2luOiAwO1xuXHRcdFx0XHRcdGNvbG9yOiAjY2RkNmVkO1xuXHRcdFx0XHRcdGZvbnQtc2l6ZTogLjc3cmVtO1xuXHRcdFx0XHRcdHBhZGRpbmctYm90dG9tOiA3cHg7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRoNSB7XG5cdFx0XHRcdFx0bWFyZ2luOiAwO1xuXHRcdFx0XHRcdGNvbG9yOiB2YXIoLS13aGl0ZSk7XG5cdFx0XHRcdFx0Zm9udC1zaXplOiAuNzdyZW07XG5cblx0XHRcdFx0XHRpb24taWNvbiB7XG5cdFx0XHRcdFx0XHRjb2xvcjogI2NkZDZlZDtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblxuXHRcdFx0fVxuXG5cdFx0XHQuYXBwb2ludG1lbnRfZGV0YWlscyB7XG5cdFx0XHRcdHBhZGRpbmc6IDlweCAxMnB4O1xuXHRcdFx0XHRib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuXHRcdFx0XHRib3JkZXItbGVmdDogMDtcblx0XHRcdFx0d2lkdGg6IDEwMCU7XG5cdFx0XHRcdGJvcmRlci1yYWRpdXM6IDBweCA0cHggNHB4IDBweDtcblxuXHRcdFx0XHRoMyB7XG5cdFx0XHRcdFx0bWFyZ2luOiAwO1xuXHRcdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xuXHRcdFx0XHRcdGZvbnQtc2l6ZTogMS4ycmVtO1xuXHRcdFx0XHRcdHBhZGRpbmctYm90dG9tOiA1cHg7XG5cdFx0XHRcdFx0Zm9udC13ZWlnaHQ6IDcwMDtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGg0IHtcblx0XHRcdFx0XHRtYXJnaW46IDA7XG5cdFx0XHRcdFx0Y29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuXHRcdFx0XHRcdGZvbnQtc2l6ZTogLjc3cmVtO1xuXHRcdFx0XHRcdHBhZGRpbmctYm90dG9tOiA3cHg7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRoNSB7XG5cdFx0XHRcdFx0bWFyZ2luOiAwO1xuXHRcdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xuXHRcdFx0XHRcdGZvbnQtc2l6ZTogLjc3cmVtO1xuXHRcdFx0XHRcdGZvbnQtd2VpZ2h0OiA2MDA7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cdH1cbn1cblxuXG4uY29udGFpbmVyIHtcblx0ZGlzcGxheTogZmxleDtcblx0ZmxleC1mbG93OiByb3cgd3JhcDtcblx0aGVpZ2h0OiAxMDAlO1xuXHQuaXRlbSB7XG5cdCAgd2lkdGg6IDUwJTtcblx0ICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cdCAgLmNvbnRhaW5lci13cmFwcGVyIHtcblx0XHRoZWlnaHQ6IDExNXB4O1xuXHRcdG92ZXJmbG93OiBoaWRkZW47XG5cdFx0LXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMHB4IDdweCAwcHggcmdiYSgwLCAwLCAwLCAwLjc1KTtcblx0XHQtbW96LWJveC1zaGFkb3c6IDBweCAwcHggN3B4IDBweCByZ2JhKDAsIDAsIDAsIDAuNzUpO1xuXHRcdGJveC1zaGFkb3c6IDBweCAwcHggN3B4IDBweCByZ2JhKDAsIDAsIDAsIDAuNzUpO1xuXHRcdGJvcmRlci1yYWRpdXM6IDEwcHg7XG5cdFx0bWFyZ2luOiAxMHB4O1xuXHRcdC5vdmVybGF5IHtcblx0XHQgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC4xNSk7XG5cdFx0ICBoZWlnaHQ6IDEwMCU7XG5cdFx0ICB6LWluZGV4OiAyO1xuXHRcdH1cblx0ICB9XG5cdCAgLnNlcnZpY2UtdGl0bGUge1xuXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcblx0XHRib3R0b206IDIwcHg7XG5cdFx0bGVmdDogMjBweDtcblx0XHRjb2xvcjogI2ZmZjtcblx0XHRmb250LXdlaWdodDogNjAwO1xuXHRcdGxpbmUtaGVpZ2h0OiAyNXB4O1xuXHRcdHdpZHRoOiBtaW4tY29udGVudDtcblx0ICB9XG5cdH1cbiAgfVxuICBcbiIsImlvbi1oZWFkZXIgaW9uLXRvb2xiYXIgaW9uLWJ1dHRvbnMgaW9uLW1lbnUtYnV0dG9uIHtcbiAgY29sb3I6IHZhcigtLXdoaXRlKSAhaW1wb3J0YW50O1xufVxuaW9uLWhlYWRlciBpb24tdG9vbGJhciBpb24tdGl0bGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcbiAgd2lkdGg6IDEwMCU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgcGFkZGluZzogMCAxNXB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xufVxuaW9uLWhlYWRlciBpb24tdG9vbGJhciBpb24tdGl0bGUgaW1nIHtcbiAgd2lkdGg6IDExMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogNXB4O1xufVxuaW9uLWhlYWRlciAuaGVhZGVyX2JnIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuaW9uLWhlYWRlciAuc2VhcmNoX2JveCB7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlucHV0X2ZpbGVkX2JnKTtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgcGFkZGluZzogMCAxNXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHotaW5kZXg6IDk5O1xuICB3aWR0aDogY2FsYygxMDAlIC0gMzBweCk7XG4gIG1pbi1oZWlnaHQ6IDUycHg7XG4gIG1hcmdpbi10b3A6IDE0cHg7XG59XG5pb24taGVhZGVyIC5zZWFyY2hfYm94IGlvbi1pY29uIHtcbiAgY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuICBmb250LXNpemU6IDEuNHJlbTtcbiAgbWluLXdpZHRoOiAzNXB4O1xuICBoZWlnaHQ6IDM1cHg7XG4gIGxpbmUtaGVpZ2h0OiAzNXB4O1xuICB6LWluZGV4OiA5OTtcbn1cbmlvbi1oZWFkZXIgLnNlYXJjaF9ib3ggaW9uLXNlYXJjaGJhciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XG4gIC0tY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuICAtLXBsYWNlaG9sZGVyLW9wYWNpdHk6IDE7XG4gIC0tcGxhY2Vob2xkZXItZm9udC13ZWlnaHQ6IDQwMCAhaW1wb3J0YW50O1xuICAtLWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLnNlcnZpY2VzIHtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDQwcHgpO1xuICBtYXJnaW46IDAgYXV0bztcbiAgcGFkZGluZy10b3A6IDMwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAzNXB4O1xufVxuLnNlcnZpY2VzIGgyIHtcbiAgbWFyZ2luOiAwO1xuICBmb250LXNpemU6IDEuMnJlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbWFyZ2luLWJvdHRvbTogN3B4O1xufVxuLnNlcnZpY2VzIGlvbi1yb3cge1xuICBtYXJnaW46IDAgLTEwcHg7XG59XG4uc2VydmljZXMgaW9uLXJvdyBpb24tY29sIHtcbiAgcGFkZGluZzogMTBweDtcbn1cbi5zZXJ2aWNlcyBpb24tcm93IGlvbi1jb2wgLnNlcnZpY2VzX2JveCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5zZXJ2aWNlcyBpb24tcm93IGlvbi1jb2wgLnNlcnZpY2VzX2JveCAuaW1nX2JveCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLnNlcnZpY2VzIGlvbi1yb3cgaW9uLWNvbCAuc2VydmljZXNfYm94OjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNDgpO1xufVxuLnNlcnZpY2VzIGlvbi1yb3cgaW9uLWNvbCAuc2VydmljZXNfYm94IGgzIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDA7XG4gIGxlZnQ6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogdmFyKC0td2hpdGUpO1xuICB6LWluZGV4OiA5OTtcbiAgbWFyZ2luOiAwO1xuICBmb250LXNpemU6IDEuMXJlbTtcbiAgcGFkZGluZzogMTJweDtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xuICBsaW5lLWhlaWdodDogMjFweDtcbn1cblxuaW9uLWxpc3Qge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICB3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuaW9uLWxpc3QgaDIge1xuICBtYXJnaW46IDA7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBtYXJnaW4tYm90dG9tOiA3cHg7XG59XG5pb24tbGlzdCBoMiBzcGFuIHtcbiAgY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuICBmb250LXNpemU6IDAuODVyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1pbi13aWR0aDogODBweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIHtcbiAgcGFkZGluZzogMTJweCAwO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcbiAgLS1pbm5lci1taW4taGVpZ2h0OiB1bnNldCAhaW1wb3J0YW50O1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIC0taGlnaGxpZ2gtY29sb3ItZm9jdXNlZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDhweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5pb24tbGlzdCBpb24taXRlbSAuaXRlbV9pbm5lciAuZGF0ZV90aW1lIHtcbiAgYmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XG4gIHBhZGRpbmc6IDEwcHggMTJweDtcbiAgbWluLXdpZHRoOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogNHB4IDBweCAwcHggNHB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmRhdGVfdGltZSBoMyB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXdoaXRlKTtcbiAgZm9udC1zaXplOiAxLjE1cmVtO1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmRhdGVfdGltZSBoNCB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6ICNjZGQ2ZWQ7XG4gIGZvbnQtc2l6ZTogMC43N3JlbTtcbiAgcGFkZGluZy1ib3R0b206IDdweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC5kYXRlX3RpbWUgaDUge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS13aGl0ZSk7XG4gIGZvbnQtc2l6ZTogMC43N3JlbTtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC5kYXRlX3RpbWUgaDUgaW9uLWljb24ge1xuICBjb2xvcjogI2NkZDZlZDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC5hcHBvaW50bWVudF9kZXRhaWxzIHtcbiAgcGFkZGluZzogOXB4IDEycHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIGJvcmRlci1sZWZ0OiAwO1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogMHB4IDRweCA0cHggMHB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gLml0ZW1faW5uZXIgLmFwcG9pbnRtZW50X2RldGFpbHMgaDMge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xuICBmb250LXNpemU6IDEuMnJlbTtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC5hcHBvaW50bWVudF9kZXRhaWxzIGg0IHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XG4gIGZvbnQtc2l6ZTogMC43N3JlbTtcbiAgcGFkZGluZy1ib3R0b206IDdweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIC5pdGVtX2lubmVyIC5hcHBvaW50bWVudF9kZXRhaWxzIGg1IHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcbiAgZm9udC1zaXplOiAwLjc3cmVtO1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4uY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3cgd3JhcDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmNvbnRhaW5lciAuaXRlbSB7XG4gIHdpZHRoOiA1MCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jb250YWluZXIgLml0ZW0gLmNvbnRhaW5lci13cmFwcGVyIHtcbiAgaGVpZ2h0OiAxMTVweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMHB4IDdweCAwcHggcmdiYSgwLCAwLCAwLCAwLjc1KTtcbiAgLW1vei1ib3gtc2hhZG93OiAwcHggMHB4IDdweCAwcHggcmdiYSgwLCAwLCAwLCAwLjc1KTtcbiAgYm94LXNoYWRvdzogMHB4IDBweCA3cHggMHB4IHJnYmEoMCwgMCwgMCwgMC43NSk7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIG1hcmdpbjogMTBweDtcbn1cbi5jb250YWluZXIgLml0ZW0gLmNvbnRhaW5lci13cmFwcGVyIC5vdmVybGF5IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjE1KTtcbiAgaGVpZ2h0OiAxMDAlO1xuICB6LWluZGV4OiAyO1xufVxuLmNvbnRhaW5lciAuaXRlbSAuc2VydmljZS10aXRsZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAyMHB4O1xuICBsZWZ0OiAyMHB4O1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbGluZS1oZWlnaHQ6IDI1cHg7XG4gIHdpZHRoOiBtaW4tY29udGVudDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/fesm2015/angular-fire-database.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");






let HomePage = class HomePage {
    constructor(route, db, alertController, _sanitizer) {
        this.route = route;
        this.db = db;
        this.alertController = alertController;
        this._sanitizer = _sanitizer;
        this.categories = [];
    }
    ngOnInit() {
        let cat = this.db
            .list('/categories')
            .valueChanges()
            .subscribe((cat) => {
            this.categories = cat;
        });
    }
    getUrl(value) {
        return this._sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${value})`);
    }
    adddetails() {
        window.localStorage.setItem("cate", this.categoryName);
        console.log("category name" + this.categoryName);
        this.route.navigate(['./select-date-time']);
    }
    appointment_info() {
        this.route.navigate(['./appointment-info']);
    }
    view_all() {
        this.route.navigate(['./my-appointments']);
    }
    view_cat() {
        this.route.navigate(['./category-all']);
    }
    select_task() {
        this.route.navigate(['./select-task']);
    }
};
HomePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }
];
HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"],
        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]])
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map