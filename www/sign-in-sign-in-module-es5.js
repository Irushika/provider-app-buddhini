(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sign-in-sign-in-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/sign-in/sign-in.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/sign-in/sign-in.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSignInSignInPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header [translucent]=\"true\">\r\n\t<ion-toolbar>\r\n\t\t<ion-title  *ngIf=\"!otpScreen\"></ion-title>\r\n\t\t<!-- <ion-title *ngIf=\"otpScreen\"></ion-title> -->\r\n\t</ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"!otpScreen\" padding>\r\n\t<div class=\"banner ion-text-center\">\r\n\t\t<img src=\"assets/images/dark_logo.jpeg\">\r\n\t</div>\r\n\t<div class=\"form\">\r\n\t\t<!-- <h2>{{'enter_phone_number' | translate}}<br>{{'to_continue' | translate}}</h2> -->\r\n\t\t<ion-list lines=\"none\">\r\n\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<ion-input type=\"number\" value=\"\" placeholder=\"{{'enter_phone_number' | translate}}\" [(ngModel)]=\"cellNumber\"></ion-input>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-button size=\"large\" shape=\"block\" class=\"btn\" (click)=\"continue()\" id=\"phoneNumber\">{{'continue' | translate}}</ion-button>\r\n\t\t</ion-list>\r\n\t</div>\r\n</ion-content>\r\n\r\n<!-- <ion-content> -->\r\n\t<!-- <div> -->\r\n\t\t<!-- <p>\r\n\t\t\t{{'verification_tex1' | translate}}<br>\r\n\t\t\t{{'verification_tex2' | translate}}<br>\r\n\t\t\t{{'verification_tex3' | translate}}\r\n\t\t</p>\r\n\t\t<ion-list lines=\"none\">\r\n\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<ion-label position=\"fixed\">{{'enter_otp' | translate}}</ion-label>\r\n\t\t\t\t\t<ion-input type=\"name\" value=\"\" placeholder=\"{{'enter_6_digit_otp' | translate}}\" [(ngModel)]=\"otpNumber\"></ion-input>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\t\t\t<ion-button size=\"large\" shape=\"block\" class=\"btn\" (click)=\"verify()\">{{'continue' | translate}}</ion-button>\r\n\t\t</ion-list>\r\n\t\t<h2 class=\"d-flex\">\r\n\t\t\r\n\t\t\t<span class=\"end\">{{'send_again' | translate}}</span>\r\n\t\t</h2> -->\r\n\t<!-- </div> -->\r\n<!-- </ion-content> -->\r\n  <div id=\"recaptcha-container\"></div>";
    /***/
  },

  /***/
  "./src/app/services/user.service.ts":
  /*!******************************************!*\
    !*** ./src/app/services/user.service.ts ***!
    \******************************************/

  /*! exports provided: UserService */

  /***/
  function srcAppServicesUserServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserService", function () {
      return UserService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    let UserService = class UserService {
      constructor() {
        // behaviour subject
        this._$userSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]('');
      }

      getUserSubject() {
        return this._$userSubject.asObservable();
      }

    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], UserService);
    /***/
  },

  /***/
  "./src/app/sign-in/sign-in-routing.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/sign-in/sign-in-routing.module.ts ***!
    \***************************************************/

  /*! exports provided: SignInPageRoutingModule */

  /***/
  function srcAppSignInSignInRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SignInPageRoutingModule", function () {
      return SignInPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _sign_in_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./sign-in.page */
    "./src/app/sign-in/sign-in.page.ts");

    const routes = [{
      path: '',
      component: _sign_in_page__WEBPACK_IMPORTED_MODULE_3__["SignInPage"]
    }];
    let SignInPageRoutingModule = class SignInPageRoutingModule {};
    SignInPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], SignInPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/sign-in/sign-in.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/sign-in/sign-in.module.ts ***!
    \*******************************************/

  /*! exports provided: SignInPageModule */

  /***/
  function srcAppSignInSignInModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SignInPageModule", function () {
      return SignInPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _sign_in_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./sign-in-routing.module */
    "./src/app/sign-in/sign-in-routing.module.ts");
    /* harmony import */


    var _sign_in_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./sign-in.page */
    "./src/app/sign-in/sign-in.page.ts");

    let SignInPageModule = class SignInPageModule {};
    SignInPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"], _sign_in_routing_module__WEBPACK_IMPORTED_MODULE_6__["SignInPageRoutingModule"]],
      declarations: [_sign_in_page__WEBPACK_IMPORTED_MODULE_7__["SignInPage"]]
    })], SignInPageModule);
    /***/
  },

  /***/
  "./src/app/sign-in/sign-in.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/sign-in/sign-in.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppSignInSignInPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".banner {\n  position: relative;\n  width: calc(100% - 40px);\n  margin: 0 auto;\n  height: 220px;\n}\n.banner img {\n  position: absolute;\n  top: 60px;\n  left: 0;\n  right: 0;\n  margin: auto;\n  width: 130px;\n  display: block;\n}\n.form h2 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 600;\n  font-size: 1.3rem;\n  line-height: 21px;\n  padding-bottom: 24px;\n}\n.form .or_continue_with {\n  padding-top: 40px;\n}\n.form .or_continue_with .button.btn.button-block {\n  margin-bottom: 15px;\n}\n.form .or_continue_with .button.btn.button-block img {\n  width: 18px;\n  position: relative;\n  left: -11px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2lnbi1pbi9FOlxcRVxcc2VydmljZVxcZmlyZWJhc2UgMDRcXHByb3ZpZGVyLWFwcC1idWRkaGluaS9zcmNcXGFwcFxcc2lnbi1pblxcc2lnbi1pbi5wYWdlLnNjc3MiLCJzcmMvYXBwL3NpZ24taW4vc2lnbi1pbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxrQkFBQTtFQUNBLHdCQUFBO0VBQ0EsY0FBQTtFQUNBLGFBQUE7QUNDRDtBRENDO0VBQ0Msa0JBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7QUNDRjtBRElDO0VBQ0MsU0FBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7QUNERjtBRElDO0VBQ0MsaUJBQUE7QUNGRjtBRElFO0VBQ0MsbUJBQUE7QUNGSDtBRElHO0VBQ0MsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ0ZKIiwiZmlsZSI6InNyYy9hcHAvc2lnbi1pbi9zaWduLWluLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iYW5uZXIge1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHR3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XHJcblx0bWFyZ2luOiAwIGF1dG87XHJcblx0aGVpZ2h0OiAyMjBweDtcclxuXHJcblx0aW1nIHtcclxuXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdHRvcDogNjBweDtcclxuXHRcdGxlZnQ6IDA7XHJcblx0XHRyaWdodDogMDtcclxuXHRcdG1hcmdpbjogYXV0bztcclxuXHRcdHdpZHRoOiAxMzBweDtcclxuXHRcdGRpc3BsYXk6IGJsb2NrO1xyXG5cdH1cclxufVxyXG5cclxuLmZvcm0ge1xyXG5cdGgyIHtcclxuXHRcdG1hcmdpbjogMDtcclxuXHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xyXG5cdFx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHRcdGZvbnQtc2l6ZTogMS4zcmVtO1xyXG5cdFx0bGluZS1oZWlnaHQ6IDIxcHg7XHJcblx0XHRwYWRkaW5nLWJvdHRvbTogMjRweDtcclxuXHR9XHJcblxyXG5cdC5vcl9jb250aW51ZV93aXRoIHtcclxuXHRcdHBhZGRpbmctdG9wOiA0MHB4O1xyXG5cclxuXHRcdC5idXR0b24uYnRuLmJ1dHRvbi1ibG9jayB7XHJcblx0XHRcdG1hcmdpbi1ib3R0b206IDE1cHg7XHJcblxyXG5cdFx0XHRpbWcge1xyXG5cdFx0XHRcdHdpZHRoOiAxOHB4O1xyXG5cdFx0XHRcdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRcdFx0XHRsZWZ0OiAtMTFweDtcclxuXHRcdFx0fVxyXG5cclxuXHJcblx0XHRcdFxyXG5cclxuXHRcdH1cclxuXHR9XHJcbn0iLCIuYmFubmVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBoZWlnaHQ6IDIyMHB4O1xufVxuLmJhbm5lciBpbWcge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNjBweDtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIG1hcmdpbjogYXV0bztcbiAgd2lkdGg6IDEzMHB4O1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuLmZvcm0gaDIge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXNpemU6IDEuM3JlbTtcbiAgbGluZS1oZWlnaHQ6IDIxcHg7XG4gIHBhZGRpbmctYm90dG9tOiAyNHB4O1xufVxuLmZvcm0gLm9yX2NvbnRpbnVlX3dpdGgge1xuICBwYWRkaW5nLXRvcDogNDBweDtcbn1cbi5mb3JtIC5vcl9jb250aW51ZV93aXRoIC5idXR0b24uYnRuLmJ1dHRvbi1ibG9jayB7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG4uZm9ybSAub3JfY29udGludWVfd2l0aCAuYnV0dG9uLmJ0bi5idXR0b24tYmxvY2sgaW1nIHtcbiAgd2lkdGg6IDE4cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbGVmdDogLTExcHg7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/sign-in/sign-in.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/sign-in/sign-in.page.ts ***!
    \*****************************************/

  /*! exports provided: SignInPage */

  /***/
  function srcAppSignInSignInPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SignInPage", function () {
      return SignInPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_window_reference_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../services/window-reference.service */
    "./src/app/services/window-reference.service.ts");
    /* harmony import */


    var _services_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../services/user.service */
    "./src/app/services/user.service.ts");
    /* harmony import */


    var firebase__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! firebase */
    "./node_modules/firebase/dist/index.esm.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _angular_fire_database__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/fire/database */
    "./node_modules/@angular/fire/fesm2015/angular-fire-database.js");

    let SignInPage = class SignInPage {
      constructor(navCtrl, route, loadingCtrl, alertCtrl, storage, windowReference, userService, alertController, db) {
        this.navCtrl = navCtrl;
        this.route = route;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.windowReference = windowReference;
        this.userService = userService;
        this.alertController = alertController;
        this.db = db;
        this.otpScreen = false;
      }

      send() {
        this.route.navigate(['./veirifcation']);
      }

      ngOnInit() {
        console.log('check', firebase__WEBPACK_IMPORTED_MODULE_6__["default"].auth().currentUser);
        this.windowReference.storeWindowRef = this.windowReference.windowRef;
        this.windowReference.storeWindowRef.recaptchaVerifier = new firebase__WEBPACK_IMPORTED_MODULE_6__["default"].auth.RecaptchaVerifier('recaptcha-container', {
          size: 'invisible'
        });
        this.windowReference.storeWindowRef.recaptchaVerifier.render();
      }

      continue() {
        this.phone = '+94' + this.cellNumber.toString();
        this.mynumber = '0' + this.cellNumber.toString();
        window.localStorage.setItem("mynumber", this.mynumber);
        console.log(this.phone);
        firebase__WEBPACK_IMPORTED_MODULE_6__["default"].database().ref().child("users").orderByChild('phoneNumber').equalTo(this.phone).on('value', ev => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
          if (ev.val()) {
            let getUsers = Object.values(ev.val());
            console.log('getUsers', getUsers);

            if (getUsers.some(item => item.phoneNumber === this.phone)) {
              // if user is not provider, login , then alert you are not provider and can't log
              let user = getUsers[0];
              if (user.position == 'Provider') this.sendnumber();else this.presentAlert('you are not provider');
              this.storage.set('user', {
                UID: Object.keys(ev.val())[0]
              });
            } else {
              this.route.navigate(['./veirifcation/' + this.phone]);
            }
          } else if (!ev.val()) {
            this.route.navigate(['./veirifcation/' + this.phone]);
          }
        }));
      }

      sendnumber() {
        window.localStorage.setItem("userPhone", this.phone);
        console.log("send number " + this.phone);
        this.phone.length > 0 ? this.route.navigate(['./new-appoinment']) : this.presentAlert('No user found.');
      }

      sendCode() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
          console.log("Sending Code");
          const loader = yield this.loadingCtrl.create();
          loader.present();
          const appVerifier = this.windowReference.storeWindowRef.recaptchaVerifier;
          const num = '+94' + this.cellNumber.toString();
          console.log("verifier", appVerifier);
          console.log("num", num);
          firebase__WEBPACK_IMPORTED_MODULE_6__["default"].auth().signInWithPhoneNumber(num, appVerifier).then(result => {
            this.windowReference.storeWindowRef.confirmationResult = result;
            this.otpScreen = true;
            loader.dismiss();
          }).catch(err => {
            loader.dismiss();
            this.alertCtrl.create({
              header: 'Invalid Number',
              message: "".concat(err),
              buttons: ['Ok']
            }).then(res => {
              res.present();
            });
          });
        });
      }

      presentAlert(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
          const alert = yield this.alertController.create({
            header: 'Alert',
            message,
            buttons: ['OK']
          });
          yield alert.present();
        });
      }

    };

    SignInPage.ctorParameters = () => [{
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
    }, {
      type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
    }, {
      type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"]
    }, {
      type: _services_window_reference_service__WEBPACK_IMPORTED_MODULE_4__["WindowReference"]
    }, {
      type: _services_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
    }, {
      type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_8__["AngularFireDatabase"]
    }];

    SignInPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-sign-in',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./sign-in.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/sign-in/sign-in.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./sign-in.page.scss */
      "./src/app/sign-in/sign-in.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], _services_window_reference_service__WEBPACK_IMPORTED_MODULE_4__["WindowReference"], _services_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _angular_fire_database__WEBPACK_IMPORTED_MODULE_8__["AngularFireDatabase"]])], SignInPage);
    /***/
  }
}]);
//# sourceMappingURL=sign-in-sign-in-module-es5.js.map