(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["my-profile-my-profile-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/my-profile/my-profile.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/my-profile/my-profile.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppMyProfileMyProfilePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n\t<ion-toolbar>\r\n\t\t<ion-buttons slot=\"start\">\r\n\t\t\t<ion-menu-button></ion-menu-button>\r\n\t\t</ion-buttons>\r\n\t\t<ion-title></ion-title>\r\n\t</ion-toolbar>\r\n\t<h1>{{'my_profile' | translate}}</h1>\r\n</ion-header>\r\n<ion-content fullscreen>\r\n\t<div class=\"form\">\r\n\t\t<ion-list lines=\"none\">\r\n\t\t\t\r\n\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<ion-label position=\"fixed\">{{'full_name' | translate}}</ion-label>\r\n\t\t\t\t\t<ion-input color=\"primary\"  [(ngModel)]=\"name\" (ionBlur)=\"updateDatabasename()\"> </ion-input>\r\n\t\t\t\t\t\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<ion-label position=\"fixed\">{{'phone_number' | translate}}</ion-label>\r\n\t\t\t\t\t<ion-label color=\"primary\">{{ mynumber }}</ion-label>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<ion-label position=\"fixed\">{{'email_address' | translate}}</ion-label>\r\n\t\t\t\t\t<ion-input color=\"primary\"  [(ngModel)]=\"email\" (ionBlur)=\"updateDatabaseemail()\"> </ion-input>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<ion-label position=\"fixed\">{{'Bio' | translate}}</ion-label>\r\n\t\t\t\t\t  <ion-textarea color=\"primary\"  [(ngModel)]=\"bio\" (ionBlur)=\"updateDatabasebio()\"></ion-textarea>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<ion-label position=\"fixed\">{{'Service Category' | translate}}</ion-label>\r\n\t\t\t\t\t<ion-label color=\"primary\"> {{user?.category}}</ion-label>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t<div class=\"item_inner\">\r\n\t\t\t\t\t<ion-label position=\"fixed\">{{'City' | translate}}</ion-label>\r\n\t\t\t\t\t<ion-input color=\"primary\"  [(ngModel)]=\"city\" (ionBlur)=\"updateDatabasecity()\"> </ion-input>\r\n\t\t\t\t</div>\r\n\t\t\t</ion-item>\r\n\r\n\t\t</ion-list>\r\n\t</div>\r\n</ion-content>\r\n\r\n";
    /***/
  },

  /***/
  "./src/app/my-profile/my-profile-routing.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/my-profile/my-profile-routing.module.ts ***!
    \*********************************************************/

  /*! exports provided: MyProfilePageRoutingModule */

  /***/
  function srcAppMyProfileMyProfileRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyProfilePageRoutingModule", function () {
      return MyProfilePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _my_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./my-profile.page */
    "./src/app/my-profile/my-profile.page.ts");

    const routes = [{
      path: '',
      component: _my_profile_page__WEBPACK_IMPORTED_MODULE_3__["MyProfilePage"]
    }];
    let MyProfilePageRoutingModule = class MyProfilePageRoutingModule {};
    MyProfilePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], MyProfilePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/my-profile/my-profile.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/my-profile/my-profile.module.ts ***!
    \*************************************************/

  /*! exports provided: MyProfilePageModule */

  /***/
  function srcAppMyProfileMyProfileModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyProfilePageModule", function () {
      return MyProfilePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _my_profile_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./my-profile-routing.module */
    "./src/app/my-profile/my-profile-routing.module.ts");
    /* harmony import */


    var _my_profile_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./my-profile.page */
    "./src/app/my-profile/my-profile.page.ts");

    let MyProfilePageModule = class MyProfilePageModule {};
    MyProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"], _my_profile_routing_module__WEBPACK_IMPORTED_MODULE_6__["MyProfilePageRoutingModule"]],
      declarations: [_my_profile_page__WEBPACK_IMPORTED_MODULE_7__["MyProfilePage"]]
    })], MyProfilePageModule);
    /***/
  },

  /***/
  "./src/app/my-profile/my-profile.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/my-profile/my-profile.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppMyProfileMyProfilePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".form {\n  padding-top: 30px;\n}\n.form ion-list .my_address {\n  padding-top: 20px;\n  padding-bottom: 15px;\n}\n.form ion-list .my_address h2 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 700;\n  font-size: 1.7rem;\n  padding-bottom: 26px;\n}\n.form ion-list .my_address ion-item {\n  padding: 18px 18px;\n  border-radius: 4px;\n  background: var(--input_filed_bg);\n  margin-bottom: 10px;\n}\n.form ion-list .my_address ion-item ion-label {\n  position: relative;\n  margin: 0;\n  padding-bottom: 0;\n}\n.form ion-list .my_address ion-item ion-label h3 {\n  margin: 0;\n  text-transform: uppercase;\n  font-size: 1.15rem;\n  font-weight: 600;\n  letter-spacing: 1.5px;\n  padding-bottom: 10px;\n  color: var(--text-black);\n}\n.form ion-list .my_address ion-item ion-label p {\n  color: var(--text-light);\n  font-size: 1.12rem !important;\n  font-weight: 400 !important;\n  margin: 0;\n}\nion-footer {\n  padding: 0 20px 20px 20px;\n  background: var(--white);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbXktcHJvZmlsZS9FOlxcRVxcc2VydmljZVxcZmlyZWJhc2UgMDRcXHByb3ZpZGVyLWFwcC1idWRkaGluaS9zcmNcXGFwcFxcbXktcHJvZmlsZVxcbXktcHJvZmlsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL215LXByb2ZpbGUvbXktcHJvZmlsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxpQkFBQTtBQ0NEO0FERUU7RUFDQyxpQkFBQTtFQUNBLG9CQUFBO0FDQUg7QURFRztFQUNDLFNBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDRCxvQkFBQTtBQ0FIO0FER0c7RUFDQyxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUNBQUE7RUFDQSxtQkFBQTtBQ0RKO0FESUk7RUFDQyxrQkFBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtBQ0ZMO0FESUs7RUFDQyxTQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtFQUNBLHdCQUFBO0FDRk47QURLSztFQUNDLHdCQUFBO0VBQ0EsNkJBQUE7RUFDQSwyQkFBQTtFQUNBLFNBQUE7QUNITjtBRFlBO0VBQ0MseUJBQUE7RUFDQSx3QkFBQTtBQ1REIiwiZmlsZSI6InNyYy9hcHAvbXktcHJvZmlsZS9teS1wcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mb3JtIHtcclxuXHRwYWRkaW5nLXRvcDogMzBweDtcclxuXHJcblx0aW9uLWxpc3Qge1xyXG5cdFx0Lm15X2FkZHJlc3Mge1xyXG5cdFx0XHRwYWRkaW5nLXRvcDogMjBweDtcclxuXHRcdFx0cGFkZGluZy1ib3R0b206IDE1cHg7XHJcblxyXG5cdFx0XHRoMiB7XHJcblx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xyXG5cdFx0XHRcdGZvbnQtd2VpZ2h0OiA3MDA7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxLjdyZW07XHJcblx0XHRcdHBhZGRpbmctYm90dG9tOiAyNnB4O1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpb24taXRlbSB7XHJcblx0XHRcdFx0cGFkZGluZzogMThweCAxOHB4O1xyXG5cdFx0XHRcdGJvcmRlci1yYWRpdXM6IDRweDtcclxuXHRcdFx0XHRiYWNrZ3JvdW5kOiB2YXIoLS1pbnB1dF9maWxlZF9iZyk7XHJcblx0XHRcdFx0bWFyZ2luLWJvdHRvbTogMTBweDtcclxuXHJcblxyXG5cdFx0XHRcdGlvbi1sYWJlbCB7XHJcblx0XHRcdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRwYWRkaW5nLWJvdHRvbTogMDtcclxuXHJcblx0XHRcdFx0XHRoMyB7XHJcblx0XHRcdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRcdFx0dGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuXHRcdFx0XHRcdFx0Zm9udC1zaXplOiAxLjE1cmVtO1xyXG5cdFx0XHRcdFx0XHRmb250LXdlaWdodDogNjAwO1xyXG5cdFx0XHRcdFx0XHRsZXR0ZXItc3BhY2luZzogMS41cHg7XHJcblx0XHRcdFx0XHRcdHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG5cdFx0XHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1ibGFjayk7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0cCB7XHJcblx0XHRcdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcclxuXHRcdFx0XHRcdFx0Zm9udC1zaXplOiAxLjEycmVtICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0XHRcdGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcclxuXHRcdFx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbmlvbi1mb290ZXJ7XHJcblx0cGFkZGluZzogMCAyMHB4IDIwcHggMjBweDtcclxuXHRiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XHJcbn1cclxuIiwiLmZvcm0ge1xuICBwYWRkaW5nLXRvcDogMzBweDtcbn1cbi5mb3JtIGlvbi1saXN0IC5teV9hZGRyZXNzIHtcbiAgcGFkZGluZy10b3A6IDIwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNXB4O1xufVxuLmZvcm0gaW9uLWxpc3QgLm15X2FkZHJlc3MgaDIge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xuICBmb250LXdlaWdodDogNzAwO1xuICBmb250LXNpemU6IDEuN3JlbTtcbiAgcGFkZGluZy1ib3R0b206IDI2cHg7XG59XG4uZm9ybSBpb24tbGlzdCAubXlfYWRkcmVzcyBpb24taXRlbSB7XG4gIHBhZGRpbmc6IDE4cHggMThweDtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pbnB1dF9maWxlZF9iZyk7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG4uZm9ybSBpb24tbGlzdCAubXlfYWRkcmVzcyBpb24taXRlbSBpb24tbGFiZWwge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZy1ib3R0b206IDA7XG59XG4uZm9ybSBpb24tbGlzdCAubXlfYWRkcmVzcyBpb24taXRlbSBpb24tbGFiZWwgaDMge1xuICBtYXJnaW46IDA7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMS4xNXJlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDEuNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgY29sb3I6IHZhcigtLXRleHQtYmxhY2spO1xufVxuLmZvcm0gaW9uLWxpc3QgLm15X2FkZHJlc3MgaW9uLWl0ZW0gaW9uLWxhYmVsIHAge1xuICBjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XG4gIGZvbnQtc2l6ZTogMS4xMnJlbSAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogNDAwICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMDtcbn1cblxuaW9uLWZvb3RlciB7XG4gIHBhZGRpbmc6IDAgMjBweCAyMHB4IDIwcHg7XG4gIGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/my-profile/my-profile.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/my-profile/my-profile.page.ts ***!
    \***********************************************/

  /*! exports provided: MyProfilePage */

  /***/
  function srcAppMyProfileMyProfilePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyProfilePage", function () {
      return MyProfilePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/fire/database */
    "./node_modules/@angular/fire/fesm2015/angular-fire-database.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _services_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../services/user.service */
    "./src/app/services/user.service.ts");
    /* harmony import */


    var firebase__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! firebase */
    "./node_modules/firebase/dist/index.esm.js");

    let MyProfilePage = class MyProfilePage {
      constructor(route, activatedRoute, userService, db, alertController) {
        this.route = route;
        this.activatedRoute = activatedRoute;
        this.userService = userService;
        this.db = db;
        this.alertController = alertController;
      }

      ngOnInit() {
        this.loaddetails();
      }

      loaddetails() {
        console.log("my-profile");
        let phone = window.localStorage.getItem("userPhone");

        if (phone) {
          console.log(phone);
          this.db.list("/users", ref => ref.orderByChild("phoneNumber").equalTo(phone)).valueChanges().subscribe(user => {
            if (user.length > 0) {
              this.user = user[0];
              this.name = this.user.name;
              this.email = this.user.email;
              this.bio = this.user.bio;
              this.category = this.user.category;
              this.city = this.user.city; // window.localStorage.setItem("userInfo", JSON.stringify(user[0]));
            } else {
              this.presentAlert();
            }
          });
          let data = JSON.parse(window.localStorage.getItem("addresses"));
          if (data) this.addresses = data;
        } else {
          this.user = JSON.parse(window.localStorage.getItem("userInfo"));
          console.log("params not working");
        }

        this.mynumber = window.localStorage.getItem("mynumber");
        console.log(this.mynumber);
      }

      updateDatabasename() {
        this.UID = localStorage.getItem('UID');
        console.log(this.UID);
        console.log(this.name);
        firebase__WEBPACK_IMPORTED_MODULE_6__["default"].database().ref("users/").child("".concat(this.UID)).update({
          name: this.name
        });
        console.log('update done');
      }

      updateDatabaseemail() {
        this.UID = localStorage.getItem('UID');
        console.log(this.UID);
        console.log(this.email);
        firebase__WEBPACK_IMPORTED_MODULE_6__["default"].database().ref("users/").child("".concat(this.UID)).update({
          email: this.email
        });
        console.log('update done');
      }

      updateDatabasebio() {
        this.UID = localStorage.getItem('UID');
        console.log(this.UID);
        console.log(this.bio);
        firebase__WEBPACK_IMPORTED_MODULE_6__["default"].database().ref("users/").child("".concat(this.UID)).update({
          bio: this.bio
        });
        console.log('update done');
      }

      updateDatabasecity() {
        this.UID = localStorage.getItem('UID');
        console.log(this.UID);
        console.log(this.city);
        firebase__WEBPACK_IMPORTED_MODULE_6__["default"].database().ref("users/").child("".concat(this.UID)).update({
          city: this.city
        });
        console.log('update done');
      }

      presentAlert() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
          const alert = yield this.alertController.create({
            header: "Alert",
            message: "No user found.",
            buttons: ["OK"]
          });
          yield alert.present();
        });
      }

      addadress() {
        this.route.navigate(["./add-address"]);
      }

    };

    MyProfilePage.ctorParameters = () => [{
      type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
    }, {
      type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
    }, {
      type: _services_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"]
    }, {
      type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
    }];

    MyProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: "app-my-profile",
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./my-profile.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/my-profile/my-profile.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./my-profile.page.scss */
      "./src/app/my-profile/my-profile.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _services_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"], _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]])], MyProfilePage);
    /***/
  },

  /***/
  "./src/app/services/user.service.ts":
  /*!******************************************!*\
    !*** ./src/app/services/user.service.ts ***!
    \******************************************/

  /*! exports provided: UserService */

  /***/
  function srcAppServicesUserServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserService", function () {
      return UserService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    let UserService = class UserService {
      constructor() {
        // behaviour subject
        this._$userSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]('');
      }

      getUserSubject() {
        return this._$userSubject.asObservable();
      }

    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], UserService);
    /***/
  }
}]);
//# sourceMappingURL=my-profile-my-profile-module-es5.js.map