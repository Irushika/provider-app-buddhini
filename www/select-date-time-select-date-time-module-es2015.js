(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["select-date-time-select-date-time-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/select-date-time/select-date-time.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/select-date-time/select-date-time.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n\t<ion-toolbar>\r\n\t\t<ion-buttons slot=\"start\">\r\n\t\t\t<ion-back-button text=\"\" icon=\"chevron-back-outline\"></ion-back-button>\r\n\t\t</ion-buttons>\r\n\t\t<ion-title>\r\n\t\t\t<ion-icon class=\"zmdi zmdi-circle active\"></ion-icon>\r\n\t\t\t<ion-icon class=\"zmdi zmdi-circle\"></ion-icon>\r\n\t\t\t<ion-icon class=\"zmdi zmdi-circle\"></ion-icon>\r\n\t\t</ion-title>\r\n\t</ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content fullscreen>\r\n\t<div class=\"form\">\r\n\t\t<ion-list lines=\"none\">\r\n\t\t\r\n\t\t\t<ion-card>\r\n\t\t\t\t<h2>{{'select_date' | translate}} </h2>\r\n\t\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t\t\r\n\t\t\t\t\t<ion-datetime displayFormat=\"D MMMM YYYY\" min=\"2021-03-15\" max=\"2222-12-31\" placeholder=\"{{'select_date' | translate}}\" [(ngModel)]=\"date\"></ion-datetime>\r\n\t\t\t\t\r\n\t\t\t\t\t<ion-icon slot=\"end\"></ion-icon>\r\n\t\t\t\t</ion-item>\r\n\t\t\t</ion-card>\r\n\r\n\t\t\t<ion-card>\r\n\t\t\t\t<h2>{{'select_time' | translate}} </h2>\r\n\t\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t\t\r\n\t\t\t\t\t<ion-datetime displayFormat=\"HH:mm\" pickerFormat=\"HH:mm\" placeholder=\"{{'select_time' | translate}}\" [(ngModel)]=\"time\"></ion-datetime>\r\n\t\t\t\t\r\n\t\t\t\t\t<ion-icon slot=\"end\"></ion-icon>\r\n\t\t\t\t</ion-item>\r\n\t\t\t</ion-card>\r\n\r\n\t\t\t<ion-card>\r\n\t\t\t\t<h2>{{'Estimated Budget' | translate}} </h2>\r\n\t\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t\t<ion-input placeholder=\"{{'Budject' | translate}}\" type=\"number\" [(ngModel)]=\"budject\" ></ion-input>\r\n\t\t\t\t</ion-item>\r\n\t\t\t</ion-card>\r\n\r\n\t\t\t<ion-card>\r\n\t\t\t\t<h2>{{'Estimated Time(Hours)' | translate}} </h2>\r\n\t\t\t\t<ion-item lines=\"none\">\r\n\t\t\t\t\t<ion-input placeholder=\"{{'Estimate Time' | translate}}\" type=\"number\" [(ngModel)]=\"estimatetime\" ></ion-input>\r\n\t\t\t\t</ion-item>\r\n\t\t\t</ion-card>\r\n\r\n\t\t</ion-list>\r\n\t</div>\r\n</ion-content>\r\n<ion-footer class=\"ion-no-border d-flex\">\r\n\t<ion-button size=\"large\" shape=\"block\" class=\"btn end\" (click)=\"continue()\">{{'continue' | translate}}</ion-button>\r\n</ion-footer>");

/***/ }),

/***/ "./src/app/select-date-time/select-date-time-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/select-date-time/select-date-time-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: SelectDateTimePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectDateTimePageRoutingModule", function() { return SelectDateTimePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _select_date_time_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-date-time.page */ "./src/app/select-date-time/select-date-time.page.ts");




const routes = [
    {
        path: '',
        component: _select_date_time_page__WEBPACK_IMPORTED_MODULE_3__["SelectDateTimePage"]
    }
];
let SelectDateTimePageRoutingModule = class SelectDateTimePageRoutingModule {
};
SelectDateTimePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectDateTimePageRoutingModule);



/***/ }),

/***/ "./src/app/select-date-time/select-date-time.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/select-date-time/select-date-time.module.ts ***!
  \*************************************************************/
/*! exports provided: SelectDateTimePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectDateTimePageModule", function() { return SelectDateTimePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _select_date_time_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-date-time-routing.module */ "./src/app/select-date-time/select-date-time-routing.module.ts");
/* harmony import */ var _select_date_time_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./select-date-time.page */ "./src/app/select-date-time/select-date-time.page.ts");








let SelectDateTimePageModule = class SelectDateTimePageModule {
};
SelectDateTimePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"],
            _select_date_time_routing_module__WEBPACK_IMPORTED_MODULE_6__["SelectDateTimePageRoutingModule"]
        ],
        declarations: [_select_date_time_page__WEBPACK_IMPORTED_MODULE_7__["SelectDateTimePage"]]
    })
], SelectDateTimePageModule);



/***/ }),

/***/ "./src/app/select-date-time/select-date-time.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/select-date-time/select-date-time.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header ion-toolbar ion-title {\n  position: absolute !important;\n  top: 0;\n  left: 0;\n  width: 100%;\n  padding: 0 15px !important;\n  text-align: center;\n}\nion-header ion-toolbar ion-title ion-icon {\n  color: var(--text-light2);\n  margin: 0 5px;\n  font-size: 0.85rem;\n}\nion-header ion-toolbar ion-title ion-icon.active {\n  color: var(--primary);\n}\n.form ion-list {\n  padding-top: 17px;\n}\n.form ion-list ion-card {\n  margin: 0;\n  width: 100%;\n  box-shadow: none;\n  border-radius: 0;\n  margin-bottom: 15px;\n}\n.form ion-list ion-card h2 {\n  font-size: 1.3rem;\n  padding-bottom: 22px;\n  color: var(--text-dark);\n  margin: 0;\n  font-weight: 600;\n}\n.form ion-list ion-card ion-item {\n  background: var(--input_filed_bg) !important;\n  padding: 0 19px;\n  --min-height: 54px;\n}\n.form ion-list ion-card ion-item ion-datetime {\n  padding: 0;\n  color: var(--text-light);\n  font-size: 1.12rem !important;\n  font-weight: 400 !important;\n}\n.form ion-list ion-card ion-item ion-icon {\n  margin: 0;\n  color: var(--primary);\n  font-size: 1.5rem;\n  min-width: 35px;\n}\n.form ion-list ion-card ion-item .item_inner {\n  padding: 19px 0;\n}\n.form ion-list ion-card ion-item .item_inner h3 {\n  margin: 0;\n  text-transform: uppercase;\n  font-size: 1.1rem;\n  font-weight: 600;\n  letter-spacing: 1.5px;\n  padding-bottom: 10px;\n}\n.form ion-list ion-card ion-item .item_inner p {\n  color: var(--text-light);\n  font-size: 1.12rem !important;\n  font-weight: 400 !important;\n  margin: 0;\n}\nion-footer {\n  background: var(--white);\n  padding: 10px 20px 20px 20px;\n}\nion-footer .amount {\n  min-width: 50%;\n  max-width: 50%;\n}\nion-footer .amount h3 {\n  margin: 0;\n  color: var(--text-light);\n  font-weight: 400;\n  font-size: 0.85rem;\n  padding-bottom: 5px;\n}\nion-footer .amount h2 {\n  margin: 0;\n  color: var(--text-dark);\n  font-weight: 600;\n  font-size: 1.2rem;\n}\nion-footer .button.btn {\n  min-width: 50%;\n  max-width: 50%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VsZWN0LWRhdGUtdGltZS9FOlxcRVxcc2VydmljZVxcZmlyZWJhc2UgMDRcXHByb3ZpZGVyLWFwcC1idWRkaGluaS9zcmNcXGFwcFxcc2VsZWN0LWRhdGUtdGltZVxcc2VsZWN0LWRhdGUtdGltZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3NlbGVjdC1kYXRlLXRpbWUvc2VsZWN0LWRhdGUtdGltZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0M7RUFDQyw2QkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0VBQ0Esa0JBQUE7QUNBRjtBREVFO0VBQ0MseUJBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7QUNBSDtBREVHO0VBQ0MscUJBQUE7QUNBSjtBRE9DO0VBQ0MsaUJBQUE7QUNKRjtBRE1FO0VBQ0MsU0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUNKSDtBRE1HO0VBQ0MsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLHVCQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0FDSko7QURPRztFQUNDLDRDQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDTEo7QURPSTtFQUNDLFVBQUE7RUFDQSx3QkFBQTtFQUNBLDZCQUFBO0VBQ0EsMkJBQUE7QUNMTDtBRFFJO0VBQ0MsU0FBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDTkw7QURTSTtFQUNDLGVBQUE7QUNQTDtBRFNLO0VBQ0MsU0FBQTtFQUNBLHlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0Esb0JBQUE7QUNQTjtBRFVLO0VBQ0Msd0JBQUE7RUFDQSw2QkFBQTtFQUNBLDJCQUFBO0VBQ0EsU0FBQTtBQ1JOO0FEbUJBO0VBQ0Msd0JBQUE7RUFDQSw0QkFBQTtBQ2hCRDtBRGtCQztFQUNDLGNBQUE7RUFDQSxjQUFBO0FDaEJGO0FEa0JFO0VBQ0MsU0FBQTtFQUNBLHdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDaEJIO0FEbUJFO0VBQ0MsU0FBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQ2pCSDtBRHFCQztFQUNDLGNBQUE7RUFDQSxjQUFBO0FDbkJGIiwiZmlsZSI6InNyYy9hcHAvc2VsZWN0LWRhdGUtdGltZS9zZWxlY3QtZGF0ZS10aW1lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXIge1xyXG5cdGlvbi10b29sYmFyIGlvbi10aXRsZSB7XHJcblx0XHRwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcclxuXHRcdHRvcDogMDtcclxuXHRcdGxlZnQ6IDA7XHJcblx0XHR3aWR0aDogMTAwJTtcclxuXHRcdHBhZGRpbmc6IDAgMTVweCAhaW1wb3J0YW50O1xyXG5cdFx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuXHRcdGlvbi1pY29uIHtcclxuXHRcdFx0Y29sb3I6IHZhcigtLXRleHQtbGlnaHQyKTtcclxuXHRcdFx0bWFyZ2luOiAwIDVweDtcclxuXHRcdFx0Zm9udC1zaXplOiAwLjg1cmVtO1xyXG5cclxuXHRcdFx0Ji5hY3RpdmUge1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS1wcmltYXJ5KTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLmZvcm0ge1xyXG5cdGlvbi1saXN0IHtcclxuXHRcdHBhZGRpbmctdG9wOiAxN3B4O1xyXG5cclxuXHRcdGlvbi1jYXJkIHtcclxuXHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHR3aWR0aDogMTAwJTtcclxuXHRcdFx0Ym94LXNoYWRvdzogbm9uZTtcclxuXHRcdFx0Ym9yZGVyLXJhZGl1czogMDtcclxuXHRcdFx0bWFyZ2luLWJvdHRvbTogMTVweDtcclxuXHJcblx0XHRcdGgyIHtcclxuXHRcdFx0XHRmb250LXNpemU6IDEuM3JlbTtcclxuXHRcdFx0XHRwYWRkaW5nLWJvdHRvbTogMjJweDtcclxuXHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1kYXJrKTtcclxuXHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aW9uLWl0ZW0ge1xyXG5cdFx0XHRcdGJhY2tncm91bmQ6IHZhcigtLWlucHV0X2ZpbGVkX2JnKSAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdHBhZGRpbmc6IDAgMTlweDtcclxuXHRcdFx0XHQtLW1pbi1oZWlnaHQ6IDU0cHg7XHJcblxyXG5cdFx0XHRcdGlvbi1kYXRldGltZSB7XHJcblx0XHRcdFx0XHRwYWRkaW5nOiAwO1xyXG5cdFx0XHRcdFx0Y29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAxLjEycmVtICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0XHRmb250LXdlaWdodDogNDAwICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRpb24taWNvbiB7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRjb2xvcjogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRcdFx0XHRmb250LXNpemU6IDEuNXJlbTtcclxuXHRcdFx0XHRcdG1pbi13aWR0aDogMzVweDtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdC5pdGVtX2lubmVyIHtcclxuXHRcdFx0XHRcdHBhZGRpbmc6IDE5cHggMDtcclxuXHJcblx0XHRcdFx0XHRoMyB7XHJcblx0XHRcdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRcdFx0dGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuXHRcdFx0XHRcdFx0Zm9udC1zaXplOiAxLjFyZW07XHJcblx0XHRcdFx0XHRcdGZvbnQtd2VpZ2h0OiA2MDA7XHJcblx0XHRcdFx0XHRcdGxldHRlci1zcGFjaW5nOiAxLjVweDtcclxuXHRcdFx0XHRcdFx0cGFkZGluZy1ib3R0b206IDEwcHg7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0cCB7XHJcblx0XHRcdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcclxuXHRcdFx0XHRcdFx0Zm9udC1zaXplOiAxLjEycmVtICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0XHRcdGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcclxuXHRcdFx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcblxyXG5cclxuaW9uLWZvb3RlciB7XHJcblx0YmFja2dyb3VuZDogdmFyKC0td2hpdGUpO1xyXG5cdHBhZGRpbmc6IDEwcHggMjBweCAyMHB4IDIwcHg7XHJcblxyXG5cdC5hbW91bnQge1xyXG5cdFx0bWluLXdpZHRoOiA1MCU7XHJcblx0XHRtYXgtd2lkdGg6IDUwJTtcclxuXHJcblx0XHRoMyB7XHJcblx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0Y29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xyXG5cdFx0XHRmb250LXdlaWdodDogNDAwO1xyXG5cdFx0XHRmb250LXNpemU6IC44NXJlbTtcclxuXHRcdFx0cGFkZGluZy1ib3R0b206IDVweDtcclxuXHRcdH1cclxuXHJcblx0XHRoMiB7XHJcblx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0Y29sb3I6IHZhcigtLXRleHQtZGFyayk7XHJcblx0XHRcdGZvbnQtd2VpZ2h0OiA2MDA7XHJcblx0XHRcdGZvbnQtc2l6ZTogMS4ycmVtO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0LmJ1dHRvbi5idG4ge1xyXG5cdFx0bWluLXdpZHRoOiA1MCU7XHJcblx0XHRtYXgtd2lkdGg6IDUwJTtcclxuXHR9XHJcbn0iLCJpb24taGVhZGVyIGlvbi10b29sYmFyIGlvbi10aXRsZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAwIDE1cHggIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuaW9uLWhlYWRlciBpb24tdG9vbGJhciBpb24tdGl0bGUgaW9uLWljb24ge1xuICBjb2xvcjogdmFyKC0tdGV4dC1saWdodDIpO1xuICBtYXJnaW46IDAgNXB4O1xuICBmb250LXNpemU6IDAuODVyZW07XG59XG5pb24taGVhZGVyIGlvbi10b29sYmFyIGlvbi10aXRsZSBpb24taWNvbi5hY3RpdmUge1xuICBjb2xvcjogdmFyKC0tcHJpbWFyeSk7XG59XG5cbi5mb3JtIGlvbi1saXN0IHtcbiAgcGFkZGluZy10b3A6IDE3cHg7XG59XG4uZm9ybSBpb24tbGlzdCBpb24tY2FyZCB7XG4gIG1hcmdpbjogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG4uZm9ybSBpb24tbGlzdCBpb24tY2FyZCBoMiB7XG4gIGZvbnQtc2l6ZTogMS4zcmVtO1xuICBwYWRkaW5nLWJvdHRvbTogMjJweDtcbiAgY29sb3I6IHZhcigtLXRleHQtZGFyayk7XG4gIG1hcmdpbjogMDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5mb3JtIGlvbi1saXN0IGlvbi1jYXJkIGlvbi1pdGVtIHtcbiAgYmFja2dyb3VuZDogdmFyKC0taW5wdXRfZmlsZWRfYmcpICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDAgMTlweDtcbiAgLS1taW4taGVpZ2h0OiA1NHB4O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWNhcmQgaW9uLWl0ZW0gaW9uLWRhdGV0aW1lIHtcbiAgcGFkZGluZzogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xuICBmb250LXNpemU6IDEuMTJyZW0gIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDQwMCAhaW1wb3J0YW50O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWNhcmQgaW9uLWl0ZW0gaW9uLWljb24ge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiB2YXIoLS1wcmltYXJ5KTtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIG1pbi13aWR0aDogMzVweDtcbn1cbi5mb3JtIGlvbi1saXN0IGlvbi1jYXJkIGlvbi1pdGVtIC5pdGVtX2lubmVyIHtcbiAgcGFkZGluZzogMTlweCAwO1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWNhcmQgaW9uLWl0ZW0gLml0ZW1faW5uZXIgaDMge1xuICBtYXJnaW46IDA7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMS4xcmVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBsZXR0ZXItc3BhY2luZzogMS41cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xufVxuLmZvcm0gaW9uLWxpc3QgaW9uLWNhcmQgaW9uLWl0ZW0gLml0ZW1faW5uZXIgcCB7XG4gIGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcbiAgZm9udC1zaXplOiAxLjEycmVtICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwO1xufVxuXG5pb24tZm9vdGVyIHtcbiAgYmFja2dyb3VuZDogdmFyKC0td2hpdGUpO1xuICBwYWRkaW5nOiAxMHB4IDIwcHggMjBweCAyMHB4O1xufVxuaW9uLWZvb3RlciAuYW1vdW50IHtcbiAgbWluLXdpZHRoOiA1MCU7XG4gIG1heC13aWR0aDogNTAlO1xufVxuaW9uLWZvb3RlciAuYW1vdW50IGgzIHtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGZvbnQtc2l6ZTogMC44NXJlbTtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cbmlvbi1mb290ZXIgLmFtb3VudCBoMiB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHZhcigtLXRleHQtZGFyayk7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xufVxuaW9uLWZvb3RlciAuYnV0dG9uLmJ0biB7XG4gIG1pbi13aWR0aDogNTAlO1xuICBtYXgtd2lkdGg6IDUwJTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/select-date-time/select-date-time.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/select-date-time/select-date-time.page.ts ***!
  \***********************************************************/
/*! exports provided: SelectDateTimePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectDateTimePage", function() { return SelectDateTimePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _location_location_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../location/location.page */ "./src/app/location/location.page.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/fesm2015/angular-fire-database.js");







let SelectDateTimePage = class SelectDateTimePage {
    constructor(modalController, route, storage, activatedRoute, db) {
        this.modalController = modalController;
        this.route = route;
        this.storage = storage;
        this.activatedRoute = activatedRoute;
        this.db = db;
    }
    ngOnInit() {
        this.JID = this.activatedRoute.snapshot.paramMap.get('JID');
        this.loadJobDetails();
    }
    location() {
        this.modalController.create({ component: _location_location_page__WEBPACK_IMPORTED_MODULE_2__["LocationPage"] }).then((modalElement) => {
            modalElement.present();
        });
    }
    loadJobDetails() {
        if (this.JID) {
            console.log(this.JID);
            window.localStorage.setItem("myjobid", this.JID);
            this.db.database.ref("/jobs/" + this.JID).on('value', (snapshot) => {
                this.job = snapshot.val();
                console.log(this.job);
                window.localStorage.setItem("jobInfo", JSON.stringify(this.job));
                this.date = this.job.date;
                this.time = this.job.time;
                console.log(this.time);
                this.budject = this.job.budject;
            });
        }
        else {
            this.job = JSON.parse(window.localStorage.getItem("jobInfo"));
            console.log("params not working");
        }
    }
    continue() {
        this.date = this.formatDate(this.date);
        // this.time=this.formatTime(this.time);
        console.log(this.date);
        console.log(this.time);
        window.localStorage.setItem("provider_date", this.date);
        window.localStorage.setItem("provider_time", this.time);
        window.localStorage.setItem("provider_budject", this.budject);
        window.localStorage.setItem("provider_Estimate_Time", this.estimatetime);
        console.log("date : " + this.date + "time :" + this.time + " budject" + this.budject + "hourse" + this.estimatetime);
        this.route.navigate(['./confirm-submit']);
    }
    formatDate(dateStr) {
        //date parsing
        let date = new Date(dateStr);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        return year + '-' + month + '-' + day;
    }
    formatTime(dateStr) {
        let date = new Date(dateStr);
        let minuts = date.getMinutes();
        let hourse = date.getHours();
        return hourse + ':' + minuts;
    }
};
SelectDateTimePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_6__["AngularFireDatabase"] }
];
SelectDateTimePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-select-date-time',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./select-date-time.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/select-date-time/select-date-time.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./select-date-time.page.scss */ "./src/app/select-date-time/select-date-time.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
        _angular_fire_database__WEBPACK_IMPORTED_MODULE_6__["AngularFireDatabase"]])
], SelectDateTimePage);



/***/ })

}]);
//# sourceMappingURL=select-date-time-select-date-time-module-es2015.js.map