(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contact-us-contact-us-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppContactUsContactUsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header class=\"bg_transparent\">\r\n\t<ion-toolbar>\r\n\t\t<ion-buttons slot=\"start\">\r\n\t\t\t<ion-menu-button></ion-menu-button>\r\n\t\t</ion-buttons>\r\n\t\t<ion-title>\r\n\t\t</ion-title>\r\n\t</ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content fullscreen [scrollEvents]=\"true\">\r\n\t<div class=\"map\">\r\n\t\t<img src=\"assets/images/map.png\">\r\n\t\t<ion-icon class=\"zmdi zmdi-pin\" style=\"top:42%; right: 50%;\"></ion-icon>\r\n\t</div>\r\n\r\n\t<ion-list lines=\"none\">\r\n\t\t<ion-item>\r\n\t\t\t<div class=\"text_box\">\r\n\t\t\t\t<h2>{{\"we_are_here\" | translate}}</h2>\r\n\t\t\t\t<p>1124, Red fort Villas, Hemilton Road, New York, USA</p>\r\n\t\t\t</div>\r\n\t\t\t<ion-icon class=\"zmdi zmdi-navigation ion-text-center\" slot=\"end\"></ion-icon>\r\n\t\t</ion-item>\r\n\t\t<ion-item>\r\n\t\t\t<div class=\"text_box\">\r\n\t\t\t\t<h2>{{\"call_us\" | translate}}</h2>\r\n\t\t\t\t<p>+1 987 654 3210</p>\r\n\t\t\t</div>\r\n\t\t\t<ion-icon class=\"zmdi zmdi-phone ion-text-center\" slot=\"end\"></ion-icon>\r\n\t\t</ion-item>\r\n\t\t<ion-item>\r\n\t\t\t<div class=\"text_box\">\r\n\t\t\t\t<h2>{{\"email_us\" | translate}}</h2>\r\n\t\t\t\t<p>repairhub@help.com</p>\r\n\t\t\t</div>\r\n\t\t\t<ion-icon class=\"zmdi zmdi-email ion-text-center\" slot=\"end\"></ion-icon>\r\n\t\t</ion-item>\r\n\t</ion-list>\r\n\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/contact-us/contact-us-routing.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/contact-us/contact-us-routing.module.ts ***!
    \*********************************************************/

  /*! exports provided: ContactUsPageRoutingModule */

  /***/
  function srcAppContactUsContactUsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactUsPageRoutingModule", function () {
      return ContactUsPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _contact_us_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./contact-us.page */
    "./src/app/contact-us/contact-us.page.ts");

    const routes = [{
      path: '',
      component: _contact_us_page__WEBPACK_IMPORTED_MODULE_3__["ContactUsPage"]
    }];
    let ContactUsPageRoutingModule = class ContactUsPageRoutingModule {};
    ContactUsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ContactUsPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/contact-us/contact-us.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/contact-us/contact-us.module.ts ***!
    \*************************************************/

  /*! exports provided: ContactUsPageModule */

  /***/
  function srcAppContactUsContactUsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactUsPageModule", function () {
      return ContactUsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _contact_us_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./contact-us-routing.module */
    "./src/app/contact-us/contact-us-routing.module.ts");
    /* harmony import */


    var _contact_us_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./contact-us.page */
    "./src/app/contact-us/contact-us.page.ts");

    let ContactUsPageModule = class ContactUsPageModule {};
    ContactUsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"], _contact_us_routing_module__WEBPACK_IMPORTED_MODULE_6__["ContactUsPageRoutingModule"]],
      declarations: [_contact_us_page__WEBPACK_IMPORTED_MODULE_7__["ContactUsPage"]]
    })], ContactUsPageModule);
    /***/
  },

  /***/
  "./src/app/contact-us/contact-us.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/contact-us/contact-us.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppContactUsContactUsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-header ion-toolbar {\n  position: absolute;\n  left: 0;\n  width: 100%;\n}\n\n.map {\n  height: 380px;\n  overflow: hidden;\n  position: relative;\n}\n\n.map ion-icon {\n  position: absolute;\n  z-index: 99;\n  color: var(--primary);\n  font-size: 2.2rem;\n  overflow: hidden;\n}\n\n.map::after {\n  content: \"\";\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  width: 100%;\n  height: 50%;\n  background: -webkit-gradient(linear, left top, left bottom, from(rgba(255, 255, 255, 0)), to(white));\n  background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 0%, white 100%);\n}\n\nion-list {\n  background: var(--transparent) !important;\n  padding: 0;\n  width: calc(100% - 40px);\n  margin: 0 auto;\n}\n\nion-list ion-item {\n  padding: 0px 0px;\n  border-radius: 4px;\n  background: var(--white);\n  --inner-padding-end: 0px;\n  --inner-min-height: unset !important;\n  --padding-start: 0;\n  --highligh-color-focused: var(--transparent) !important;\n  --background: var(--transparent);\n  --min-height: unset;\n  margin-bottom: 35px;\n}\n\nion-list ion-item h2 {\n  font-size: 1.4rem;\n  padding-bottom: 8px;\n  color: var(--text-dark);\n  margin: 0;\n  font-weight: 600;\n}\n\nion-list ion-item p {\n  color: var(--text-light);\n  font-size: 1.12rem !important;\n  font-weight: 400 !important;\n  margin: 0;\n}\n\nion-list ion-item ion-icon {\n  background: var(--primary);\n  color: var(--white);\n  margin: 0;\n  border-radius: 50%;\n  min-width: 55px;\n  height: 55px;\n  line-height: 55px;\n  font-size: 1.6rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29udGFjdC11cy9FOlxcRVxcc2VydmljZVxcZmlyZWJhc2UgMDRcXHByb3ZpZGVyLWFwcC1idWRkaGluaS9zcmNcXGFwcFxcY29udGFjdC11c1xcY29udGFjdC11cy5wYWdlLnNjc3MiLCJzcmMvYXBwL2NvbnRhY3QtdXMvY29udGFjdC11cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxrQkFBQTtFQUNBLE9BQUE7RUFDQSxXQUFBO0FDQ0Q7O0FERUE7RUFDQyxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ0NEOztBRENDO0VBQ0Msa0JBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FDQ0Y7O0FERUM7RUFDQyxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0Esb0dBQUE7RUFBQSw2RUFBQTtBQ0FGOztBRE1BO0VBQ0MseUNBQUE7RUFDQSxVQUFBO0VBQ0Esd0JBQUE7RUFDQSxjQUFBO0FDSEQ7O0FES0M7RUFDQyxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esd0JBQUE7RUFDQSx3QkFBQTtFQUNBLG9DQUFBO0VBQ0Esa0JBQUE7RUFDQSx1REFBQTtFQUNBLGdDQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0hGOztBREtFO0VBQ0MsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0FDSEg7O0FETUU7RUFDQyx3QkFBQTtFQUNBLDZCQUFBO0VBQ0EsMkJBQUE7RUFDQSxTQUFBO0FDSkg7O0FET0U7RUFDQywwQkFBQTtFQUNBLG1CQUFBO0VBQ0EsU0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FDTEgiLCJmaWxlIjoic3JjL2FwcC9jb250YWN0LXVzL2NvbnRhY3QtdXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlciBpb24tdG9vbGJhciB7XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdGxlZnQ6IDA7XHJcblx0d2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5tYXAge1xyXG5cdGhlaWdodDogMzgwcHg7XHJcblx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG5cdGlvbi1pY29uIHtcclxuXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdHotaW5kZXg6IDk5O1xyXG5cdFx0Y29sb3I6IHZhcigtLXByaW1hcnkpO1xyXG5cdFx0Zm9udC1zaXplOiAyLjJyZW07IFxyXG5cdFx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHR9XHJcblxyXG5cdCY6OmFmdGVyIHtcclxuXHRcdGNvbnRlbnQ6ICcnO1xyXG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0Ym90dG9tOiAwO1xyXG5cdFx0bGVmdDogMDtcclxuXHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0aGVpZ2h0OiA1MCU7XHJcblx0XHRiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gYm90dG9tLCByZ2JhKDI1NSwyNTUsMjU1LDApIDAlLHJnYmEoMjU1LDI1NSwyNTUsMSkgMTAwJSk7XHJcblxyXG5cdH1cclxufVxyXG5cclxuXHJcbmlvbi1saXN0IHtcclxuXHRiYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcclxuXHRwYWRkaW5nOiAwO1xyXG5cdHdpZHRoOiBjYWxjKDEwMCUgLSA0MHB4KTtcclxuXHRtYXJnaW46IDAgYXV0bztcclxuXHJcblx0aW9uLWl0ZW0ge1xyXG5cdFx0cGFkZGluZzogMHB4IDBweDtcclxuXHRcdGJvcmRlci1yYWRpdXM6IDRweDtcclxuXHRcdGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcclxuXHRcdC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcclxuXHRcdC0taW5uZXItbWluLWhlaWdodDogdW5zZXQgIWltcG9ydGFudDtcclxuXHRcdC0tcGFkZGluZy1zdGFydDogMDtcclxuXHRcdC0taGlnaGxpZ2gtY29sb3ItZm9jdXNlZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XHJcblx0XHQtLWJhY2tncm91bmQ6IHZhcigtLXRyYW5zcGFyZW50KTtcclxuXHRcdC0tbWluLWhlaWdodDogdW5zZXQ7XHJcblx0XHRtYXJnaW4tYm90dG9tOiAzNXB4O1xyXG5cclxuXHRcdGgyIHtcclxuXHRcdFx0Zm9udC1zaXplOiAxLjRyZW07XHJcblx0XHRcdHBhZGRpbmctYm90dG9tOiA4cHg7XHJcblx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xyXG5cdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdGZvbnQtd2VpZ2h0OiA2MDA7XHJcblx0XHR9XHJcblxyXG5cdFx0cCB7XHJcblx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcclxuXHRcdFx0Zm9udC1zaXplOiAxLjEycmVtICFpbXBvcnRhbnQ7XHJcblx0XHRcdGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcclxuXHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlvbi1pY29uIHtcclxuXHRcdFx0YmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRcdGNvbG9yOiB2YXIoLS13aGl0ZSk7XHJcblx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0Ym9yZGVyLXJhZGl1czogNTAlO1xyXG5cdFx0XHRtaW4td2lkdGg6IDU1cHg7XHJcblx0XHRcdGhlaWdodDogNTVweDtcclxuXHRcdFx0bGluZS1oZWlnaHQ6IDU1cHg7XHJcblx0XHRcdGZvbnQtc2l6ZTogMS42cmVtO1xyXG5cclxuXHRcdH1cclxuXHR9XHJcbn0iLCJpb24taGVhZGVyIGlvbi10b29sYmFyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLm1hcCB7XG4gIGhlaWdodDogMzgwcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYXAgaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDk5O1xuICBjb2xvcjogdmFyKC0tcHJpbWFyeSk7XG4gIGZvbnQtc2l6ZTogMi4ycmVtO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLm1hcDo6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogNTAlO1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gYm90dG9tLCByZ2JhKDI1NSwgMjU1LCAyNTUsIDApIDAlLCB3aGl0ZSAxMDAlKTtcbn1cblxuaW9uLWxpc3Qge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcbiAgcGFkZGluZzogMDtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDQwcHgpO1xuICBtYXJnaW46IDAgYXV0bztcbn1cbmlvbi1saXN0IGlvbi1pdGVtIHtcbiAgcGFkZGluZzogMHB4IDBweDtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcbiAgLS1pbm5lci1taW4taGVpZ2h0OiB1bnNldCAhaW1wb3J0YW50O1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIC0taGlnaGxpZ2gtY29sb3ItZm9jdXNlZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tdHJhbnNwYXJlbnQpO1xuICAtLW1pbi1oZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW4tYm90dG9tOiAzNXB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0gaDIge1xuICBmb250LXNpemU6IDEuNHJlbTtcbiAgcGFkZGluZy1ib3R0b206IDhweDtcbiAgY29sb3I6IHZhcigtLXRleHQtZGFyayk7XG4gIG1hcmdpbjogMDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIHAge1xuICBjb2xvcjogdmFyKC0tdGV4dC1saWdodCk7XG4gIGZvbnQtc2l6ZTogMS4xMnJlbSAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogNDAwICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIGlvbi1pY29uIHtcbiAgYmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XG4gIGNvbG9yOiB2YXIoLS13aGl0ZSk7XG4gIG1hcmdpbjogMDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBtaW4td2lkdGg6IDU1cHg7XG4gIGhlaWdodDogNTVweDtcbiAgbGluZS1oZWlnaHQ6IDU1cHg7XG4gIGZvbnQtc2l6ZTogMS42cmVtO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/contact-us/contact-us.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/contact-us/contact-us.page.ts ***!
    \***********************************************/

  /*! exports provided: ContactUsPage */

  /***/
  function srcAppContactUsContactUsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactUsPage", function () {
      return ContactUsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    let ContactUsPage = class ContactUsPage {
      constructor() {}

      ngOnInit() {}

    };
    ContactUsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-contact-us',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./contact-us.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./contact-us.page.scss */
      "./src/app/contact-us/contact-us.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], ContactUsPage);
    /***/
  }
}]);
//# sourceMappingURL=contact-us-contact-us-module-es5.js.map